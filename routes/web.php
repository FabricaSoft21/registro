<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Example Route

use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Route;
use Symfony\Component\Routing\Route as SymfonyRoute;

Route::group(['middleware' => ['auth', 'role:administrador|tecnica|poblaciones|instructor']], function () {

    // User needs to be authenticated to enter here.
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/', 'HomeController@index');
    Route::match(['get', 'post'], '/dashboard', function(){

        return view('dashboard');
    });

    Route::get('/dashboard', 'HomeController@index');

    Route::view('/examples/plugin', 'examples.plugin');
    Route::view('/examples/blank', 'examples.blank');
    Route::view('/aprendiz/edit','aprendices.edit');

    // Route users

    Route::get('/user/create','UserController@create');
    Route::post('/user/create','UserController@store');
    Route::get('/user','UserController@index');
    Route::get('/user/edit/{id}','UserController@edit');
    Route::post('/user/edit/{id}','UserController@update')->name('editUser');
    Route::get('/user/delete/{id}','UserController@destroy');
    Route::get('/user/restore/{id}','UserController@restore');
    // Route::post('')

    // Routes aprendices

    Route::get('/aprendiz/create','AprendizController@create');
    Route::post('/aprendiz/create','AprendizController@store');
    Route::get('/aprendiz','AprendizController@index');
    Route::get('/aprendiz/edit/{id}','AprendizController@edit');
    Route::post('/aprendiz/edit/{id}','AprendizController@update');
    Route::get('/aprendiz/show/{id}','AprendizController@show');

    // Importar excel aprendiz
    Route::post('/aprendiz','AprendizController@import')->name('import');
  
    //Routes discapacidad
    Route::post('/discapacidad/create','TipoDiscapacidadController@save')->name('saveDis');

    //Routes familia
    Route::post('/familia/create','FamiliaController@save')->name('saveFami');
    // Routes rector

    Route::get('/rector/create','RectorController@create');
    Route::post('/rector/create','RectorController@store');
    Route::get('/rector','RectorController@index');
    Route::get('/rector/edit/{id}','RectorController@edit');
    Route::post('/rector/edit/{id}','RectorController@update');
    Route::get('/rector/delete/{id}','RectorController@destroy');
    Route::get('/rector/restore/{id}','RectorController@restore');

    //Routes rector Ajax
    Route::post('/create/rector','RectorController@save')->name('saveR');
    Route::get('/rector/show/{id}','RectorController@show');

    //Importar excel rector
    Route::post('/rector','RectorController@import');

    //Routes coordinador
    Route::get('/coordinador/create','CoordinacionController@create');
    Route::post('/coordinador/create','CoordinacionController@store');
    Route::get('/coordinador','CoordinacionController@index');
    Route::get('/coordinador/edit/{id}','CoordinacionController@edit');
    Route::post('/coordinador/edit/{id}','CoordinacionController@update');
    Route::get('/coordinador/delete/{id}','CoordinacionController@destroy');
    Route::get('/coordinador/restore/{id}','CoordinacionController@restore');

    //Routes coordinador Ajax
    Route::post('/create/coordinador','CoordinacionController@save')->name('saveC');
    Route::get('/coordinador/show/{id}','CoordinacionController@show');

    //Routes profesión ajax
    Route::post('/create/profesion','ProfesionController@save')->name('savePro');

    // Importar excel coordinador
    Route::post('/coordinador','CoordinacionController@import');

    //Routes programación
    Route::post('/create/programacion','ProgramacionController@save')->name('saveP');
    Route::get('/utils/programa','ProgramacionController@create');
    Route::post('/utils/programa','ProgramacionController@store')->name('programa.save');

    //Routes programación Ajax
    Route::get('/utils/programa/{type}','ProgramacionController@getLugar');
    Route::get('/utils/event/{id}','ProgramacionController@getDias')->name('getDay');
    Route::get('/eventsDay/{id}','ProgramacionController@getName');


    //Routes Colegio

    Route::get('/instiEdu/create','ColegioController@create');
    Route::post('/instiEdu/create','ColegioController@store');
    Route::get('/instiEdu','ColegioController@index');
    Route::get('/instiEdu/edit/{id}','ColegioController@edit');
    Route::post('/instiEdu/edit/{id}','ColegioController@update');
    Route::get('/instiEdu/delete/{id}','ColegioController@destroy');
    Route::get('/instiEdu/restore/{id}','ColegioController@restore');


    // Importar Excel colegio
    Route::post('/instiEdu','ColegioController@import')->name('importColegio');

    //Routes CheckList

    Route::post('/checklist/save','CheckListController@store')->name('saveCE');
    Route::post('/checklist/edit','CheckListController@update')->name('updateCE');


    //Routes Instructores
    Route::get('/instructor/create','InstructorController@create');
    Route::post('/instructor/create','InstructorController@store');
    Route::get('/instructor','InstructorController@index');
    Route::get('/instructor/edit/{id}','InstructorController@edit');
    Route::post('/instructor/edit/{id}','InstructorController@update');
    Route::get('/instructor/delete/{id}','InstructorController@destroy');
    Route::get('/instructor/restore/{id}','InstructorController@restore');

    // Importar excel instructor
    Route::post('/instructor','InstructorController@import')->name('importI');

    //Routes docentes
    Route::get('/docente/create','DocenteController@create');
    Route::post('/docente/create','DocenteController@store');
    Route::get('/docente','DocenteController@index');
    Route::get('/docente/edit/{id}','DocenteController@edit');
    Route::post('/docente/edit/{id}','DocenteController@update');
    Route::get('/docente/delete/{id}','DocenteController@destroy');
    Route::get('/docente/restore/{id}','DocenteController@restore');

    //Importar excel docentes
    Route::post('/docente','DocenteController@import');

    //Routes formación
    Route::get('/formacion/create','FormacionController@create');
    Route::post('/formacion/create','FormacionController@store');
    Route::get('/formacion','FormacionController@index');
    Route::get('/formacion/edit/{id}','FormacionController@edit');
    Route::post('/formacion/edit/{id}','FormacionController@update');

    //Routes formación Ajax
    Route::get('/formacion/show/{id}','FormacionController@getStudents');
    Route::get('/formacion/showI/{id}','FormacionController@getInstructores');
    Route::get('/formacion/showD/{id}','FormacionController@getDocente');
    Route::get('/formacion/showE/{id}','FormacionController@getEmpresa');

    //Routes programa formación
    Route::get('/programa/create','ProgramaFormacionController@create');
    Route::post('/programa/create','ProgramaFormacionController@store');
    Route::get('/programa','ProgramaFormacionController@index');
    Route::get('/programa/edit/{id}','ProgramaFormacionController@edit');
    Route::post('/programa/edit/{id}','ProgramaFormacionController@update');

    // Importar excel programas
    Route::post('/programa','ProgramaFormacionController@import')->name('importP');

    //Routes aprendiz_has_programa
    Route::get('/aprendiz_has_programa/create','AprendizHasProgramasController@create');
    Route::post('/aprendiz_has_programa/create','AprendizHasProgramasController@store')->name('detailA');
    Route::get('/aprendiz_has_programa','AprendizHasProgramasController@index');
    Route::get('/aprendiz_has_programa/edit/{id}','AprendizHasProgramasController@edit');
    Route::post('/aprendiz_has_programa/edit/{id}','AprendizHasProgramasController@destroy')->name('removeA');

    //Routes instructor_has_formacion

    Route::get('/instructor_has_formacion/create','InstructorHasformacionController@create');
    Route::post('/instructor_has_formacion/create','InstructorHasformacionController@store')->name('detailI');
    Route::get('/instructor_has_formacion','InstructorHasformacionController@index');
    Route::get('/instructor_has_formacion/edit/{id}','InstructorHasformacionController@edit');
    Route::post('/instructor_has_formacion/edit/{id}','InstructorHasformacionController@destroy')->name('removeI');

    //Routes programa-has_competencias

    Route::get('/programa_has_competencia/create','ProgramaHasCompetenciasController@create');
    Route::post('/programa_has_competencia/create','ProgramaHasCompetenciasController@store')->name('detailP');
    Route::get('/programa_has_competencia','ProgramaHasCompetenciasController@index');
    Route::get('/programa_has_competencia/edit/{id}','ProgramaHasCompetenciasController@edit');
    Route::post('/programa_has_competencia/edit/{id}','ProgramaHasCompetenciasController@destroy')->name('removeC');

    //Routes instructor_has_competencias

    Route::get('/instructor_has_competencias/create','InstructorHasCompetenciasController@create');
    Route::post('/instructor_has_competencias/create','InstructorHasCompetenciasController@store')->name('addCompe');
    Route::get('/instructor_has_competencias','InstructorHasCompetenciasController@index');
    Route::get('/instructor_has_competencias/edit/{id}','InstructorHasCompetenciasController@edit');
    Route::post('/instructor_has_competencias/edit/{id}','InstructorHasCompetenciasController@destroy')->name('removeCi');

    //Routes empresa_has_aprendices

    Route::get('/empresa_has_aprendices/create','EmpresaHasAprendicesController@create');
    Route::post('/empresa_has_aprendices/create','EmpresaHasAprendicesController@store')->name('detailEmp');
    Route::get('/empresa_has_aprendices','EmpresaHasAprendicesController@index');
    Route::get('/empresa_has_aprendices/edit/{id}','EmpresaHasAprendicesController@edit');
    Route::post('/empresa_has_aprendices/edit/{id}','EmpresaHasAprendicesController@destroy')->name('removeE');
    

    //Routes fichas
    Route::get('/ficha/create','FichaController@create');
    Route::post('/ficha/create','FichaController@store');
    Route::get('/ficha','FichaController@index');
    Route::get('/ficha/edit/{id}','FichaController@edit');
    Route::post('/ficha/edit/{id}','FichaController@update');

    //Routes municipio

    Route::get('/municipio/{id}/departamento','MunicipioController@byDepartment');

    //Routes calendario
    Route::get('/programacion', 'FormacionController@show');
    Route::get('/events/{id}', 'FormacionController@getFormacion')->name('DataFormacion');
    Route::get('/eventsI/{id}', 'ProgramacionController@getEvents');
    Route::get('/events/edit/{id}', 'ProgramacionController@edit');
    Route::post('/events/edit/{id}', 'ProgramacionController@update');
    Route::get('/events', 'ProgramacionController@show');
    Route::view('/landing', 'landing');

    // Route selecciona opción
    Route::get('/seleccionar/categoria/{id}','HomeController@optionSelected');

    //Routes empresa
    Route::get('/empresa/create','EmpresaController@create');
    Route::post('/empresa/create','EmpresaController@store');
    Route::get('/empresa','EmpresaController@index');
    Route::get('/empresa/edit/{id}','EmpresaController@edit');
    Route::post('/empresa/edit/{id}','EmpresaController@update');
    Route::get('/empresa/delete/{id}','EmpresaController@destroy');
    Route::get('/empresa/restore/{id}','EmpresaController@restore');

    //Importar excel empresa
    Route::post('/empresa','EmpresaController@import');

    //Routes PoblacionVulnerable
    Route::get('/vulnera/create','PoblacionVulneController@create');
    Route::post('/vulnera/create','PoblacionVulneController@store')->name('vulnerable');
    Route::get('/vulnera/edit/{id}','PoblacionVulneController@edit');
    Route::get('/vulnera/show','PoblacionVulneController@show')->name('showPv');
    Route::post('/vulnera/edit/{id}','PoblacionVulneController@update');
    Route::delete('/vulnera/delete/{id}','PoblacionVulneController@destroy')->name('deleteP');


    //Routes competencia
    Route::get('/compentencia/create','CompetenciaController@create');
    Route::post('/compentencia/create','CompetenciaController@store')->name('saveCo');
    Route::get('/compentencia','CompetenciaController@index');
    Route::get('/compentencia/edit/{id}','CompetenciaController@edit');
    Route::post('/compentencia/edit/{id}','CompetenciaController@update');

    Route::post('/lista/edit/{id}','ColegioController@list')->name('list');
    Route::get('/lista/show/{id}','ColegioController@getList')->name('getList');

    //Routes competencias Ajax

    Route::get('/getCompetencias','CompetenciaController@getList');

    //Routes aprendiz_has_docentes

    Route::get('/aprendiz_has_docentes/create','AprendizHasDocentesController@create');
    Route::post('/aprendiz_has_docentes/create','AprendizHasDocentesController@store')->name('ahdc');
    Route::get('/aprendiz_has_docentes','AprendizHasDocentesController@index');
    Route::get('/aprendiz_has_docentes/edit/{id}','AprendizHasDocentesController@edit');
    Route::post('/aprendiz_has_docentes/edit/{id}','AprendizHasDocentesController@destroy')->name('removeD');

    //Routes Ajax competencia
    Route::get('/competencia/show/{id}','CompetenciaController@show');
    Route::get('/competencia/showI/{id}','CompetenciaController@showI');

    Route::get('/reporte/aprendiz','AprendizController@report')->name('reportA');
    Route::get('/reporte/instructor','InstructorController@report')->name('reportI');
    Route::get('/reporte/programacion','ProgramacionController@report')->name('reportP');



});


Auth::routes(["register" => false]); 
