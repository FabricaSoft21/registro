<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programacion', function (Blueprint $table) {
            $table->BigIncrements('id');
            $table->unsignedBigInteger('idinstructor');
            $table->foreign('idinstructor')->references('id')->on('instructor');
            $table->unsignedBigInteger('idFormacion');
            $table->foreign('idFormacion')->references('id')->on('programa_formacion');
            $table->date('fechaInicio');
            $table->date('fechaFinal');
            $table->time('horaInicio');
            $table->time('horaFinal');
            $table->string('lugar');
            $table->unsignedBigInteger('idColegio')->nullable();
            $table->foreign('idColegio')->references('id')->on('colegio');
            $table->unsignedBigInteger('idEmpresa')->nullable();
            $table->foreign('idEmpresa')->references('id')->on('empresa');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programacion');
    }
}
