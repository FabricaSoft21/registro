<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAprendizHasDiscapacidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aprendiz_has_discapacidades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('idDiscapacidad');
            $table->foreign('idDiscapacidad')->references('id')->on('tipo_discapacidad');
            $table->unsignedBigInteger('idAprendiz');
            $table->foreign('idAprendiz')->references('id')->on('aprendiz');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aprendiz_has_discapacidades');
    }
}
