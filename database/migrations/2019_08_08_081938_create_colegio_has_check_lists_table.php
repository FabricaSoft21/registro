<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColegioHasCheckListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colegio_has_check_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('idColegio');
            $table->foreign('idColegio')->references('id')->on('colegio');
            $table->unsignedBigInteger('idCheckList');
            $table->foreign('idCheckList')->references('id')->on('check_list');
            $table->boolean('state')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colegio_has_check_lists');
    }
}
