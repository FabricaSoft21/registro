<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('docente', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',50);
            $table->string('apellido',50);
            $table->string('correo',45)->unique();
            $table->unsignedBigInteger('idcolegio');
            $table->foreign('idcolegio')->references('id')->on('colegio')->nullable();
            $table->unsignedBigInteger('idprograma');
            $table->foreign('idprograma')->references('id')->on('programa_formacion');
            $table->string('telefono',50);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('docente');
    }
}
