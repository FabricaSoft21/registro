<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoblacionVulnesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poblacion_vulnes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('metaAprendiz');
            $table->integer('ejecAprendiz')->nullable();
            $table->integer('metaCupos');
            $table->integer('ejecCupos')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('poblacion_vulnes');
    }
}
