<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAprendizsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aprendiz', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',50);
            $table->string('apellido',50);
            $table->date('fechaNacimiento');
            $table->string('numDoc',15)->unique();
            $table->string('correo',50)->unique();
            $table->string('telefono',15);
            $table->string('direccion',50);
            $table->string('nombreAcudiente',50)->nullable();
            $table->string('telAcudiente',50)->nullable();
            $table->unsignedBigInteger('tipoAprendiz');
            $table->unsignedBigInteger('idColegio')->nullable();
            $table->foreign('idColegio')->references('id')->on('colegio');
            $table->unsignedBigInteger('idGrado')->nullable();
            $table->foreign('idGrado')->references('id')->on('grado');
            $table->unsignedBigInteger('idestado')->default(1);
            $table->foreign('idestado')->references('id')->on('estado_aprendiz');
            $table->unsignedBigInteger('idTipoDoc');
            $table->foreign('idTipoDoc')->references('id')->on('tipo_doc');
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aprendizs');
    }
}
