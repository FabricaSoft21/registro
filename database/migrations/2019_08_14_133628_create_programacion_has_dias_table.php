<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramacionHasDiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programacion_has_dias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('idProgramacion');
            $table->foreign('idProgramacion')->references('id')->on('programacion');
            $table->unsignedBigInteger('idDia');
            $table->foreign('idDia')->references('id')->on('dia');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programacion_has_dias');
    }
}
