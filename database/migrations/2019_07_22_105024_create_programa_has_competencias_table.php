<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramaHasCompetenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programa_has_competencias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('idPrograma');
            $table->foreign("idPrograma")-> references('id')->on('programa_formacion');
            $table->unsignedBigInteger('idCompetencia');
            $table->foreign("idCompetencia")-> references('id')->on('competencia');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programa_has_competencias');
    }
}
