<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFichasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ficha', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombreFicha',50);
            $table->string('codigo',50);
            $table->unsignedBigInteger('idProgramaFormacion');
            $table->foreign('idProgramaFormacion')->references('id')->on('programa_formacion');
            $table->unsignedBigInteger('idTipoFicha')->default(1);
            $table->foreign('idTipoFicha')->references('id')->on('tipo_ficha');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ficha');
    }
}
