<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaHasAprendicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_has_aprendices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('idEmpresa');
            $table->foreign('idEmpresa')->references('id')->on('empresa');
            $table->unsignedBigInteger('idAprendizHasPrograma');
            $table->foreign('idAprendizHasPrograma')->references('id')->on('aprendiz_has_programas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa_has_aprendices');
    }
}
