<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAprendizHasProgramasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aprendiz_has_programas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('idAprendiz');
            $table->foreign('idAprendiz')->references('id')->on('aprendiz');
            $table->unsignedBigInteger('idFormacion');
            $table->foreign('idFormacion')->references('id')->on('formacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aprendiz_has_programas');
    }
}
