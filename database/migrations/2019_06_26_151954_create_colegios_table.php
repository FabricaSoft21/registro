<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColegiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colegio', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('nit',50)->unique();
            $table->string('codigoDane',50)->unique();
            $table->string('nombreColegio',50);
            $table->string('direccion',50);
            $table->string('correo',50);
            $table->string('numResolucion',50);
            $table->string('telefono',15);
            $table->unsignedBigInteger('idDepartamento');
            $table->foreign('idDepartamento')->references('id')->on('departamento');
            $table->unsignedBigInteger('idMunicipio');
            $table->foreign('idMunicipio')->references('id')->on('municipios');
            $table->unsignedBigInteger('idCategoria');
            $table->foreign('idCategoria')->references('id')->on('categoria_colegio');
            $table->unsignedBigInteger('idcoordinacion');
            $table->foreign('idcoordinacion')->references('id')->on('coordinacion');
            $table->unsignedBigInteger('idRector');
            $table->foreign('idRector')->references('id')->on('rector');
            $table->unsignedBigInteger('idTipo');
            $table->foreign('idTipo')->references('id')->on('tipo_colegios');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colegio');
    }
}
