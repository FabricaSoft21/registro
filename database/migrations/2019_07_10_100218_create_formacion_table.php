<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formacion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('idFicha');
            $table->foreign('idFicha')->references('id')->on('ficha');
            $table->date('fechaInicio');
            $table->date('fechaFinal');
            $table->string('jornada');
            $table->time('horaInicio');
            $table->time('horaFinal');
            $table->date('fechaInicioProd')->nullable();
            $table->unsignedBigInteger('idEstado')->default(1);
            $table->foreign('idEstado')->references('id')->on('estado_formacion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formacion');
    }
}
