<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstructorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instructor', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('idTipoDoc');
            $table->foreign('idTipoDoc')->references('id')->on('tipo_doc');
            $table->integer('numDoc')->unique();
            $table->string('nombre',50);
            $table->string('apellido',60);
            $table->string('correoSena',45)->unique();
            $table->string('correoAlterno',50)->unique();
            $table->string('telefono',10);
            $table->string('celular',15)->nullable();
            $table->unsignedBigInteger('idArea');
            $table->foreign('idArea')->references('id')->on('area');
            $table->unsignedBigInteger('idContrato');
            $table->foreign('idContrato')->references('id')->on('contrato');
            $table->date('fechaInicioCo');
            $table->date('fechaFinalCo');
            $table->unsignedBigInteger('idProfesion');
            $table->foreign('idProfesion')->references('id')->on('profesion');
            $table->unsignedBigInteger('idDepartamento');
            $table->foreign('idDepartamento')->references('id')->on('departamento');
            $table->unsignedBigInteger('idMunicipio');
            $table->foreign('idMunicipio')->references('id')->on('municipios');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instructor');
    }
}
