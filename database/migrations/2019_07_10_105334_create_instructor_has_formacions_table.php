<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstructorHasformacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instructor_hasformacions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('idInstructor');
            $table->foreign('idInstructor')->references('id')->on('instructor');
            $table->unsignedBigInteger('idAprendizHasPrograma');
            $table->foreign('idAprendizHasPrograma')->references('id')->on('aprendiz_has_programas');
            $table->unsignedBigInteger('idTipoInstructor')->nullable();
            $table->foreign('idTipoInstructor')->references('id')->on('tipo_instructor');
            $table->timestamps();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instructor_hasformacions');
    }
}
