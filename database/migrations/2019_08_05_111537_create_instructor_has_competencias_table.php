<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstructorHasCompetenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instructor_has_competencias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('idCompetencia');
            $table->unsignedBigInteger('idInstructor');
            $table->foreign('idCompetencia')->references('id')->on('competencia');
            $table->foreign('idInstructor')->references('id')->on('instructor');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instructor_has_competencias');
    }
}
