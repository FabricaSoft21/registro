<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramaFormacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programa_formacion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',50);
            //si es un curso se le pide la familia:bilinguismo etc
            $table->unsignedBigInteger('idFamilia')->nullable();
            $table->foreign('idFamilia')->references('id')->on('familia');
            $table->string('codigo',20);
            $table->string('version',30);
            $table->unsignedBigInteger('idNivel');
            $table->foreign('idNivel')->references('id')->on('nivel');
            $table->unsignedBigInteger('idTipo_formacion')->nullable();
            $table->foreign('idTipo_formacion')->references('id')->on('tipo_formacion');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programa_formacion');
    }
}
