<?php

use Illuminate\Database\Seeder;
use App\models\Clasificacion;

class ClasificacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Clasificacion::create([

            'nombre' => 'Titulada'

        ]);

        Clasificacion::create([

            'nombre' => 'Complementaria'

        ]);

        Clasificacion::create([

            'nombre' => 'Poblaciones vulnerables'

        ]);

        Clasificacion::create([

            'nombre' => 'Competencias básicas'

        ]);

        Clasificacion::create([

            'nombre' => 'Media técnica'

        ]);

    }
}
