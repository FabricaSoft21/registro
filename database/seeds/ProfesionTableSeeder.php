<?php

use Illuminate\Database\Seeder;
use App\models\profesion;
class ProfesionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        profesion::create([

            'nombre'=>' Ingeniero de sistemas',
            'descripcion' => 'apto para dar intrucciones sobre lenguajes d eprogramación y solución de problemas informáticos'

        ]);
        profesion:: create([
            'nombre'=> 'Ánalista de problemas',
            'descripcion'=> 'apto para dar instrucción sobre modelos y mapas de problemas análiticos'
        ]);
    }
}
