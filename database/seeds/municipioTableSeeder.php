<?php

use Illuminate\Database\Seeder;
use App\models\municipio;

class municipioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        municipio::create( [
            'id'=>1,
            'idDepartamento'=>1,
            'nombre'=>'MEDELLIN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>2,
            'idDepartamento'=>1,
            'nombre'=>'ABEJORRAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>3,
         'idDepartamento'=>1,
            'nombre'=>'ABRIAQUI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>4,
            'idDepartamento'=>1,
            'nombre'=>'ALEJANDRIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>5,
            'idDepartamento'=>1,
            'nombre'=>'AMAGA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>6,
            'idDepartamento'=>1,
            'nombre'=>'AMALFI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>7,
            'idDepartamento'=>1,
            'nombre'=>'ANDES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>8,
            'idDepartamento'=>1,
            'nombre'=>'ANGELOPOLIS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>9,
            'idDepartamento'=>1,
            'nombre'=>'ANGOSTURA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>10,
            'idDepartamento'=>1,
            'nombre'=>'ANORI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>11,
            'idDepartamento'=>1,
            'nombre'=>'ANTIOQUIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>12,
            'idDepartamento'=>1,
            'nombre'=>'ANZA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>13,
            'idDepartamento'=>1,
            'nombre'=>'APARTADO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>14,
            'idDepartamento'=>1,
            'nombre'=>'ARBOLETES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>15,
            'idDepartamento'=>1,
            'nombre'=>'ARGELIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>16,
            'idDepartamento'=>1,
            'nombre'=>'ARMENIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>17,
            'idDepartamento'=>1,
            'nombre'=>'BARBOSA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>18,
            'idDepartamento'=>1,
            'nombre'=>'BELMIRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>19,
            'idDepartamento'=>1,
            'nombre'=>'BELLO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>20,
            'idDepartamento'=>1,
            'nombre'=>'BETANIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>21,
            'idDepartamento'=>1,
            'nombre'=>'BETULIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>22,
            'idDepartamento'=>1,

            'nombre'=>'BOLIVAR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>23,
            'idDepartamento'=>1,

            'nombre'=>'BRICEÑO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>24,
            'idDepartamento'=>1,

            'nombre'=>'BURITICA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>25,
            'idDepartamento'=>1,

            'nombre'=>'CACERES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>26,
            'idDepartamento'=>1,

            'nombre'=>'CAICEDO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>27,
            'idDepartamento'=>1,

            'nombre'=>'CALDAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>28,
            'idDepartamento'=>1,

            'nombre'=>'CAMPAMENTO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>29,
            'idDepartamento'=>1,

            'nombre'=>'CAÑASGORDAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>30,
            'idDepartamento'=>1,

            'nombre'=>'CARACOLI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>31,
            'idDepartamento'=>1,

            'nombre'=>'CARAMANTA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>32,
            'idDepartamento'=>1,

            'nombre'=>'CAREPA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>33,
            'idDepartamento'=>1,

            'nombre'=>'CARMEN DE VIBORAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>34,
            'idDepartamento'=>1,

            'nombre'=>'CAROLINA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>35,
            'idDepartamento'=>1,

            'nombre'=>'CAUCASIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>36,
            'idDepartamento'=>1,

            'nombre'=>'CHIGORODO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>37,
            'idDepartamento'=>1,

            'nombre'=>'CISNEROS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>38,
            'idDepartamento'=>1,

            'nombre'=>'COCORNA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>39,
            'idDepartamento'=>1,

            'nombre'=>'CONCEPCION'
            ] );
            
            
                        
            municipio::create( [
            'id'=>40,
            'idDepartamento'=>1,

            'nombre'=>'CONCORDIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>41,
            'idDepartamento'=>1,

            'nombre'=>'COPACABANA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>42,
            'idDepartamento'=>1,

            'nombre'=>'DABEIBA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>43,
            'idDepartamento'=>1,

            'nombre'=>'DON MATIAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>44,
            'idDepartamento'=>1,

            'nombre'=>'EBEJICO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>45,
            'idDepartamento'=>1,

            'nombre'=>'EL BAGRE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>46,
            'idDepartamento'=>1,

            'nombre'=>'ENTRERRIOS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>47,
            'idDepartamento'=>1,

            'nombre'=>'ENVIGADO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>48,
            'idDepartamento'=>1,

            'nombre'=>'FREDONIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>49,
            'idDepartamento'=>1,

            'nombre'=>'FRONTINO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>50,
            'idDepartamento'=>1,

            'nombre'=>'GIRALDO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>51,
            'idDepartamento'=>1,

            'nombre'=>'GIRARDOTA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>52,
            'idDepartamento'=>1,

            'nombre'=>'GOMEZ PLATA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>53,
            'idDepartamento'=>1,

            'nombre'=>'GRANADA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>54,
            'idDepartamento'=>1,

            'nombre'=>'GUADALUPE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>55,
            'idDepartamento'=>1,

            'nombre'=>'GUARNE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>56,
            'idDepartamento'=>1,

            'nombre'=>'GUATAPE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>57,
            'idDepartamento'=>1,

            'nombre'=>'HELICONIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>58,
            'idDepartamento'=>1,

            'nombre'=>'HISPANIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>59,
            'idDepartamento'=>1,

            'nombre'=>'ITAGUI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>60,
            'idDepartamento'=>1,

            'nombre'=>'ITUANGO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>61,
            'idDepartamento'=>1,

            'nombre'=>'JARDIN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>62,
            'idDepartamento'=>1,

            'nombre'=>'JERICO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>63,
            'idDepartamento'=>1,

            'nombre'=>'LA CEJA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>64,
            'idDepartamento'=>1,

            'nombre'=>'LA ESTRELLA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>65,
            'idDepartamento'=>1,

            'nombre'=>'LA PINTADA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>66,
            'idDepartamento'=>1,

            'nombre'=>'LA UNION'
            ] );
            
            
                        
            municipio::create( [
            'id'=>67,
            'idDepartamento'=>1,

            'nombre'=>'LIBORINA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>68,
            'idDepartamento'=>1,

            'nombre'=>'MACEO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>69,
            'idDepartamento'=>1,

            'nombre'=>'MARINILLA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>70,
            'idDepartamento'=>1,

            'nombre'=>'MONTEBELLO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>71,
            'idDepartamento'=>1,

            'nombre'=>'MURINDO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>72,
            'idDepartamento'=>1,

            'nombre'=>'MUTATA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>73,
            'idDepartamento'=>1,

            'nombre'=>'NARIÑO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>74,
            'idDepartamento'=>1,

            'nombre'=>'NECOCLI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>75,
            'idDepartamento'=>1,

            'nombre'=>'NECHI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>76,
            'idDepartamento'=>1,

            'nombre'=>'OLAYA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>77,
            'idDepartamento'=>1,

            'nombre'=>'PEÑOL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>78,
            'idDepartamento'=>1,

            'nombre'=>'PEQUE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>79,
            'idDepartamento'=>1,

            'nombre'=>'PUEBLORRICO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>80,
            'idDepartamento'=>1,

            'nombre'=>'PUERTO BERRIO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>81,
            'idDepartamento'=>1,

            'nombre'=>'PUERTO NARE (LA MAGDALENA)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>82,
            'idDepartamento'=>1,

            'nombre'=>'PUERTO TRIUNFO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>83,
            'idDepartamento'=>1,

            'nombre'=>'REMEDIOS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>84,
            'idDepartamento'=>1,

            'nombre'=>'RETIRO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>85,
            'idDepartamento'=>1,

            'nombre'=>'RIONEGRO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>86,
            'idDepartamento'=>1,

            'nombre'=>'SABANALARGA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>87,
            'idDepartamento'=>1,

            'nombre'=>'SABANETA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>88,
            'idDepartamento'=>1,

            'nombre'=>'SALGAR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>89,
            'idDepartamento'=>1,

            'nombre'=>'SAN ANDRES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>90,
            'idDepartamento'=>1,

            'nombre'=>'SAN CARLOS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>91,
            'idDepartamento'=>1,

            'nombre'=>'SAN FRANCISCO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>92,
            'idDepartamento'=>1,

            'nombre'=>'SAN JERONIMO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>93,
            'idDepartamento'=>1,

            'nombre'=>'SAN JOSE DE LA MONTAÑA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>94,
            'idDepartamento'=>1,

            'nombre'=>'SAN JUAN DE URABA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>95,
            'idDepartamento'=>1,

            'nombre'=>'SAN LUIS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>96,
            'idDepartamento'=>1,

            'nombre'=>'SAN PEDRO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>97,
            'idDepartamento'=>1,

            'nombre'=>'SAN PEDRO DE URABA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>98,
            'idDepartamento'=>1,

            'nombre'=>'SAN RAFAEL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>99,
            'idDepartamento'=>1,

            'nombre'=>'SAN ROQUE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>100,
            'idDepartamento'=>1,

            'nombre'=>'SAN VICENTE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>101,
            'idDepartamento'=>1,

            'nombre'=>'SANTA BARBARA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>102,
            'idDepartamento'=>1,

            'nombre'=>'SANTA ROSA DE OSOS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>103,
            'idDepartamento'=>1,

            'nombre'=>'SANTO DOMINGO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>104,
            'idDepartamento'=>1,

            'nombre'=>'SANTUARIO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>105,
            'idDepartamento'=>1,

            'nombre'=>'SEGOVIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>106,
            'idDepartamento'=>1,

            'nombre'=>'SONSON'
            ] );
            
            
                        
            municipio::create( [
            'id'=>107,
            'idDepartamento'=>1,

            'nombre'=>'SOPETRAN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>108,
            'idDepartamento'=>1,

            'nombre'=>'TAMESIS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>109,
            'idDepartamento'=>1,

            'nombre'=>'TARAZA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>110,
            'idDepartamento'=>1,

            'nombre'=>'TARSO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>111,
            'idDepartamento'=>1,

            'nombre'=>'TITIRIBI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>112,
            'idDepartamento'=>1,

            'nombre'=>'TOLEDO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>113,
            'idDepartamento'=>1,

            'nombre'=>'TURBO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>114,
            'idDepartamento'=>1,

            'nombre'=>'URAMITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>115,
            'idDepartamento'=>1,

            'nombre'=>'URRAO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>116,
            'idDepartamento'=>1,

            'nombre'=>'VALDIVIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>117,
            'idDepartamento'=>1,

            'nombre'=>'VALPARAISO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>118,
            'idDepartamento'=>1,

            'nombre'=>'VEGACHI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>119,
            'idDepartamento'=>1,

            'nombre'=>'VENECIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>120,
            'idDepartamento'=>1,

            'nombre'=>'VIGIA DEL FUERTE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>121,
            'idDepartamento'=>1,

            'nombre'=>'YALI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>122,
            'idDepartamento'=>1,

            'nombre'=>'YARUMAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>123,
            'idDepartamento'=>1,

            'nombre'=>'YOLOMBO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>124,
            'idDepartamento'=>1,

            'nombre'=>'YONDO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>125,
            'idDepartamento'=>1,

            'nombre'=>'ZARAGOZA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>126,
         'idDepartamento'=>2,
            'nombre'=>'BARRANQUILLA (DISTRITO ESPECIAL INDUSTRIAL Y PORTUARIO DE BARRANQUILLA)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>127,
            'idDepartamento'=>2,
            'nombre'=>'BARANOA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>128,
            'idDepartamento'=>2,

            'nombre'=>'CAMPO DE LA CRUZ'
            ] );
            
            
                        
            municipio::create( [
            'id'=>129,
            'idDepartamento'=>2,

            'nombre'=>'CANDELARIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>130,
            'idDepartamento'=>2,

            'nombre'=>'GALAPA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>131,
            'idDepartamento'=>2,

            'nombre'=>'JUAN DE ACOSTA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>132,
            'idDepartamento'=>2,

            'nombre'=>'LURUACO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>133,
            'idDepartamento'=>2,

            'nombre'=>'MALAMBO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>134,
            'idDepartamento'=>2,

            'nombre'=>'MANATI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>135,
            'idDepartamento'=>2,

            'nombre'=>'PALMAR DE VARELA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>136,
            'idDepartamento'=>2,

            'nombre'=>'PIOJO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>137,
            'idDepartamento'=>2,

            'nombre'=>'POLO NUEVO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>138,
            'idDepartamento'=>2,

            'nombre'=>'PONEDERA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>139,
            'idDepartamento'=>2,

            'nombre'=>'PUERTO COLOMBIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>140,
            'idDepartamento'=>2,

            'nombre'=>'REPELON'
            ] );
            
            
                        
            municipio::create( [
            'id'=>141,
            'idDepartamento'=>2,

            'nombre'=>'SABANAGRANDE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>142,
            'idDepartamento'=>2,

            'nombre'=>'SABANALARGA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>143,
            'idDepartamento'=>2,

            'nombre'=>'SANTA LUCIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>144,
            'idDepartamento'=>2,

            'nombre'=>'SANTO TOMAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>145,
            'idDepartamento'=>2,

            'nombre'=>'SOLEDAD'
            ] );
            
            
                        
            municipio::create( [
            'id'=>146,
            'idDepartamento'=>2,

            'nombre'=>'SUAN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>147,
            'idDepartamento'=>2,

            'nombre'=>'TUBARA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>148,
            'idDepartamento'=>2,

            'nombre'=>'USIACURI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>149,
         'idDepartamento'=>3,
            'nombre'=>'Santa Fe de Bogotá'
            ] );
            
            
                        
            municipio::create( [
            'id'=>150,
         'idDepartamento'=>3,
            'nombre'=>'USAQUEN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>151,
         'idDepartamento'=>3,
            'nombre'=>'CHAPINERO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>152,
         'idDepartamento'=>3,
            'nombre'=>'SANTA FE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>153,
         'idDepartamento'=>3,
            'nombre'=>'SAN CRISTOBAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>154,
         'idDepartamento'=>3,
            'nombre'=>'USME'
            ] );
            
            
                        
            municipio::create( [
            'id'=>155,
         'idDepartamento'=>3,
            'nombre'=>'TUNJUELITO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>156,
         'idDepartamento'=>3,
            'nombre'=>'BOSA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>157,
         'idDepartamento'=>3,
            'nombre'=>'KENNEDY'
            ] );
            
            
                        
            municipio::create( [
            'id'=>158,
         'idDepartamento'=>3,
            'nombre'=>'FONTIBON'
            ] );
            
            
                        
            municipio::create( [
            'id'=>159,
            'idDepartamento'=>3,
            'nombre'=>'ENGATIVA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>160,
            'idDepartamento'=>3,
            'nombre'=>'SUBA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>161,
            'idDepartamento'=>3,
            'nombre'=>'BARRIOS UNIDOS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>162,
            'idDepartamento'=>3,
            'nombre'=>'TEUSAQUILLO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>163,
            'idDepartamento'=>3,
            'nombre'=>'MARTIRES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>164,
            'idDepartamento'=>3,
            'nombre'=>'ANTONIO NARIÑO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>165,
            'idDepartamento'=>3,
            'nombre'=>'PUENTE ARANDA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>166,
            'idDepartamento'=>3,
            'nombre'=>'CANDELARIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>167,
            'idDepartamento'=>3,
            'nombre'=>'RAFAEL URIBE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>168,
            'idDepartamento'=>3,
            'nombre'=>'CIUDAD BOLIVAR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>169,
            'idDepartamento'=>3,
            'nombre'=>'SUMAPAZ'
            ] );
            
            
                        
            municipio::create( [
            'id'=>170,
         'idDepartamento'=>4,
            'nombre'=>'CARTAGENA (DISTRITO TURISTICO Y CULTURAL DE CARTAGENA)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>171,
         'idDepartamento'=>4,
            'nombre'=>'ACHI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>172,
            'idDepartamento'=>4,
            'nombre'=>'ALTOS DEL ROSARIO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>173,
            'idDepartamento'=>4,
            'nombre'=>'ARENAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>174,
            'idDepartamento'=>4,
            'nombre'=>'ARJONA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>175,
            'idDepartamento'=>4,
            'nombre'=>'ARROYOHONDO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>176,
            'idDepartamento'=>4,
            'nombre'=>'BARRANCO DE LOBA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>177,
            'idDepartamento'=>4,

            'nombre'=>'CALAMAR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>178,
            'idDepartamento'=>4,

            'nombre'=>'CANTAGALLO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>179,
            'idDepartamento'=>4,

            'nombre'=>'CICUCO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>180,
            'idDepartamento'=>4,

            'nombre'=>'CORDOBA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>181,
            'idDepartamento'=>4,

            'nombre'=>'CLEMENCIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>182,
            'idDepartamento'=>4,

            'nombre'=>'EL CARMEN DE BOLIVAR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>183,
            'idDepartamento'=>4,

            'nombre'=>'EL GUAMO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>184,
            'idDepartamento'=>4,

            'nombre'=>'EL PEÑON'
            ] );
            
            
                        
            municipio::create( [
            'id'=>185,
            'idDepartamento'=>4,

            'nombre'=>'HATILLO DE LOBA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>186,
            'idDepartamento'=>4,

            'nombre'=>'MAGANGUE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>187,
            'idDepartamento'=>4,

            'nombre'=>'MAHATES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>188,
            'idDepartamento'=>4,

            'nombre'=>'MARGARITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>189,
            'idDepartamento'=>4,

            'nombre'=>'MARIA LA BAJA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>190,
            'idDepartamento'=>4,

            'nombre'=>'MONTECRISTO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>191,
            'idDepartamento'=>4,

            'nombre'=>'MOMPOS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>192,
            'idDepartamento'=>4,

            'nombre'=>'MORALES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>193,
            'idDepartamento'=>4,

            'nombre'=>'PINILLOS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>194,
            'idDepartamento'=>4,

            'nombre'=>'REGIDOR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>195,
            'idDepartamento'=>4,

            'nombre'=>'RIO VIEJO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>196,
            'idDepartamento'=>4,

            'nombre'=>'SAN CRISTOBAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>197,
            'idDepartamento'=>4,

            'nombre'=>'SAN ESTANISLAO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>198,
            'idDepartamento'=>4,

            'nombre'=>'SAN FERNANDO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>199,
            'idDepartamento'=>4,

            'nombre'=>'SAN JACINTO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>200,
            'idDepartamento'=>4,

            'nombre'=>'SAN JACINTO DEL CAUCA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>201,
            'idDepartamento'=>4,

            'nombre'=>'SAN JUAN NEPOMUCENO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>202,
            'idDepartamento'=>4,

            'nombre'=>'SAN MARTIN DE LOBA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>203,
            'idDepartamento'=>4,

            'nombre'=>'SAN PABLO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>204,
            'idDepartamento'=>4,

            'nombre'=>'SANTA CATALINA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>205,
            'idDepartamento'=>4,

            'nombre'=>'SANTA ROSA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>206,
            'idDepartamento'=>4,

            'nombre'=>'SANTA ROSA DEL SUR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>207,
            'idDepartamento'=>4,

            'nombre'=>'SIMITI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>208,
            'idDepartamento'=>4,

            'nombre'=>'SOPLAVIENTO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>209,
            'idDepartamento'=>4,

            'nombre'=>'TALAIGUA NUEVO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>210,
            'idDepartamento'=>4,

            'nombre'=>'TIQUISIO (PUERTO RICO)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>211,
            'idDepartamento'=>4,

            'nombre'=>'TURBACO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>212,
            'idDepartamento'=>4,

            'nombre'=>'TURBANA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>213,
            'idDepartamento'=>4,

            'nombre'=>'VILLANUEVA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>214,
            'idDepartamento'=>4,

            'nombre'=>'ZAMBRANO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>215,
         'idDepartamento'=>5,
            'nombre'=>'TUNJA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>216,
            'idDepartamento'=>5,
            'nombre'=>'ALMEIDA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>217,
            'idDepartamento'=>5,
            'nombre'=>'AQUITANIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>218,
            'idDepartamento'=>5,
            'nombre'=>'ARCABUCO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>219,
            'idDepartamento'=>5,
            'nombre'=>'BELEN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>220,
            'idDepartamento'=>5,
            'nombre'=>'BERBEO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>221,
            'idDepartamento'=>5,
            'nombre'=>'BETEITIVA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>222,
            'idDepartamento'=>5,
            'nombre'=>'BOAVITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>223,
            'idDepartamento'=>5,

            'nombre'=>'BOYACA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>224,
            'idDepartamento'=>5,

            'nombre'=>'BRICEÑO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>225,
            'idDepartamento'=>5,

            'nombre'=>'BUENAVISTA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>226,
            'idDepartamento'=>5,

            'nombre'=>'BUSBANZA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>227,
            'idDepartamento'=>5,

            'nombre'=>'CALDAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>228,
            'idDepartamento'=>5,

            'nombre'=>'CAMPOHERMOSO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>229,
            'idDepartamento'=>5,

            'nombre'=>'CERINZA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>230,
            'idDepartamento'=>5,

            'nombre'=>'CHINAVITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>231,
            'idDepartamento'=>5,

            'nombre'=>'CHIQUINQUIRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>232,
            'idDepartamento'=>5,

            'nombre'=>'CHISCAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>233,
            'idDepartamento'=>5,

            'nombre'=>'CHITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>234,
            'idDepartamento'=>5,

            'nombre'=>'CHITARAQUE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>235,
            'idDepartamento'=>5,

            'nombre'=>'CHIVATA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>236,
            'idDepartamento'=>5,

            'nombre'=>'CIENEGA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>237,
            'idDepartamento'=>5,

            'nombre'=>'COMBITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>238,
            'idDepartamento'=>5,

            'nombre'=>'COPER'
            ] );
            
            
                        
            municipio::create( [
            'id'=>239,
            'idDepartamento'=>5,

            'nombre'=>'CORRALES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>240,
            'idDepartamento'=>5,

            'nombre'=>'COVARACHIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>241,
            'idDepartamento'=>5,

            'nombre'=>'CUBARA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>242,
            'idDepartamento'=>5,

            'nombre'=>'CUCAITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>243,
            'idDepartamento'=>5,

            'nombre'=>'CUITIVA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>244,
            'idDepartamento'=>5,

            'nombre'=>'CHIQUIZA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>245,
            'idDepartamento'=>5,

            'nombre'=>'CHIVOR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>246,
            'idDepartamento'=>5,

            'nombre'=>'DUITAMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>247,
            'idDepartamento'=>5,

            'nombre'=>'EL COCUY'
            ] );
            
            
                        
            municipio::create( [
            'id'=>248,
            'idDepartamento'=>5,

            'nombre'=>'EL ESPINO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>249,
            'idDepartamento'=>5,

            'nombre'=>'FIRAVITOBA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>250,
            'idDepartamento'=>5,

            'nombre'=>'FLORESTA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>251,
            'idDepartamento'=>5,

            'nombre'=>'GACHANTIVA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>252,
            'idDepartamento'=>5,

            'nombre'=>'GAMEZA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>253,
            'idDepartamento'=>5,

            'nombre'=>'GARAGOA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>254,
            'idDepartamento'=>5,

            'nombre'=>'GUACAMAYAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>255,
            'idDepartamento'=>5,

            'nombre'=>'GUATEQUE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>256,
            'idDepartamento'=>5,

            'nombre'=>'GUAYATA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>257,
            'idDepartamento'=>5,

            'nombre'=>'GUICAN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>258,
            'idDepartamento'=>5,

            'nombre'=>'IZA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>259,
            'idDepartamento'=>5,

            'nombre'=>'JENESANO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>260,
            'idDepartamento'=>5,

            'nombre'=>'JERICO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>261,
            'idDepartamento'=>5,

            'nombre'=>'LABRANZAGRANDE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>262,
            'idDepartamento'=>5,

            'nombre'=>'LA CAPILLA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>263,
            'idDepartamento'=>5,

            'nombre'=>'LA VICTORIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>264,
            'idDepartamento'=>5,

            'nombre'=>'LA UVITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>265,
            'idDepartamento'=>5,

            'nombre'=>'VILLA DE LEIVA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>266,
            'idDepartamento'=>5,

            'nombre'=>'MACANAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>267,
            'idDepartamento'=>5,

            'nombre'=>'MARIPI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>268,
            'idDepartamento'=>5,

            'nombre'=>'MIRAFLORES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>269,
            'idDepartamento'=>5,

            'nombre'=>'MONGUA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>270,
            'idDepartamento'=>5,

            'nombre'=>'MONGUI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>271,
            'idDepartamento'=>5,

            'nombre'=>'MONIQUIRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>272,
            'idDepartamento'=>5,

            'nombre'=>'MOTAVITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>273,
            'idDepartamento'=>5,

            'nombre'=>'MUZO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>274,
            'idDepartamento'=>5,

            'nombre'=>'NOBSA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>275,
            'idDepartamento'=>5,

            'nombre'=>'NUEVO COLON'
            ] );
            
            
                        
            municipio::create( [
            'id'=>276,
            'idDepartamento'=>5,

            'nombre'=>'OICATA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>277,
            'idDepartamento'=>5,

            'nombre'=>'OTANCHE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>278,
            'idDepartamento'=>5,

            'nombre'=>'PACHAVITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>279,
            'idDepartamento'=>5,

            'nombre'=>'PAEZ'
            ] );
            
            
                        
            municipio::create( [
            'id'=>280,
            'idDepartamento'=>5,

            'nombre'=>'PAIPA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>281,
            'idDepartamento'=>5,

            'nombre'=>'PAJARITO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>282,
            'idDepartamento'=>5,

            'nombre'=>'PANQUEBA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>283,
            'idDepartamento'=>5,

            'nombre'=>'PAUNA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>284,
            'idDepartamento'=>5,

            'nombre'=>'PAYA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>285,
            'idDepartamento'=>5,

            'nombre'=>'PAZ DEL RIO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>286,
            'idDepartamento'=>5,

            'nombre'=>'PESCA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>287,
            'idDepartamento'=>5,

            'nombre'=>'PISBA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>288,
            'idDepartamento'=>5,

            'nombre'=>'PUERTO BOYACA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>289,
            'idDepartamento'=>5,

            'nombre'=>'QUIPAMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>290,
            'idDepartamento'=>5,

            'nombre'=>'RAMIRIQUI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>291,
            'idDepartamento'=>5,

            'nombre'=>'RAQUIRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>292,
            'idDepartamento'=>5,

            'nombre'=>'RONDON'
            ] );
            
            
                        
            municipio::create( [
            'id'=>293,
            'idDepartamento'=>5,

            'nombre'=>'SABOYA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>294,
            'idDepartamento'=>5,

            'nombre'=>'SACHICA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>295,
            'idDepartamento'=>5,

            'nombre'=>'SAMACA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>296,
            'idDepartamento'=>5,

            'nombre'=>'SAN EDUARDO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>297,
            'idDepartamento'=>5,

            'nombre'=>'SAN JOSE DE PARE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>298,
            'idDepartamento'=>5,

            'nombre'=>'SAN LUIS DE GACENO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>299,
            'idDepartamento'=>5,

            'nombre'=>'SAN MATEO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>300,
            'idDepartamento'=>5,

            'nombre'=>'SAN MIGUEL DE SEMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>301,
            'idDepartamento'=>5,

            'nombre'=>'SAN PABLO DE BORBUR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>302,
            'idDepartamento'=>5,

            'nombre'=>'SANTANA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>303,
            'idDepartamento'=>5,

            'nombre'=>'SANTA MARIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>304,
            'idDepartamento'=>5,

            'nombre'=>'SANTA ROSA DE VITERBO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>305,
            'idDepartamento'=>5,

            'nombre'=>'SANTA SOFIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>306,
            'idDepartamento'=>5,

            'nombre'=>'SATIVANORTE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>307,
            'idDepartamento'=>5,

            'nombre'=>'SATIVASUR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>308,
            'idDepartamento'=>5,

            'nombre'=>'SIACHOQUE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>309,
            'idDepartamento'=>5,

            'nombre'=>'SOATA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>310,
            'idDepartamento'=>5,

            'nombre'=>'SOCOTA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>311,
            'idDepartamento'=>5,

            'nombre'=>'SOCHA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>312,
            'idDepartamento'=>5,

            'nombre'=>'SOGAMOSO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>313,
            'idDepartamento'=>5,

            'nombre'=>'SOMONDOCO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>314,
            'idDepartamento'=>5,

            'nombre'=>'SORA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>315,
            'idDepartamento'=>5,

            'nombre'=>'SOTAQUIRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>316,
            'idDepartamento'=>5,

            'nombre'=>'SORACA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>317,
            'idDepartamento'=>5,

            'nombre'=>'SUSACON'
            ] );
            
            
                        
            municipio::create( [
            'id'=>318,
            'idDepartamento'=>5,

            'nombre'=>'SUTAMARCHAN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>319,
            'idDepartamento'=>5,

            'nombre'=>'SUTATENZA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>320,
            'idDepartamento'=>5,

            'nombre'=>'TASCO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>321,
            'idDepartamento'=>5,

            'nombre'=>'TENZA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>322,
            'idDepartamento'=>5,

            'nombre'=>'TIBANA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>323,
            'idDepartamento'=>5,

            'nombre'=>'TIBASOSA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>324,
            'idDepartamento'=>5,

            'nombre'=>'TINJACA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>325,
            'idDepartamento'=>5,

            'nombre'=>'TIPACOQUE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>326,
            'idDepartamento'=>5,

            'nombre'=>'TOCA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>327,
            'idDepartamento'=>5,

            'nombre'=>'TOGUI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>328,
            'idDepartamento'=>5,

            'nombre'=>'TOPAGA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>329,
            'idDepartamento'=>5,

            'nombre'=>'TOTA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>330,
            'idDepartamento'=>5,

            'nombre'=>'TUNUNGUA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>331,
            'idDepartamento'=>5,

            'nombre'=>'TURMEQUE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>332,
            'idDepartamento'=>5,

            'nombre'=>'TUTA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>333,
            'idDepartamento'=>5,

            'nombre'=>'TUTASA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>334,
            'idDepartamento'=>5,

            'nombre'=>'UMBITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>335,
            'idDepartamento'=>5,

            'nombre'=>'VENTAQUEMADA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>336,
            'idDepartamento'=>5,

            'nombre'=>'VIRACACHA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>337,
            'idDepartamento'=>5,

            'nombre'=>'ZETAQUIRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>338,
         'idDepartamento'=>6,
            'nombre'=>'MANIZALES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>339,
            'idDepartamento'=>6,
            'nombre'=>'AGUADAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>340,
            'idDepartamento'=>6,
            'nombre'=>'ANSERMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>341,
            'idDepartamento'=>6,
            'nombre'=>'ARANZAZU'
            ] );
            
            
                        
            municipio::create( [
            'id'=>342,
            'idDepartamento'=>6,
            'nombre'=>'BELALCAZAR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>343,
            'idDepartamento'=>6,

            'nombre'=>'CHINCHINA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>344,
            'idDepartamento'=>6,

            'nombre'=>'FILADELFIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>345,
            'idDepartamento'=>6,

            'nombre'=>'LA DORADA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>346,
            'idDepartamento'=>6,

            'nombre'=>'LA MERCED'
            ] );
            
            
                        
            municipio::create( [
            'id'=>347,
            'idDepartamento'=>6,

            'nombre'=>'MANZANARES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>348,
            'idDepartamento'=>6,

            'nombre'=>'MARMATO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>349,
            'idDepartamento'=>6,

            'nombre'=>'MARQUETALIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>350,
            'idDepartamento'=>6,

            'nombre'=>'MARULANDA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>351,
            'idDepartamento'=>6,

            'nombre'=>'NEIRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>352,
            'idDepartamento'=>6,

            'nombre'=>'NORCASIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>353,
            'idDepartamento'=>6,

            'nombre'=>'PACORA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>354,
            'idDepartamento'=>6,

            'nombre'=>'PALESTINA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>355,
            'idDepartamento'=>6,

            'nombre'=>'PENSILVANIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>356,
            'idDepartamento'=>6,

            'nombre'=>'RIOSUCIO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>357,
            'idDepartamento'=>6,

            'nombre'=>'RISARALDA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>358,
            'idDepartamento'=>6,

            'nombre'=>'SALAMINA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>359,
            'idDepartamento'=>6,

            'nombre'=>'SAMANA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>360,
            'idDepartamento'=>6,

            'nombre'=>'SAN JOSE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>361,
            'idDepartamento'=>6,

            'nombre'=>'SUPIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>362,
            'idDepartamento'=>6,

            'nombre'=>'VICTORIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>363,
            'idDepartamento'=>6,

            'nombre'=>'VILLAMARIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>364,
            'idDepartamento'=>6,

            'nombre'=>'VITERBO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>365,
         'idDepartamento'=>7,
            'nombre'=>'FLORENCIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>366,
            'idDepartamento'=>7,
            'nombre'=>'ALBANIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>367,
            'idDepartamento'=>7,
            'nombre'=>'BELEN DE LOS ANDAQUIES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>368,
            'idDepartamento'=>7,

            'nombre'=>'CARTAGENA DEL CHAIRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>369,
            'idDepartamento'=>7,

            'nombre'=>'CURILLO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>370,
            'idDepartamento'=>7,

            'nombre'=>'EL DONCELLO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>371,
            'idDepartamento'=>7,

            'nombre'=>'EL PAUJIL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>372,
            'idDepartamento'=>7,

            'nombre'=>'LA MONTAÑITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>373,
            'idDepartamento'=>7,

            'nombre'=>'MILAN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>374,
            'idDepartamento'=>7,

            'nombre'=>'MORELIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>375,
            'idDepartamento'=>7,

            'nombre'=>'PUERTO RICO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>376,
            'idDepartamento'=>7,

            'nombre'=>'SAN JOSE DE FRAGUA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>377,
            'idDepartamento'=>7,

            'nombre'=>'SAN VICENTE DEL CAGUAN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>378,
            'idDepartamento'=>7,

            'nombre'=>'SOLANO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>379,
            'idDepartamento'=>7,

            'nombre'=>'SOLITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>380,
            'idDepartamento'=>7,

            'nombre'=>'VALPARAISO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>381,
         'idDepartamento'=>8,
            'nombre'=>'POPAYAN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>382,
            'idDepartamento'=>8,
            'nombre'=>'ALMAGUER'
            ] );
            
            
                        
            municipio::create( [
            'id'=>383,
            'idDepartamento'=>8,
            'nombre'=>'ARGELIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>384,
            'idDepartamento'=>8,
            'nombre'=>'BALBOA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>385,
            'idDepartamento'=>8,

            'nombre'=>'BOLIVAR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>386,
            'idDepartamento'=>8,

            'nombre'=>'BUENOS AIRES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>387,
            'idDepartamento'=>8,

            'nombre'=>'CAJIBIO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>388,
            'idDepartamento'=>8,

            'nombre'=>'CALDONO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>389,
            'idDepartamento'=>8,

            'nombre'=>'CALOTO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>390,
            'idDepartamento'=>8,

            'nombre'=>'CORINTO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>391,
            'idDepartamento'=>8,

            'nombre'=>'EL TAMBO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>392,
            'idDepartamento'=>8,

            'nombre'=>'FLORENCIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>393,
            'idDepartamento'=>8,

            'nombre'=>'GUAPI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>394,
            'idDepartamento'=>8,

            'nombre'=>'INZA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>395,
            'idDepartamento'=>8,

            'nombre'=>'JAMBALO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>396,
            'idDepartamento'=>8,

            'nombre'=>'LA SIERRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>397,
            'idDepartamento'=>8,

            'nombre'=>'LA VEGA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>398,
            'idDepartamento'=>8,

            'nombre'=>'LOPEZ (MICAY)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>399,
            'idDepartamento'=>8,

            'nombre'=>'MERCADERES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>400,
            'idDepartamento'=>8,

            'nombre'=>'MIRANDA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>401,
            'idDepartamento'=>8,

            'nombre'=>'MORALES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>402,
            'idDepartamento'=>8,

            'nombre'=>'PADILLA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>403,
            'idDepartamento'=>8,

            'nombre'=>'PAEZ (BELALCAZAR)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>404,
            'idDepartamento'=>8,

            'nombre'=>'PATIA (EL BORDO)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>405,
            'idDepartamento'=>8,

            'nombre'=>'PIAMONTE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>406,
            'idDepartamento'=>8,

            'nombre'=>'PIENDAMO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>407,
            'idDepartamento'=>8,

            'nombre'=>'PUERTO TEJADA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>408,
            'idDepartamento'=>8,

            'nombre'=>'PURACE (COCONUCO)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>409,
            'idDepartamento'=>8,

            'nombre'=>'ROSAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>410,
            'idDepartamento'=>8,

            'nombre'=>'SAN SEBASTIAN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>411,
            'idDepartamento'=>8,

            'nombre'=>'SANTANDER DE QUILICHAO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>412,
            'idDepartamento'=>8,

            'nombre'=>'SANTA ROSA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>413,
            'idDepartamento'=>8,

            'nombre'=>'SILVIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>414,
            'idDepartamento'=>8,

            'nombre'=>'SOTARA (PAISPAMBA)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>415,
            'idDepartamento'=>8,

            'nombre'=>'SUAREZ'
            ] );
            
            
                        
            municipio::create( [
            'id'=>416,
            'idDepartamento'=>8,

            'nombre'=>'TIMBIO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>417,
            'idDepartamento'=>8,

            'nombre'=>'TIMBIQUI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>418,
            'idDepartamento'=>8,

            'nombre'=>'TORIBIO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>419,
            'idDepartamento'=>8,

            'nombre'=>'TOTORO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>420,
            'idDepartamento'=>8,

            'nombre'=>'VILLARICA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>421,
         'idDepartamento'=>9,
            'nombre'=>'VALLEDUPAR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>422,
            'idDepartamento'=>9,
            'nombre'=>'AGUACHICA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>423,
            'idDepartamento'=>9,
            'nombre'=>'AGUSTIN CODAZZI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>424,
            'idDepartamento'=>9,
            'nombre'=>'ASTREA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>425,
            'idDepartamento'=>9,
            'nombre'=>'BECERRIL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>426,
            'idDepartamento'=>9,
            'nombre'=>'BOSCONIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>427,
            'idDepartamento'=>9,

            'nombre'=>'CHIMICHAGUA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>428,
            'idDepartamento'=>9,

            'nombre'=>'CHIRIGUANA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>429,
            'idDepartamento'=>9,

            'nombre'=>'CURUMANI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>430,
            'idDepartamento'=>9,

            'nombre'=>'EL COPEY'
            ] );
            
            
                        
            municipio::create( [
            'id'=>431,
            'idDepartamento'=>9,

            'nombre'=>'EL PASO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>432,
            'idDepartamento'=>9,

            'nombre'=>'GAMARRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>433,
            'idDepartamento'=>9,

            'nombre'=>'GONZALEZ'
            ] );
            
            
                        
            municipio::create( [
            'id'=>434,
            'idDepartamento'=>9,

            'nombre'=>'LA GLORIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>435,
            'idDepartamento'=>9,

            'nombre'=>'LA JAGUA IBIRICO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>436,
            'idDepartamento'=>9,

            'nombre'=>'MANAURE (BALCON DEL CESAR)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>437,
            'idDepartamento'=>9,

            'nombre'=>'PAILITAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>438,
            'idDepartamento'=>9,

            'nombre'=>'PELAYA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>439,
            'idDepartamento'=>9,

            'nombre'=>'PUEBLO BELLO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>440,
            'idDepartamento'=>9,

            'nombre'=>'RIO DE ORO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>441,
            'idDepartamento'=>9,

            'nombre'=>'LA PAZ (ROBLES)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>442,
            'idDepartamento'=>9,

            'nombre'=>'SAN ALBERTO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>443,
            'idDepartamento'=>9,

            'nombre'=>'SAN DIEGO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>444,
            'idDepartamento'=>9,

            'nombre'=>'SAN MARTIN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>445,
            'idDepartamento'=>9,

            'nombre'=>'TAMALAMEQUE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>446,
         'idDepartamento'=>10,
            'nombre'=>'MONTERIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>447,
            'idDepartamento'=>10,
            'nombre'=>'AYAPEL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>448,
            'idDepartamento'=>10,
            'nombre'=>'BUENAVISTA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>449,
            'idDepartamento'=>10,
            'nombre'=>'CANALETE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>450,
            'idDepartamento'=>10,

            'nombre'=>'CERETE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>451,
            'idDepartamento'=>10,

            'nombre'=>'CHIMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>452,
            'idDepartamento'=>10,

            'nombre'=>'CHINU'
            ] );
            
            
                        
            municipio::create( [
            'id'=>453,
            'idDepartamento'=>10,

            'nombre'=>'CIENAGA DE ORO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>454,
            'idDepartamento'=>10,

            'nombre'=>'COTORRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>455,
            'idDepartamento'=>10,

            'nombre'=>'LA APARTADA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>456,
            'idDepartamento'=>10,

            'nombre'=>'LORICA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>457,
            'idDepartamento'=>10,

            'nombre'=>'LOS CORDOBAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>458,
            'idDepartamento'=>10,

            'nombre'=>'MOMIL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>459,
            'idDepartamento'=>10,

            'nombre'=>'MONTELIBANO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>460,
            'idDepartamento'=>10,

            'nombre'=>'MOÑITOS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>461,
            'idDepartamento'=>10,

            'nombre'=>'PLANETA RICA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>462,
            'idDepartamento'=>10,

            'nombre'=>'PUEBLO NUEVO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>463,
            'idDepartamento'=>10,

            'nombre'=>'PUERTO ESCONDIDO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>464,
            'idDepartamento'=>10,

            'nombre'=>'PUERTO LIBERTADOR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>465,
            'idDepartamento'=>10,

            'nombre'=>'PURISIMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>466,
            'idDepartamento'=>10,

            'nombre'=>'SAHAGUN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>467,
            'idDepartamento'=>10,

            'nombre'=>'SAN ANDRES SOTAVENTO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>468,
            'idDepartamento'=>10,

            'nombre'=>'SAN ANTERO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>469,
            'idDepartamento'=>10,

            'nombre'=>'SAN BERNARDO DEL VIENTO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>470,
            'idDepartamento'=>10,

            'nombre'=>'SAN CARLOS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>471,
            'idDepartamento'=>10,

            'nombre'=>'SAN PELAYO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>472,
            'idDepartamento'=>10,

            'nombre'=>'TIERRALTA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>473,
            'idDepartamento'=>10,

            'nombre'=>'VALENCIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>474,
         'idDepartamento'=>11,
            'nombre'=>'AGUA DE DIOS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>475,
            'idDepartamento'=>11,
            'nombre'=>'ALBAN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>476,
            'idDepartamento'=>11,
            'nombre'=>'ANAPOIMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>477,
            'idDepartamento'=>11,
            'nombre'=>'ANOLAIMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>478,
            'idDepartamento'=>11,
            'nombre'=>'ARBELAEZ'
            ] );
            
            
                        
            municipio::create( [
            'id'=>479,
            'idDepartamento'=>11,
            'nombre'=>'BELTRAN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>480,
            'idDepartamento'=>11,
            'nombre'=>'BITUIMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>481,
            'idDepartamento'=>11,
            'nombre'=>'BOJACA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>482,
            'idDepartamento'=>11,

            'nombre'=>'CABRERA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>483,
            'idDepartamento'=>11,

            'nombre'=>'CACHIPAY'
            ] );
            
            
                        
            municipio::create( [
            'id'=>484,
            'idDepartamento'=>11,

            'nombre'=>'CAJICA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>485,
            'idDepartamento'=>11,

            'nombre'=>'CAPARRAPI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>486,
            'idDepartamento'=>11,

            'nombre'=>'CAQUEZA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>487,
            'idDepartamento'=>11,

            'nombre'=>'CARMEN DE CARUPA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>488,
            'idDepartamento'=>11,

            'nombre'=>'CHAGUANI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>489,
            'idDepartamento'=>11,

            'nombre'=>'CHIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>490,
            'idDepartamento'=>11,

            'nombre'=>'CHIPAQUE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>491,
            'idDepartamento'=>11,

            'nombre'=>'CHOACHI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>492,
            'idDepartamento'=>11,

            'nombre'=>'CHOCONTA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>493,
            'idDepartamento'=>11,

            'nombre'=>'COGUA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>494,
            'idDepartamento'=>11,

            'nombre'=>'COTA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>495,
            'idDepartamento'=>11,

            'nombre'=>'CUCUNUBA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>496,
            'idDepartamento'=>11,

            'nombre'=>'EL COLEGIO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>497,
            'idDepartamento'=>11,

            'nombre'=>'EL PEÑON'
            ] );
            
            
                        
            municipio::create( [
            'id'=>498,
            'idDepartamento'=>11,

            'nombre'=>'EL ROSAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>499,
            'idDepartamento'=>11,

            'nombre'=>'FACATATIVA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>500,
            'idDepartamento'=>11,

            'nombre'=>'FOMEQUE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>501,
            'idDepartamento'=>11,

            'nombre'=>'FOSCA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>502,
            'idDepartamento'=>11,

            'nombre'=>'FUNZA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>503,
            'idDepartamento'=>11,

            'nombre'=>'FUQUENE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>504,
            'idDepartamento'=>11,

            'nombre'=>'FUSAGASUGA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>505,
            'idDepartamento'=>11,

            'nombre'=>'GACHALA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>506,
            'idDepartamento'=>11,

            'nombre'=>'GACHANCIPA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>507,
            'idDepartamento'=>11,

            'nombre'=>'GACHETA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>508,
            'idDepartamento'=>11,

            'nombre'=>'GAMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>509,
            'idDepartamento'=>11,

            'nombre'=>'GIRARDOT'
            ] );
            
            
                        
            municipio::create( [
            'id'=>510,
            'idDepartamento'=>11,

            'nombre'=>'GRANADA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>511,
            'idDepartamento'=>11,

            'nombre'=>'GUACHETA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>512,
            'idDepartamento'=>11,

            'nombre'=>'GUADUAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>513,
            'idDepartamento'=>11,

            'nombre'=>'GUASCA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>514,
            'idDepartamento'=>11,

            'nombre'=>'GUATAQUI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>515,
            'idDepartamento'=>11,

            'nombre'=>'GUATAVITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>516,
            'idDepartamento'=>11,

            'nombre'=>'GUAYABAL DE SIQUIMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>517,
            'idDepartamento'=>11,

            'nombre'=>'GUAYABETAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>518,
            'idDepartamento'=>11,

            'nombre'=>'GUTIERREZ'
            ] );
            
            
                        
            municipio::create( [
            'id'=>519,
            'idDepartamento'=>11,

            'nombre'=>'JERUSALEN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>520,
            'idDepartamento'=>11,

            'nombre'=>'JUNIN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>521,
            'idDepartamento'=>11,

            'nombre'=>'LA CALERA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>522,
            'idDepartamento'=>11,

            'nombre'=>'LA MESA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>523,
            'idDepartamento'=>11,

            'nombre'=>'LA PALMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>524,
            'idDepartamento'=>11,

            'nombre'=>'LA PEÑA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>525,
            'idDepartamento'=>11,

            'nombre'=>'LA VEGA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>526,
            'idDepartamento'=>11,

            'nombre'=>'LENGUAZAQUE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>527,
            'idDepartamento'=>11,

            'nombre'=>'MACHETA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>528,
            'idDepartamento'=>11,

            'nombre'=>'MADRID'
            ] );
            
            
                        
            municipio::create( [
            'id'=>529,
            'idDepartamento'=>11,

            'nombre'=>'MANTA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>530,
            'idDepartamento'=>11,

            'nombre'=>'MEDINA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>531,
            'idDepartamento'=>11,

            'nombre'=>'MOSQUERA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>532,
            'idDepartamento'=>11,

            'nombre'=>'NARIÑO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>533,
            'idDepartamento'=>11,

            'nombre'=>'NEMOCON'
            ] );
            
            
                        
            municipio::create( [
            'id'=>534,
            'idDepartamento'=>11,

            'nombre'=>'NILO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>535,
            'idDepartamento'=>11,

            'nombre'=>'NIMAIMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>536,
            'idDepartamento'=>11,

            'nombre'=>'NOCAIMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>537,
            'idDepartamento'=>11,

            'nombre'=>'VENECIA (OSPINA PEREZ)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>538,
            'idDepartamento'=>11,

            'nombre'=>'PACHO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>539,
            'idDepartamento'=>11,

            'nombre'=>'PAIME'
            ] );
            
            
                        
            municipio::create( [
            'id'=>540,
            'idDepartamento'=>11,

            'nombre'=>'PANDI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>541,
            'idDepartamento'=>11,

            'nombre'=>'PARATEBUENO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>542,
            'idDepartamento'=>11,

            'nombre'=>'PASCA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>543,
            'idDepartamento'=>11,

            'nombre'=>'PUERTO SALGAR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>544,
            'idDepartamento'=>11,

            'nombre'=>'PULI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>545,
            'idDepartamento'=>11,

            'nombre'=>'QUEBRADANEGRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>546,
            'idDepartamento'=>11,

            'nombre'=>'QUETAME'
            ] );
            
            
                        
            municipio::create( [
            'id'=>547,
            'idDepartamento'=>11,

            'nombre'=>'QUIPILE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>548,
            'idDepartamento'=>11,

            'nombre'=>'APULO (RAFAEL REYES)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>549,
            'idDepartamento'=>11,

            'nombre'=>'RICAURTE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>550,
            'idDepartamento'=>11,

            'nombre'=>'SAN ANTONIO DEL TEQUENDAMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>551,
            'idDepartamento'=>11,

            'nombre'=>'SAN BERNARDO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>552,
            'idDepartamento'=>11,

            'nombre'=>'SAN CAYETANO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>553,
            'idDepartamento'=>11,

            'nombre'=>'SAN FRANCISCO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>554,
            'idDepartamento'=>11,

            'nombre'=>'SAN JUAN DE RIOSECO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>555,
            'idDepartamento'=>11,

            'nombre'=>'SASAIMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>556,
            'idDepartamento'=>11,

            'nombre'=>'SESQUILE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>557,
            'idDepartamento'=>11,

            'nombre'=>'SIBATE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>558,
            'idDepartamento'=>11,

            'nombre'=>'SILVANIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>559,
            'idDepartamento'=>11,

            'nombre'=>'SIMIJACA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>560,
            'idDepartamento'=>11,

            'nombre'=>'SOACHA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>561,
            'idDepartamento'=>11,

            'nombre'=>'SOPO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>562,
            'idDepartamento'=>11,

            'nombre'=>'SUBACHOQUE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>563,
            'idDepartamento'=>11,

            'nombre'=>'SUESCA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>564,
            'idDepartamento'=>11,

            'nombre'=>'SUPATA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>565,
            'idDepartamento'=>11,

            'nombre'=>'SUSA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>566,
            'idDepartamento'=>11,

            'nombre'=>'SUTATAUSA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>567,
            'idDepartamento'=>11,

            'nombre'=>'TABIO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>568,
            'idDepartamento'=>11,

            'nombre'=>'TAUSA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>569,
            'idDepartamento'=>11,

            'nombre'=>'TENA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>570,
            'idDepartamento'=>11,

            'nombre'=>'TENJO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>571,
            'idDepartamento'=>11,

            'nombre'=>'TIBACUY'
            ] );
            
            
                        
            municipio::create( [
            'id'=>572,
            'idDepartamento'=>11,

            'nombre'=>'TIBIRITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>573,
            'idDepartamento'=>11,

            'nombre'=>'TOCAIMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>574,
            'idDepartamento'=>11,

            'nombre'=>'TOCANCIPA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>575,
            'idDepartamento'=>11,

            'nombre'=>'TOPAIPI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>576,
            'idDepartamento'=>11,

            'nombre'=>'UBALA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>577,
            'idDepartamento'=>11,

            'nombre'=>'UBAQUE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>578,
            'idDepartamento'=>11,

            'nombre'=>'UBATE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>579,
            'idDepartamento'=>11,

            'nombre'=>'UNE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>580,
            'idDepartamento'=>11,

            'nombre'=>'UTICA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>581,
            'idDepartamento'=>11,

            'nombre'=>'VERGARA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>582,
            'idDepartamento'=>11,

            'nombre'=>'VIANI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>583,
            'idDepartamento'=>11,

            'nombre'=>'VILLAGOMEZ'
            ] );
            
            
                        
            municipio::create( [
            'id'=>584,
            'idDepartamento'=>11,

            'nombre'=>'VILLAPINZON'
            ] );
            
            
                        
            municipio::create( [
            'id'=>585,
            'idDepartamento'=>11,

            'nombre'=>'VILLETA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>586,
            'idDepartamento'=>11,

            'nombre'=>'VIOTA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>587,
            'idDepartamento'=>11,

            'nombre'=>'YACOPI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>588,
            'idDepartamento'=>11,

            'nombre'=>'ZIPACON'
            ] );
            
            
                        
            municipio::create( [
            'id'=>589,
            'idDepartamento'=>11,

            'nombre'=>'ZIPAQUIRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>590,
         'idDepartamento'=>12,
            'nombre'=>'QUIBDO (SAN FRANCISCO DE QUIBDO)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>591,
         'idDepartamento'=>12,
            'nombre'=>'ACANDI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>592,
            'idDepartamento'=>12,
            'nombre'=>'ALTO BAUDO (PIE DE PATO)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>593,
            'idDepartamento'=>12,
            'nombre'=>'ATRATO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>594,
            'idDepartamento'=>12,
            'nombre'=>'BAGADO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>595,
            'idDepartamento'=>12,
            'nombre'=>'BAHIA SOLANO (MUTIS)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>596,
            'idDepartamento'=>12,
            'nombre'=>'BAJO BAUDO (PIZARRO)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>597,
            'idDepartamento'=>12,
            'nombre'=>'BOJAYA (BELLAVISTA)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>598,
            'idDepartamento'=>12,

            'nombre'=>'CANTON DE SAN PABLO (MANAGRU)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>599,
            'idDepartamento'=>12,

            'nombre'=>'CONDOTO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>600,
            'idDepartamento'=>12,

            'nombre'=>'EL CARMEN DE ATRATO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>601,
            'idDepartamento'=>12,

            'nombre'=>'LITORAL DEL BAJO SAN JUAN (SANTA GENOVEVA DE DOCORDO)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>602,
            'idDepartamento'=>12,

            'nombre'=>'ISTMINA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>603,
            'idDepartamento'=>12,

            'nombre'=>'JURADO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>604,
            'idDepartamento'=>12,

            'nombre'=>'LLORO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>605,
            'idDepartamento'=>12,

            'nombre'=>'MEDIO ATRATO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>606,
            'idDepartamento'=>12,

            'nombre'=>'MEDIO BAUDO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>607,
            'idDepartamento'=>12,

            'nombre'=>'NOVITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>608,
            'idDepartamento'=>12,

            'nombre'=>'NUQUI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>609,
            'idDepartamento'=>12,

            'nombre'=>'RIOQUITO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>610,
            'idDepartamento'=>12,

            'nombre'=>'RIOSUCIO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>611,
            'idDepartamento'=>12,

            'nombre'=>'SAN JOSE DEL PALMAR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>612,
            'idDepartamento'=>12,

            'nombre'=>'SIPI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>613,
            'idDepartamento'=>12,

            'nombre'=>'TADO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>614,
            'idDepartamento'=>12,

            'nombre'=>'UNGUIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>615,
            'idDepartamento'=>12,

            'nombre'=>'UNION PANAMERICANA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>616,
         'idDepartamento'=>13,
            'nombre'=>'NEIVA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>617,
         'idDepartamento'=>13,
            'nombre'=>'ACEVEDO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>618,
            'idDepartamento'=>13,
            'nombre'=>'AGRADO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>619,
            'idDepartamento'=>13,
            'nombre'=>'AIPE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>620,
            'idDepartamento'=>13,
            'nombre'=>'ALGECIRAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>621,
            'idDepartamento'=>13,
            'nombre'=>'ALTAMIRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>622,
            'idDepartamento'=>13,
            'nombre'=>'BARAYA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>623,
            'idDepartamento'=>13,

            'nombre'=>'CAMPOALEGRE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>624,
            'idDepartamento'=>13,

            'nombre'=>'COLOMBIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>625,
            'idDepartamento'=>13,

            'nombre'=>'ELIAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>626,
            'idDepartamento'=>13,

            'nombre'=>'GARZON'
            ] );
            
            
                        
            municipio::create( [
            'id'=>627,
            'idDepartamento'=>13,

            'nombre'=>'GIGANTE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>628,
            'idDepartamento'=>13,

            'nombre'=>'GUADALUPE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>629,
            'idDepartamento'=>13,

            'nombre'=>'HOBO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>630,
            'idDepartamento'=>13,

            'nombre'=>'IQUIRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>631,
            'idDepartamento'=>13,

            'nombre'=>'ISNOS (SAN JOSE DE ISNOS)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>632,
            'idDepartamento'=>13,

            'nombre'=>'LA ARGENTINA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>633,
            'idDepartamento'=>13,

            'nombre'=>'LA PLATA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>634,
            'idDepartamento'=>13,

            'nombre'=>'NATAGA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>635,
            'idDepartamento'=>13,

            'nombre'=>'OPORAPA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>636,
            'idDepartamento'=>13,

            'nombre'=>'PAICOL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>637,
            'idDepartamento'=>13,

            'nombre'=>'PALERMO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>638,
            'idDepartamento'=>13,

            'nombre'=>'PALESTINA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>639,
            'idDepartamento'=>13,

            'nombre'=>'PITAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>640,
            'idDepartamento'=>13,

            'nombre'=>'PITALITO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>641,
            'idDepartamento'=>13,

            'nombre'=>'RIVERA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>642,
            'idDepartamento'=>13,

            'nombre'=>'SALADOBLANCO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>643,
            'idDepartamento'=>13,

            'nombre'=>'SAN AGUSTIN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>644,
            'idDepartamento'=>13,

            'nombre'=>'SANTA MARIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>645,
            'idDepartamento'=>13,

            'nombre'=>'SUAZA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>646,
            'idDepartamento'=>13,

            'nombre'=>'TARQUI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>647,
            'idDepartamento'=>13,

            'nombre'=>'TESALIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>648,
            'idDepartamento'=>13,

            'nombre'=>'TELLO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>649,
            'idDepartamento'=>13,

            'nombre'=>'TERUEL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>650,
            'idDepartamento'=>13,

            'nombre'=>'TIMANA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>651,
            'idDepartamento'=>13,

            'nombre'=>'VILLAVIEJA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>652,
            'idDepartamento'=>13,

            'nombre'=>'YAGUARA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>653,
         'idDepartamento'=>14,
            'nombre'=>'RIOHACHA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>654,
            'idDepartamento'=>14,
            'nombre'=>'BARRANCAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>655,
            'idDepartamento'=>14,
            'nombre'=>'DIBULLA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>656,
            'idDepartamento'=>14,
            'nombre'=>'DISTRACCION'
            ] );
            
            
                        
            municipio::create( [
            'id'=>657,
            'idDepartamento'=>14,

            'nombre'=>'EL MOLINO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>658,
            'idDepartamento'=>14,

            'nombre'=>'FONSECA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>659,
            'idDepartamento'=>14,

            'nombre'=>'HATONUEVO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>660,
            'idDepartamento'=>14,

            'nombre'=>'LA JAGUA DEL PILAR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>661,
            'idDepartamento'=>14,

            'nombre'=>'MAICAO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>662,
            'idDepartamento'=>14,

            'nombre'=>'MANAURE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>663,
            'idDepartamento'=>14,

            'nombre'=>'SAN JUAN DEL CESAR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>664,
            'idDepartamento'=>14,

            'nombre'=>'URIBIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>665,
            'idDepartamento'=>14,

            'nombre'=>'URUMITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>666,
            'idDepartamento'=>14,

            'nombre'=>'VILLANUEVA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>667,
         'idDepartamento'=>15,
            'nombre'=>'SANTA MARTA (DISTRITO TURISTICO CULTURAL E HISTORICO DE SANTA MARTA)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>668,
            'idDepartamento'=>15,
            'nombre'=>'ALGARROBO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>669,
            'idDepartamento'=>15,
            'nombre'=>'ARACATACA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>670,
            'idDepartamento'=>15,
            'nombre'=>'ARIGUANI (EL DIFICIL)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>671,
            'idDepartamento'=>15,

            'nombre'=>'CERRO SAN ANTONIO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>672,
            'idDepartamento'=>15,

            'nombre'=>'CHIVOLO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>673,
            'idDepartamento'=>15,

            'nombre'=>'CIENAGA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>674,
            'idDepartamento'=>15,

            'nombre'=>'CONCORDIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>675,
            'idDepartamento'=>15,

            'nombre'=>'EL BANCO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>676,
            'idDepartamento'=>15,

            'nombre'=>'EL PIÑON'
            ] );
            
            
                        
            municipio::create( [
            'id'=>677,
            'idDepartamento'=>15,

            'nombre'=>'EL RETEN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>678,
            'idDepartamento'=>15,

            'nombre'=>'FUNDACION'
            ] );
            
            
                        
            municipio::create( [
            'id'=>679,
            'idDepartamento'=>15,

            'nombre'=>'GUAMAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>680,
            'idDepartamento'=>15,

            'nombre'=>'PEDRAZA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>681,
            'idDepartamento'=>15,

            'nombre'=>'PIJIÑO DEL CARMEN (PIJIÑO)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>682,
            'idDepartamento'=>15,

            'nombre'=>'PIVIJAY'
            ] );
            
            
                        
            municipio::create( [
            'id'=>683,
            'idDepartamento'=>15,

            'nombre'=>'PLATO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>684,
            'idDepartamento'=>15,

            'nombre'=>'PUEBLOVIEJO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>685,
            'idDepartamento'=>15,

            'nombre'=>'REMOLINO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>686,
            'idDepartamento'=>15,

            'nombre'=>'SABANAS DE SAN ANGEL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>687,
            'idDepartamento'=>15,

            'nombre'=>'SALAMINA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>688,
            'idDepartamento'=>15,

            'nombre'=>'SAN SEBASTIAN DE BUENAVISTA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>689,
            'idDepartamento'=>15,

            'nombre'=>'SAN ZENON'
            ] );
            
            
                        
            municipio::create( [
            'id'=>690,
            'idDepartamento'=>15,

            'nombre'=>'SANTA ANA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>691,
            'idDepartamento'=>15,

            'nombre'=>'SITIONUEVO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>692,
            'idDepartamento'=>15,

            'nombre'=>'TENERIFE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>693,
         'idDepartamento'=>16,
            'nombre'=>'VILLAVICENCIO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>694,
         'idDepartamento'=>16,
            'nombre'=>'ACACIAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>695,
            'idDepartamento'=>16,

            'nombre'=>'BARRANCA DE UPIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>696,
            'idDepartamento'=>16,

            'nombre'=>'CABUYARO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>697,
            'idDepartamento'=>16,

            'nombre'=>'CASTILLA LA NUEVA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>698,
            'idDepartamento'=>16,

            'nombre'=>'SAN LUIS DE CUBARRAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>699,
            'idDepartamento'=>16,

            'nombre'=>'CUMARAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>700,
            'idDepartamento'=>16,

            'nombre'=>'EL CALVARIO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>701,
            'idDepartamento'=>16,

            'nombre'=>'EL CASTILLO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>702,
            'idDepartamento'=>16,

            'nombre'=>'EL DORADO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>703,
            'idDepartamento'=>16,

            'nombre'=>'FUENTE DE ORO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>704,
            'idDepartamento'=>16,

            'nombre'=>'GRANADA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>705,
            'idDepartamento'=>16,

            'nombre'=>'GUAMAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>706,
            'idDepartamento'=>16,

            'nombre'=>'MAPIRIPAN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>707,
            'idDepartamento'=>16,

            'nombre'=>'MESETAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>708,
            'idDepartamento'=>16,

            'nombre'=>'LA MACARENA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>709,
            'idDepartamento'=>16,

            'nombre'=>'LA URIBE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>710,
            'idDepartamento'=>16,

            'nombre'=>'LEJANIAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>711,
            'idDepartamento'=>16,

            'nombre'=>'PUERTO CONCORDIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>712,
            'idDepartamento'=>16,

            'nombre'=>'PUERTO GAITAN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>713,
            'idDepartamento'=>16,

            'nombre'=>'PUERTO LOPEZ'
            ] );
            
            
                        
            municipio::create( [
            'id'=>714,
            'idDepartamento'=>16,

            'nombre'=>'PUERTO LLERAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>715,
            'idDepartamento'=>16,

            'nombre'=>'PUERTO RICO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>716,
            'idDepartamento'=>16,

            'nombre'=>'RESTREPO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>717,
            'idDepartamento'=>16,

            'nombre'=>'SAN CARLOS DE GUAROA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>718,
            'idDepartamento'=>16,

            'nombre'=>'SAN JUAN DE ARAMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>719,
            'idDepartamento'=>16,

            'nombre'=>'SAN JUANITO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>720,
            'idDepartamento'=>16,

            'nombre'=>'SAN MARTIN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>721,
            'idDepartamento'=>16,

            'nombre'=>'VISTAHERMOSA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>722,
         'idDepartamento'=>17,
            'nombre'=>'PASTO (SAN JUAN DE PASTO)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>723,
            'idDepartamento'=>17,
            'nombre'=>'ALBAN (SAN JOSE)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>724,
            'idDepartamento'=>17,
            'nombre'=>'ALDANA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>725,
            'idDepartamento'=>17,
            'nombre'=>'ANCUYA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>726,
            'idDepartamento'=>17,
            'nombre'=>'ARBOLEDA (BERRUECOS)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>727,
            'idDepartamento'=>17,
            'nombre'=>'BARBACOAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>728,
            'idDepartamento'=>17,
            'nombre'=>'BELEN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>729,
            'idDepartamento'=>17,

            'nombre'=>'BUESACO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>730,
            'idDepartamento'=>17,

            'nombre'=>'COLON (GENOVA)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>731,
            'idDepartamento'=>17,

            'nombre'=>'CONSACA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>732,
            'idDepartamento'=>17,

            'nombre'=>'CONTADERO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>733,
            'idDepartamento'=>17,

            'nombre'=>'CORDOBA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>734,
            'idDepartamento'=>17,

            'nombre'=>'CUASPUD (CARLOSAMA)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>735,
            'idDepartamento'=>17,

            'nombre'=>'CUMBAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>736,
            'idDepartamento'=>17,

            'nombre'=>'CUMBITARA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>737,
            'idDepartamento'=>17,

            'nombre'=>'CHACHAGUI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>738,
            'idDepartamento'=>17,

            'nombre'=>'EL CHARCO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>739,
            'idDepartamento'=>17,

            'nombre'=>'EL PEÑOL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>740,
            'idDepartamento'=>17,

            'nombre'=>'EL ROSARIO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>741,
            'idDepartamento'=>17,

            'nombre'=>'EL TABLON'
            ] );
            
            
                        
            municipio::create( [
            'id'=>742,
            'idDepartamento'=>17,

            'nombre'=>'EL TAMBO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>743,
            'idDepartamento'=>17,

            'nombre'=>'FUNES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>744,
            'idDepartamento'=>17,

            'nombre'=>'GUACHUCAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>745,
            'idDepartamento'=>17,

            'nombre'=>'GUAITARILLA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>746,
            'idDepartamento'=>17,

            'nombre'=>'GUALMATAN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>747,
            'idDepartamento'=>17,

            'nombre'=>'ILES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>748,
            'idDepartamento'=>17,

            'nombre'=>'IMUES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>749,
            'idDepartamento'=>17,

            'nombre'=>'IPIALES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>750,
            'idDepartamento'=>17,

            'nombre'=>'LA CRUZ'
            ] );
            
            
                        
            municipio::create( [
            'id'=>751,
            'idDepartamento'=>17,

            'nombre'=>'LA FLORIDA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>752,
            'idDepartamento'=>17,

            'nombre'=>'LA LLANADA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>753,
            'idDepartamento'=>17,

            'nombre'=>'LA TOLA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>754,
            'idDepartamento'=>17,

            'nombre'=>'LA UNION'
            ] );
            
            
                        
            municipio::create( [
            'id'=>755,
            'idDepartamento'=>17,

            'nombre'=>'LEIVA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>756,
            'idDepartamento'=>17,

            'nombre'=>'LINARES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>757,
            'idDepartamento'=>17,

            'nombre'=>'LOS ANDES (SOTOMAYOR)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>758,
            'idDepartamento'=>17,

            'nombre'=>'MAGUI (PAYAN)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>759,
            'idDepartamento'=>17,

            'nombre'=>'MALLAMA (PIEDRANCHA)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>760,
            'idDepartamento'=>17,

            'nombre'=>'MOSQUERA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>761,
            'idDepartamento'=>17,

            'nombre'=>'OLAYA HERRERA (BOCAS DE SATINGA)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>762,
            'idDepartamento'=>17,

            'nombre'=>'OSPINA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>763,
            'idDepartamento'=>17,

            'nombre'=>'FRANCISCO PIZARRO (SALAHONDA)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>764,
            'idDepartamento'=>17,

            'nombre'=>'POLICARPA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>765,
            'idDepartamento'=>17,

            'nombre'=>'POTOSI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>766,
            'idDepartamento'=>17,

            'nombre'=>'PROVIDENCIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>767,
            'idDepartamento'=>17,

            'nombre'=>'PUERRES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>768,
            'idDepartamento'=>17,

            'nombre'=>'PUPIALES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>769,
            'idDepartamento'=>17,

            'nombre'=>'RICAURTE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>770,
            'idDepartamento'=>17,

            'nombre'=>'ROBERTO PAYAN (SAN JOSE)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>771,
            'idDepartamento'=>17,

            'nombre'=>'SAMANIEGO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>772,
            'idDepartamento'=>17,

            'nombre'=>'SANDONA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>773,
            'idDepartamento'=>17,

            'nombre'=>'SAN BERNARDO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>774,
            'idDepartamento'=>17,

            'nombre'=>'SAN LORENZO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>775,
            'idDepartamento'=>17,

            'nombre'=>'SAN PABLO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>776,
            'idDepartamento'=>17,

            'nombre'=>'SAN PEDRO DE CARTAGO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>777,
            'idDepartamento'=>17,

            'nombre'=>'SANTA BARBARA (ISCUANDE)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>778,
            'idDepartamento'=>17,

            'nombre'=>'SANTA CRUZ (GUACHAVES)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>779,
            'idDepartamento'=>17,

            'nombre'=>'SAPUYES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>780,
            'idDepartamento'=>17,

            'nombre'=>'TAMINANGO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>781,
            'idDepartamento'=>17,

            'nombre'=>'TANGUA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>782,
            'idDepartamento'=>17,

            'nombre'=>'TUMACO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>783,
            'idDepartamento'=>17,

            'nombre'=>'TUQUERRES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>784,
            'idDepartamento'=>17,

            'nombre'=>'YACUANQUER'
            ] );
            
            
                        
            municipio::create( [
            'id'=>785,
         'idDepartamento'=>18,
            'nombre'=>'CUCUTA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>786,
         'idDepartamento'=>18,
            'nombre'=>'ABREGO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>787,
            'idDepartamento'=>18,
            'nombre'=>'ARBOLEDAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>788,
            'idDepartamento'=>18,
            'nombre'=>'BOCHALEMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>789,
            'idDepartamento'=>18,

            'nombre'=>'BUCARASICA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>790,
            'idDepartamento'=>18,

            'nombre'=>'CACOTA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>791,
            'idDepartamento'=>18,

            'nombre'=>'CACHIRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>792,
            'idDepartamento'=>18,

            'nombre'=>'CHINACOTA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>793,
            'idDepartamento'=>18,

            'nombre'=>'CHITAGA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>794,
            'idDepartamento'=>18,

            'nombre'=>'CONVENCION'
            ] );
            
            
                        
            municipio::create( [
            'id'=>795,
            'idDepartamento'=>18,

            'nombre'=>'CUCUTILLA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>796,
            'idDepartamento'=>18,

            'nombre'=>'DURANIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>797,
            'idDepartamento'=>18,

            'nombre'=>'EL CARMEN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>798,
            'idDepartamento'=>18,

            'nombre'=>'EL TARRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>799,
            'idDepartamento'=>18,

            'nombre'=>'EL ZULIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>800,
            'idDepartamento'=>18,

            'nombre'=>'GRAMALOTE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>801,
            'idDepartamento'=>18,

            'nombre'=>'HACARI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>802,
            'idDepartamento'=>18,

            'nombre'=>'HERRAN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>803,
            'idDepartamento'=>18,

            'nombre'=>'LABATECA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>804,
            'idDepartamento'=>18,

            'nombre'=>'LA ESPERANZA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>805,
            'idDepartamento'=>18,

            'nombre'=>'LA PLAYA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>806,
            'idDepartamento'=>18,

            'nombre'=>'LOS PATIOS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>807,
            'idDepartamento'=>18,

            'nombre'=>'LOURDES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>808,
            'idDepartamento'=>18,

            'nombre'=>'MUTISCUA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>809,
            'idDepartamento'=>18,

            'nombre'=>'OCAÑA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>810,
            'idDepartamento'=>18,

            'nombre'=>'PAMPLONA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>811,
            'idDepartamento'=>18,

            'nombre'=>'PAMPLONITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>812,
            'idDepartamento'=>18,

            'nombre'=>'PUERTO SANTANDER'
            ] );
            
            
                        
            municipio::create( [
            'id'=>813,
            'idDepartamento'=>18,

            'nombre'=>'RAGONVALIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>814,
            'idDepartamento'=>18,

            'nombre'=>'SALAZAR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>815,
            'idDepartamento'=>18,

            'nombre'=>'SAN CALIXTO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>816,
            'idDepartamento'=>18,

            'nombre'=>'SAN CAYETANO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>817,
            'idDepartamento'=>18,

            'nombre'=>'SANTIAGO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>818,
            'idDepartamento'=>18,

            'nombre'=>'SARDINATA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>819,
            'idDepartamento'=>18,

            'nombre'=>'SILOS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>820,
            'idDepartamento'=>18,

            'nombre'=>'TEORAMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>821,
            'idDepartamento'=>18,

            'nombre'=>'TIBU'
            ] );
            
            
                        
            municipio::create( [
            'id'=>822,
            'idDepartamento'=>18,

            'nombre'=>'TOLEDO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>823,
            'idDepartamento'=>18,

            'nombre'=>'VILLACARO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>824,
            'idDepartamento'=>18,

            'nombre'=>'VILLA DEL ROSARIO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>825,
         'idDepartamento'=>19,
            'nombre'=>'ARMENIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>826,
            'idDepartamento'=>19,

            'nombre'=>'BUENAVISTA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>827,
            'idDepartamento'=>19,

            'nombre'=>'CALARCA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>828,
            'idDepartamento'=>19,

            'nombre'=>'CIRCASIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>829,
            'idDepartamento'=>19,

            'nombre'=>'CORDOBA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>830,
            'idDepartamento'=>19,

            'nombre'=>'FILANDIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>831,
            'idDepartamento'=>19,

            'nombre'=>'GENOVA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>832,
            'idDepartamento'=>19,

            'nombre'=>'LA TEBAIDA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>833,
            'idDepartamento'=>19,

            'nombre'=>'MONTENEGRO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>834,
            'idDepartamento'=>19,

            'nombre'=>'PIJAO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>835,
            'idDepartamento'=>19,

            'nombre'=>'QUIMBAYA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>836,
            'idDepartamento'=>19,

            'nombre'=>'SALENTO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>837,
         'idDepartamento'=>20,
            'nombre'=>'PEREIRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>838,
            'idDepartamento'=>20,
            'nombre'=>'APIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>839,
            'idDepartamento'=>20,
            'nombre'=>'BALBOA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>840,
            'idDepartamento'=>20,
            'nombre'=>'BELEN DE UMBRIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>841,
            'idDepartamento'=>20,

            'nombre'=>'DOS QUEBRADAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>842,
            'idDepartamento'=>20,

            'nombre'=>'GUATICA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>843,
            'idDepartamento'=>20,

            'nombre'=>'LA CELIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>844,
            'idDepartamento'=>20,

            'nombre'=>'LA VIRGINIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>845,
            'idDepartamento'=>20,

            'nombre'=>'MARSELLA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>846,
            'idDepartamento'=>20,

            'nombre'=>'MISTRATO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>847,
            'idDepartamento'=>20,

            'nombre'=>'PUEBLO RICO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>848,
            'idDepartamento'=>20,

            'nombre'=>'QUINCHIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>849,
            'idDepartamento'=>20,

            'nombre'=>'SANTA ROSA DE CABAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>850,
            'idDepartamento'=>20,

            'nombre'=>'SANTUARIO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>851,
         'idDepartamento'=>21,
            'nombre'=>'BUCARAMANGA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>852,
            'idDepartamento'=>21,
            'nombre'=>'AGUADA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>853,
            'idDepartamento'=>21,
            'nombre'=>'ALBANIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>854,
            'idDepartamento'=>21,
            'nombre'=>'ARATOCA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>855,
            'idDepartamento'=>21,
            'nombre'=>'BARBOSA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>856,
            'idDepartamento'=>21,
            'nombre'=>'BARICHARA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>857,
            'idDepartamento'=>21,
            'nombre'=>'BARRANCABERMEJA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>858,
            'idDepartamento'=>21,
            'nombre'=>'BETULIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>859,
            'idDepartamento'=>21,

            'nombre'=>'BOLIVAR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>860,
            'idDepartamento'=>21,

            'nombre'=>'CABRERA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>861,
            'idDepartamento'=>21,

            'nombre'=>'CALIFORNIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>862,
            'idDepartamento'=>21,

            'nombre'=>'CAPITANEJO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>863,
            'idDepartamento'=>21,

            'nombre'=>'CARCASI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>864,
            'idDepartamento'=>21,

            'nombre'=>'CEPITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>865,
            'idDepartamento'=>21,

            'nombre'=>'CERRITO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>866,
            'idDepartamento'=>21,

            'nombre'=>'CHARALA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>867,
            'idDepartamento'=>21,

            'nombre'=>'CHARTA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>868,
            'idDepartamento'=>21,

            'nombre'=>'CHIMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>869,
            'idDepartamento'=>21,

            'nombre'=>'CHIPATA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>870,
            'idDepartamento'=>21,

            'nombre'=>'CIMITARRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>871,
            'idDepartamento'=>21,

            'nombre'=>'CONCEPCION'
            ] );
            
            
                        
            municipio::create( [
            'id'=>872,
            'idDepartamento'=>21,

            'nombre'=>'CONFINES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>873,
            'idDepartamento'=>21,

            'nombre'=>'CONTRATACION'
            ] );
            
            
                        
            municipio::create( [
            'id'=>874,
            'idDepartamento'=>21,

            'nombre'=>'COROMORO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>875,
            'idDepartamento'=>21,

            'nombre'=>'CURITI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>876,
            'idDepartamento'=>21,

            'nombre'=>'EL CARMEN DE CHUCURY'
            ] );
            
            
                        
            municipio::create( [
            'id'=>877,
            'idDepartamento'=>21,

            'nombre'=>'EL GUACAMAYO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>878,
            'idDepartamento'=>21,

            'nombre'=>'EL PEÑON'
            ] );
            
            
                        
            municipio::create( [
            'id'=>879,
            'idDepartamento'=>21,

            'nombre'=>'EL PLAYON'
            ] );
            
            
                        
            municipio::create( [
            'id'=>880,
            'idDepartamento'=>21,

            'nombre'=>'ENCINO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>881,
            'idDepartamento'=>21,

            'nombre'=>'ENCISO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>882,
            'idDepartamento'=>21,

            'nombre'=>'FLORIAN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>883,
            'idDepartamento'=>21,

            'nombre'=>'FLORIDABLANCA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>884,
            'idDepartamento'=>21,

            'nombre'=>'GALAN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>885,
            'idDepartamento'=>21,

            'nombre'=>'GAMBITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>886,
            'idDepartamento'=>21,

            'nombre'=>'GIRON'
            ] );
            
            
                        
            municipio::create( [
            'id'=>887,
            'idDepartamento'=>21,

            'nombre'=>'GUACA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>888,
            'idDepartamento'=>21,

            'nombre'=>'GUADALUPE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>889,
            'idDepartamento'=>21,

            'nombre'=>'GUAPOTA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>890,
            'idDepartamento'=>21,

            'nombre'=>'GUAVATA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>891,
            'idDepartamento'=>21,

            'nombre'=>'GUEPSA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>892,
            'idDepartamento'=>21,

            'nombre'=>'HATO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>893,
            'idDepartamento'=>21,

            'nombre'=>'JESUS MARIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>894,
            'idDepartamento'=>21,

            'nombre'=>'JORDAN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>895,
            'idDepartamento'=>21,

            'nombre'=>'LA BELLEZA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>896,
            'idDepartamento'=>21,

            'nombre'=>'LANDAZURI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>897,
            'idDepartamento'=>21,

            'nombre'=>'LA PAZ'
            ] );
            
            
                        
            municipio::create( [
            'id'=>898,
            'idDepartamento'=>21,

            'nombre'=>'LEBRIJA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>899,
            'idDepartamento'=>21,

            'nombre'=>'LOS SANTOS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>900,
            'idDepartamento'=>21,

            'nombre'=>'MACARAVITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>901,
            'idDepartamento'=>21,

            'nombre'=>'MALAGA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>902,
            'idDepartamento'=>21,

            'nombre'=>'MATANZA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>903,
            'idDepartamento'=>21,

            'nombre'=>'MOGOTES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>904,
            'idDepartamento'=>21,

            'nombre'=>'MOLAGAVITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>905,
            'idDepartamento'=>21,

            'nombre'=>'OCAMONTE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>906,
            'idDepartamento'=>21,

            'nombre'=>'OIBA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>907,
            'idDepartamento'=>21,

            'nombre'=>'ONZAGA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>908,
            'idDepartamento'=>21,

            'nombre'=>'PALMAR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>909,
            'idDepartamento'=>21,

            'nombre'=>'PALMAS DEL SOCORRO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>910,
            'idDepartamento'=>21,

            'nombre'=>'PARAMO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>911,
            'idDepartamento'=>21,

            'nombre'=>'PIEDECUESTA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>912,
            'idDepartamento'=>21,

            'nombre'=>'PINCHOTE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>913,
            'idDepartamento'=>21,

            'nombre'=>'PUENTE NACIONAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>914,
            'idDepartamento'=>21,

            'nombre'=>'PUERTO PARRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>915,
            'idDepartamento'=>21,

            'nombre'=>'PUERTO WILCHES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>916,
            'idDepartamento'=>21,

            'nombre'=>'RIONEGRO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>917,
            'idDepartamento'=>21,

            'nombre'=>'SABANA DE TORRES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>918,
            'idDepartamento'=>21,

            'nombre'=>'SAN ANDRES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>919,
            'idDepartamento'=>21,

            'nombre'=>'SAN BENITO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>920,
            'idDepartamento'=>21,

            'nombre'=>'SAN GIL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>921,
            'idDepartamento'=>21,

            'nombre'=>'SAN JOAQUIN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>922,
            'idDepartamento'=>21,

            'nombre'=>'SAN JOSE DE MIRANDA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>923,
            'idDepartamento'=>21,

            'nombre'=>'SAN MIGUEL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>924,
            'idDepartamento'=>21,

            'nombre'=>'SAN VICENTE DE CHUCURI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>925,
            'idDepartamento'=>21,

            'nombre'=>'SANTA BARBARA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>926,
            'idDepartamento'=>21,

            'nombre'=>'SANTA HELENA DEL OPON'
            ] );
            
            
                        
            municipio::create( [
            'id'=>927,
            'idDepartamento'=>21,

            'nombre'=>'SIMACOTA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>928,
            'idDepartamento'=>21,

            'nombre'=>'SOCORRO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>929,
            'idDepartamento'=>21,

            'nombre'=>'SUAITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>930,
            'idDepartamento'=>21,

            'nombre'=>'SUCRE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>931,
            'idDepartamento'=>21,

            'nombre'=>'SURATA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>932,
            'idDepartamento'=>21,

            'nombre'=>'TONA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>933,
            'idDepartamento'=>21,

            'nombre'=>'VALLE SAN JOSE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>934,
            'idDepartamento'=>21,

            'nombre'=>'VELEZ'
            ] );
            
            
                        
            municipio::create( [
            'id'=>935,
            'idDepartamento'=>21,

            'nombre'=>'VETAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>936,
            'idDepartamento'=>21,

            'nombre'=>'VILLANUEVA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>937,
            'idDepartamento'=>21,

            'nombre'=>'ZAPATOCA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>938,
         'idDepartamento'=>22,
            'nombre'=>'SINCELEJO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>939,
            'idDepartamento'=>22,

            'nombre'=>'BUENAVISTA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>940,
            'idDepartamento'=>22,

            'nombre'=>'CAIMITO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>941,
            'idDepartamento'=>22,

            'nombre'=>'COLOSO (RICAURTE)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>942,
            'idDepartamento'=>22,

            'nombre'=>'COROZAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>943,
            'idDepartamento'=>22,

            'nombre'=>'CHALAN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>944,
            'idDepartamento'=>22,

            'nombre'=>'GALERAS (NUEVA GRANADA)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>945,
            'idDepartamento'=>22,

            'nombre'=>'GUARANDA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>946,
            'idDepartamento'=>22,

            'nombre'=>'LA UNION'
            ] );
            
            
                        
            municipio::create( [
            'id'=>947,
            'idDepartamento'=>22,

            'nombre'=>'LOS PALMITOS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>948,
            'idDepartamento'=>22,

            'nombre'=>'MAJAGUAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>949,
            'idDepartamento'=>22,

            'nombre'=>'MORROA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>950,
            'idDepartamento'=>22,

            'nombre'=>'OVEJAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>951,
            'idDepartamento'=>22,

            'nombre'=>'PALMITO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>952,
            'idDepartamento'=>22,

            'nombre'=>'SAMPUES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>953,
            'idDepartamento'=>22,

            'nombre'=>'SAN BENITO ABAD'
            ] );
            
            
                        
            municipio::create( [
            'id'=>954,
            'idDepartamento'=>22,

            'nombre'=>'SAN JUAN DE BETULIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>955,
            'idDepartamento'=>22,

            'nombre'=>'SAN MARCOS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>956,
            'idDepartamento'=>22,

            'nombre'=>'SAN ONOFRE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>957,
            'idDepartamento'=>22,

            'nombre'=>'SAN PEDRO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>958,
            'idDepartamento'=>22,

            'nombre'=>'SINCE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>959,
            'idDepartamento'=>22,

            'nombre'=>'SUCRE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>960,
            'idDepartamento'=>22,

            'nombre'=>'TOLU'
            ] );
            
            
                        
            municipio::create( [
            'id'=>961,
            'idDepartamento'=>22,

            'nombre'=>'TOLUVIEJO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>962,
         'idDepartamento'=>23,
            'nombre'=>'IBAGUE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>963,
            'idDepartamento'=>23,
            'nombre'=>'ALPUJARRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>964,
            'idDepartamento'=>23,
            'nombre'=>'ALVARADO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>965,
            'idDepartamento'=>23,
            'nombre'=>'AMBALEMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>966,
            'idDepartamento'=>23,
            'nombre'=>'ANZOATEGUI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>967,
            'idDepartamento'=>23,
            'nombre'=>'ARMERO (GUAYABAL)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>968,
            'idDepartamento'=>23,
            'nombre'=>'ATACO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>969,
            'idDepartamento'=>23,

            'nombre'=>'CAJAMARCA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>970,
            'idDepartamento'=>23,

            'nombre'=>'CARMEN APICALA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>971,
            'idDepartamento'=>23,

            'nombre'=>'CASABIANCA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>972,
            'idDepartamento'=>23,

            'nombre'=>'CHAPARRAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>973,
            'idDepartamento'=>23,

            'nombre'=>'COELLO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>974,
            'idDepartamento'=>23,

            'nombre'=>'COYAIMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>975,
            'idDepartamento'=>23,

            'nombre'=>'CUNDAY'
            ] );
            
            
                        
            municipio::create( [
            'id'=>976,
            'idDepartamento'=>23,

            'nombre'=>'DOLORES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>977,
            'idDepartamento'=>23,

            'nombre'=>'ESPINAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>978,
            'idDepartamento'=>23,

            'nombre'=>'FALAN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>979,
            'idDepartamento'=>23,

            'nombre'=>'FLANDES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>980,
            'idDepartamento'=>23,

            'nombre'=>'FRESNO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>981,
            'idDepartamento'=>23,

            'nombre'=>'GUAMO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>982,
            'idDepartamento'=>23,

            'nombre'=>'HERVEO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>983,
            'idDepartamento'=>23,

            'nombre'=>'HONDA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>984,
            'idDepartamento'=>23,

            'nombre'=>'ICONONZO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>985,
            'idDepartamento'=>23,

            'nombre'=>'LERIDA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>986,
            'idDepartamento'=>23,

            'nombre'=>'LIBANO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>987,
            'idDepartamento'=>23,

            'nombre'=>'MARIQUITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>988,
            'idDepartamento'=>23,

            'nombre'=>'MELGAR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>989,
            'idDepartamento'=>23,

            'nombre'=>'MURILLO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>990,
            'idDepartamento'=>23,

            'nombre'=>'NATAGAIMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>991,
            'idDepartamento'=>23,

            'nombre'=>'ORTEGA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>992,
            'idDepartamento'=>23,

            'nombre'=>'PALOCABILDO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>993,
            'idDepartamento'=>23,

            'nombre'=>'PIEDRAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>994,
            'idDepartamento'=>23,

            'nombre'=>'PLANADAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>995,
            'idDepartamento'=>23,

            'nombre'=>'PRADO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>996,
            'idDepartamento'=>23,

            'nombre'=>'PURIFICACION'
            ] );
            
            
                        
            municipio::create( [
            'id'=>997,
            'idDepartamento'=>23,

            'nombre'=>'RIOBLANCO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>998,
            'idDepartamento'=>23,

            'nombre'=>'RONCESVALLES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>999,
            'idDepartamento'=>23,

            'nombre'=>'ROVIRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1000,
            'idDepartamento'=>23,

            'nombre'=>'SALDAÑA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1001,
            'idDepartamento'=>23,

            'nombre'=>'SAN ANTONIO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1002,
            'idDepartamento'=>23,

            'nombre'=>'SAN LUIS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1003,
            'idDepartamento'=>23,

            'nombre'=>'SANTA ISABEL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1004,
            'idDepartamento'=>23,

            'nombre'=>'SUAREZ'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1005,
            'idDepartamento'=>23,

            'nombre'=>'VALLE DE SAN JUAN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1006,
            'idDepartamento'=>23,

            'nombre'=>'VENADILLO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1007,
            'idDepartamento'=>23,

            'nombre'=>'VILLAHERMOSA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1008,
            'idDepartamento'=>23,

            'nombre'=>'VILLARRICA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1009,
         'idDepartamento'=>24,
            'nombre'=>'CALI (SANTIAGO DE CALI)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1010,
            'idDepartamento'=>24,
            'nombre'=>'ALCALA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1011,
            'idDepartamento'=>24,
            'nombre'=>'ANDALUCIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1012,
            'idDepartamento'=>24,
            'nombre'=>'ANSERMANUEVO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1013,
            'idDepartamento'=>24,
            'nombre'=>'ARGELIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1014,
            'idDepartamento'=>24,

            'nombre'=>'BOLIVAR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1015,
            'idDepartamento'=>24,

            'nombre'=>'BUENAVENTURA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1016,
            'idDepartamento'=>24,

            'nombre'=>'BUGA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1017,
            'idDepartamento'=>24,

            'nombre'=>'BUGALAGRANDE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1018,
            'idDepartamento'=>24,

            'nombre'=>'CAICEDONIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1019,
            'idDepartamento'=>24,

            'nombre'=>'CALIMA (DARIEN)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1020,
            'idDepartamento'=>24,

            'nombre'=>'CANDELARIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1021,
            'idDepartamento'=>24,

            'nombre'=>'CARTAGO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1022,
            'idDepartamento'=>24,

            'nombre'=>'DAGUA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1023,
            'idDepartamento'=>24,

            'nombre'=>'EL AGUILA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1024,
            'idDepartamento'=>24,

            'nombre'=>'EL CAIRO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1025,
            'idDepartamento'=>24,

            'nombre'=>'EL CERRITO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1026,
            'idDepartamento'=>24,

            'nombre'=>'EL DOVIO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1027,
            'idDepartamento'=>24,

            'nombre'=>'FLORIDA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1028,
            'idDepartamento'=>24,

            'nombre'=>'GINEBRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1029,
            'idDepartamento'=>24,

            'nombre'=>'GUACARI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1030,
            'idDepartamento'=>24,

            'nombre'=>'JAMUNDI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1031,
            'idDepartamento'=>24,

            'nombre'=>'LA CUMBRE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1032,
            'idDepartamento'=>24,

            'nombre'=>'LA UNION'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1033,
            'idDepartamento'=>24,

            'nombre'=>'LA VICTORIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1034,
            'idDepartamento'=>24,

            'nombre'=>'OBANDO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1035,
            'idDepartamento'=>24,

            'nombre'=>'PALMIRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1036,
            'idDepartamento'=>24,

            'nombre'=>'PRADERA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1037,
            'idDepartamento'=>24,

            'nombre'=>'RESTREPO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1038,
            'idDepartamento'=>24,

            'nombre'=>'RIOFRIO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1039,
            'idDepartamento'=>24,

            'nombre'=>'ROLDANILLO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1040,
            'idDepartamento'=>24,

            'nombre'=>'SAN PEDRO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1041,
            'idDepartamento'=>24,

            'nombre'=>'SEVILLA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1042,
            'idDepartamento'=>24,

            'nombre'=>'TORO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1043,
            'idDepartamento'=>24,

            'nombre'=>'TRUJILLO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1044,
            'idDepartamento'=>24,

            'nombre'=>'TULUA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1045,
            'idDepartamento'=>24,

            'nombre'=>'ULLOA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1046,
            'idDepartamento'=>24,

            'nombre'=>'VERSALLES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1047,
            'idDepartamento'=>24,

            'nombre'=>'VIJES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1048,
            'idDepartamento'=>24,

            'nombre'=>'YOTOCO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1049,
            'idDepartamento'=>24,

            'nombre'=>'YUMBO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1050,
            'idDepartamento'=>24,

            'nombre'=>'ZARZAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1051,
         'idDepartamento'=>25,
            'nombre'=>'ARAUCA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1052,
            'idDepartamento'=>25,
            'nombre'=>'ARAUQUITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1053,
            'idDepartamento'=>25,

            'nombre'=>'CRAVO NORTE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1054,
            'idDepartamento'=>25,

            'nombre'=>'FORTUL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1055,
            'idDepartamento'=>25,

            'nombre'=>'PUERTO RONDON'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1056,
            'idDepartamento'=>25,

            'nombre'=>'SARAVENA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1057,
            'idDepartamento'=>25,

            'nombre'=>'TAME'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1058,
         'idDepartamento'=>26,
            'nombre'=>'YOPAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1059,
            'idDepartamento'=>26,
            'nombre'=>'AGUAZUL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1060,
            'idDepartamento'=>26,
            'nombre'=>'CHAMEZA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1061,
            'idDepartamento'=>26,

            'nombre'=>'HATO COROZAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1062,
            'idDepartamento'=>26,

            'nombre'=>'LA SALINA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1063,
            'idDepartamento'=>26,

            'nombre'=>'MANI'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1064,
            'idDepartamento'=>26,

            'nombre'=>'MONTERREY'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1065,
            'idDepartamento'=>26,

            'nombre'=>'NUNCHIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1066,
            'idDepartamento'=>26,

            'nombre'=>'OROCUE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1067,
            'idDepartamento'=>26,

            'nombre'=>'PAZ DE ARIPORO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1068,
            'idDepartamento'=>26,

            'nombre'=>'PORE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1069,
            'idDepartamento'=>26,

            'nombre'=>'RECETOR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1070,
            'idDepartamento'=>26,

            'nombre'=>'SABANALARGA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1071,
            'idDepartamento'=>26,

            'nombre'=>'SACAMA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1072,
            'idDepartamento'=>26,

            'nombre'=>'SAN LUIS DE PALENQUE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1073,
            'idDepartamento'=>26,

            'nombre'=>'TAMARA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1074,
            'idDepartamento'=>26,

            'nombre'=>'TAURAMENA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1075,
            'idDepartamento'=>26,

            'nombre'=>'TRINIDAD'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1076,
            'idDepartamento'=>26,

            'nombre'=>'VILLANUEVA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1077,
         'idDepartamento'=>27,
            'nombre'=>'MOCOA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1078,
            'idDepartamento'=>27,

            'nombre'=>'COLON'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1079,
            'idDepartamento'=>27,

            'nombre'=>'ORITO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1080,
            'idDepartamento'=>27,

            'nombre'=>'PUERTO ASIS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1081,
            'idDepartamento'=>27,

            'nombre'=>'PUERTO CAICEDO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1082,
            'idDepartamento'=>27,

            'nombre'=>'PUERTO GUZMAN'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1083,
            'idDepartamento'=>27,

            'nombre'=>'PUERTO LEGUIZAMO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1084,
            'idDepartamento'=>27,

            'nombre'=>'SIBUNDOY'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1085,
            'idDepartamento'=>27,

            'nombre'=>'SAN FRANCISCO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1086,
            'idDepartamento'=>27,

            'nombre'=>'SAN MIGUEL (LA DORADA)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1087,
            'idDepartamento'=>27,

            'nombre'=>'SANTIAGO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1088,
            'idDepartamento'=>27,

            'nombre'=>'LA HORMIGA (VALLE DEL GUAMUEZ)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1089,
            'idDepartamento'=>27,

            'nombre'=>'VILLAGARZON'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1090,
         'idDepartamento'=>28,
            'nombre'=>'SAN ANDRES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1091,
            'idDepartamento'=>28,

            'nombre'=>'PROVIDENCIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1092,
         'idDepartamento'=>29,
            'nombre'=>'LETICIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1093,
            'idDepartamento'=>29,

            'nombre'=>'EL ENCANTO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1094,
            'idDepartamento'=>29,

            'nombre'=>'LA CHORRERA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1095,
            'idDepartamento'=>29,

            'nombre'=>'LA PEDRERA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1096,
            'idDepartamento'=>29,

            'nombre'=>'LA VICTORIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1097,
            'idDepartamento'=>29,

            'nombre'=>'MIRITI-PARANA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1098,
            'idDepartamento'=>29,

            'nombre'=>'PUERTO ALEGRIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1099,
            'idDepartamento'=>29,

            'nombre'=>'PUERTO ARICA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1100,
            'idDepartamento'=>29,

            'nombre'=>'PUERTO NARIÑO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1101,
            'idDepartamento'=>29,

            'nombre'=>'PUERTO SANTANDER'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1102,
            'idDepartamento'=>29,

            'nombre'=>'TARAPACA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1103,
         'idDepartamento'=>30,
            'nombre'=>'PUERTO INIRIDA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1104,
            'idDepartamento'=>30,

            'nombre'=>'BARRANCO MINAS'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1105,
            'idDepartamento'=>30,

            'nombre'=>'SAN FELIPE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1106,
            'idDepartamento'=>30,

            'nombre'=>'PUERTO COLOMBIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1107,
            'idDepartamento'=>30,

            'nombre'=>'LA GUADALUPE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1108,
            'idDepartamento'=>30,

            'nombre'=>'CACAHUAL'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1109,
            'idDepartamento'=>30,

            'nombre'=>'PANA PANA (CAMPO ALEGRE)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1110,
            'idDepartamento'=>30,

            'nombre'=>'MORICHAL (MORICHAL NUEVO)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1111,
         'idDepartamento'=>31,
            'nombre'=>'SAN JOSE DEL GUAVIARE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1112,
            'idDepartamento'=>31,
            'nombre'=>'CALAMAR'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1113,
            'idDepartamento'=>31,
            'nombre'=>'EL RETORNO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1114,
            'idDepartamento'=>31,

            'nombre'=>'MIRAFLORES'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1115,
         'idDepartamento'=>32,
            'nombre'=>'MITU'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1116,
            'idDepartamento'=>32,

            'nombre'=>'CARURU'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1117,
            'idDepartamento'=>32,

            'nombre'=>'PACOA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1118,
            'idDepartamento'=>32,

            'nombre'=>'TARAIRA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1119,
            'idDepartamento'=>32,

            'nombre'=>'PAPUNAUA (MORICHAL)'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1120,
            'idDepartamento'=>32,

            'nombre'=>'YAVARATE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1121,
         'idDepartamento'=>33,
            'nombre'=>'PUERTO CARREÑO'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1122,
            'idDepartamento'=>33,

            'nombre'=>'LA PRIMAVERA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1123,
            'idDepartamento'=>33,

            'nombre'=>'SANTA RITA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1124,
            'idDepartamento'=>33,

            'nombre'=>'SANTA ROSALIA'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1125,
            'idDepartamento'=>33,

            'nombre'=>'SAN JOSE DE OCUNE'
            ] );
            
            
                        
            municipio::create( [
            'id'=>1126,
            'idDepartamento'=>33,

            'nombre'=>'CUMARIBO'
            ] );
            
            
    }
}
