<?php

use Illuminate\Database\Seeder;
use App\models\tipoCompetencia;

class TipoCompetenciaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        tipoCompetencia::create([

        'nombre' => 'Técnica'

        ]);

        tipoCompetencia::create([

            'nombre' => 'Básica'

        ]);

        tipoCompetencia::create([

            'nombre' => 'Transversal'

        ]);

        tipoCompetencia::create([

            'nombre' => 'Complementaria'

        ]);

    }
}
