<?php

use Illuminate\Database\Seeder;
use App\models\estadoFomarcion;

class estadoFormacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        estadoFomarcion::create([

            'nombre' => 'Iniciada'

        ]);

        estadoFomarcion::create([

            'nombre' => 'Finalizada'

        ]);
    }
}
