<?php

use Illuminate\Database\Seeder;
use App\models\familia;

class FamiliaTableeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        familia::create([
            'nombre' => 'Bilingue'
        ]);
    }
}
