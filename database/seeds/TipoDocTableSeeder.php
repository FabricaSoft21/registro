<?php

use Illuminate\Database\Seeder;
use App\models\tipoDoc;

class TipoDocTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        tipoDoc::create([

            'nombre' => 'CC'
        ]);

        tipoDoc::create([

            'nombre' => 'RC'
        ]);

        tipoDoc::create([

            'nombre' => 'TI'
        ]);
    }
}
