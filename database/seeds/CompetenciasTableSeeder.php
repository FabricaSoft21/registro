<?php

use Illuminate\Database\Seeder;
use App\models\competencia;

class CompetenciasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        competencia::create([

            'nombre' => 'ANALIZAR LOS REQUISITOS DEL CLIENTE PARA CONSTRUIR EL SISTEMA DE INFORMACION.',
            'idTipoCompetencia' => 1

        ]);
        competencia::create([

            'nombre' => 'Aplicar buenas prácticas de calidad en el proceso de desarrollo de software de acuerdo con el referente adoptado por la empresa.',
            'idTipoCompetencia' => 2
        ]);

        competencia::create([

            'nombre' => 'Comprende textos en ingles de forma escrita y auditiva.',
            'idTipoCompetencia' => 3
        ]);
    }
}
