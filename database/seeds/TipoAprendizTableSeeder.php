<?php

use Illuminate\Database\Seeder;
use App\models\tipo_aprendiz;

class TipoAprendizTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        tipo_aprendiz::create([

            'nombre' => 'Titulada'

        ]);

        tipo_aprendiz::create([

            'nombre' => 'Complementaria'

        ]);

    }
}
