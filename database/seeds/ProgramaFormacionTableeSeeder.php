<?php

use Illuminate\Database\Seeder;
use App\models\programaFormacion;

class ProgramaFormacionTableeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        programaFormacion::create([

            'nombre' => 'Análisis y desarrollo de sistemas de información',
            'idNivel' => 1,
            'idFamilia' => 1,
            'codigo' => '45538',
            'version' => '3.0',
            'idTipo_formacion' => 1,
            
        ]);
    }
}
