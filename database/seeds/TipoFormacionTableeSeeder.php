<?php

use Illuminate\Database\Seeder;
use App\models\tipo_formacion;

class TipoFormacionTableeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        tipo_formacion::create([

            'nombre' => 'Virtualidad Titulada',
            'idClasificacion' => 1

        ]);

        tipo_formacion::create([

            'nombre' => 'Virtualidad Complementaria',
            'idClasificacion' => 2

        ]);

        tipo_formacion::create([

            'nombre' => 'Poblaciones Vulnerables',
            'idClasificacion' => 3

        ]);

        tipo_formacion::create([

            'nombre' => 'Competencias básicas',
            'idClasificacion' => 4

        ]);

        tipo_formacion::create([

            'nombre' => 'Media técnica',
            'idClasificacion' => 5

        ]);


    }
}
