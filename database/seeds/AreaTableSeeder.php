<?php

use Illuminate\Database\Seeder;
use App\models\area;

class AreaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        area::create([

            'codigo' => '1001',
            'nombre' => 'TITULADA VIRTUAL',
            'descripcion' => 'mamami'

        ]);

        area::create([

            'codigo' => '1002',
            'nombre' => 'COMPLEMENTARIA VIRTUAL',
            'descripcion' => 'mamami'

        ]);

        area::create([

            'codigo' => '1003',
            'nombre' => 'POBLACIÓN ESPECIAL',
            'descripcion' => 'mamami'

        ]);
        area::create([

            'codigo' => '1003',
            'nombre' => 'COMPETENCIAS BÁSICAS',
            'descripcion' => 'mamami'

        ]);
        area::create([

            'codigo' => '1003',
            'nombre' => 'MEDIA TECNICA',
            'descripcion' => 'mamami'

        ]);
    }
}
