<?php

use Illuminate\Database\Seeder;
use App\models\nivel;

class nivelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        nivel::create([

        'nombre' => "Técnico"
            
        ]);

        nivel::create([

            'nombre' => "Tecnólogo"
                
            ]);
    }
}
