<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::create(['name' => 'create user']);
        Permission::create(['name' => 'read user']);
        Permission::create(['name' => 'update user']);
        Permission::create(['name' => 'delete user']);

        // Permission::create(['name' => 'create rol']);
        // Permission::create(['name' => 'read rol']);
        // Permission::create(['name' => 'update rol']);
        // Permission::create(['name' => 'delete rol']);

        // Permission::create(['name' => 'create permission']);
        // Permission::create(['name' => 'read permission']);
        // Permission::create(['name' => 'update permission']);
        // Permission::create(['name' => 'delete permission']);

        // create roles and assign created permissions

        // this can be done as separate statements

        $role = Role::create(['name' => 'tecnica']);
        $role->givePermissionTo('read user');

        $role = Role::create(['name' => 'poblaciones']);
        $role->givePermissionTo('read user');

        $role = Role::create(['name' => 'administrador']);
        $role->givePermissionTo(Permission::all());

    }
}
