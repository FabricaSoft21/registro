<?php

use Illuminate\Database\Seeder;
use App\models\checkList;

class checkListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        checkList::create([

            'pregunta' => 'Diseño curricular del programa de formación.'

        ]);

        checkList::create([

            'pregunta' => 'Proyecto Formativo – Generado como PDF de Sofía Plus.	'

        ]);

        checkList::create([

            'pregunta' => 'Plan de trabajo concertado con el aprendiz para el desarrollo de la ruta de aprendizaje.'

        ]);

        checkList::create([

            'pregunta' => 'Guías de aprendizaje en formato vigente. Deben ser entregadas las guías de aprendizaje de todo el proce-so formativo.'

        ]);

        checkList::create([

            'pregunta' => 'Registros de asistencias e inasistencias de los apren-dices asociados a la ruta de aprendizaje. Tener en cuenta que las inasistencias deben estar registradas en Sofía Plus, lo cual se verifica con el reporte que comparte el apoyo administrativo de la coordinación académica.'

        ]);

        checkList::create([

            'pregunta' => 'Planeación Pedagógica – Generar Excel de Sofía Plus.'

        ]);

        checkList::create([

            'pregunta' => 'Reporte de juicios de evaluación del programa. Con fecha de reporte un día antes de la entrega del in-forme.'

        ]);

        checkList::create([

            'pregunta' => 'Pantallazo de evidencia donde se identifique que todos los aprendices se encuentran asociados a la ruta de aprendizaje.'

        ]);

        checkList::create([

            'pregunta' => 'Pantallazos de aprendices asociados a la ruta.'

        ]);

        checkList::create([

            'pregunta' => 'Documento en Word donde se relacione el listado de aprendices que presenten novedades tales como: llamado de atención verbal y/o escrito – Actividades complementarias pendientes (adjuntar el formato de actividad complementaria diligenciado y firmado) – Planes de mejoramiento (adjuntar el formato de plan de mejoramiento diligenciado y firmado)'

        ]);

        checkList::create([

            'pregunta' => 'Informe ejecutivo con el análisis cualitativo del grupo de formación y la IE.'

        ]);

        checkList::create([

            'pregunta' => 'En las fichas de grado undécimo ningún aprendiz debe quedar en estado FORMACIÓN con el objetivo de hacer el respectivo cierre de la ficha.'

        ]);

        checkList::create([

            'pregunta' => 'Entrega de materiales y herramientas utilizados para el desarrollo de la formación.'

        ]);

        checkList::create([

            'pregunta' => 'Informe y producto del semillero de investigación.'

        ]);
    }
}