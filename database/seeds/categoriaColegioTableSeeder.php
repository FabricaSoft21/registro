<?php

use Illuminate\Database\Seeder;
use App\models\categoria_colegio;

class categoriaColegioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        categoria_colegio::create([

            'nombre' => 'Técnica'

        ]);
        categoria_colegio::create([

            'nombre' => 'Académica'

        ]);

        
        
    }
}
