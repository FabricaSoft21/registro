<?php

use Illuminate\Database\Seeder;
use App\models\grado;

class GradoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        grado::create([

            'grado' => 'Décimo'

        ]);

        grado::create([

            'grado' => 'Once'

        ]);

    }
}
