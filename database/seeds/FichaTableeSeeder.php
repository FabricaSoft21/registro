<?php

use Illuminate\Database\Seeder;
use App\models\ficha;

class FichaTableeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ficha::create([

            'nombreFicha' => 'ADSI',
            'codigo' => '1597430',
            'idProgramaFormacion' => 1,
            'idTipoFicha' => 1
            
        ]);
    }
}
