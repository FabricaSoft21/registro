<?php

use Illuminate\Database\Seeder;
use App\models\departamento;

class departamentoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        departamento::create( [
            'id'=>1,
            'nombre'=>'Antioquia',
            ] );
            
            
                        
            departamento::create( [
            'id'=>2,
            'nombre'=>'Atlantico',
            ] );
            
            
                        
            departamento::create( [
            'id'=>3,
            'nombre'=>'D. C. Santa Fe de Bogotá',

            ] );
            
            
                        
            departamento::create( [
            'id'=>4,
            'nombre'=>'Bolivar',

            ] );
            
            
                        
            departamento::create( [
            'id'=>5,
            'nombre'=>'Boyaca',

            ] );
            
            
                        
            departamento::create( [
            'id'=>6,
            'nombre'=>'Caldas',

            ] );
            
            
                        
            departamento::create( [
            'id'=>7,
            'nombre'=>'Caqueta',

            ] );
            
            
                        
            departamento::create( [
            'id'=>8,
            'nombre'=>'Cauca',

            ] );
            
            
                        
            departamento::create( [
            'id'=>9,
            'nombre'=>'Cesar',

            ] );
            
            
                        
            departamento::create( [
            'id'=>10,
            'nombre'=>'Cordova',

            ] );
            
            
                        
            departamento::create( [
            'id'=>11,
            'nombre'=>'Cundinamarca',

            ] );
            
            
                        
            departamento::create( [
            'id'=>12,
            'nombre'=>'Choco',

            ] );
            
            
                        
            departamento::create( [
            'id'=>13,
            'nombre'=>'Huila',

            ] );
            
            
                        
            departamento::create( [
            'id'=>14,
            'nombre'=>'La Guajira',

            ] );
            
            
                        
            departamento::create( [
            'id'=>15,
            'nombre'=>'Magdalena',

            ] );
            
            
                        
            departamento::create( [
            'id'=>16,
            'nombre'=>'Meta',

            ] );
            
            
                        
            departamento::create( [
            'id'=>17,
            'nombre'=>'Nariño',

            ] );
            
            
                        
            departamento::create( [
            'id'=>18,
            'nombre'=>'Norte de Santander',

            ] );
            
            
                        
            departamento::create( [
            'id'=>19,
            'nombre'=>'Quindio',

            ] );
            
            
                        
            departamento::create( [
            'id'=>20,
            'nombre'=>'Risaralda',

            ] );
            
            
                        
            departamento::create( [
            'id'=>21,
            'nombre'=>'Santander',

            ] );
            
            
                        
            departamento::create( [
            'id'=>22,
            'nombre'=>'Sucre',

            ] );
            
            
                        
            departamento::create( [
            'id'=>23,
            'nombre'=>'Tolima',

            ] );
            
            
                        
            departamento::create( [
            'id'=>24,
            'nombre'=>'Valle',

            ] );
            
            
                        
            departamento::create( [
            'id'=>25,
            'nombre'=>'Arauca',

            ] );
            
            
                        
            departamento::create( [
            'id'=>26,
            'nombre'=>'Casanare',

            ] );
            
            
                        
            departamento::create( [
            'id'=>27,
            'nombre'=>'Putumayo',

            ] );
            
            
                        
            departamento::create( [
            'id'=>28,
            'nombre'=>'San Andres',

            ] );
            
            
                        
            departamento::create( [
            'id'=>29,
            'nombre'=>'Amazonas',

            ] );
            
            
                        
            departamento::create( [
            'id'=>30,
            'nombre'=>'Guainia',

            ] );
            
            
                        
            departamento::create( [
            'id'=>31,
            'nombre'=>'Guaviare',

            ] );
            
            
                        
            departamento::create( [
            'id'=>32,
            'nombre'=>'Vaupes',

            ] );
            
            
                        
            departamento::create( [
            'id'=>33,
            'nombre'=>'Vichada',

            ] );
                        
    }
}
