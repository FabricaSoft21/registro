<?php

use Illuminate\Database\Seeder;
use App\models\tipo_ficha;

class TipoFichaTableeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        tipo_ficha::create([
            'nombre' => 'Etapa lectiva'
        ]);

        tipo_ficha::create([
            'nombre' => 'Etapa productiva'
        ]);
    }
}
