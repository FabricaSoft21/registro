<?php

use Illuminate\Database\Seeder;
use App\models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;



class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('12345678'),
            'selected_category_id' => 1
        ]);

        $admin->assignRole('administrador');

        $tecnico = User::create([

            'name' => 'Técnica',
            'email' => 'tec@gmail.com',
            'password' => bcrypt('12345678'),
            'selected_category_id' => 5

        ]);

        $tecnico->assignRole('tecnica');


        $poblacion = User::create([

            'name' => 'Población',
            'email' => 'pv@gmail.com',
            'password' => bcrypt('12345678'),
            'selected_category_id' => 3

        ]);

        $poblacion->assignRole('poblaciones');

    }
}
