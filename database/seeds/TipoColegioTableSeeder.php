<?php

use Illuminate\Database\Seeder;
use App\models\tipo_colegio;

class TipoColegioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
            tipo_colegio::create([
    
                'nombre' => 'Público'
            ]);
    
            tipo_colegio::create([
    
                'nombre' => 'Privado'
            ]);
    
    }
}
