<?php

use Illuminate\Database\Seeder;
use App\models\tipoDiscapacidad;

class TipoDiscTableeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        tipoDiscapacidad::create([

        'nombre' => 'Miopía',
        'descripcion' => 'Afección en la que los objetos cercanos se ven claramente, pero no así los que están lejos.',
        ]);

        tipoDiscapacidad::create([

            'nombre' => 'Hola',
            'descripcion' => 'Afección en la que los objetos cercanos se ven claramente, pero no así los que están lejos.',
            ]);

            tipoDiscapacidad::create([

                'nombre' => 'Dos',
                'descripcion' => 'Afección en la que los objetos cercanos se ven claramente, pero no así los que están lejos.',
                ]);
    }
}
