<?php

use App\models\tipoInstructor;
use Illuminate\Database\Seeder;

class tipoInstructorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        tipoInstructor::create([

            'nombre'=>'Líder'

        ]);
        tipoInstructor::create([

            'nombre'=>'Técnico'
        ]);

        
    }
}
