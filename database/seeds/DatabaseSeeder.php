<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesAndPermissionTableSeeder::class);
        $this->call(ClasificacionTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ContratoTableSeeder::class);
        $this->call(ProfesionTableSeeder::class);
        $this->call(TipoColegioTableSeeder::class);
        $this->call(categoriaColegioTableSeeder::class);
        $this->call(estadoAprendizTableSeeder::class);
        $this->call(departamentoTableSeeder::class);
        $this->call(municipioTableSeeder::class);
        $this->call(AreaTableSeeder::class);
        $this->call(GradoTableSeeder::class);
        $this->call(TipoDocTableSeeder::class);
        $this->call(TipoDiscTableeSeeder::class);
        $this->call(TipoFichaTableeSeeder::class);
        $this->call(nivelTableSeeder::class);
        $this->call(FamiliaTableeSeeder::class);
        $this->call(TipoFormacionTableeSeeder::class);
        $this->call(TipoPoblacionTableSeeder::class);
        $this->call(TipoCompetenciaTableSeeder::class);
        $this->call(CompetenciasTableSeeder::class);
        $this->call(ProgramaFormacionTableeSeeder::class);
        $this->call(estadoFormacionTableSeeder::class);
        $this->call(FichaTableeSeeder::class);
        $this->call(checkListTableSeeder::class);
        $this->call(diaTableSeeder::class);
        $this->call(tipoInstructorTableSeeder::class);
    }
}
