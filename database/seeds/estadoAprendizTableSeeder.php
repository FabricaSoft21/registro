<?php

use Illuminate\Database\Seeder;
use App\models\estadoAprendiz;

class estadoAprendizTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        estadoAprendiz::create([
            'nombre' => 'Sin formación'
        ]);

        estadoAprendiz::create([
            'nombre' => 'En formación'
        ]);

        estadoAprendiz::create([
            'nombre' => 'Cancelado'
        ]);

        estadoAprendiz::create([
            'nombre' => 'Retirado'
        ]);
        
        estadoAprendiz::create([
            'nombre' => 'Finalizado'
        ]);
        
    }
}
