<?php

use Illuminate\Database\Seeder;
use App\models\tipo_poblacion;

class TipoPoblacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        tipo_poblacion::create([

            'nombre' => 'INPEC',
      
        ]);
        tipo_poblacion::create([

            'nombre' => 'Desplazados por fénomenos naturales',
      
        ]);
        tipo_poblacion::create([

            'nombre' => 'Discapacitados',
      
        ]);
        tipo_poblacion::create([

            'nombre' => 'Jóvenes vulnerables',
      
        ]);
        tipo_poblacion::create([

            'nombre' => 'Adolescentes con conflicto con la ley penal',
      
        ]);
        tipo_poblacion::create([

            'nombre' => 'Mujer cabeza de hogar',
      
        ]);
        tipo_poblacion::create([

            'nombre' => 'Negritudes(Negros)',
      
        ]);

        tipo_poblacion::create([

            'nombre' => 'Afrocolombianos',
      
        ]);

        tipo_poblacion::create([

            'nombre' => 'Procesos de reintregración y adolescentes desvinculados de grupos Armados Organizados al mergen de la ley',
      
        ]);
        tipo_poblacion::create([

            'nombre' => 'Tercera Edad',
      
        ]);
        tipo_poblacion::create([

            'nombre' => 'Adolescente Trabajor',
      
        ]);

        tipo_poblacion::create([

            'nombre' => 'Remitidos por el PAL',
      
        ]);
        tipo_poblacion::create([

            'nombre' => 'Soldados campesinos',
      
        ]);

        tipo_poblacion::create([

            'nombre' => 'Sobrevivientes Minas Antipersonales',
      
        ]);

        tipo_poblacion::create([

            'nombre' => 'DESPLAZADOS POR LA VIOLENCIA -RED UNIDOS',
      
        ]);


    }
}
