<?php

use Illuminate\Database\Seeder;
use App\models\dias;

class diaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    
            dias::create([
                'nombre' => 'Lunes'
            ]);
    
            dias::create([
                'nombre' => 'Martes'
            ]);
    
            dias::create([
                'nombre' => 'Miercoles'
            ]);
            
            dias::create([
                'nombre' => 'Jueves'
            ]);

            dias::create([
                'nombre' => 'Viernes'
            ]);
            dias::create([
                'nombre' => 'Sabado'
            ]);
            dias::create([
                'nombre' => 'Domingo'
            ]);
        }
}
