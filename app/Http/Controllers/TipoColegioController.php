<?php

namespace App\Http\Controllers;

use App\tipo_colegio;
use Illuminate\Http\Request;

class TipoColegioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tipo_colegio  $tipo_colegio
     * @return \Illuminate\Http\Response
     */
    public function show(tipo_colegio $tipo_colegio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tipo_colegio  $tipo_colegio
     * @return \Illuminate\Http\Response
     */
    public function edit(tipo_colegio $tipo_colegio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tipo_colegio  $tipo_colegio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tipo_colegio $tipo_colegio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tipo_colegio  $tipo_colegio
     * @return \Illuminate\Http\Response
     */
    public function destroy(tipo_colegio $tipo_colegio)
    {
        //
    }
}
