<?php

namespace App\Http\Controllers;

use App\models\Colegio;
use App\models\rector;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \Validator;

class RectorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function save(Request $request)
    {

        $rules = [
            'nombre' => 'required|string',
            'apellido' => 'required|string',
            'telefono' => 'required|integer',
            'correo' => 'required|email|string|unique:rector,correo',
        ];

        $error = Validator::make($request->all(), $rules);
        $last = rector::all()->last();

        if ($error->fails()) {}

        $form_data = array(
            'id' => $last['id'] + 1,
            'nombre' => $request->nombre,
            'apellido' => $request->apellido,
            'telefono' => $request->telefono,
            'correo' => $request->correo,
        );

        rector::create($form_data);
        $data = rector::where('id', $last['id'] + 1)->get();

        return response()->json(['success' => 'Rector agregado con éxito.', 'data' => $data]);
    }

    public function import(Request $request)
    {

        $rules = [
            'file-selected' => 'required|mimes:xls,xlsx',
        ];

        $message = array(
            'message' => 'Importación de datos realizada con éxito',
            'alert-type' => 'success',
        );

        $this->validate($request, $rules, $message);

        //
        $path = $request->file('file-selected')->getRealPath();

        $data = Excel::load($path)->get();

        // $area = null;

        // Se imprime la data para obtener los encabezados("heading") del Excel y su respectiva información ("items")
        // dd($data);

        if ($data->count() > 0) {
            foreach ($data as $key => $value) {

                // dd($value);
                $last = rector::all()->last();
                // $doc = tipoDoc::select('tipo_doc.id')
                // ->where('tipo_doc.nombre',$value['tipo_documento'])
                // ->first();

                // $area = area::select('area.id')
                // ->where('area.nombre',$value['area'])
                // ->first();

                // $unique[] = array (strval($value['numero_identificacion']));

                $insert_data = array(

                    
                    'nombre' => $value['nombre'],
                    'apellido' => $value['apellido'],
                    'correo' => $value['correo'],
                    'telefono' => $value['telefono'],
                    "created_at" => date('Y-m-d h:i:s'),
                    "updated_at" => null,
                );

                // Se intenta agregar a la base de datos dentro de una estructura try-catch,
                // para excepcionar el error de duplicidad de datos y agregar solo los que no están
                try {
                    DB::table('rector')->insert($insert_data);
                } catch (\Throwable $th) {
                    // dd($th);
                }
            }

            if (empty($insert_data)) {
                $message = array(
                    'message' => 'Error al importar datos',
                    'alert-type' => 'error',
                );
            }
        }

        return redirect('/rector')->with($message);

        $message = array(
            'message' => 'Error al importar datos',
            'alert-type' => 'error',
        );
        return redirect('/rector')->with($message);
    }

    public function index()
    {{
        $rectores = Rector::withTrashed()->get();
        $cant = rector::withTrashed()
        ->count();
        return view('rectores.index')->with(compact('rectores', 'cant'));
    }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       

        return view('rectores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        {
            $rules = [
                'nombre' => 'required|string',
                'apellido' => 'required|string',
                'correo' => 'required|email|string|unique:rector,correo',
                'telefono' => 'required|integer',
            ];

            $message = array(
                'message' => 'Rector registrado con éxito',
                'alert-type' => 'success',
            );

            $this->validate($request, $rules, $message);

            Rector::create($request->all());

            return redirect('/rector')->with($message);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\rector  $rector
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $colegio = Colegio::where('idRector', $id)->get();

        return response()->json($colegio);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\rector  $rector
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $rectores = Rector::findOrFail($id);

        return view('rectores.edit', compact('rectores'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\rector  $rector
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rectores = Rector::findOrFail($id);
        $rules = [
            'nombre' => 'required|string',
            'apellido' => 'required|string',
            'correo' => 'required|email|string|unique:rector,correo,' . $id,
            'telefono' => 'required|integer',

        ];

        $message = array(
            'message' => 'Rector actualizado con éxito',
            'alert-type' => 'success',
        );

        $this->validate($request, $rules, $message);

        $rectores->update($request->all());

        return redirect('/rector')->with($message);
    }

    public function destroy($id)
    {
        rector::destroy($id);

        $message = array(
            'message' => 'Rector cambiado de estado con éxito',
            'alert-type' => 'success',
        );

        return redirect('/rector')->with($message);
    }

    public function restore($id)
    {

        rector::withTrashed()->find($id)->restore();

        $message = array(
            'message' => 'Rector cambiado de estado con éxito',
            'alert-type' => 'success',
        );

        return back()->with($message);

    }
}
