<?php

namespace App\Http\Controllers;

use App\models\EmpresaHasPoblacion;
use Illuminate\Http\Request;

class EmpresaHasPoblacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\EmpresaHasPoblacion  $empresaHasPoblacion
     * @return \Illuminate\Http\Response
     */
    public function show(EmpresaHasPoblacion $empresaHasPoblacion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\EmpresaHasPoblacion  $empresaHasPoblacion
     * @return \Illuminate\Http\Response
     */
    public function edit(EmpresaHasPoblacion $empresaHasPoblacion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\EmpresaHasPoblacion  $empresaHasPoblacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmpresaHasPoblacion $empresaHasPoblacion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\EmpresaHasPoblacion  $empresaHasPoblacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmpresaHasPoblacion $empresaHasPoblacion)
    {
        //
    }
}
