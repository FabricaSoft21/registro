<?php

namespace App\Http\Controllers;

use App\models\selected_categorie;
use Illuminate\Http\Request;

class SelectedCategorieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\selected_categorie  $selected_categorie
     * @return \Illuminate\Http\Response
     */
    public function show(selected_categorie $selected_categorie)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\selected_categorie  $selected_categorie
     * @return \Illuminate\Http\Response
     */
    public function edit(selected_categorie $selected_categorie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\selected_categorie  $selected_categorie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, selected_categorie $selected_categorie)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\selected_categorie  $selected_categorie
     * @return \Illuminate\Http\Response
     */
    public function destroy(selected_categorie $selected_categorie)
    {
        //
    }
}
