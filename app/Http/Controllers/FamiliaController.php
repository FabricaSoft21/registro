<?php

namespace App\Http\Controllers;

use App\models\familia;
use Illuminate\Http\Request;
use \Validator;

class FamiliaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function save(Request $request)
    {

        $rules = [
            'nombre' => 'required|string|unique:familia,nombre',

        ];

        $error = Validator::make($request->all(), $rules);
        $last = familia::all()->last();

        if ($error->fails()) {

            return response()->json(['errors' => $error->errors()
            ->all()]);
        }

        $form_data = array(
            'id' => $last['id'] + 1,
            'nombre' =>  $request->nombre,
        );

        familia::create($form_data);
        $data = familia::where('id', $last['id'] + 1)->get();

        return response()->json(['success' => 'Familia agregada con éxito.', 'data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Responses
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\familia  $familia
     * @return \Illuminate\Http\Response
     */
    public function show(familia $familia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\familia  $familia
     * @return \Illuminate\Http\Response
     */
    public function edit(familia $familia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\familia  $familia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, familia $familia)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\familia  $familia
     * @return \Illuminate\Http\Response
     */
    public function destroy(familia $familia)
    {
        //
    }
}
