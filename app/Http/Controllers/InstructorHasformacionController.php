<?php

namespace App\Http\Controllers;

use App\models\instructorHasformacion;
use Illuminate\Http\Request;
use App\models\AprendizHasProgramas;
use App\models\instructor;

class InstructorHasformacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ids = $request->idI;
        $formacion = (int) $request->input('idFormacion');

        $aprendices =  AprendizHasProgramas::where('idFormacion', $formacion)->get();

        if (!$ids == null) {

            $cant = count($ids);
        } else {

            $message = array(
                'message' => 'Debe seleccionar al menos un instructor',
                'alert-type' => 'error',
            );

            return redirect('/instructor')->with($message);
        }

        $cont = instructorHasformacion::distinct('idInstructor')
            ->join('aprendiz_has_programas', 'instructor_hasformacions.idAprendizHasPrograma', 'aprendiz_has_programas.id')
            ->where('aprendiz_has_programas.idFormacion', $formacion)
            ->where('instructor_hasformacions.idTipoInstructor', 1)
            ->count('idInstructor');

        if (count($aprendices) == 0) {
            $message = array(
                'message' => 'No hay aprendices en la formación',
                'alert-type' => 'error',
            );

            return redirect('/instructor')->with($message);
        } else {

            if ($cont == 1 && $request->input('tipoInstructor') == 1) {
                $message = array(
                    'message' => 'La formación ya tiene un lider',
                    'alert-type' => 'error',
                );

                return redirect('/instructor')->with($message);
            } else {

                foreach ($ids as $i) {

                    foreach ($aprendices as $aprendiz) {

                        instructorHasformacion::updateOrCreate(

                            [
                                "idInstructor" => $i,
                                "idAprendizHasPrograma" => $aprendiz->id,
                                "idTipoInstructor" => $request->input('tipoInstructor')

                            ],
                            [
                                "idInstructor" => $i,
                                "idAprendizHasPrograma" => $aprendiz->id,
                                "idTipoInstructor" => $request->input('tipoInstructor')

                            ]
                        );
                    }
                }
            }
        }

        $message = array(
            'message' => 'Instructores asociados a la ficha' . ' con éxito',
            'alert-type' => 'success',
        );

        return redirect('/formacion')->with($message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\instructorHasformacion  $instructorHasformacion
     * @return \Illuminate\Http\Response
     */
    public function show(instructorHasformacion $instructorHasformacion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\instructorHasformacion  $instructorHasformacion
     * @return \Illuminate\Http\Response
     */
    public function edit(instructorHasformacion $instructorHasformacion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\instructorHasformacion  $instructorHasformacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, instructorHasformacion $instructorHasformacion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\instructorHasformacion  $instructorHasformacion
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {

        $id = $request->input('idFormacion');

        $selected = $request->input('idInstructor', []);

        instructorHasformacion::whereIn('idInstructor', $selected)
            ->join("aprendiz_has_programas", "instructor_hasformacions.idAprendizHasPrograma", "aprendiz_has_programas.id")
            ->where('aprendiz_has_programas.idFormacion', $id)
            ->delete();

        $message = array(
            'message' => 'Instructores removidos con éxito',
            'alert-type' => 'success'
        );

        return back()->with($message);
    }
}
