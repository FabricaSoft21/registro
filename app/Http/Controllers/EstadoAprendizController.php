<?php

namespace App\Http\Controllers;

use App\models\estadoAprendiz;
use Illuminate\Http\Request;

class EstadoAprendizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\estadoAprendiz  $estadoAprendiz
     * @return \Illuminate\Http\Response
     */
    public function show(estadoAprendiz $estadoAprendiz)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\estadoAprendiz  $estadoAprendiz
     * @return \Illuminate\Http\Response
     */
    public function edit(estadoAprendiz $estadoAprendiz)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\estadoAprendiz  $estadoAprendiz
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, estadoAprendiz $estadoAprendiz)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\estadoAprendiz  $estadoAprendiz
     * @return \Illuminate\Http\Response
     */
    public function destroy(estadoAprendiz $estadoAprendiz)
    {
        //
    }
}
