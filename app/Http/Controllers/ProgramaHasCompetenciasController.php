<?php

namespace App\Http\Controllers;

use App\models\Programa_has_competencias;
use Illuminate\Http\Request;
use App\models\competencia;

class ProgramaHasCompetenciasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $com =  $request ->id;

        $cont = count($com);

        for ($i=0; $i < $cont; $i++) {

            Programa_has_competencias::create(
                ['idCompetencia' => $com[$i],'idPrograma' => $request->input('idPrograma')],
                [
                "idCompetencia" => $com[$i],
                "idPrograma" => $request->input('idPrograma')
            ]);
        }

        $message = array(
            'message' => 'Competencias asociadas con éxito',
            'alert-type' => 'success'
        );

        return back()->with($message);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Programa_has_competencias  $programa_has_competencias
     * @return \Illuminate\Http\Response
     */
    public function show(Programa_has_competencias $programa_has_competencias)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Programa_has_competencias  $programa_has_competencias
     * @return \Illuminate\Http\Response
     */
    public function edit(Programa_has_competencias $programa_has_competencias)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Programa_has_competencias  $programa_has_competencias
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Programa_has_competencias $programa_has_competencias)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Programa_has_competencias  $programa_has_competencias
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $id = $request->input('idPrograma');

        $selected = $request->input('idCompetencias', []);

        Programa_has_competencias::whereIn('idCompetencia',$selected)
        ->where('idPrograma',$id)
        ->delete();

        $message = array(
            'message' => 'Competencias removidas con éxito',
            'alert-type' => 'success'
        );

        return back()->with($message);

    }
    
}
