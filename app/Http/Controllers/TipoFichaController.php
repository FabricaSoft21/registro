<?php

namespace App\Http\Controllers;

use App\models\tipo_ficha;
use Illuminate\Http\Request;

class TipoFichaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\tipo_ficha  $tipo_ficha
     * @return \Illuminate\Http\Response
     */
    public function show(tipo_ficha $tipo_ficha)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\tipo_ficha  $tipo_ficha
     * @return \Illuminate\Http\Response
     */
    public function edit(tipo_ficha $tipo_ficha)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\tipo_ficha  $tipo_ficha
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tipo_ficha $tipo_ficha)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\tipo_ficha  $tipo_ficha
     * @return \Illuminate\Http\Response
     */
    public function destroy(tipo_ficha $tipo_ficha)
    {
        //
    }
}
