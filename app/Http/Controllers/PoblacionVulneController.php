<?php

namespace App\Http\Controllers;

use App\models\poblacionVulne;
use Illuminate\Http\Request;
use \Validator;
use App\models\Aprendiz;
use App\models\aprendizHasProgramas;
use Nexmo\Client\Response\Response;

class PoblacionVulneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'metaAprendiz' => 'required|string',
            'metaCupos' => 'required|string',
            // ' metaAprendiz' => 'required|string',
        ];

        $error = Validator::make($request->all(),$rules);
        $ejecAp = AprendizHasProgramas::distinct()
        ->join('aprendiz','aprendiz_has_programas.idAprendiz','aprendiz.id')
        ->where('tipoAprendiz',3)
        ->count();

        $ejecCupo = AprendizHasProgramas::distinct()
        ->join("aprendiz", "aprendiz_has_programas.idAprendiz", "aprendiz.id")
        ->join('estado_aprendiz', 'aprendiz.idestado', 'estado_aprendiz.id')
        ->join("formacion", "aprendiz_has_programas.idFormacion", "formacion.id")
        ->join("ficha", "formacion.idFicha", "ficha.id")
        ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
        ->where('programa_formacion.idTipo_formacion', 2)
        ->where('aprendiz.tipoAprendiz',3)
        ->Orwhere('programa_formacion.idTipo_formacion', 4)
        ->count();

        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()
            ->all()]);
        }

        $form_data = array(
            
            'metaAprendiz' => $request->metaAprendiz,
            'metaCupos' => $request->metaCupos,
            'ejecAprendiz' => $ejecAp,
            'ejecCupos' => $ejecCupo
        );



        poblacionVulne::create($form_data);

        $data = poblacionVulne::all();

        return response()->json(['success' => 'Metas agregadas con éxito.','data' => $data]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\poblacionVulne  $poblacionVulne
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $data = poblacionVulne::all();
        $last = poblacionVulne::all()->last();

        $ejecAp =AprendizHasProgramas::distinct()
        ->join('aprendiz','aprendiz_has_programas.idAprendiz','aprendiz.id')
        ->where('aprendiz.tipoAprendiz',3)
        ->count();

        $ejecCupo = AprendizHasProgramas::distinct()
        ->join("aprendiz", "aprendiz_has_programas.idAprendiz", "aprendiz.id")
        ->join('estado_aprendiz', 'aprendiz.idestado', 'estado_aprendiz.id')
        ->join("formacion", "aprendiz_has_programas.idFormacion", "formacion.id")
        ->join("ficha", "formacion.idFicha", "ficha.id")
        ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
        ->where('programa_formacion.idTipo_formacion', 2)
        ->where('aprendiz.tipoAprendiz',3)
        ->Orwhere('programa_formacion.idTipo_formacion', 4)
        ->count();

        poblacionVulne::where('id',$last['id'])->update([

            'ejecAprendiz' => $ejecAp,
            'ejecCupos' => $ejecCupo

        ]);

        return response()->json(['percentageA' => $ejecAp,'data' => $data]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\poblacionVulne  $poblacionVulne
     * @return \Illuminate\Http\Response
     */
    public function edit(poblacionVulne $poblacionVulne)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\poblacionVulne  $poblacionVulne
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, poblacionVulne $poblacionVulne)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\poblacionVulne  $poblacionVulne
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $id = poblacionVulne::all()->last();

        $pobla = poblacionVulne::find($id["id"]);

        $pobla->delete();

        $message = array (
            "message" => "Eliminado con éxito",
            "alert-type" =>"success"
        );
        return back()->with($message);

    }
}
