<?php

namespace App\Http\Controllers;

use App\models\programacion_has_dias;
use Illuminate\Http\Request;

class ProgramacionHasDiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\programacion_has_dias  $programacion_has_dias
     * @return \Illuminate\Http\Response
     */
    public function show(programacion_has_dias $programacion_has_dias)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\programacion_has_dias  $programacion_has_dias
     * @return \Illuminate\Http\Response
     */
    public function edit(programacion_has_dias $programacion_has_dias)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\programacion_has_dias  $programacion_has_dias
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, programacion_has_dias $programacion_has_dias)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\programacion_has_dias  $programacion_has_dias
     * @return \Illuminate\Http\Response
     */
    public function destroy(programacion_has_dias $programacion_has_dias)
    {
        //
    }
}
