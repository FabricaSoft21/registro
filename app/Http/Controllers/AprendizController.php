<?php

namespace App\Http\Controllers;

use App\models\Aprendiz;
use App\models\aprendizHasDiscapacidades;
use App\models\Colegio;
use App\models\grado;
use App\models\ficha;
use App\models\tipoDiscapacidad;
use App\models\tipoDoc;
use App\models\Clasificacion;
use App\models\estadoAprendiz;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Excel;
use PDF;
use App\models\Formacion;
use App\models\aprendizHasProgramas;

class AprendizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function import(Request $request)
    {

        $rules = [
            'file-selected' => 'required|mimes:xls,xlsx',
        ];

        $message = array(
            'message' => 'Importación de datos realizada con éxito',
            'alert-type' => 'success',
        );

        $this->validate($request, $rules, $message);

        $path = $request->file('file-selected')->getRealPath();

        $data = Excel::load($path)->get();

        // try {
        if ($data->count() > 0) {

            foreach ($data as $key => $value) {

                $doc = tipoDoc::select('tipo_doc.id')
                    ->where('tipo_doc.nombre', $value['tipo_documento'])
                    ->first();


                $colegio = Colegio::select('colegio.id')
                    ->where('colegio.nombreColegio', $value['colegio'])
                    ->first();

                $grado = grado::select('grado.id')
                    ->where('grado.grado', $value['grado'])
                    ->first();

                if ($doc == null) {

                    $message = array(
                        'message' => 'Tipo Documento, Institución Educativa o grado, no encontrados',
                        'alert-type' => 'error',
                    );
                    return redirect('/aprendiz')->with($message);
                } elseif ($colegio == null && auth()->user()->selected_category_id == 5) {

                    $message = array(
                        'message' =>  'Institución Educativa  no encontrada',
                        'alert-type' => 'error',
                    );
                    return redirect('/aprendiz')->with($message);
                } elseif ($grado ==  null && auth()->user()->selected_category_id == 5) {

                    $message = array(
                        'message' =>  'Grado no encontrado',
                        'alert-type' => 'error',
                    );
                }


                $insert_data = array(

                    'nombre' => $value['nombre'],
                    'apellido' => $value['apellido'],
                    'fechaNacimiento' => $value['fecha_de_nacimiento'],
                    'idTipoDoc' => $doc["id"],
                    'numDoc' => $value['numero_identificacion'],
                    'correo' => $value['correo'],
                    'telefono' => $value['telefono'],
                    'direccion' => $value['direccion'],
                    'nombreAcudiente' => $value['nombre_del_acudiente'],
                    'telAcudiente' => $value['telefono_acudiente'],
                    'idColegio' => $colegio["id"],
                    'tipoAprendiz' => auth()->user()->selected_category_id,
                    'idGrado' => $grado["id"],
                    "created_at" =>  \Carbon\Carbon::now(),
                    "updated_at" => null

                );

                try {
                    DB::table('aprendiz')->insert($insert_data);
                } catch (\Throwable $th) {
                    //throw $th;
                }
            }

            if (empty($insert_data)) {

                $message = array(
                    'message' => 'Error al importar datos',
                    'alert-type' => 'error',
                );
            }
        }

        return redirect('/aprendiz')->with($message);
        // } catch (\Throwable $th) {
        $message = array(
            'message' => 'Error al importar datos',
            'alert-type' => 'error',
        );
        return redirect('/aprendiz')->with($message);
        // }
    }

    public function index()
    {
        $disc = tipoDiscapacidad::all();
        $cant = Aprendiz::count();
        $aprendices = Aprendiz::all();

        $FTitulada = Formacion::select('ficha.*', 'formacion.*')
            ->join('ficha', 'formacion.idFicha', 'ficha.id')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->where('idTipo_formacion', 1)
            ->where('formacion.idEstado', 1)
            ->get();

        $FComplementaria = Formacion::select('ficha.*', 'formacion.*')
            ->join('ficha', 'formacion.idFicha', 'ficha.id')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->where('idTipo_formacion', 2)
            ->where('formacion.idEstado', 1)

            ->get();

        $FPoblacion = Formacion::select('ficha.*', 'formacion.*')
            ->join('ficha', 'formacion.idFicha', 'ficha.id')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->where('idTipo_formacion', 3)
            ->where('formacion.idEstado', 1)
            ->get();

        $FCompetencia = Formacion::select('ficha.*', 'formacion.*')
            ->join('ficha', 'formacion.idFicha', 'ficha.id')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->where('idTipo_formacion', 4)
            ->where('formacion.idEstado', 1)

            ->get();

        $FMedia = Formacion::select('ficha.*', 'formacion.*')
            ->join('ficha', 'formacion.idFicha', 'ficha.id')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->where('idTipo_formacion', 5)
            ->where('formacion.idEstado', 1)
            ->get();

        $cursos = Formacion::select('ficha.*', 'formacion.*')
            ->join('ficha', 'formacion.idFicha', 'ficha.id')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->where('idTipo_formacion', 2)
            ->Orwhere('idTipo_formacion', 4)->get();

        $titulada = aprendiz::where('aprendiz.tipoAprendiz', 1)
            ->get();

        $complementaria = aprendiz::where('aprendiz.tipoAprendiz', 2)->get();

        $poblacion = aprendiz::where('aprendiz.tipoAprendiz', 3)->get();

        $competencia = aprendiz::where('aprendiz.tipoAprendiz', 4)->get();

        $media = aprendiz::where('aprendiz.tipoAprendiz', 5)->get();

        return view('aprendices.index')->with(compact('aprendices', 'cursos', 'cant', 'disc', 'titulada', 'complementaria', 'poblacion', 'competencia', 'media', 'FTitulada', 'FComplementaria', 'FPoblacion', 'FCompetencia', 'FMedia'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    //Aprendices pueden estar en titulada y también en complementaria
    public function create()
    {
        $docs = tipoDoc::all();
        $categoria = Clasificacion::all();
        $disc = tipoDiscapacidad::all();
        $grado = grado::all();
        $colegio = Colegio::all();
        $last = Aprendiz::all()->last();

        return view('aprendices.create')->with(compact('docs', 'disc', 'grado', 'colegio', 'last', 'categoria'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (auth()->user()->selected_category_id == 5) {
            $rules = [
                'nombre' => 'required|string|max:50',
                'apellido' => 'required|string|max:50',
                'fechaNacimiento' => 'required|date',
                'numDoc' => 'required|string|max:15|unique:aprendiz,numDoc',
                'idTipoDoc' => 'required|integer',
                'idestado' => 'integer',
                'correo' => 'required|string|unique:aprendiz,correo|max:50',
                'telefono' => 'required|string|max:15',
                'direccion' => 'required|string|max:50',
                'nombreAcudiente' => 'required|string|max:50',
                'telAcudiente' => 'required|string|max:15',
                'idColegio' => 'required|integer',
                'idGrado' => 'required|integer',
                'tipoAprendiz' => 'required|exists:clasificacion,id',

            ];
        } else {

            $rules = [
                'nombre' => 'required|string|max:50',
                'apellido' => 'required|string|max:50',
                'fechaNacimiento' => 'required|date',
                'numDoc' => 'required|string|max:15|unique:aprendiz,numDoc',
                'idTipoDoc' => 'required|integer',
                'idestado' => 'integer',
                'correo' => 'required|string|unique:aprendiz,correo|max:50',
                'telefono' => 'required|string|max:15',
                'direccion' => 'required|string|max:50',
                'nombreAcudiente' => 'nullable|string|max:50',
                'telAcudiente' => 'nullable|string|max:15',
                'idColegio' => 'nullable|string',
                'idGrado' => 'nullable|string',
                'tipoAprendiz' => 'required|exists:clasificacion,id',

            ];
        }

        $message = array(
            'message' => 'Aprendiz registrado con éxito',
            'alert-type' => 'success',
        );
        $this->validate($request, $rules, $message);
        Aprendiz::create($request->all());

        $serv = $request->input('idDiscapacidad', []);
        $cant = count($serv);

        for ($i = 0; $i < $cant; $i++) {

            aprendizHasDiscapacidades::create([
                "idAprendiz" => $request->idAprendiz,
                "idDiscapacidad" => $serv[$i],
            ]);
        }

        return redirect('/aprendiz')->with($message);
    }

    public function show($id)
    {

        $discapacidades = aprendizHasDiscapacidades::select("aprendiz_has_discapacidades.*", "tipo_discapacidad.nombre as nombreDiscapacidad")
            ->join("tipo_discapacidad", "aprendiz_has_discapacidades.idDiscapacidad", "tipo_discapacidad.id")
            ->where('aprendiz_has_discapacidades.idAprendiz', $id)
            ->get();

        $ficha = aprendizHasProgramas::select('ficha.*')
            ->join('formacion', 'aprendiz_has_programas.idFormacion', 'formacion.id')
            ->join('ficha', 'formacion.idFicha', 'ficha.id')
            ->where('aprendiz_has_programas.idAprendiz', $id)->first();

        $data[] = "";
        if ($discapacidades != "") {
            foreach ($discapacidades as $key => $value) {

                $data[] = $value->nombreDiscapacidad;
            }
        }

        $disc = tipoDiscapacidad::select('tipo_discapacidad.*')
            ->whereIn('nombre', $data)->get();
        return response()->json(["disc" => $disc, "fic" => $ficha]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Aprendiz  $aprendiz
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $docs = tipoDoc::all();
        $disc = tipoDiscapacidad::all();
        $countDisc = tipoDiscapacidad::count();
        $grado = grado::all();
        $colegio = Colegio::all();
        $last = Aprendiz::all()->last();
        $estado = estadoAprendiz::all();
        $aprendiz = Aprendiz::findOrFail($id);

        $discapacidades = aprendizHasDiscapacidades::select("aprendiz_has_discapacidades.*", "tipo_discapacidad.nombre as nombreDiscapacidad")
            ->join("tipo_discapacidad", "aprendiz_has_discapacidades.idDiscapacidad", "tipo_discapacidad.id")
            ->where('aprendiz_has_discapacidades.idAprendiz', $id)
            ->get();

        $data[] = "";
        if ($discapacidades != "") {
            foreach ($discapacidades as $key => $value) {

                $data[] = $value->nombreDiscapacidad;
            }
        }

        return view('aprendices.edit')->with(compact('disc', 'docs', 'grado', 'aprendiz', 'colegio', 'data', 'countDisc', 'estado'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Aprendiz  $aprendiz
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Aprendiz  $aprendiz
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $aprendiz = Aprendiz::findOrFail($id);
        $selected = $request->input('idDiscapacidad', []);
        aprendizHasDiscapacidades::whereNotIn('idDiscapacidad', $selected)
            ->where('idAprendiz', $id)
            ->delete();

        $more = $request->Discapacidades;


        if (auth()->user()->selected_category_id == 5) {
            $rules = [
                'nombre' => 'required|string|max:50',
                'apellido' => 'required|string|max:50',
                'fechaNacimiento' => 'required|date',
                'numDoc' => 'required|string|max:15|unique:aprendiz,numDoc,' . $id,
                'idTipoDoc' => 'required|integer',
                'idestado' => 'integer',
                'correo' => 'required|string|max:50|unique:aprendiz,correo,' . $id,
                'telefono' => 'required|string|max:15',
                'direccion' => 'required|string|max:50',
                'nombreAcudiente' => 'required|string|max:50',
                'telAcudiente' => 'required|string|max:15',
                'idColegio' => 'required|integer',
                'idGrado' => 'required|integer',
                'tipoAprendiz' => '',

            ];
        } else {

            $rules = [
                'nombre' => 'required|string|max:50',
                'apellido' => 'required|string|max:50',
                'fechaNacimiento' => 'required|date',
                'numDoc' => 'required|string|max:15|unique:aprendiz,numDoc,' . $id,
                'idTipoDoc' => 'required|integer',
                'idestado' => 'integer',
                'correo' => 'required|string|max:50|unique:aprendiz,correo,' . $id,
                'telefono' => 'required|string|max:15',
                'direccion' => 'required|string|max:50',
                'nombreAcudiente' => 'nullable|string|max:50',
                'telAcudiente' => 'nullable|string|max:15',
                'idColegio' => 'nullable|string',
                'idGrado' => 'nullable|string',
                'tipoAprendiz' => '',

            ];
        }


        $message = array(
            'message' => 'Aprendiz modificado con éxito',
            'alert-type' => 'success',
        );

        $this->validate($request, $rules, $message);

        if ($more != null) {

            for ($i = 0; $i < count($more); $i++) {

                aprendizHasDiscapacidades::create([
                    "idAprendiz" => $id,
                    "idDiscapacidad" => $more[$i],
                ]);
            }
        }

        $aprendiz->update($request->all());

        return redirect('/aprendiz')->with($message);
    }
    public function report()
    {

        ini_set('max_execution_time', 300);

        $aprendizF = DB::table("aprendiz_has_programas")
            ->join("aprendiz", "aprendiz_has_programas.idAprendiz", "=", "aprendiz.id")
            ->join('tipo_doc', 'aprendiz.idTipoDoc', 'tipo_doc.id')
            ->join("formacion", "aprendiz_has_programas.idFormacion", "=", "formacion.id")
            ->join("ficha", "formacion.idFicha", "=", "ficha.id")
            ->join("programa_formacion", "ficha.idProgramaFormacion", "=", "programa_formacion.id")
            ->select("aprendiz_has_programas.*", "aprendiz.*", "formacion.*", "ficha.*", "programa_formacion.nombre as programa", "tipo_doc.nombre as tipoDoc")
            ->where("aprendiz.idestado", 2)
            ->get();


        $temp_name = "InformaAprendiz_" . $this->random_string() . ".pdf";
        $number = $this->random_string();
        return PDF::loadView("reportes/reportAprendiz", compact(['aprendizF', 'number']))->stream($temp_name);
    }

    protected function random_string()
    {
        $key = '';
        $keys = array_merge(range(0, 9));

        for ($i = 0; $i < 5; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Aprendiz  $aprendiz
     * @return \Illuminate\Http\Response
     */
    public function destroy(Aprendiz $aprendiz)
    {
        //
    }
}
