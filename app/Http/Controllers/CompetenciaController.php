<?php

namespace App\Http\Controllers;

use App\models\competencia;
use App\models\instructorHasCompetencias;
use App\models\Programa_has_competencias;
use Illuminate\Http\Request;
use \Validator;
use Auth;
use App\models\tipoCompetencia;

class CompetenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cant = competencia::count();

        return view('competencias.index')->with(compact('cant'));
    }

    public function getList(){

        $competencias = null;

        if(Auth::user()->selected_category_id == 1){

            $competencias = competencia::select('competencia.*')
            ->where('idTipoCompetencia',1)
            ->orWhere('idTipoCompetencia',3)
            ->get();

        }elseif(Auth::user()->selected_category_id == 2){

            $competencias = competencia::select('competencia.*')
            ->where('idTipoCompetencia',2)
            ->get();

        }elseif(Auth::user()->selected_category_id == 3){

            $competencias = competencia::select('competencia.*')
            ->where('idTipoCompetencia',2)
            ->get();

        }elseif(Auth::user()->selected_category_id == 4){

            $competencias = competencia::select('competencia.*')
            ->where('idTipoCompetencia',2)
            ->get();

        }elseif(Auth::user()->selected_category_id == 5){

            $compe = [1,2,3];
            $competencias = competencia::select('competencia.*')
            ->whereIn('idTipoCompetencia',$compe)
            ->get();
        }

        return response()->json(["data" => $competencias]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = [
            'nombre' => 'required|string',
            'idTipoCompetencia' => 'required'
        ];

        $error = Validator::make($request->all(),$rules);
        $last = competencia::all()->last();


        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()
            ->all()]);
        }

        $form_data = array(
            'id' => $last['id'] + 1,
            'nombre' =>  $request->nombre,
            'idTipoCompetencia' =>$request->idTipoCompetencia
        );

        competencia::create($form_data);
        $data = competencia::all();

        return response()->json(['success' => 'Competencia agregado con éxito.','data' => $data]);

    }




    /**
     * Display the specified resource.
     *
     * @param  \App\models\competencia  $competencia
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $competencias = Programa_has_competencias::select("programa_has_competencias.*", "competencia.*")
        ->join("competencia", "programa_has_competencias.idCompetencia", "competencia.id")
        ->where('programa_has_competencias.idPrograma', $id)
        ->get();

        return response()->json(["data" => $competencias]);

    }

        public function showI($id)
        {

            $competencias = instructorHasCompetencias::select("instructor_has_competencias.*", "competencia.*")
            ->join("competencia", "instructor_has_competencias.idCompetencia", "competencia.id")
            ->where('instructor_has_competencias.idInstructor', $id)
            ->get();

            return response()->json(["data" => $competencias]);
        }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\competencia  $competencia
     * @return \Illuminate\Http\Response
     */
    public function edit(competencia $competencia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\competencia  $competencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, competencia $competencia)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\competencia  $competencia
     * @return \Illuminate\Http\Response
     */
    public function destroy(competencia $competencia)
    {
        //
    }
}
