<?php

namespace App\Http\Controllers;

use App\models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware(['permission:create user'], ['only' => ['create', 'store', 'index']]);
        $this->middleware(['permission:read user'], ['only' => 'show']);
        // $this->middleware(['permission:update user'], ['only' => ['edit', 'update']]);
        $this->middleware(['permission:delete user'], ['only' => 'delete', 'restore']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::withTrashed()
            ->where('id', '!=', auth()->id())->get();
        return view('users.index')->with(compact('user'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all()->pluck('name', 'id');
        return view('users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:100',
            'email' => 'required|string|max:100',
            'password' => 'required|string|min:8',
            'password_confirmation' => 'required|same:password',
            'rol' => 'required|string',
        ];

        $usuario = new User;

        $usuario->name = $request->name;
        $usuario->email = $request->email;
        $usuario->password = bcrypt($request->password);

        $message = array(
            'message' => 'Usuario creado con éxito',
            'alert-type' => 'success',
        );

        $this->validate($request, $rules, $message);

        //$datosUsuario =  request()->except(['_token','confirm_password']);

        if ($usuario->save()) {

            $usuario->assignRole($request->rol);

        }

        return redirect('/user')->with($message);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\cr  $cr
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\cr  $cr
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::all()->pluck('name', 'id');
        $usuario = User::findOrFail($id);
        return view('users.edit', compact('usuario', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\cr  $cr
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuario = User::findOrFail($id);
        $usuario->name = $request->name;
        $usuario->email = $request->email;
        $usuario->password = bcrypt($request->password);

        $rules = [
            'name' => 'required|string|max:100',
            'email' => 'required|string|max:100',
            'rol' => 'required|string',
        ];

        if ($request->password == null) {
            $message = array(
                'message' => 'Usuario modificado con éxito',
                'alert-type' => 'success',
            );
            $this->validate($request, $rules, $message);
            $usuario->syncRoles([$request->rol]);
            //$usuario->password = $request->password;
            $datosUsuario = request()->except(['password', '_token', '_method', 'password_confirmation', 'rol']);
            User::where('id', $id)->update($datosUsuario);

        } else if ($request->password != null) {

            $message = array(
                'message' => 'Usuario modificado con éxito',
                'alert-type' => 'success',
            );

            $this->validate($request, $rules, $message);
            $usuario->syncRoles([$request->rol]);
            $usuario->save();
        }

        if (!(auth()->user()->hasRole('administrador'))) {
            return back()->with($message);
        } else {
            return redirect('/user')->with($message);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\cr  $cr
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $usuario = User::findOrFail($id);
        // $usuario->removeRole($usuario->roles->implode('name', ', '));
        User::destroy($id);

        $message = array(
            'message' => 'Usuario cambiado de estado con éxito',
            'alert-type' => 'success',
        );

        return redirect('/user')->with($message);
    }

    public function restore($id)
    {

        User::withTrashed()->find($id)->restore();

        $message = array(
            'message' => 'Usuario cambiado de estado con éxito',
            'alert-type' => 'success',
        );

        return back()->with($message);

    }

}
