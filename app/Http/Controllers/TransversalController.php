<?php

namespace App\Http\Controllers;

use App\models\transversak;
use Illuminate\Http\Request;

class TransversalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\transversak  $transversak
     * @return \Illuminate\Http\Response
     */
    public function show(transversak $transversak)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\transversak  $transversak
     * @return \Illuminate\Http\Response
     */
    public function edit(transversak $transversak)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\transversak  $transversak
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, transversak $transversak)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\transversak  $transversak
     * @return \Illuminate\Http\Response
     */
    public function destroy(transversak $transversak)
    {
        //
    }
}
