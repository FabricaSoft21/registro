<?php

namespace App\Http\Controllers;

use App\models\tipo_poblacion;
use Illuminate\Http\Request;

class TipoPoblacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\tipo_poblacion  $tipo_poblacion
     * @return \Illuminate\Http\Response
     */
    public function show(tipo_poblacion $tipo_poblacion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\tipo_poblacion  $tipo_poblacion
     * @return \Illuminate\Http\Response
     */
    public function edit(tipo_poblacion $tipo_poblacion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\tipo_poblacion  $tipo_poblacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tipo_poblacion $tipo_poblacion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\tipo_poblacion  $tipo_poblacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(tipo_poblacion $tipo_poblacion)
    {
        //
    }
}
