<?php

namespace App\Http\Controllers;
use App\models\Colegio;
use App\models\programaFormacion;
use App\models\docente;
use Illuminate\Http\Request;
use Nexmo\Message\Message;
use App\models\Formacion;
use Excel;
use Illuminate\Support\Facades\DB;

class DocenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function import(Request $request)
    {

        $rules = [
            'file-selected' => 'required|mimes:xls,xlsx',
        ];

        $message = array(
            'message' => 'Importación de datos realizada con éxito',
            'alert-type' => 'success',
        );

        $this->validate($request, $rules, $message);

        $path = $request->file('file-selected')->getRealPath();

        $data = Excel::load($path)->get();

        // try {
        if ($data->count() > 0) {

            foreach ($data as $key => $value) {

                $idColegio = colegio::select('colegio.id')
                    ->where('colegio.nombreColegio', $value['institucion_educativa'])
                    ->first();

                $idPrograma = programaFormacion::select('programa_formacion.id')
                    ->where('programa_formacion.nombre', $value['programa_formacion'])
                    ->first();

                    if($idColegio == null){
                        
                        $message = array(
                            'message' => 'Institución Educativa no encontrada',
                            'alert-type' => 'error'
                        );
                        return redirect('/docente')->with($message);

                    }elseif($idPrograma==null){
                        $message = array(
                            'message' => 'Programa de formación no encontrado',
                            'alert-type' => 'error'
                        );
                        return redirect('/docente')->with($message);
                    }

                $last = docente::all()->last();

                $insert_data = array(

                    'id' => $last['id'] + 1,
                    'nombre' => $value['nombre'],
                    'apellido' => $value['apellido'],
                    'correo' => $value['correo'],
                    'idcolegio' => $idColegio['id'],
                    'idprograma' => $idPrograma['id'],
                    'telefono' => $value['telefono'],
                    "created_at" => date('Y-m-d h:i:s'),
                    "updated_at" => null
                );

                try {
                    DB::table('docente')->insert($insert_data);
                } catch (\Throwable $th) {
                    // dd($th);
                }
            }

            if (empty($insert_data)) {
                $message = array(
                    'message' => 'Error al importar datos',
                    'alert-type' => 'error'
                );
            }
        }

        return redirect('/docente')->with($message);

        $message = array(
            'message' => 'Error al importar datos',
            'alert-type' => 'error'
        );
        return redirect('/docente')->with($message);
    }
    public function index()
    {
        //

        $docente = docente::withTrashed()->get();
        $colegio=Colegio::all();
        $programa=programaFormacion::all();
        $cant = docente::withTrashed()
        ->count();
        $formacion = Formacion::select("formacion.*", "ficha.codigo","ficha.nombreFicha")
        ->join ("ficha","formacion.idFicha","ficha.id")
        ->get();
        return view('docentes.index')-> with(compact('docente','cant','programa','colegio','formacion'));
    

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $colegio=Colegio::all();
        $programa=programaFormacion::select("programa_formacion.*")
        ->where ("idTipo_formacion",5)->get();
        return view('docentes.create')-> with(compact('programa','colegio'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = [
        'nombre'=> 'required|string',
         'apellido'=> 'required|string',
         'correo' => 'required|string',
         'idcolegio'=> 'required',
         'idprograma'=>'required',
         'telefono'=>'required|integer'

        ];
        $message = array(
            'message' => 'Docente par registrado con éxito',
            'alert-type' => 'success'
        );

        $this->validate($request, $rules, $message);

        docente::create($request->all());


        return redirect('/docente')->with($message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\docente  $docente
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\docente  $docente
     * @return \Illuminate\Http\Response
     */
    public function edit(docente $docente, $id)
    {
        //
        $docente = docente::findOrFail($id);
        $colegio=Colegio::all();
        $programa=programaFormacion::select("programa_formacion.*")
        ->where ("idTipo_formacion",5)->get();
        return view('docentes.edit',compact('docente','programa','colegio'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\docente  $docente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    
    
        $docente=docente::findOrFail($id);
        $rules = [
            'nombre',
             'apellido',
             'correo',
             'idcolegio,'.$id,
             'idprograma,'.$id,
             'telefono'
    
            ];
            $message = array(
                'message' => 'Docente par actualizado con éxito',
                'alert-type' => 'success'
            );
           
            $this->validate($request, $rules, $message);
    
            $docente->update($request->all());
            // $docente::where($id)->update($request->all());
    
            return redirect('/docente')->with($message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\docente  $docente
     * @return \Illuminate\Http\Response
     */
 

    public function destroy($id)
    {

        docente::destroy($id);

        $message = array(
            'message' => 'Docente cambiado de estado con éxito',
            'alert-type' => 'success',
        );

        return redirect('/docente')->with($message);
    }

    public function restore($id){

        docente::withTrashed()->find($id)->restore();

        $message = array(
            'message' => 'Docente cambiado de estado con éxito',
            'alert-type' => 'success',
        );

        return back()->with($message);

    }
}
