<?php

namespace App\Http\Controllers;

use App\models\estado_competencia;
use Illuminate\Http\Request;

class EstadoCompetenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\estado_competencia  $estado_competencia
     * @return \Illuminate\Http\Response
     */
    public function show(estado_competencia $estado_competencia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\estado_competencia  $estado_competencia
     * @return \Illuminate\Http\Response
     */
    public function edit(estado_competencia $estado_competencia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\estado_competencia  $estado_competencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, estado_competencia $estado_competencia)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\estado_competencia  $estado_competencia
     * @return \Illuminate\Http\Response
     */
    public function destroy(estado_competencia $estado_competencia)
    {
        //
    }
}
