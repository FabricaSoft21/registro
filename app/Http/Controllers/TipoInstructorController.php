<?php

namespace App\Http\Controllers;

use App\models\tipoInstructor;
use Illuminate\Http\Request;

class TipoInstructorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\tipoInstructor  $tipoInstructor
     * @return \Illuminate\Http\Response
     */
    public function show(tipoInstructor $tipoInstructor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\tipoInstructor  $tipoInstructor
     * @return \Illuminate\Http\Response
     */
    public function edit(tipoInstructor $tipoInstructor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\tipoInstructor  $tipoInstructor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tipoInstructor $tipoInstructor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\tipoInstructor  $tipoInstructor
     * @return \Illuminate\Http\Response
     */
    public function destroy(tipoInstructor $tipoInstructor)
    {
        //
    }
}
