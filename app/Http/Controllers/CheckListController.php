<?php

namespace App\Http\Controllers;

use App\models\checkList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \Validator;

class CheckListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = [
            $request->column_name => 'required|string|unique:check_list,pregunta',
        ];

        $error = Validator::make($request->all(), $rules);
        $last = checkList::all()->last();

        if ($error->fails())
         {

         }

        $form_data = array(
            'id' => $last['id'] + 1,
            'pregunta' =>  $request->column_value,
        );

        try {
            checkList::create($form_data);
        } catch (\Throwable $th) { }

        $data = checkList::where('id', $last['id'] + 1)->get();

        return response()->json(['success' => 'Pregunta agregada con éxito.', 'data' => $data]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\checkList  $checkList
     * @return \Illuminate\Http\Response
     */
    public function show(checkList $checkList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\checkList  $checkList
     * @return \Illuminate\Http\Response
     */
    public function edit(checkList $checkList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\checkList  $checkList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        try {
            if ($request->ajax()) {
                $data = array(
                    'pregunta' =>  $request->column_value
                );
                DB::table('check_list')
                    ->where('id', $request->id)
                    ->update($data);
                echo '<div class="alert alert-success">Datos actualizados con éxito</div>';
            }
        } catch (\Throwable $th) {

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\checkList  $checkList
     * @return \Illuminate\Http\Response
     */
    public function destroy(checkList $checkList)
    {
        //
    }
}
