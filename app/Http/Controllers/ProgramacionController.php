<?php

namespace App\Http\Controllers;

use App\models\Colegio;
use App\models\dias;
use App\models\empresa;
use App\models\programacion;
use Illuminate\Http\Request;
use App\models\programaFormacion;
use App\models\instructor;
use App\models\programacion_has_dias;
use \Validator;
use Illuminate\Support\Facades\DB;
use PDF;

class ProgramacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $instructor = instructor::all();
        $formacion = programaFormacion::all();
        $programacion = programacion::all();

        return view('utils.progra')->with(compact('instructor', 'formacion', 'programacion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function save(Request $request)
    {


        $rules = [
            'idinstructor' => 'required',
            'idFormacion' => 'required',
            'fechaInicio' => 'required|date',
            'fechaFinal' => 'required|date',
            'horaInicio' => 'required',
            'horaFinal' => 'required',
            'lugar' => 'required',
            'idColegio' => 'nullable',
            'idEmpresa' => 'nullable'
        ];

        $error = Validator::make($request->all(), $rules);


        if ($error->fails()) {
        }

        $data = $request->input('dias', []);
        $count = count($data);
        

        $form_data = array(
            
            'idinstructor' =>  $request->idinstructor,
            'idFormacion' => $request->idFormacion,
            'fechaInicio' => date("Y-m-d", strtotime($request->fechaInicio)),
            'fechaFinal' => date("Y-m-d", strtotime($request->fechaFinal)),
            'horaInicio' => $request->horaInicio,
            'horaFinal' => $request->horaFinal,
            'lugar'   => $request->lugar,
            'idColegio' => $request->idColegio,
            'idEmpresa' => $request->idEmpresa
        );
        

        $ultimo = programacion::create($form_data);

        

        for ($i = 0; $i < $count; $i++) {

            programacion_has_dias::create([

                "idProgramacion" => $ultimo->id,
                "idDia" =>  $data[$i]

            ]);
        }

        return response()->json(['success' => 'Programación agregada con éxito.']);
    }

    public function create()
    {
        // $programa=programaFormacion::all();
        // $instructor=instructor::all();

        //     return view('utils.programa')->with(compact('$programa','instructor'));

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // $rules=[
        //     'idintructor' => 'required',
        //     'idprograma'=>'requerid',
        //     'fechaInicio'=>'required|date',
        //     'fechaFinal'=>'required|date',
        //     'horaIncio'=>'required',
        //     'horaFinal'=>'required',
        //     'idEmpresa'=>'nullable',
        //     'idColegio'=>'nullable'

        // ];

        // $message = array(
        //     'message' => 'Programación registrada con éxito',
        //     'alert-type' => 'success'
        // );

        // $this->validate($request, $rules, $message);

    }



    /**
     * Display the specified resource.
     *
     * @param  \App\models\programacion  $programacion
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

        $cant = programacion::count();
        $programaFormacion = programacion::all();
        $colegio = Colegio::all();
        $empresa = Empresa::all();

        return view('utils.show')->with(compact('programaFormacion', 'cant', 'colegio', 'empresa'));
    }

    public function getLugar($type)
    {

        if ($type == 1) {
            $data = Colegio::all();
        } else {
            $data = empresa::all();
        }

        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\programacion  $programacion
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $programacion = programacion::findOrFail($id);

        $instructor = instructor::all();
        $programaFormacion = programaFormacion::all();
        $colegio = Colegio::all();
        $empresa = Empresa::all();
        $dia = dias::all();

        $dias = programacion_has_dias::select("programacion_has_dias.*", "dia.id as dia")
            ->join("dia", "programacion_has_dias.idDia", "dia.id")
            ->where('programacion_has_dias.idProgramacion', $id)
            ->get();

        $data[] = "";
        if ($dias != "") {
            foreach ($dias as $key => $value) {

                $data[] = $value->dia;
            }
        }

        return view('utils.edit')->with(compact('programacion', 'instructor', 'programaFormacion', 'dia', 'colegio', 'empresa', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\programacion  $programacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = [
            'idinstructor' => 'required',
            'idFormacion' => 'required',
            'fechaInicio' => 'required|date',
            'fechaFinal' => 'required|date',
            'horaInicio' => 'required',
            'horaFinal' => 'required',
            'lugar' => 'required',
            'idColegio' => 'nullable',
            'idEmpresa' => 'nullable'
        ];


        $message = array(
            'message' => 'Programación actulizada con éxito',
            'alert-type' => 'success'
        );

        $this->validate($request, $rules, $message);

        $programacion = programacion::findOrFail($id);

        $programacion->update($request->all());

        if ($request->input('idColegio') != null) {

            $programacion->update([

                'idEmpresa' => null

            ]);
        } elseif ($request->input('idEmpresa') != null) {

            $programacion->update([

                'idColegio' => null

            ]);
        }

        $selected = $request->input('dias', []);
        $count = count($selected);

        for ($i = 0; $i < $count; $i++) {

            programacion_has_dias::updateOrCreate([

                "idDia" =>  $selected[$i],

                "idProgramacion" => $id,
                "idDia" =>  $selected[$i]

            ]);
        }

        programacion_has_dias::whereNotIn('idDia', $selected)
            ->where('idProgramacion', $id)
            ->delete();



        return redirect('/events')->with($message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\programacion  $programacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(programacion $programacion)
    {
        //
    }

    public function getEvents($id)
    {

        $data = programacion::select('programacion.*', 'instructor.*', 'programa_formacion.nombre as nombrePrograma', 'profesion.nombre as profesion', 'programacion.id as idProgramacion')
            ->join('instructor', 'programacion.idInstructor', 'instructor.id')
            ->join('programa_formacion', 'programacion.idFormacion', 'programa_formacion.id')
            ->join('profesion', 'instructor.idProfesion', 'profesion.id')
            ->with('colegio', 'empresa')
            ->where('idInstructor', $id)->get();

        foreach ($data as $key => $value) {

            $fechaInicial = strtotime($value->fechaInicio);

            $fechaFinal = strtotime($value->fechaFinal);

            $dias = $this->getDias($value->idProgramacion);

            $data[$key]["dias"] = $dias;

            $datesToDays = [];

            for ($i = $fechaInicial; $i <= $fechaFinal; $i += 86400) {
                if (array_search(date('w', $i), $dias) !== false) {
                    $datesToDays[] = date('Y-m-d', $i);
                }
            }

            $data[$key]["fechas"] = $datesToDays;
        }

        return response()->json($data);
    }


    public function getDias($id)
    {

        $days = ["Lunes" => 1, "Martes" => 2, "Miercoles" => 3, "Jueves" => 4, "Viernes" => 5, "Sabado" => 6, "Domingo" => 0];

        $dias = programacion_has_dias::select("programacion_has_dias.*", "dia.nombre as dia")
            ->join("dia", "programacion_has_dias.idDia", "dia.id")
            ->where('programacion_has_dias.idProgramacion', $id)
            ->get();

        $data = [];
        if ($dias != "") {
            foreach ($dias as $key => $value) {

                if ($value->dia != "") {
                    $data[] = $days[$value->dia];
                }
            }
        }


        return $data;
    }

    public function getName($id)
    {

        $dias = programacion_has_dias::select("programacion_has_dias.*", "dia.nombre as dia")
            ->join("dia", "programacion_has_dias.idDia", "dia.id")
            ->where('programacion_has_dias.idProgramacion', $id)
            ->get();

        $data = [];
        if ($dias != "") {
            foreach ($dias as $key => $value) {

                if ($value->dia != "") {
                    $data[] = $value->dia;
                }
            }
        }


        return response()->json($data);
    }
    public function report()
    {

        ini_set('max_execution_time', 300);

        $programacion =programacion::select("programacion.*","programacion.id as idProgramacion", "instructor.*", "programa_formacion.nombre as programa")
            ->join('instructor','programacion.idinstructor','instructor.id')
            ->join("programa_formacion", "programacion.idFormacion", "=", "programa_formacion.id")
            ->with('colegio', 'empresa')
            ->get();

            foreach ($programacion as $value) {

                $value->dias = DB::table('programacion_has_dias')
                ->join('dia','programacion_has_dias.idDia','dia.id')
                ->where('idProgramacion',$value->idProgramacion)->get()->toArray();

            }


        $temp_name = "InformeProgramacion_" . $this->random_string() . ".pdf";
        $number = $this->random_string();
        return PDF::loadView("reportes/reportProgramacion", compact(['programacion','number']))->stream($temp_name);

    }

    protected function random_string()
    {
        $key = '';
        $keys = array_merge(range(0,9));

        for($i=0; $i<5; $i++)
        {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }


}
