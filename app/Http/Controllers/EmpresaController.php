<?php

namespace App\Http\Controllers;

use App\models\empresa;
use App\models\Formacion;
use App\models\EmpresaHasPoblacion;
use Illuminate\Http\Request;
use App\models\tipo_poblacion;
use Excel;
use Illuminate\Support\Facades\DB;

class EmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {

        $rules = [
            'file-selected' => 'required|mimes:xls,xlsx'
        ];

        $message = array(
            'message' => 'Importación de datos realizada con éxito',
            'alert-type' => 'success'
        );

        $this->validate($request, $rules, $message);


        //
        $path = $request->file('file-selected')->getRealPath();

        $data = Excel::load($path)->get();

        // $area = null;

        // Se imprime la data para obtener los encabezados("heading") del Excel y su respectiva información ("items")
        // dd($data);   

        if ($data->count() > 0) {
            foreach ($data as $key => $value) {

                // dd($value);
                $last = empresa::all()->last();
                // $doc = tipoDoc::select('tipo_doc.id')
                // ->where('tipo_doc.nombre',$value['tipo_documento'])
                // ->first();

                // $area = area::select('area.id')
                // ->where('area.nombre',$value['area'])
                // ->first();

                // $unique[] = array (strval($value['numero_identificacion']));

                $insert_data = array(

                    'id' => $last['id'] + 1,
                    'nit' => $value ['nit'],
                    'nombre' => $value['nombre'],
                    'correo' => $value['correo'],
                    'telefono' => $value['telefono'],
                    'nombreEncargado'=>$value['nombre_encargado'],
                    'telefonoEncargado'=>$value['telefono_encargado'],
                    "created_at" =>  date('Y-m-d h:i:s'),
                    "updated_at" => null
                );


                // Se intenta agregar a la base de datos dentro de una estructura try-catch,
                // para excepcionar el error de duplicidad de datos y agregar solo los que no están
                try {
                    DB::table('empresa')->insert($insert_data);
                } catch (\Throwable $th) {
                    // dd($th);
                }
            }

            if (empty($insert_data)) {
                $message = array(
                    'message' => 'Error al importar datos',
                    'alert-type' => 'error'
                );
            }
        }

        return redirect('/empresa')->with($message);

        $message = array(
            'message' => 'Error al importar datos',
            'alert-type' => 'error'
        );
        return redirect('/empresa')->with($message);
    }

  

    public function index()
    {
        $formacion= Formacion::select('ficha.*','formacion.*')
        ->join('ficha','formacion.idFicha','ficha.id')
        ->join('programa_formacion','ficha.idProgramaFormacion','programa_formacion.id')
        ->where('idTipo_formacion',3)->get();


        $tipoPobla=tipo_poblacion::all();
        $cant = empresa::withTrashed()
        ->count();
        $empresa = empresa::withTrashed()->get();
        return view('empresas.index')->with(compact('empresa','cant','tipoPobla','formacion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipoPobla=tipo_poblacion::all();
        $last = empresa::all()->last();
        return view('empresas.create')->with(compact('tipoPobla','last'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules=[

            'nit'=> 'required|unique:empresa,nit',
            'nombre'=>'required|string',
            'correo'=> 'required|email|unique:empresa,correo',
            'telefono'=>'required|string',
            'nombreEncargado'=>'required|string',
            'telefonoEncargado'=>'required|string',
            'idTipoPoblacion'=>'nullable|integer'
    

        ];

        $message = array(
            'message' => 'Empresa registrada con éxito',
            'alert-type' => 'success',
        );

        
        
        $this->validate($request,$rules,$message);

        empresa::create($request->all());

    
        $sea = $request->input('idPoblacion', []);
        $cant = count($sea);

        for ($i = 0; $i < $cant; $i++) {

            EmpresaHasPoblacion::create([
                "idEmpresa" => $request->idEmpresa,
                "idPoblacion" => $sea[$i],
            ]);

        }

        return redirect('/empresa')->with($message);


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        

        $poblacion = EmpresaHasPoblacion::select("empresa_has_poblacion ..*", "tipo.nombre as nomprePoblacion")
        ->join("tipo_poblacion", "empresa_has_poblacion.idPoblacion", "tipo_poblacion.id")
        ->where('empresa_has_poblacion.idPoblacion', $id)
        ->get();

    $data[] = "";
    if ($poblacion != "") {
        foreach ($poblacion as $key => $value) {

            $data[] = $value->nomprePoblacion;

        }
    }

    $tipoPobla = tipo_poblacion::select('tipo_poblacion.*')
        ->whereIn('nombre', $data)->get();
        return response()->json($tipoPobla);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function edit(empresa $empresa, $id)
    {
        //
        $tipoPobla = tipo_poblacion::all();
        $empresa = empresa::findOrFail($id);
        $countPobla = tipo_poblacion::count();
        $last=empresa::all()->last();

        $poblacion = EmpresaHasPoblacion::select("empresa_has_poblacion.*", "tipo_poblacion.nombre as nombrePoblacion")
        ->join("tipo_poblacion", "empresa_has_poblacion.idPoblacion", "tipo_poblacion.id")
        ->where('empresa_has_poblacion.idEmpresa', $id)
        ->get();

    $data[] = "";
    if ($poblacion != "") {
        foreach ($poblacion as $key => $value) {

            $data[] = $value->nombrePoblacion;

        }
    }

    return view('empresas.edit')->with(compact( 'tipoPobla','empresa','countPobla','data'));



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)

    {
        //
        $empresa=empresa::findOrFail($id);

        $selected = $request->input('idPoblacion', []);
        EmpresaHasPoblacion::whereNotIn('idPoblacion', $selected)
            ->where('idEmpresa', $id)
            ->delete();

        $more = $request->Poblaciones;

        $rules=[

            'nit'=> 'required|unique:empresa,nit,'.$id,
            'nombre'=>'required|string',
            'correo'=> 'required|email|unique:empresa,correo,' .$id,
            'telefono'=>'required|string',
            'nombreEncargado'=>'required|string',
            'telefonoEncargado'=>'required|string',
            'idTipoPoblacion'=>'nullable|integer'
    

        ];

        
        $message = array(
            'message' => 'Empresa Actualizada con éxito',
            'alert-type' => 'success',
        );
        
        $this->validate($request,$rules,$message);
        if ($more != null) {

            for ($i = 0; $i < count($more); $i++) {

                EmpresaHasPoblacion::create([
                    "idEmpresa" => $id,
                    "idPoblacion" => $more[$i],
                ]);
            }
        }
        $empresa->update($request->all());

        return redirect('/empresa')->with($message);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\empresa  $empresa
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        empresa::destroy($id);

        $message = array(
            'message' => 'Empresa cambiado de estado con éxito',
            'alert-type' => 'success',
        );

        return redirect('/empresa')->with($message);
    }

    public function restore($id){

        empresa::withTrashed()->find($id)->restore();

        $message = array(
            'message' => 'Empresa cambiado de estado con éxito',
            'alert-type' => 'success',
        );

        return back()->with($message);

    }
}
