<?php

namespace App\Http\Controllers;

use App\models\tipoDiscapacidad;
use Illuminate\Http\Request;
use \Validator;

class TipoDiscapacidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    
    public function save(Request $request)
    {

        $rules = [
            'nombre' => 'required|string',
            'descripcion' => 'required|string',
        ];

        $error = Validator::make($request->all(), $rules);
        $last = tipoDiscapacidad::all()->last();

        if ($error->fails()) {

        }

        $form_data = array(
            'id' => $last['id'] + 1,
            'nombre' =>  $request->nombre,
            'descripcion' => $request->descripcion,
          
        );

        tipoDiscapacidad::create($form_data);
        $data = tipoDiscapacidad::where('id', $last['id'] + 1)->get();

        return response()->json(['success' => 'Discapacidad agregada con éxito.', 'data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\tipoDiscapacidad  $tipoDiscapacidad
     * @return \Illuminate\Http\Response
     */
    public function show(tipoDiscapacidad $tipoDiscapacidad)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\tipoDiscapacidad  $tipoDiscapacidad
     * @return \Illuminate\Http\Response
     */
    public function edit(tipoDiscapacidad $tipoDiscapacidad)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\tipoDiscapacidad  $tipoDiscapacidad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tipoDiscapacidad $tipoDiscapacidad)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\tipoDiscapacidad  $tipoDiscapacidad
     * @return \Illuminate\Http\Response
     */
    public function destroy(tipoDiscapacidad $tipoDiscapacidad)
    {
        //
    }
}
