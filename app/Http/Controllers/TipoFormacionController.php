<?php

namespace App\Http\Controllers;

use App\models\tipo_formacion;
use Illuminate\Http\Request;

class TipoFormacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\tipo_formacion  $tipo_formacion
     * @return \Illuminate\Http\Response
     */
    public function show(tipo_formacion $tipo_formacion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\tipo_formacion  $tipo_formacion
     * @return \Illuminate\Http\Response
     */
    public function edit(tipo_formacion $tipo_formacion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\tipo_formacion  $tipo_formacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tipo_formacion $tipo_formacion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\tipo_formacion  $tipo_formacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(tipo_formacion $tipo_formacion)
    {
        //
    }
}
