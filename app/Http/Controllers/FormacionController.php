<?php

namespace App\Http\Controllers;

use App\models\Formacion;
use App\models\ficha;
use App\models\Aprendiz;
use App\models\estadoFomarcion;
use App\models\AprendizHasProgramas;
use App\models\InstructorHasformacion;
use App\models\docente;
use Illuminate\Http\Request;
use App\models\aprendizHasDocentes;
use App\models\dias;
use App\models\empresa;
use App\models\EmpresaHasAprendices;
use App\models\instructor;
use App\models\programacion;
use App\models\programaFormacion;

class FormacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        Formacion::whereDate('fechaFinal', '=', date('Y-m-d'))
            ->update([
                'idEstado' => 2
            ]);

        $titulada = Formacion::select('formacion.*')
            ->join('ficha', 'formacion.idFicha', 'ficha.id')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->join('tipo_formacion', 'programa_formacion.idTipo_formacion', 'tipo_formacion.id')
            ->where('tipo_formacion.id', 1)->get();


        $complementaria = Formacion::select('formacion.*')
            ->join('ficha', 'formacion.idFicha', 'ficha.id')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->join('tipo_formacion', 'programa_formacion.idTipo_formacion', 'tipo_formacion.id')
            ->where('tipo_formacion.id', 2)->get();


        $poblacion = Formacion::select('formacion.*')
            ->join('ficha', 'formacion.idFicha', 'ficha.id')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->join('tipo_formacion', 'programa_formacion.idTipo_formacion', 'tipo_formacion.id')
            ->where('tipo_formacion.id', 3)->get();

        $competencia = Formacion::select('formacion.*')
            ->join('ficha', 'formacion.idFicha', 'ficha.id')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->join('tipo_formacion', 'programa_formacion.idTipo_formacion', 'tipo_formacion.id')
            ->where('tipo_formacion.id', 4)->get();

        $media = Formacion::select('formacion.*')
            ->join('ficha', 'formacion.idFicha', 'ficha.id')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->join('tipo_formacion', 'programa_formacion.idTipo_formacion', 'tipo_formacion.id')
            ->where('tipo_formacion.id', 5)->get();

        $formacion = Formacion::all();

        $cant = Formacion::count();

        return view('formacion.index')->with(compact('formacion', 'cant', 'titulada', 'complementaria', 'poblacion', 'competencia', 'media'));
    }

    public function create()
    {

        $fichaT = ficha::select('ficha.*')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->where('idTipo_formacion', 1)
            ->where('idTipoFicha','!=',2)
            ->get();

        $fichaC = ficha::select('ficha.*')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->where('idTipo_formacion', 2)
            ->where('idTipoFicha','!=',2)
            ->get();

        $fichaP = ficha::select('ficha.*')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->where('idTipo_formacion', 3)
            ->where('idTipoFicha','!=',2)
            ->get();

        $fichaCB = ficha::select('ficha.*')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->where('idTipo_formacion', 4)
            ->where('idTipoFicha','!=',2)
            ->get();

        $fichaM = ficha::select('ficha.*')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->where('idTipo_formacion', 5)
            ->where('idTipoFicha','!=',2)
            ->get();

        $estado = estadoFomarcion::all();

        return view('formacion.create')->with(compact('estado', 'fichaT', 'fichaC', 'fichaP', 'fichaCB', 'fichaM'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'idFicha' => 'required|unique:formacion,idFicha',
            'fechaInicio' => 'required|date',
            'fechaFinal' => 'required|date',
            'jornada' => 'required|string|in:AM,PM',
            'horaInicio' => 'required',
            'horaFinal' => 'required',
            'fechaInicioProd' => 'nullable',
            'idEstado' => '',

        ];

        $messages = [

            'idFicha.unique' => 'La ficha ya ha sido asociada a una formación.'
        ];

        $message = array(
            'message' => 'Formación registrada con éxito',
            'alert-type' => 'success'
        );

        $this->validate($request, $rules, $message, $messages);

        // dd($request->all());

        Formacion::insert($request->except('_token','idEstado'));

        return redirect('/formacion')->with($message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Fomarcion  $fomarcion
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        $dia=dias::all();
        $formacion = Formacion::all();
        $ficha = ficha::all();
        $estado = estadoFomarcion::all();
        $docente = docente::all();
        $instructor = instructor::all();
        $programaFormacion = programaFormacion::all();
        $programacion=programacion::all();

        return view('utils.calendar')->with(compact('ficha', 'estado', 'formacion', 'docente', 'instructor', 'programaFormacion','dia','programacion'));
    }

    public function getFormacion($id)
    {

        $data = Formacion::select('formacion.*', 'ficha.*', 'estado_formacion.nombre as estadoFormacion')
            ->join('ficha', 'formacion.idFicha', 'ficha.id')
            ->join('estado_formacion', 'formacion.idEstado', 'estado_formacion.id')
            ->where('idFicha', $id)->get();

        return response()->json($data);
    }


    public function getStudents($id)
    {

        $aprendices = AprendizHasProgramas::select("aprendiz_has_programas.*", "aprendiz.*", "formacion.*", "ficha.*", "aprendiz.id as aprendiz_id", "estado_aprendiz.nombre as estadoAprendiz")
            ->join("aprendiz", "aprendiz_has_programas.idAprendiz", "aprendiz.id")
            ->join('estado_aprendiz', 'aprendiz.idestado', 'estado_aprendiz.id')
            ->join("formacion", "aprendiz_has_programas.idFormacion", "formacion.id")
            ->join("ficha", "formacion.idFicha", "ficha.id")
            ->where('aprendiz_has_programas.idFormacion', $id)
            ->where('aprendiz.idestado', '!=', 1)
            ->get();

        return response()->json(["data" => $aprendices]);
    }

    public function getInstructores($id)
    {

        $instructor = InstructorHasformacion::select("instructor.*","tipo_instructor.nombre as tipo")
            ->join("instructor", "instructor_hasformacions.idInstructor", "instructor.id")
            ->join("aprendiz_has_programas", "instructor_hasformacions.idAprendizHasPrograma", "aprendiz_has_programas.id")
            ->join('tipo_instructor','instructor_hasformacions.idTipoInstructor','tipo_instructor.id')
            ->where('aprendiz_has_programas.idFormacion', $id)
            ->distinct()
            ->get();

        return response()->json(["data" => $instructor]);
    }

    public function getDocente($id)
    {

        $docente = aprendizHasDocentes::select("docente.*")
            ->join("docente", "aprendiz_has_docentes.idDocente", "docente.id")
            ->join("aprendiz_has_programas", "aprendiz_has_docentes.idAprendizHasPrograma", "aprendiz_has_programas.id")
            ->where('aprendiz_has_programas.idFormacion', $id)
            ->distinct()
            ->get();

        return response()->json(["data" => $docente]);
    }

    public function getEmpresa($id)
    {

        $empresa = EmpresaHasAprendices::select("empresa.*")
            ->join("empresa", "empresa_has_aprendices.idEmpresa", "empresa.id")
            ->join("aprendiz_has_programas", "empresa_has_aprendices.idAprendizHasPrograma", "aprendiz_has_programas.id")
            ->where('aprendiz_has_programas.idFormacion', $id)
            ->distinct()
            ->get();

        return response()->json(["data" => $empresa]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Fomarcion  $fomarcion
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $formacion = Formacion::findOrFail($id);

        $fichaT = ficha::select('ficha.*')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->where('idTipo_formacion', 1)
            ->get();

        $fichaC = ficha::select('ficha.*')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->where('idTipo_formacion', 2)
            ->get();

        $fichaP = ficha::select('ficha.*')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->where('idTipo_formacion', 3)
            ->get();

        $fichaCB = ficha::select('ficha.*')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->where('idTipo_formacion', 4)
            ->get();

        $fichaM = ficha::select('ficha.*')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->where('idTipo_formacion', 5)
            ->get();
        $estado = estadoFomarcion::all();

        return view('formacion.edit')->with(compact('estado', 'formacion', 'fichaT', 'fichaC', 'fichaP', 'fichaCB', 'fichaM'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Fomarcion  $fomarcion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $formacion = Formacion::findOrFail($id);

            Aprendiz::select('aprendiz.*')
            ->join('aprendiz_has_programas', 'aprendiz_has_programas.idAprendiz', 'aprendiz.id')
            ->get();

        $rules = [
            'idFicha' => 'required|unique:formacion,idFicha,' . $id,
            'fechaInicio' => 'required|date',
            'fechaFinal' => 'required|date',
            'jornada' => 'required|string|in:AM,PM',
            'horaInicio' => 'required',
            'horaFinal' => 'required',
            'fechaInicioProd' => 'nullable',
            'idEstado' => 'required',

        ];

        $messages = [

            'idFicha.unique' => 'La ficha ya ha sido asociada a una formación.'
        ];

        $message = array(
            'message' => 'Formación registrada con éxito',
            'alert-type' => 'success'
        );

        $this->validate($request, $rules, $message, $messages);

        if ($formacion->idEstado == 1) {

            $ids = [1, 3 , 4];
            Aprendiz::where('aprendiz.idestado', 1)
                ->whereNotIn('aprendiz.idestado', $ids)
                ->join('aprendiz_has_programas', 'aprendiz_has_programas.idAprendiz', 'aprendiz.id')
                ->where('aprendiz_has_programas.idFormacion', $id)
                ->update([
                    'idestado' => 3,
                ]);
        }

        $formacion->update($request->all());

        return redirect('/formacion')->with($message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Fomarcion  $fomarcion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fomarcion $fomarcion)
    {
        //
    }
}
