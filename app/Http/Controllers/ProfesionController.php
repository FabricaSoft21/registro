<?php

namespace App\Http\Controllers;

use App\models\profesion;
use Illuminate\Http\Request;
use \Validator;



class ProfesionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function save(Request $request){

        $rules = [
            'nombre' => 'required|string|unique:profesion,nombre',
            'descripcion' => 'required|string',
          
        ];

        $error = Validator::make($request->all(),$rules);
        $last = profesion::all()->last();

        if($error->fails())
        {
    
        }

        $form_data = array(
            'id' => $last['id'] + 1,
            'nombre' =>  $request->nombre,
            'descripcion' => $request->descripcion,
      
        );

        profesion::create($form_data);
        $data = profesion::where('id',$last['id'] + 1)->get();

        return response()->json(['success' => 'Profesión agregada con éxito.', 'data' => $data]);

     }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\profesion  $profesion
     * @return \Illuminate\Http\Response
     */
    public function show(profesion $profesion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\profesion  $profesion
     * @return \Illuminate\Http\Response
     */
    public function edit(profesion $profesion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\profesion  $profesion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, profesion $profesion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\profesion  $profesion
     * @return \Illuminate\Http\Response
     */
    public function destroy(profesion $profesion)
    {
        //
    }
}
