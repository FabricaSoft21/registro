<?php

namespace App\Http\Controllers;

use App\models\Aprendiz;
use App\models\Colegio;
use App\models\empresa;
use App\models\ficha;
use App\models\instructor;
use App\models\poblacionVulne;
use App\models\programaFormacion;
use DB;
use Illuminate\Support\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $poblacion = poblacionVulne::all();
        if (auth()->user()->hasRole('poblaciones')) {

            $aprendices = Aprendiz::all()
                ->where('aprendiz.tipoAprendiz', 3)
                ->count();

            $programas_formacion = programaFormacion::all()
                ->where('programa_formacion.idTipo_formacion', 3)
                ->count();

            $instructores = instructor::all()
                ->where('instructor.idArea', 3)
                ->count();


            $empresa = empresa::all()->count();

            $instituciones = Colegio::all()->count();
        } else if (auth()->user()->hasRole('tecnica')) {

            $aprendices = Aprendiz::all()
                ->where('aprendiz.tipoAprendiz', 5)
                ->count();

            $programas_formacion = programaFormacion::all()
                ->where('programa_formacion.idTipo_formacion', 5)
                ->count();

            $instructores = instructor::all()
                ->where('instructor.idArea', 5)
                ->count();

            $empresa = empresa::all()->count();

            $instituciones = Colegio::all()->count();
        } else {
            $aprendices = Aprendiz::all()->count();

            $programas_formacion = programaFormacion::all()->count();

            $instructores = instructor::all()->count();

            $empresa = empresa::all()->count();

            $instituciones = Colegio::all()->count();
        }

        $id = poblacionVulne::all()->last();

        $dates = collect();
        foreach (range(-6, 0) as $i) {
            $date = Carbon::now()->addDays($i)->format('Y-m-d');
            $dates->put($date, 0);
        }

        $posts = Aprendiz::where('created_at', '>=', $dates->keys()->first())
            ->groupBy('date')
            ->orderBy('date')
            ->get([
                DB::raw('DATE( created_at ) as date'),
                DB::raw('COUNT( * ) as "count"'),
            ])
            ->pluck('count', 'date');

        $dates = $dates->merge($posts);

        return view('/dashboard')->with(compact('id', 'dates','poblacion', 'programas_formacion', 'instructores', 'empresa', 'instituciones', 'aprendices'));

    }

    public function optionSelected($id)
    {

        $user = auth()->user();
        $user->selected_category_id = $id;
        $user->save();

        return back();
    }
}
