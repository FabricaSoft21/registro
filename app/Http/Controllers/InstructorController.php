<?php

namespace App\Http\Controllers;

use App\models\instructor;
use App\models\municipio;
use App\models\departamento;
use App\models\contrato;
use App\models\profesion;
use App\models\area;
use App\models\programaFormacion;
use App\models\tipoDoc;
use App\models\AprendizHasProgramas;
use App\models\Formacion;
use App\models\instructorHasformacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Excel;
use PDF;
use App\models\tipoCompetencia;
use App\models\tipoInstructor;

class InstructorController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {

        $rules = [
            'file-selected' => 'required|mimes:xls,xlsx',
        ];

        $message = array(
            'message' => 'Importación de datos realizada con éxito',
            'alert-type' => 'success',
        );

        $this->validate($request, $rules, $message);

        $path = $request->file('file-selected')->getRealPath();

        $data = Excel::load($path)->get();

        // try {
        if ($data->count() > 0) {

            foreach ($data as $key => $value) {

                $idrea = area::select('area.id')
                    ->where('area.nombre', $value['area'])
                    ->first();

                $contra = contrato::select('contrato.id')
                    ->where('contrato.nombre', $value['contrato'])
                    ->first();

                $profe = profesion::select('profesion.id')
                    ->where('profesion.nombre', $value['profesion'])
                    ->first();

                $departa = departamento::select('departamento.id')
                    ->where('departamento.nombre', $value['departamento'])
                    ->first();

                $muni = municipio::select('municipios.id')
                    ->where('municipios.nombre', $value['municipio'])
                    ->first();
                    
                    if ($idrea==null) {
                        $message = array(
                            'message' => 'Area no encontrada',
                            'alert-type' => 'error'
                        );
                        return redirect('/docente')->with($message);

                    } elseif ($contra==null) {
                        $message = array(
                            'message' => 'Tipo de contrato no encontrado',
                            'alert-type' => 'error'
                        );
                        return redirect('/docente')->with($message);
                        
                    }elseif($profe==null){
                        $message = array(
                            'message' => 'Profesión no encontrado',
                            'alert-type' => 'error'
                        );
                        return redirect('/docente')->with($message);

                    } elseif ($departa==null) {
                        $message = array(
                            'message' => 'Departamento no encontrad',
                            'alert-type' => 'error'
                        );
                        return redirect('/docente')->with($message);

                    } elseif ($muni==null) {
                        $message = array(
                            'message' => 'Municipio no encontrado',
                            'alert-type' => 'error'
                        );
                        return redirect('/docente')->with($message);
                    }
                    

                $last = instructor::all()->last();

                $insert_data = array(

                    'id' => $last['id'] + 1,
                    'idTipoDoc' => 1,
                    'numDoc' => $value['numero_documento'],
                    'nombre' => $value['nombre'],
                    'apellido' => $value['apellido'],
                    'correoSena' => $value['correo_sena'],
                    'correoAlterno' => $value['correo_alterno'],
                    'telefono' => $value['telefono'],
                    'celular' => $value['celular'],
                    'idArea' => $idrea['id'],
                    'idContrato' => $contra['id'],
                    'fechaInicioCo' => $value['fecha_inicio_contrato'],
                    'fechaFinalCo' => $value['fecha_final_contrato'],
                    'idProfesion' => $profe['id'],
                    'idDepartamento' => $departa['id'],
                    'idMunicipio' => $muni['id'],
                    "created_at" => date('Y-m-d h:i:s'),
                    "updated_at" => null
                );

                try {
                    DB::table('instructor')->insert($insert_data);
                } catch (\Throwable $th) {
                    // dd($th);
                }
            }

            if (empty($insert_data)) {
                $message = array(
                    'message' => 'Error al importar datos',
                    'alert-type' => 'error'
                );
            }
        }

        return redirect('/instructor')->with($message);

        $message = array(
            'message' => 'Error al importar datos',
            'alert-type' => 'error'
        );
        return redirect('/instructor')->with($message);
    }


    public function index()
    {

        // Instructores área titulada
        $titulada = instructor::withTrashed()
        ->where('idArea', 1)
         ->get();

        // Instructores área complementaria
        $complementaria = instructor::withTrashed()
        ->where('idArea', 2)
         ->get();

        // Instructores poblaciones vulnerables
        $PVulnerables = instructor::withTrashed()
        ->where('idArea', 3)
         ->get();

        // Instructores área competencias básicas
        $competencia = instructor::withTrashed()
        ->where('idArea', 4)
         ->get();

        // Instructores área media técnica
        $media = instructor::withTrashed()
        ->where('idArea', 5)
         ->get();

        $FTitulada = Formacion::select('ficha.*', 'formacion.*')
            ->join('ficha', 'formacion.idFicha', 'ficha.id')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->where('idTipo_formacion', 1)
            ->where('formacion.idEstado',1)
            ->get();

        $FComplementaria = Formacion::select('ficha.*', 'formacion.*')
            ->join('ficha', 'formacion.idFicha', 'ficha.id')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->where('idTipo_formacion', 2)
            ->where('formacion.idEstado',1)
            ->get();

        $FPoblacion = Formacion::select('ficha.*', 'formacion.*')
            ->join('ficha', 'formacion.idFicha', 'ficha.id')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->where('idTipo_formacion', 3)
            ->where('formacion.idEstado',1)            
            ->get();

        $FCompetencia = Formacion::select('ficha.*', 'formacion.*')
            ->join('ficha', 'formacion.idFicha', 'ficha.id')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->where('idTipo_formacion', 4)
            ->where('formacion.idEstado',1)
            ->get();

        $FMedia = Formacion::select('ficha.*', 'formacion.*')
            ->join('ficha', 'formacion.idFicha', 'ficha.id')
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->where('idTipo_formacion', 5)
            ->where('formacion.idEstado',1)
            ->get();

        $departamento = departamento::all();
        $instructor = instructor::withTrashed()->get();
        $municipio = municipio::all();
        $area = area::all();
        $tipoDoc = tipoDoc::all();
        $prograFormacion = programaFormacion::all();
        $contrato = contrato::all();
        $profesion = profesion::all();
        $cant = instructor::withTrashed()
        ->count();
        $tipoIns= tipoInstructor::all();
        $tipoC = tipoCompetencia::all();
        $formacion = Formacion::select("formacion.*", "ficha.codigo", "ficha.nombreFicha")
            ->join("ficha", "formacion.idFicha", "ficha.id")
            ->get();
        return view('instructores.index')->with(compact('FTitulada', 'FComplementaria', 'FPoblacion', 'FCompetencia', 'FMedia', 'instructor', 'cant', 'area', 'tipoDoc', 'prograFormacion', 'contrato', 'profesion', 'municipio', 'formacion', 'PVulnerables', 'complementaria', 'titulada', 'competencia', 'media','tipoC','tipoIns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $departamento = departamento::all();
        $municipio = municipio::all();
        $area = area::all();
        $tipoDoc = tipoDoc::all();
        $prograFormacion = programaFormacion::all();
        $contrato = contrato::all();
        $profesion = profesion::all();

        return view('instructores.create')->with(compact('area', 'tipoDoc', 'prograFormacion', 'contrato', 'profesion', 'municipio', 'departamento'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = [
            'nombre' => 'required|string',
            'apellido' => 'required|string',
            'idTipoDoc' => 'required|exists:tipo_doc,id',
            'numDoc' => 'required|string|unique:instructor,numDoc',
            'correoSena' => 'required|string|email|unique:instructor,correoSena',
            'correoAlterno' => 'required|string|email|unique:instructor,correoAlterno',
            'idMunicipio' => 'required',
            // 'idDepartamento'=>'required',
            'telefono' => 'required|string',
            'celular' => 'nullable|string',
            'idArea' => 'required',
            'idContrato' => 'required',
            'fechaInicioCo' => 'required|date',
            'fechaFinalCo' => 'required|date',
            'idProfesion' => 'required',
            // 'idProgramaFormacion'=>'required'
        ];

        $message = array(
            'message' => 'Instructor registrado con éxito',
            'alert-type' => 'success',
        );
        $this->validate($request, $rules, $message);

        instructor::create($request->all());

        return redirect('/instructor')->with($message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\instructor  $instructor
     * @return \Illuminate\Http\Response
     */
    public function show(instructor $instructor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\instructor  $instructor
     * @return \Illuminate\Http\Response
     */
    public function edit(instructor $instructor, $id)
    {
        //

        $instructor = instructor::findOrFail($id);
        $municipio = municipio::all();
        $departamento = departamento::all();
        $area = area::all();
        $tipoDoc = tipoDoc::all();
        $programaFormacion = programaFormacion::all();
        $contrato = contrato::all();
        $profesion = profesion::all();
        return view('instructores.edit', compact('instructor', 'area', 'tipoDoc', 'programaFormacion', 'contrato', 'profesion', 'municipio', 'departamento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\instructor  $instructor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $instructor = instructor::findOrFail($id);
        $rules = [
            'nombre' => 'required|string',
            'apellido' => 'required|string',
            'idTipoDoc' => 'required|exists:tipo_doc,id',
            'numDoc' => 'required|integer|unique:instructor,numDoc,' . $id,
            'correoSena' => 'required|string|email|unique:instructor,correoSena,' . $id,
            'correoAlterno' => 'required|string|email|unique:instructor,correoAlterno,' . $id,
            'idMunicipio' => 'required',
            // 'idDepartamento'=>'required',
            'telefono' => 'required|integer',
            'celular' => 'nullable|string',
            'idArea' => 'required',
            'idContrato' => 'required',
            'fechaInicioCo' => 'required|date',
            'fechaFinalCo' => 'required|date',
            'idProfesion' => 'required',
            // 'idProgramaFormacion'=>'required'
        ];
        $message = array(
            'message' => "Instructor actulizado con éxito",
            'alert-type' => 'success'

        );
        $this->validate($request, $rules, $message);

        $instructor->update($request->all());

        return redirect('/instructor')->with($message);
    }

    public function report()
    {

        ini_set('max_execution_time', 300);

        $instructorF = Formacion::select("ficha.*", "programa_formacion.nombre as programa","formacion.*","formacion.id as idFormacion")
            ->join("ficha", "formacion.idFicha", "=", "ficha.id")
            ->join("programa_formacion", "ficha.idProgramaFormacion", "=", "programa_formacion.id")
            ->get();

            foreach ($instructorF as $value) {

                $value->instructores = InstructorHasformacion::select("instructor.*","tipo_doc.nombre as tipoDoc")
                ->join("instructor", "instructor_hasformacions.idInstructor", "instructor.id")
                ->join('tipo_doc','instructor.idTipoDoc','tipo_doc.id')
                ->join("aprendiz_has_programas", "instructor_hasformacions.idAprendizHasPrograma", "aprendiz_has_programas.id")
                ->where('aprendiz_has_programas.idFormacion', $value->idFormacion)
                ->distinct()
                ->get()->toArray();
            }

        $temp_name = "InformeInstructor_" . $this->random_string() . ".pdf";
        $number = $this->random_string();
        return PDF::loadView("reportes/reportInstructor", compact(['instructorF','number']))->stream($temp_name);
    }

    protected function random_string()
    {
        $key = '';
        $keys = array_merge(range(0,9));

        for($i=0; $i<5; $i++)
        {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\instructor  $instructor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $usuario = User::findOrFail($id);
        // $usuario->removeRole($usuario->roles->implode('name', ', '));
        instructor::destroy($id);

        $message = array(
            'message' => 'Instructor cambiado de estado con éxito',
            'alert-type' => 'success',
        );

        return redirect('/instructor')->with($message);
    }

    public function restore($id){

        instructor::withTrashed()->find($id)->restore();

        $message = array(
            'message' => 'Instructor cambiado de estado con éxito',
            'alert-type' => 'success',
        );

        return back()->with($message);

    }
}
