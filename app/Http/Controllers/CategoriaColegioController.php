<?php

namespace App\Http\Controllers;

use App\models\categoria_colegio;
use Illuminate\Http\Request;

class CategoriaColegioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\categoria_colegio  $categoria_colegio
     * @return \Illuminate\Http\Response
     */
    public function show(categoria_colegio $categoria_colegio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\categoria_colegio  $categoria_colegio
     * @return \Illuminate\Http\Response
     */
    public function edit(categoria_colegio $categoria_colegio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\categoria_colegio  $categoria_colegio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, categoria_colegio $categoria_colegio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\categoria_colegio  $categoria_colegio
     * @return \Illuminate\Http\Response
     */
    public function destroy(categoria_colegio $categoria_colegio)
    {
        //
    }
}
