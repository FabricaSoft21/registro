<?php

namespace App\Http\Controllers;

use App\models\dias;
use Illuminate\Http\Request;

class DiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\dias  $dias
     * @return \Illuminate\Http\Response
     */
    public function show(dias $dias)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\dias  $dias
     * @return \Illuminate\Http\Response
     */
    public function edit(dias $dias)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\dias  $dias
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, dias $dias)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\dias  $dias
     * @return \Illuminate\Http\Response
     */
    public function destroy(dias $dias)
    {
        //
    }
}
