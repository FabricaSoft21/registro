<?php

namespace App\Http\Controllers;

use App\models\instructorHasCompetencias;
use Illuminate\Http\Request;

class InstructorHasCompetenciasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $com =  $request ->id;

        $cont = count($com);

        for ($i=0; $i < $cont; $i++) {

            instructorHasCompetencias::create(
                ['idCompetencia' => $com[$i],
                'idInstructor' => $request->input('idInstructor')],
                [
                "idCompetencia" => $com[$i],
                "idInstructor" => $request->input('idInstructor')
            ]);
        }

        $message = array(
            'message' => 'Competencias asociadas con éxito',
            'alert-type' => 'success'
        );

        return back()->with($message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\instructorHasCompetencias  $instructorHasCompetencias
     * @return \Illuminate\Http\Response
     */
    public function show(instructorHasCompetencias $instructorHasCompetencias)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\instructorHasCompetencias  $instructorHasCompetencias
     * @return \Illuminate\Http\Response
     */
    public function edit(instructorHasCompetencias $instructorHasCompetencias)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\instructorHasCompetencias  $instructorHasCompetencias
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, instructorHasCompetencias $instructorHasCompetencias)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\instructorHasCompetencias  $instructorHasCompetencias
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $id = $request->input('idInstructor');

        $selected = $request->input('idCompetencias', []);

        instructorHasCompetencias::whereIn('idCompetencia',$selected)
        ->where('idInstructor',$id)
        ->delete();

        $message = array(
            'message' => 'Competencias removidas con éxito',
            'alert-type' => 'success'
        );

        return back()->with($message);

    }
    
}
