<?php

namespace App\Http\Controllers;

use App\models\estadoFomarcion;
use Illuminate\Http\Request;

class EstadoFomarcionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\estadoFomarcion  $estadoFomarcion
     * @return \Illuminate\Http\Response
     */
    public function show(estadoFomarcion $estadoFomarcion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\estadoFomarcion  $estadoFomarcion
     * @return \Illuminate\Http\Response
     */
    public function edit(estadoFomarcion $estadoFomarcion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\estadoFomarcion  $estadoFomarcion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, estadoFomarcion $estadoFomarcion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\estadoFomarcion  $estadoFomarcion
     * @return \Illuminate\Http\Response
     */
    public function destroy(estadoFomarcion $estadoFomarcion)
    {
        //
    }
}
