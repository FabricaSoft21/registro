<?php

namespace App\Http\Controllers;

use App\models\ficha;
use App\models\tipo_ficha;
use App\models\ProgramaFormacion;
use Illuminate\Http\Request;
use App\models\Formacion;

class FichaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    
    public function index()
    {

        $ficha = Formacion::select('formacion.fechaInicioProd as fecha')->get();

        Ficha::whereDate('fechaInicioProd', '=' , date('Y-m-d'))
        ->join('formacion','formacion.idFicha','ficha.id')
        ->update([
            'ficha.idTipoFicha' => 2
        ]);
    
        $titulada=ficha::select('ficha.*')
        ->join('programa_formacion','ficha.idProgramaFormacion','programa_formacion.id')
        ->where('programa_formacion.idTipo_formacion',1)->get();

        $complementaria=ficha::select('ficha.*')
        ->join('programa_formacion','ficha.idProgramaFormacion','programa_formacion.id')
        ->where('programa_formacion.idTipo_formacion',2)->get();

        $poblacion=ficha::select('ficha.*')
        ->join('programa_formacion','ficha.idProgramaFormacion','programa_formacion.id')
        ->where('programa_formacion.idTipo_formacion',3)->get();

        $competencia=ficha::select('ficha.*')
        ->join('programa_formacion','ficha.idProgramaFormacion','programa_formacion.id')
        ->where('programa_formacion.idTipo_formacion',4)->get();

        $media=ficha::select('ficha.*')
        ->join('programa_formacion','ficha.idProgramaFormacion','programa_formacion.id')
        ->where('programa_formacion.idTipo_formacion',5)->get();

        
        $cant = ficha::count();

        return view('fichas.index')->with(compact('ficha','cant','titulada','complementaria','poblacion','competencia','media'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ficha = ficha::all()->last();

        $PTitulada = ProgramaFormacion::where('idTipo_formacion',1)->get();

        $PComplementaria = ProgramaFormacion::where('idTipo_formacion',2)->get();

        $Poblacion = ProgramaFormacion::where('idTipo_formacion',3)->get();

        $PCompetencia = ProgramaFormacion::where('idTipo_formacion',4)->get();

        $PMedia = ProgramaFormacion::where('idTipo_formacion',5)->get();
   
        $tipo = tipo_ficha::all();

        return view('fichas.create')->with(compact('ficha','tipo','PTitulada','PComplementaria','Poblacion','PCompetencia','PMedia'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nombreFicha' => 'required|string',
            'codigo' => 'required|string|unique:ficha,codigo',
            'idProgramaFormacion' => 'required|integer',
            'idTipoFicha' => '',
        ];

        $message = array(
            'message' => 'Ficha registrada con éxito',
            'alert-type' => 'success'
        );

        $this->validate($request, $rules, $message);


        ficha::insert($request->except('_token','idTipoFicha'));

        return redirect('/ficha')->with($message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\ficha  $ficha
     * @return \Illuminate\Http\Response
     */
    public function show(ficha $ficha)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\ficha  $ficha
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $ficha = ficha::findOrFail($id);
        $programa = ProgramaFormacion::all();
        $tipo = tipo_ficha::all();

        return view('fichas.edit', compact('ficha','programa','tipo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\ficha  $ficha
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ficha = ficha::findOrFail($id);

        $rules = [
            'nombreFicha' => 'required|string',
            'codigo' => 'required|integer|unique:ficha,codigo,'.$id,
            'idProgramaFormacion' => 'required|integer',
            'idTipoFicha' => 'required|integer',

        ];

        $message = array(
            'message' => 'Ficha actualizada con éxito',
            'alert-type' => 'success'
        );

        $this->validate($request, $rules, $message);

        
        if($ficha->idTipoFicha != 1){

        $ficha->update($request->all());

        }


        return redirect('/ficha')->with($message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\ficha  $ficha
     * @return \Illuminate\Http\Response
     */
    public function destroy(ficha $ficha)
    {
        //
    }
}
