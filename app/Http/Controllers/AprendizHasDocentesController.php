<?php

namespace App\Http\Controllers;

use App\models\aprendizHasDocentes;
use Illuminate\Http\Request;
use App\models\aprendizHasProgramas;

class AprendizHasDocentesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $adoce = $request->id;
        $formacion = (int)$request->input('idFormacion');

        $aprendices =  AprendizHasProgramas::where('idFormacion',$formacion)->get();



        if(!$adoce == null){

            $cant = count($adoce);
        }else{

            $message = array(
                'message' => 'Debe seleccionar al menos un Docente',
                'alert-type' => 'error',
            );

         return redirect('/docente')->with($message);

        }

        foreach($adoce as $i){
            foreach($aprendices as $aprendiz ){
                aprendizHasDocentes::updateOrCreate(
                    [
                        "idDocente" => $i,
                        "idAprendizHasPrograma" => $aprendiz->id,

                    ],
                    [
                        "idDocente" => $i,
                        "idAprendizHasPrograma" => $aprendiz->id,

                    ]);
                }

            }


        $message = array(
            'message' => 'Docentes asociados a la ficha'.' con éxito',
            'alert-type' => 'success',
        );

        return redirect('/formacion')->with($message);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\models\aprendizHasDocentes  $aprendizHasDocentes
     * @return \Illuminate\Http\Response
     */
    public function show(aprendizHasDocentes $aprendizHasDocentes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\aprendizHasDocentes  $aprendizHasDocentes
     * @return \Illuminate\Http\Response
     */
    public function edit(aprendizHasDocentes $aprendizHasDocentes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\aprendizHasDocentes  $aprendizHasDocentes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, aprendizHasDocentes $aprendizHasDocentes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\aprendizHasDocentes  $aprendizHasDocentes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {

        $id = $request->input('idFormacion');

        $selected = $request->input('idDocente',[]);

        AprendizHasDocentes::whereIn('idDocente',$selected)
        ->join('aprendiz_has_programas','aprendiz_has_docentes.idAprendizHasPrograma','aprendiz_has_programas.id')
        ->where('aprendiz_has_programas.idFormacion',$id)
        ->delete();

        $message = array(
            'message' => 'Docentes removidos con éxito',
            'alert-type' => 'success',
        );

        return back()->with($message);

    }
}
