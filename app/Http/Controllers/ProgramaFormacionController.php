<?php

namespace App\Http\Controllers;

use App\models\programaFormacion;
use Illuminate\Http\Request;
use App\models\familia;
use App\models\tipo_formacion;
use App\models\nivel;
use App\models\competencia;
use Illuminate\Support\Facades\DB;
use Excel;
use App\models\tipoCompetencia;

class ProgramaFormacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     *
     */


    public function import(Request $request)
    {

        $rules = [
            'file-selected' => 'required|mimes:xls,xlsx'
        ];

        $message = array(
            'message' => 'Importación de datos realizada con éxito',
            'alert-type' => 'success'
        );

        $this->validate($request, $rules, $message);

        $path = $request->file('file-selected')->getRealPath();

        $data = Excel::load($path)->get();

        // $area = null;

        // Se imprime la data para obtener los encabezados("heading") del Excel y su respectiva información ("items")
        // dd($data);

        if ($data->count() > 0) {

            foreach ($data as $key => $value) {

                // dd($value);

                $fami = familia::select('familia.id')
                ->where('familia.nombre',$value['familia'])
                ->first();

                $nive = nivel::select('nivel.id')
                ->where('nivel.nombre',$value['nivel'])
                ->first();
                $forma = tipo_formacion::select('tipo_formacion.id')
                ->where('tipo_formacion.nombre',$value['tipo_de_formacion'])
                ->first();

                $last = programaFormacion::all()->last();

                $insert_data = array(
                'id' => $last['id'] + 1,
                'nombre' => $value['nombre'],
                'idFamilia'=> $fami['id'],
                'codigo'=>$value['codigo'],
                'version'=>$value['version'],
                'idNivel'=>$nive['id'],
                'idTipo_formacion' => $forma['id'],
                "created_at" =>  date('Y-m-d h:i:s'),
                "updated_at" => null
            );



                // Se intenta agregar a la base de datos dentro de una estructura try-catch,
                // para excepcionar el error de duplicidad de datos y agregar solo los que no están
                try {
                    DB::table('programa_formacion')->insert($insert_data);
                } catch (\Throwable $th) {
                    // dd($th);
                }
            }

            if (empty($insert_data)) {
                $message = array(
                    'message' => 'Error al importar datos',
                    'alert-type' => 'error'
                );
            }
        }

        return redirect('/programa')->with($message);

        $message = array(
            'message' => 'Error al importar datos',
            'alert-type' => 'error'
        );
        return redirect('/programa')->with($message);
    }



    public function index()
    {

        $titulada=programaFormacion::select('programa_formacion.*')
        ->where ('idTipo_formacion', 1)->get();

        $complementaria=programaFormacion::select('programa_formacion.*')
        ->where ('idTipo_formacion', 2)->get();

        $poblacion=programaFormacion::select('programa_formacion.*')
        ->where ('idTipo_formacion', 3)->get();

        $competencias=programaFormacion::select('programa_formacion.*')
        ->where ('idTipo_formacion', 4)->get();

        $media=programaFormacion::select('programa_formacion.*')
        ->where ('idTipo_formacion', 5)->get();

        $programa=programaFormacion::all();
        $nivel=nivel::all();
        $familia=familia::all();
        $tipoFormacion=tipo_formacion::all();
        $cant = programaFormacion::count();
        $competencia = competencia::all();
        $tipoC = tipoCompetencia::all();

        return view ('programasFormacion.index')->with(compact('programa','nivel','familia','tipoFormacion','cant','competencia','complementaria','titulada','poblacion','competencias','media','tipoC'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {

        $nivel=nivel::all();
        $familia=familia::all();
        $tipoFormacion=tipo_formacion::all();

        return view ('programasFormacion.create')->with(compact('nivel','familia','tipoFormacion'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules=[
            'nombre' => 'required|string',
            'idFamilia'=>'',
            'codigo'=>'required|string',
            'version'=>'required|string',
            'idNivel'=>'required',
            'idTipo_formacion'=>'required'

        ];

        $message = array(
            'message' => 'Programa registrado con éxito',
            'alert-type' => 'success'
        );

        $this->validate($request, $rules, $message);


        programaFormacion::insert($request->except('_token'));


        return redirect('/programa')->with($message);


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\programaFormacion  $programaFormacion
     * @return \Illuminate\Http\Response
     */
    public function show(programaFormacion $programaFormacion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\programaFormacion  $programaFormacion
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $programa=programaFormacion::findOrFail($id);
        $nivel=nivel::all();
        $familia=familia::all();
        $tipoFormacion=tipo_formacion::all();
        return view ('programasFormacion.edit',compact('programa','nivel','familia','tipoFormacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\programaFormacion  $programaFormacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
       $programa=programaFormacion::findOrFail($id);

       $rules=[
        'nombre' => 'required|string',
        'idFamilia'=>'',
        'codigo'=>'required|string',
        'version'=>'required|string',
        'idNivel'=>'required',
        'idTipo_formacion'=>'required'

    ];
    $message = array(
        'message' => 'Programa actualizado con éxito',
        'alert-type' => 'success'
    );

    $this->validate($request, $rules, $message);


    $programa->update($request->all());

    return redirect('/programa')->with($message);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\programaFormacion  $programaFormacion
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {



    }
}
