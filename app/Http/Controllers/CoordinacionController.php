<?php

namespace App\Http\Controllers;

use App\models\Colegio;
use App\models\coordinacion;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use \Validator;

class CoordinacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function save(Request $request)
    {

        $rules = [
            'nombre' => 'required|string',
            'apellido' => 'required|string',
            'telefono' => 'required|integer',
            'correo' => 'required|email|string|unique:coordinacion,correo',
        ];

        $error = Validator::make($request->all(), $rules);
        $last = coordinacion::all()->last();

        if ($error->fails()) {

        }

        $form_data = array(
            'id' => $last['id'] + 1,
            'nombre' => $request->nombre,
            'apellido' => $request->apellido,
            'telefono' => $request->telefono,
            'correo' => $request->correo,
        );

        coordinacion::create($form_data);
        $data = coordinacion::where('id', $last['id'] + 1)->get();

        return response()->json(['success' => 'Coordinador agregado con éxito.', 'data' => $data]);
    }

    public function import(Request $request)
    {

        $rules = [
            'file-selected' => 'required|mimes:xls,xlsx',
        ];

        $message = array(
            'message' => 'Importación de datos realizada con éxito',
            'alert-type' => 'success',
        );

        $this->validate($request, $rules, $message);

        //
        $path = $request->file('file-selected')->getRealPath();

        $data = Excel::load($path)->get();

        // $area = null;

        // Se imprime la data para obtener los encabezados("heading") del Excel y su respectiva información ("items")
        // dd($data);

        if ($data->count() > 0) {
            foreach ($data as $key => $value) {

                $last = coordinacion::all()->last();

                $insert_data = array(

                    
                    'nombre' => $value['nombre'],
                    'apellido' => $value['apellido'],
                    'telefono' => $value['telefono'],
                    'correo' => $value['correo'],
                    "created_at" => date('Y-m-d h:i:s'),
                    "updated_at" => null,
                );

                // Se intenta agregar a la base de datos dentro de una estructura try-catch,
                // para excepcionar el error de duplicidad de datos y agregar solo los que no están
                try {
                    DB::table('coordinacion')->insert($insert_data);
                } catch (\Throwable $th) {
                    // dd($th);
                }
            }

            if (empty($insert_data)) {
                $message = array(
                    'message' => 'Error al importar datos',
                    'alert-type' => 'error',
                );
            }
        }

        return redirect('/coordinador')->with($message);

        $message = array(
            'message' => 'Error al importar datos',
            'alert-type' => 'error',
        );
        return redirect('/coordinador')->with($message);
    }

    public function index()
    {
        //
        $coordinador = coordinacion::withTrashed()->get();
        $cant = coordinacion::withTrashed()
        ->count();
        return view('coordinadores.index')->with(compact('coordinador', 'cant'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        // $doc = tipoDoc::all();
        // $area = area::all();
       
        return view('coordinadores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre' => 'required|string',
            'apellido' => 'required|string',
            'telefono' => 'required|integer',
            'correo' => 'required|email|string|unique:coordinacion,correo',
        ];

        $message = array(
            'message' => 'Coordinador registrado con éxito',
            'alert-type' => 'success',
        );

        $this->validate($request, $rules, $message);

        coordinacion::insert($request->except('_token'));

        return redirect('/coordinador')->with($message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\coordinacion  $coordinacion
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $colegio = Colegio::where('idcoordinacion', $id)->get();

        return response()->json($colegio);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\coordinacion  $coordinacion
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        // $doc = tipoDoc::all();
        // $area = area::all();
        $coordi = coordinacion::findOrFail($id);

        return view('coordinadores.edit', compact('coordi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\coordinacion  $coordinacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $coordinador = coordinacion::findOrFail($id);
        $rules = [
            'nombre' => 'required|string',
            'apellido' => 'required|string',
            'telefono' => 'required|integer',
            'correo' => 'required|email|string|unique:coordinacion,correo,' . $id,

        ];

        $message = array(
            'message' => 'Coordinador actualizado con éxito',
            'alert-type' => 'success',
        );

        $this->validate($request, $rules, $message);

        $coordinador->update($request->all());

        return redirect('/coordinador')->with($message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\coordinacion  $coordinacion
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        coordinacion::destroy($id);

        $message = array(
            'message' => 'Coordinador cambiado de estado con éxito',
            'alert-type' => 'success',
        );

        return redirect('/coordinador')->with($message);
    }

    public function restore($id)
    {

        coordinacion::withTrashed()->find($id)->restore();

        $message = array(
            'message' => 'Coordinador cambiado de estado con éxito',
            'alert-type' => 'success',
        );

        return back()->with($message);

    }
}
