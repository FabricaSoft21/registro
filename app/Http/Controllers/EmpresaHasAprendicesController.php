<?php

namespace App\Http\Controllers;

use App\models\EmpresaHasAprendices;
use App\models\AprendizHasProgramas;
use Illuminate\Http\Request;

class EmpresaHasAprendicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
            $ids = $request->id;
            $formacion = (int)$request->input('idFormacion');
    
            $aprendices =  AprendizHasProgramas::where('idFormacion',$formacion)->get();
         
            if(!$ids == null){
    
                $cant = count($ids);
            }else{
    
                $message = array(
                    'message' => 'Debe seleccionar al menos una empresa',
                    'alert-type' => 'error',
                );
    
             return redirect('/empresa')->with($message);
    
            }
    
            foreach($ids as $i){
                foreach($aprendices as $aprendiz ){
                    EmpresaHasAprendices::updateOrCreate(
                        [
                            "idEmpresa" => $i,
                            "idAprendizHasPrograma" => $aprendiz->id,
    
                        ],
                        [
                            "idEmpresa" => $i,
                            "idAprendizHasPrograma" => $aprendiz->id,
    
                        ]);
                    }

                }
    
            $message = array(
                'message' => 'Empresa asociada a la ficha'.' con éxito',
                'alert-type' => 'success',
            );
    
            return redirect('/formacion')->with($message);
        }
        
    /**
     * Display the specified resource.
     *
     * @param  \App\models\EmpresaHasAprendices  $empresaHasAprendices
     * @return \Illuminate\Http\Response
     */
    public function show(EmpresaHasAprendices $empresaHasAprendices)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\EmpresaHasAprendices  $empresaHasAprendices
     * @return \Illuminate\Http\Response
     */
    public function edit(EmpresaHasAprendices $empresaHasAprendices)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\EmpresaHasAprendices  $empresaHasAprendices
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmpresaHasAprendices $empresaHasAprendices)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\EmpresaHasAprendices  $empresaHasAprendices
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)

        {

            $id = $request->input('idFormacion');
    
            $selected = $request->input('idEmpresa', []);
                
           EmpresaHasAprendices::whereIn('idEmpresa',$selected)
            ->join("aprendiz_has_programas","empresa_has_aprendices.idAprendizHasPrograma","aprendiz_has_programas.id")
            ->where('aprendiz_has_programas.idFormacion', $id)
            ->delete();

            $message = array(
                'message' => 'Empresas removidas con éxito',
                'alert-type' => 'success'
            );
    
            return back()->with($message);
    
        }

}
