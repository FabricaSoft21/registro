<?php

namespace App\Http\Controllers;

use App\models\Colegio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use\App\models\rector;
use\App\models\CheckList;
use\App\models\coordinacion;
use\App\models\municipio;
use\App\models\departamento;
use\App\models\estado_colegio;
use\App\models\categoria_colegio;
use App\models\tipo_colegio;
use App\models\colegioHasCheckList;

use Excel;
use Illuminate\Support\Facades\DB;

class ColegioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function import(Request $request)
    {

        $rules = [
            'file-selected' => 'required|mimes:xls,xlsx',
        ];

        $message = array(
            'message' => 'Importación de datos realizada con éxito',
            'alert-type' => 'success',
        );

        $this->validate($request, $rules, $message);

        $path = $request->file('file-selected')->getRealPath();

        $data = Excel::load($path)->get();

        // try {
        if ($data->count() > 0) {

            foreach ($data as $key => $value) {

                $idDepartamento = departamento::select('departamento.id')
                    ->where('departamento.nombre', $value['departamento'])
                    ->first();

                $idMunicipio = municipio::select('municipios.id')
                    ->where('municipios.nombre', $value['municipio'])
                    ->first();

                $idCategoria = categoria_colegio::select('categoria_colegio.id')
                    ->where('categoria_colegio.nombre', $value['categoria'])
                    ->first();

                $idCoordinador = coordinacion::select('coordinacion.id')
                    ->whereRaw("CONCAT(`nombre`,' ',`apellido`) = ?", [$value['coordinador']])
                    ->first();

                $idRector = rector::select('rector.id')
                    ->whereRaw("CONCAT(`nombre`,' ',`apellido`) = ?",  $value['rector'])
                    ->first();

                $idTipo = tipo_colegio::select('tipo_colegios.id')
                    ->where('tipo_colegios.nombre', $value['tipo_de_institucion'])
                    ->first();

                    if ($idDepartamento==null) {
                        $message = array(
                            'message' => 'Departamento no encontrado',
                            'alert-type' => 'error'
                        );
                        return redirect('/instiEdu')->with($message);                    
                        
                    } elseif ($idMunicipio==null){
                        $message = array(
                            'message' => 'Municipio no encontrado',
                            'alert-type' => 'error'
                        );
                        return redirect('/instiEdu')->with($message);

                    }elseif ($idCategoria==null){
                        $message = array(
                            'message' => 'Categoría del colegio no encontrada',
                            'alert-type' => 'error'
                        );
                        return redirect('/instiEdu')->with($message);

                    }elseif($idCoordinador == null){
                        $message = array(
                            'message' => 'Coordinador no encontrado',
                            'alert-type' => 'error'
                        );
                        return redirect('/instiEdu')->with($message);

                    }elseif ($idRector==null) {
                        $message = array(
                            'message' => 'Rector no encontrado',
                            'alert-type' => 'error'
                        );
                        return redirect('/instiEdu')->with($message);

                    }elseif ($idTipo==null) {
                        $message = array(
                            'message' => 'Tipo de colegio no encontrado',
                            'alert-type' => 'error'
                        );
                        return redirect('/instiEdu')->with($message);
                    }
                    

                $last = Colegio::all()->last();

                $insert_data = array(

                    'id' => $last['id'] + 1,
                    'nit'=>$value['nit'],
                    'codigoDane'=>$value['codigo_dane'],
                    'nombreColegio' => $value['nombre'],
                    'direccion' => $value['direccion'],
                    'correo' => $value['correo'],
                    'numResolucion' => $value['numero_resolucion'],
                    'telefono' => $value['telefono'],
                    'idDepartamento'=>$idDepartamento['id'],
                    'idMunicipio'=>$idMunicipio['id'],
                    'idCategoria'=>$idCategoria['id'],
                    'idCoordinacion'=>$idCoordinador['id'],
                    'idRector' => $idRector['id'],
                    'idTipo' => $idTipo ['id'],
                    "created_at" => date('Y-m-d h:i:s'),
                    "updated_at" => null
                );

                try {
                    DB::table('colegio')->insert($insert_data);
                } catch (\Throwable $th) {
                    // dd($th);
                }
            }

            if (empty($insert_data)) {
                $message = array(
                    'message' => 'Error al importar datos',
                    'alert-type' => 'error'
                );
            }
        }

        return redirect('/instiEdu')->with($message);

        $message = array(
            'message' => 'Error al importar datos',
            'alert-type' => 'error'
        );
        return redirect('/instiEdu')->with($message);
    }
    public function index()
    {
        //
        $lista=CheckList::all();
        $checked = colegioHasCheckList::all();
        $institucion=Colegio::withTrashed()->get();
        $rector=rector::all();
        $coordinador = coordinacion::all();
        $municipio=municipio::all();
        $categoriaCole=categoria_colegio::all();
        $tipo=tipo_colegio::all();
        $cant = Colegio::withTrashed()
        ->count();
        return view ('colegios.index')->with (compact('lista','institucion','cant','rector','coordinador','municipio','categoriaCole','tipo','checked'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function list($id, Request $request)
    {
        $preguntas = Input::get('pregunta');
        $checked = Input::get('check');
    
        $cant = count($preguntas);
        
        for ($i = 0; $i < $cant; $i++) {

            colegioHasCheckList::updateOrCreate(
                ["idCheckList" => $preguntas[$i],
                "idColegio" => $request->idColegio],
 
                [
                "idColegio" => $request->idColegio,
                "idCheckList" => $preguntas[$i],
                "state" => 0
                
            ]);   
            
            if($checked != null){
                colegioHasCheckList::whereIn('idCheckList',$checked)
                ->where('idColegio',$id)
                ->update(
                    [
                    "state" => 1
                ]); 
            }
                   
        }

        $message = array(
            'message' => 'Lista de chequeo actualizada con éxito',
            'alert-type' => 'success'
        );

        return back()->with($message);

    }


    public function getList($id)
    {

        $list = colegioHasCheckList::select('colegio_has_check_lists.*','check_list.id as idPregunta')
        ->join('check_list','colegio_has_check_lists.idCheckList','check_list.id')
        ->where('colegio_has_check_lists.idColegio',$id)
        ->get();

        return response()->json($list);

    }

    public function create()
    {
        
        $rector=rector::all();
        $coordinador = coordinacion::all();
        $departamento=departamento::all();
        $municipio=municipio::all();
        // $estadoColegio=estado_colegio::all();
        $categoriaCole=categoria_colegio::all();
        $tipo=tipo_colegio::all();
        return view('colegios.create')->with(compact('rector','coordinador','municipio','categoriaCole','tipo','departamento'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function updateList(Request $request, $id)
    {
        
    }
    public function store(Request $request)
    {
  
        $rules =[
            'nit' => 'required|string|unique:colegio,nit',
            'codigoDane' => 'required|string|unique:colegio,codigoDane',
            'nombreColegio' => 'required|string',
            'direccion' => 'required|string',
            'correo' => 'required|email|string|unique:colegio,correo',
            'numResolucion' => 'required|integer',
            'telefono'=>'required|integer',
            'idMunicipio'=>'required',
            // 'idEstado'=>'required',
            'idCategoria'=>'required',
            'idcoordinacion'=>'required',
            'idRector'=>'required',
            'idTipo'=>'required|nullable'
        ];

        $message = array(
            'message' => 'Institución registrada con éxito',
            'alert-type' => 'success'
        );
        
        $this->validate($request, $rules, $message);


        Colegio::create($request->all());

        return redirect('/instiEdu')->with($message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function show(Colegio $colegio)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function edit(Colegio $colegio, $id)
    {
        $colegio = Colegio::findOrFail($id);

        $rector=rector::all();
        $coordinador = coordinacion::all();
        $departamento=departamento::all();
        $municipio=municipio::all();
        // $estadoColegio=estado_colegio::all();
        $categoriaCole=categoria_colegio::all();
        $tipo=tipo_colegio::all();
        
        return view('colegios.edit',compact('rector','coordinador','municipio','categoriaCole','colegio','tipo','departamento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $colegio = Colegio::findOrFail($id);

        $rules =[
            'nit' => 'required|string|unique:colegio,nit,'.$id,
            'codigoDane' => 'required|string|unique:colegio,codigoDane,'.$id,
            'nombreColegio' => 'required|string',
            'direccion' => 'required|string',
            'correo' => 'required|string|unique:colegio,correo,'.$id,
            'numResolucion' => 'required|integer',
            'telefono'=>'required|integer',
            'idMunicipio'=>'required',
            // 'idEstado'=>'required',
            'idCategoria'=>'required',
            'idcoordinacion'=>'required',
            'idRector'=>'required',
            'idTipo'=>'required',
        ];
    
        $message = array(
            'message' => 'Institución actulizada con éxito',
            'alert-type' => 'success'
        );
        
        $this->validate($request, $rules, $message);

        $colegio->update($request->all());

        return redirect('/instiEdu')->with($message);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Colegio  $colegio
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Colegio::destroy($id);

        $message = array(
            'message' => 'Institución educativa cambiado de estado con éxito',
            'alert-type' => 'success',
        );

        return redirect('/instiEdu')->with($message);
    }

    public function restore($id){

        Colegio::withTrashed()->find($id)->restore();

        $message = array(
            'message' => 'Institución educativa cambiado de estado con éxito',
            'alert-type' => 'success',
        );

        return back()->with($message);

    }
}
