<?php

namespace App\Http\Controllers;

use App\models\Aprendiz;
use App\models\aprendizHasProgramas;
use Illuminate\Http\Request;

class AprendizHasProgramasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //|
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $ids = $request->id;

        if ($ids != null) {

            $cant = count($ids);
        } else {

            $message = array(
                'message' => 'Debe seleccionar al menos un instructor',
                'alert-type' => 'error',
            );

            return redirect('/aprendiz')->with($message);
        }

        $countAprendices = AprendizHasProgramas::distinct()
            ->join("aprendiz", "aprendiz_has_programas.idAprendiz", "aprendiz.id")
            ->join('estado_aprendiz', 'aprendiz.idestado', 'estado_aprendiz.id')
            ->join("formacion", "aprendiz_has_programas.idFormacion", "formacion.id")
            ->join("ficha", "formacion.idFicha", "ficha.id")
            ->join('programa_formacion', 'ficha.idProgramaFormacion', 'programa_formacion.id')
            ->where('aprendiz.idestado', '!=', 2)
            ->where('programa_formacion.idTipo_formacion', 2)
            ->Orwhere('programa_formacion.idTipo_formacion', 4)
            ->whereIn('aprendiz_has_programas.idAprendiz', $ids)
            ->count();

        if ($countAprendices >= 3 && $request->input('idCurso') != null)  {
            $message = array(
                'message' => 'El aprendiz seleccionado ya pertenece tres o más cursos.',
                'alert-type' => 'error',
            );

            return back()->with($message);

        } else {

            for ($i = 0; $i < $cant; $i++) {

                if ($request->input('idFormacion') != null && auth()->user()->selected_category_id == 1 || $request->input('idFormacion') != null && auth()->user()->selected_category_id == 3 || $request->input('idFormacion') != null && auth()->user()->selected_category_id == 5) {
                    aprendizHasProgramas::updateOrCreate(
                        ['idAprendiz' => $ids[$i]],
                        [
                            "idAprendiz" => $ids[$i],
                            "idFormacion" => $request->input('idFormacion'),

                        ]
                    );

                    $message = array(
                        'message' => 'Aprendices asociados a la ficha' . ' con éxito',
                        'alert-type' => 'success',
                    );

                } else if ($request->input('idCurso') != null && auth()->user()->selected_category_id == 1 || $request->input('idFormacion') != null && auth()->user()->selected_category_id == 3 || $request->input('idFormacion') != null && auth()->user()->selected_category_id == 5) {
                    aprendizHasProgramas::updateOrCreate(
                        [
                            'idAprendiz' => $ids[$i],
                            "idFormacion" => $request->input('idCurso'),
                        ],
                        [
                            "idAprendiz" => $ids[$i],
                            "idFormacion" => $request->input('idCurso'),

                        ]
                    );

                    $message = array(
                        'message' => 'Aprendices agregados al curso' . ' con éxito',
                        'alert-type' => 'success',
                    );

                } else if (auth()->user()->selected_category_id == 2 || auth()->user()->selected_category_id == 4) {
                    aprendizHasProgramas::updateOrCreate(
                        [
                            'idAprendiz' => $ids[$i],
                            "idFormacion" => $request->input('idFormacion'),
                        ],
                        [
                            "idAprendiz" => $ids[$i],
                            "idFormacion" => $request->input('idFormacion'),

                        ]
                    );

                    $message = array(
                        'message' => 'Aprendices agregados al curso' . ' con éxito',
                        'alert-type' => 'success',
                    );

                }

            }

        }

        Aprendiz::whereIn('id', $ids)
            ->update([
                'idestado' => 2,
            ]);

        return back()->with($message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\aprendizHasProgramas  $aprendizHasProgramas
     * @return \Illuminate\Http\Response
     */
    public function show(aprendizHasProgramas $aprendizHasProgramas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\aprendizHasProgramas  $aprendizHasProgramas
     * @return \Illuminate\Http\Response
     */
    public function edit(aprendizHasProgramas $aprendizHasProgramas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\aprendizHasProgramas  $aprendizHasProgramas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, aprendizHasProgramas $aprendizHasProgramas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\aprendizHasProgramas  $aprendizHasProgramas
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {

        $id = $request->input('idFormacion');

        $selected = $request->input('idAprendiz', []);

        try {
            aprendizHasProgramas::whereIn('idAprendiz', $selected)
                ->join('aprendiz', 'aprendiz.id', 'aprendiz_has_programas.idAprendiz')
                ->where('idFormacion', $id)
                ->update([
                    'aprendiz.idestado' => 3,
                ]);

            // aprendizHasProgramas::whereIn('idAprendiz', $selected)
            // ->where('idFormacion', $id)
            // ->delete();

            $message = array(
                'message' => 'Aprendices removidos con éxito',
                'alert-type' => 'success',
            );
        } catch (\Throwable $th) {

            $message = array(
                'message' => 'Hubo un error al remover los aprendices, intentelo más tarde.',
                'alert-type' => 'error',
            );
        }

        return back()->with($message);
    }
}
