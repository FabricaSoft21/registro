<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class listaChequeo extends Model
{
    //

    protected $table = "lista_chequeos";

    protected $fillable = [

        'pregunta',
        'repuesta'

    ];
    
}

