<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class programaFormacion extends Model
{
    protected $table = "programa_formacion";

    protected $fillable = [
        'codigo',
        'idNivel',
        'idFamilia',
        'idTipo_formacion',
        'version',
        'nombre',
        'idArea'
    ];

    public function familia(){
        return $this->belongsTo('App\models\familia','idFamilia');
    }

    public function nivel(){
        return $this->belongsTo('App\models\nivel','idNivel');
    }

    public function typeF(){
        return $this->belongsTo('App\models\tipo_formacion','idTipo_formacion');
    }

}
