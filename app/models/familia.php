<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class familia extends Model
{
    protected $table = "familia";

    protected $fillable = [

        'nombre'
    ];
}
