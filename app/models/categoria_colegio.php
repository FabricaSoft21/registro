<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class categoria_colegio extends Model
{
    protected $table='categoria_colegio';
    protected $fillable = [
        'nombre'
    ];
}
