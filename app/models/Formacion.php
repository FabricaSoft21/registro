<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Formacion extends Model
{
    protected $table = "formacion";

    protected $fillable = [

        'idFicha',
        'fechaInicio',
        'fechaFinal',
        'jornada',
        'horaInicio',
        'horaFinal',
        'fechaInicioProd',
        'idEstado'

    ];

    public function ficha(){
        return $this->belongsTo('App\models\ficha','idFicha');

    }

    public function estado(){
        return $this->belongsTo('App\models\estadoFomarcion','idEstado');

    }

    public $timestamps = false;

}
