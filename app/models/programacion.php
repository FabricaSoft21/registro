<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class programacion extends Model
{
    
    protected $table = "programacion";
    protected $fillable = [

            'idinstructor',
            'idFormacion',
            'fechaInicio',
            'fechaFinal',
            'horaInicio',
            'horaFinal',
            'lugar',
            'idColegio',
            'idEmpresa'
    ];
    
    public function instructor(){
        return $this->belongsTo('App\models\instructor','idinstructor');
    }
    
    public function formacion(){
        return $this->belongsTo('App\models\programaFormacion','idFormacion');
    }
    
    public function colegio(){
        return $this->belongsTo('App\models\Colegio','idColegio');
    }
    
    public function empresa(){
        return $this->belongsTo('App\models\empresa','idEmpresa');
    }
    
}
