<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class ficha extends Model
{
    protected $table = "ficha";

    public $timestamps = false;

    protected $fillable = [

        'nombreFicha',
        'codigo',
        'idProgramaFormacion',
        'idTipoFicha'
      
    ];
        
    public function programa(){
        return $this->belongsTo('App\models\programaFormacion','idProgramaFormacion');
    }

    public function estado(){
        return $this->belongsTo('App\models\tipo_ficha','idTipoFicha');
    }

}
