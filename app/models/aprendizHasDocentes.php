<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class aprendizHasDocentes extends Model
{
    //
    protected $table= 'aprendiz_has_docentes';
    protected $fillable= ['idAprendizHasPrograma', 'idDocente'];
}
