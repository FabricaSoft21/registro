<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class tipoDoc extends Model
{
    protected $table = 'tipo_doc';

    protected $fillable = [
        'nombre'
    ];

    public function user(){
        return $this->belongsTo('App\models\Aprendiz','idTipoDoc');
    }

}
