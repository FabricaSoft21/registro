<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class EmpresaHasAprendices extends Model
{
  protected $table = "empresa_has_aprendices";
  
  protected $fillable = ['idEmpresa','idAprendizHasPrograma'];

}
