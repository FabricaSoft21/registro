<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class instructorHasformacion extends Model
{
    protected $table = "instructor_hasformacions";

    protected $fillable = [

        'idInstructor',
        'idAprendizHasPrograma',
        'idTipoInstructor'
    ];
    
}
