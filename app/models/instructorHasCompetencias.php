<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class instructorHasCompetencias extends Model
{

    protected $table = "instructor_has_competencias";

    protected $fillable =
    [
        'idCompetencia',
        'idInstructor'
    ];

}
