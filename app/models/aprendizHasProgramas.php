<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class aprendizHasProgramas extends Model
{
    protected $table = "aprendiz_has_programas";

    protected $fillable = [

        'idAprendiz',
        'idFormacion'

    ];

    public $timestamps = false;
    
}
