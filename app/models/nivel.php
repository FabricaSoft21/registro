<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class nivel extends Model
{
    protected $table = "nivel";

    protected $fillable = [

        'nombre'
    ];

}
