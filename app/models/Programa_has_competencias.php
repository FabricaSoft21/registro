<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Programa_has_competencias extends Model
{
    protected $fillable = [
        
        'idPrograma',
        'idCompetencia'

    ];
}
