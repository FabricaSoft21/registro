<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class rector extends Model
{

    use SoftDeletes;

    protected $table = 'rector';
     protected $fillable = [

        'nombre',
        'apellido',
        'telefono',
        'correo'
     ];

}
