<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;
class Aprendiz extends Model
{
    protected $table = 'aprendiz';
    protected $fillable = [
        'nombre',
        'apellido',
        'fechaNacimiento',
        'idTipoDoc',
        'numDoc',
        'idestado',
        'correo',
        'telefono',
        'direccion',
        'nombreAcudiente',
        'telAcudiente',
        'idColegio',
        'idGrado',
        'tipoAprendiz'
    ];

    
    public function type(){
        return $this->belongsTo('App\models\tipoDoc','idTipoDoc');
    }

    public function state(){
        return $this->belongsTo('App\models\estadoAprendiz','idestado');
    }

    public function typeA(){
        return $this->belongsTo('App\models\tipo_aprendiz','idtipoAprendiz');

    }

    public function school(){
        return $this->belongsTo('App\models\Colegio','idColegio');

    }

    public function grade(){
        return $this->belongsTo('App\models\grado','idGrado');

    }

}