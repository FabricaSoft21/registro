<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class estado extends Model
{
    protected $table = 'estado';

    protected $fillable = [

        'nombre',
        'descripcion'
    ];
}
