<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class EmpresaHasPoblacion extends Model
{
    //
    protected $table = 'empresa_has_poblacion';

    protected $fillable = ['idEmpresa','idPoblacion'];
}
