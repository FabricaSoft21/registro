<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class ambiente extends Model
{
    protected $table ='ambiente';
    protected $fillable = [
        
        'idSede',
        'piso',
        'numero_ambiente',
        'idArea',
        'pantalla',
        'can_equipos',
        'can_sillas',
        'can_mesas',

    ];
}
