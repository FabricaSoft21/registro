<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class tipoDiscapacidad extends Model
{
    protected $table = "tipo_discapacidad";

    protected $fillable = [
        'nombre',
        'descripcion'
    ];

}
