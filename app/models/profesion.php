<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class profesion extends Model
{
    //
    protected $table = 'profesion';

    protected $fillable =[

        'nombre',
        'descripcion'

    ];
}
