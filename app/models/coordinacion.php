<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class coordinacion extends Model
{

    use SoftDeletes;

    protected $table = 'coordinacion';

    protected $fillable = [

        'nombre',
        'apellido',
        'telefono',
        'correo'
    ];


}
