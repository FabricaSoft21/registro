<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class poblacionVulne extends Model
{
    //

    protected $table='poblacion_vulnes';

    protected $fillable = [
        'metaAprendiz',
        'ejecAprendiz',
        'metaCupos',
        'ejecCupos'
    ];

}
