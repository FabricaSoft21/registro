<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class transversal extends Model
{
    
    protected $table = 'transversal';

    protected $fillable = [

        'codigo',
        'horas',
        'nombre'


    ];

}
