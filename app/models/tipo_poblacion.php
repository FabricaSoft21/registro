<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class tipo_poblacion extends Model
{
    protected $table= 'tipo_poblacion';

    protected $fillable = [
        'nombre'
    ];
    
}
