<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class docente extends Model
{
    use SoftDeletes;
    
    protected $table = 'docente';

    protected $fillable = [

         'nombre',
         'apellido',
         'correo',
         'idcolegio',
         'idprograma',
         'telefono',

    ];
    public function colegio(){
        return $this->belongsTo('App\models\colegio','idcolegio');
    }

    public function programa(){
        return $this->belongsTo('App\models\programaFormacion','idprograma');
    }
}
