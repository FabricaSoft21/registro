<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class contrato extends Model
{
    protected $table = 'contrato';

    protected $fillable = [
        
        'nombre'
    ];
}
