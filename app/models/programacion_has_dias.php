<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class programacion_has_dias extends Model
{
    //
    protected $fillable = [
        
        'idProgramacion',
        'idDia'

    ];
}
