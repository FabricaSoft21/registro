<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class sede extends Model
{
    //
    protected $table ='sede';

    protected $fillable = [

        'nombre',
        'direccion',
        'idMunicipio',
        'codigo',
        'idTipo'


    ];
}
