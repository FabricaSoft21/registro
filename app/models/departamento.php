<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class departamento extends Model
{
    protected $table = 'departamento';
     protected $fillable = [

        'nombre'
     ];
}
