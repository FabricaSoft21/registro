<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class tipo extends Model
{
    //
    protected $table = 'tipo';

    protected $fillable = [
        'nombre'
    ];

}
