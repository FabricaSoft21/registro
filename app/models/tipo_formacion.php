<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class tipo_formacion extends Model
{
    protected $table = "tipo_formacion";
     protected $fillable = [

        'nombre'
     ];
}
