<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class municipio extends Model
{
    //
    protected $table = 'municipios';
     protected $fillable = [

        'nombre',
        'idDepartamento'
     ];
}
