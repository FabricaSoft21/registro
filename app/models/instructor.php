<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class instructor extends Model
{

    use SoftDeletes;

    protected $table = 'instructor';

    protected $fillable =[
        'idTipoDoc',
        'numDoc',
        'nombre',
        'apellido',
        'correoSena',
        'correoAlterno',
        'idDepartamento',
        'idMunicipio',
        'telefono',
        'celular',
        'fechaNaci',
        'idArea',
        'idContrato',
        'fechaInicioCo',
        'fechaFinalCo',
        'idProfesion'
        // 'idProgramaFormacion'
    ];
    public function tipoDoc(){
        return $this->belongsTo('App\models\tipoDoc','idTipoDoc');
    }
    public function municipio(){
        return $this->belongsTo('App\models\municipio','idMunicipio');
    }
    public function area(){
        return $this->belongsTo('App\models\area','idArea');
    }
    public function contrato(){
        return $this->belongsTo('App\models\contrato','idContrato');
        
    }
    public function profesion(){
        return $this->belongsTo('App\models\profesion','idProfesion');
    }
    // public function programaFormacion(){
    //     return $this->belongsTo('App\models\programaFormacion','idProgramaFormacion');
    
   
}
