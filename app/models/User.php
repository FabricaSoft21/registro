<?php

namespace App\models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Notifications\ResetPassword as ResetPasswordNotification;
use Cache;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','selected_category_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','selected_category_id',
    ];

    public function sendPasswordResetNotification($token)
    {
      $this->notify(new ResetPasswordNotification($token));
    }

    public function getListOfCategoriesAttribute() {

        if(auth()->user()->hasRole('administrador')){

            return Clasificacion::all();

        }else if(auth()->user()->hasRole('tecnica')){

            return Clasificacion::where('id',5)->get();

        }else if(auth()->user()->hasRole('poblaciones')){

            return Clasificacion::where('id',3)->get();

        }else if(auth()->user()->hasRole('instructor')){
            
            return Clasificacion::all();
        }
            
    }

    public function isOnline()
    {
        return Cache::has('user-is-online-' . $this->id);
    }

}
