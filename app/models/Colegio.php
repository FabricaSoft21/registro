<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Colegio extends Model
{
    protected $table ='colegio';

    use SoftDeletes;

    protected $fillable = [

        'nit',
        'codigoDane',
        'nombreColegio',
        'direccion',
        'correo',
        'numResolucion',
        'idDepartamento',
        'idMunicipio',
        'idCategoria',
        'idcoordinacion',
        'idRector',
        'telefono',
        'idTipo',
        'idLista'
        
    ];

    public function departamento(){
        return $this->belongsTo('App\models\departamento','idDepartamento');
    }

    public function municipio(){
        return $this->belongsTo('App\models\municipio','idMunicipio');
    }
    
    public function tipoColegio(){
        return $this->belongsTo('App\models\tipo_colegio','idTipo');
        
    }

    public function categoria(){
        return $this->belongsTo('App\models\categoria_colegio','idCategoria');
    }

    public function coordinador(){
        return $this->belongsTo('App\models\coordinacion','idcoordinacion');
    }

    public function rector(){
        return $this->belongsTo('App\models\rector','idRector');
    }

    public function lista(){
        return $this->belongsTo('App\models\listaChequeo','idLista');
    }
    
}
