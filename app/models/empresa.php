<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class empresa extends Model
{
    use SoftDeletes;
    
    protected  $table = "empresa";

    protected $fillable = [

        'nit',
        'nombre',
        'correo',
        'telefono',
        'nombreEncargado',
        'telefonoEncargado',
        'idTipoPoblacion'

    ];

    public function tipoPoblacion (){

        return $this->belongsTo('App\models\tipo_poblacion','idTipoPoblacion');

    }

}
