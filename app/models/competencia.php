<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class competencia extends Model
{

    protected $table = 'competencia';
    protected $fillable =[

        'nombre',
        'idTipoCompetencia'
    ];
}
