<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class tipoInstructor extends Model
{

    protected $table = "tipo_instructor";

    protected $fillable = [
        'nombre'
    ];

}
