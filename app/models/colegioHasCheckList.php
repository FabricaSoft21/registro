<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class colegioHasCheckList extends Model
{
    protected $fillable = [

        'idColegio',
        'idCheckList',
        'state'
    ];
}
