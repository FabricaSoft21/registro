<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class aprendizHasDiscapacidades extends Model
{
    protected $table = 'aprendiz_has_discapacidades';

    protected $fillable = ['idAprendiz','idDiscapacidad'];

}



