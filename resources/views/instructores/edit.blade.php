@extends('layouts.backend')
@section('content')
<div class="content">
                    <h2 class="content-heading">Instructores</h2>
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Modificar Instructor</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option">
                                    <i class="fa fa-gears"></i>
                                </button>
                            </div>
                        </div>
                        <div class="js-wizard-validation-material block">
                                <!-- Step Tabs -->
                                <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#wizard-validation-material-step1" data-toggle="tab">1. Personal</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#wizard-validation-material-step2" data-toggle="tab">2. Vinculación</a>
                                    </li>
                            
                                </ul>
                                <!-- END Step Tabs -->

                                <!-- Form -->
                                <form class="js-wizard-validation-material-form" action="" method="post">
                                {{ csrf_field() }}
                                    <!-- Steps Content -->
                                    <div class="block-content block-content-full tab-content" style="min-height: 267px;">
                                        <!-- Step 1 -->
                                        <div class="tab-pane active" id="wizard-validation-material-step1" role="tabpanel">
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="nombre" name="nombre" value="{{$instructor->nombre}}">
                                                    <label for="nombre">Nombres <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-vcard"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="apellido" name="apellido" value="{{$instructor->apellido}}">
                                                <label for="apellido">Apellidos <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-address-card-o"></i>
                                                    </span>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    <div class="form-material">
                                        <select class="js-select2 form-control" id="idTipoDoc" name="idTipoDoc" style="width: 100%;" data-allow-clear="true">
                                            <option value="">Seleccione tipo de documento</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                            @foreach($tipoDoc as $d)
                                            <option value="{{ $d->id }}" {{$d->id == $instructor->idTipoDoc ? 'selected' : ''}}>{{$d->nombre}}</option>
                                            @endforeach
                                        </select>
                                        <label for="idTipoDoc">Tipo de documento <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="numDoc" name="numDoc" value="{{$instructor->numDoc}}">
                                                <label for="telefono">Número de documento <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                    <i class="fa fa-slack"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="correoSena" name="correoSena" value="{{$instructor->correoSena}}">
                                                <label for="correoSena">Correo Sena <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-envelope-o"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                       
                                    
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="correoAlterno" name="correoAlterno" value="{{$instructor->correoAlterno}}">
                                                <label for="correoAlterno">Correo electrónico <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-envelope-o"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                                <div class="form-material">
                                                            <select class="js-select2 form-control" id="idDepartamento" name="idDepartamento" style="width: 100%;" data-allow-clear="true">
                                                                <option value="">Seleccione departamento </option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                                @foreach($departamento as  $depa)
                                                                <option value="{{ $depa->id }}" {{$depa->id == $instructor->idDepartamento ? 'selected' : ''}}>{{$depa->nombre}}</option>
                                                                @endforeach
                                                            </select>
                                                            <label for="idContrato">Departamento <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                        <div class="form-material">
                                                                    <select class="js-select2 form-control" id="idMunicipio" name="idMunicipio" style="width: 100%;" data-allow-clear="true">
                                                                        <option value="" selected>Seleccione Municipio</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                                        @foreach($municipio as  $muni)
                                                                        <option value="{{ $muni->id }}"{{$muni->id == $instructor->idMunicipio ? 'selected' : '' }} >{{$muni->nombre}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    <label for="idMunicipio">Municipio <span class="text-danger">*</span></label>
                                                            </div>
                                                        </div>
                                            
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="telefono" name="telefono" value="{{$instructor->telefono}}">
                                                <label for="telefono">Número de contacto <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-phone"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="celular" name="celular" value="{{$instructor->celular}}">
                                                <label for="celular">Número de celular <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                    <i class="si si-screen-smartphone"></i>
                                                  
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="tab-pane" id="wizard-validation-material-step2" role="tabpanel">
                                     
                                        <div class="form-group">
                                        <div class="form-material">
                                                    <select class="js-select2 form-control" id="idArea" name="idArea" style="width: 100%;" data-allow-clear="true">
                                                        <option value="" >Seleccione el Área</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                        @foreach($area as  $ar)
                                                        <option value="{{ $ar->id }}" {{$ar->id == $instructor->idArea ? 'selected' : ''}}>{{$ar->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="idArea">Área <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                        <div class="form-material">
                                                    <select class="js-select2 form-control" id="idContrato" name="idContrato" style="width: 100%;" data-allow-clear="true">
                                                        <option value=""> Tipo Vinculación</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                        @foreach($contrato as  $c)
                                                        <option value="{{ $c->id }}" {{$c->id == $instructor->idContrato ? 'selected' : ''}}>{{$c->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="idContrato">Vinculación <span class="text-danger">*</span></label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                                <label class="col-12" for="fechaInicio" style="margin-left: -15px !important;">Contrato</label>
                                            <div class="form-material input-group floating">
                                                <div class="input-daterange input-group" data-date-format="yyyy-mm-dd" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                                <input type="text" class="form-control" id="fechaInicio" name="fechaInicioCo" placeholder="Inicio" data-week-start="1" data-autoclose="true" data-today-highlight="true" value="{{$instructor->fechaInicioCo}}">
                                                    <div class="input-group-prepend input-group-append">
                                                        <span class="input-group-text font-w600">A</span>
                                                    </div>
                                                    <input type="text" class="form-control" id="fechaFinal" name="fechaFinalCo" placeholder="Fin" data-week-start="2" data-autoclose="true" data-today-highlight="true" value="{{$instructor->fechaFinalCo}}">
                                                </div>
                                            </div>
                                        </div>

                                       <div class="form-group">
                                        <div class="form-material">
                                                    <select class="js-select2 form-control" id="idProfesion" name="idProfesion" style="width: 100%;" data-allow-clear="true">
                                                        <option value=""> Seleccione profesión</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                        @foreach($profesion as  $profe)
                                                        <option value="{{ $profe->id }}" {{$profe->id == $instructor->idProfesion? 'selected' : ''}}>{{$profe->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="idProfesion">Profesión <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                        {{-- <div class="form-group">
                                        <div class="form-material">
                                                    <select class="js-select2 form-control" id="idProgramaFormacion" name="idProgramaFormacion" style="width: 100%;" data-allow-clear="true">
                                                        <option value=""> Seleccione Programa</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                        @foreach($programaFormacion as  $programa)
                                                        <option value="{{ $programa->id }}" {{$programa->id == $instructor->idProgramaFormacion? 'selected' : ''}}>{{$programa->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="idProgramaFormacion">Programa de Formación <span class="text-danger">*</span></label>
                                            </div>
                                        </div> --}}
                                        </div>

                                        <!-- END Steps Content -->
                                    </div>
                                    <!-- Steps Navigation -->
                                    <div class="block-content block-content-sm block-content-full bg-body-light">
                                        <div class="row">
                                            <div class="col-6">
                                                <button type="button" class="btn btn-alt-secondary" data-wizard="prev">
                                                    <i class="fa fa-angle-left mr-5"></i> Atrás
                                                </button>
                                            </div>
                                            <div class="col-6 text-right">
                                                <button type="button" class="btn btn-alt-secondary" data-wizard="next">
                                                    Siguiente <i class="fa fa-angle-right ml-5"></i>
                                                </button>
                                                <button type="submit" class="btn btn-alt-success d-none" data-wizard="finish">
                                                    <i class="fa fa-check mr-5"></i> Guardar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Steps Navigation -->
                                </form>
                                <!-- END Form -->
                            </div>
@endsection
@section('js_after')
        <!-- Page JS Plugins -->
        <script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="/js/plugins/jquery-validation/additional-methods.js"></script>

        <!-- Page JS Code -->
        <script src="/js/pages/be_forms_wizard.min.js"></script>
        <script src="/js/functions/select-by-department.js"></script>


@endsection
