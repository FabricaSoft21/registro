<div class="modal fade" id="modal-fromleft-{{$ins->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-fromleft" aria-hidden="true">
<div class="modal-dialog modal-dialog-fromleft modal-lg" role="document">
    <div class="modal-content">
        <div class="block block-themed block-transparent mb-0">
            <div class="block-header bg-primary-dark">
            <h3 class="block-title">{{$ins->nombre}} {{$ins->apellido}}</h3>
                <div class="block-options">
                    <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                        <i class="si si-close"></i>
                    </button>
                </div>
            </div>
            <div class="block block-themed block-transparent mb-0">
                    <div class="block block-themed">
                            <div class="block-content block-content-full text-center bg-gd-primary">
                                <img class="img-avatar img-avatar96 img-avatar-thumb" src="/media/avatars/avatar15.jpg" alt="">

                             <h3 class="text-white mt-3">INFORMACIÓN

                                 <i class=" si si-info"></i>
                             </h3>
                             </div>
                              
                            <div class="col-md-12">
                                    <!-- Simple Wizard 2 -->
                                    <div class="js-wizard-simple block">
                                        <!-- Step Tabs -->
                                        <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#wizard-validation-material-step1" data-toggle="tab">1. Personal</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#wizard-validation-material-step2" data-toggle="tab">2. Vinculación</a>
                                    </li>
                            
                                </ul>

                            <form action="be_forms_Wizards.html" method="post">
                                    <!-- Steps Content -->
                                    <div class="block-content block-content-full tab-content" style="min-height: 267px;">
                                        <!-- Step 1 -->
                                        <div class="tab-pane active" id="wizard-validation-material-step1" role="tabpanel">
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text"  value="{{$ins->nombre}}" id="wizard-simple2-firstname" name="wizard-simple2-firstname" readonly>
                                                    <label for="nombre">Nombre</label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-vcard"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text"  value="{{$ins->apellido}}" readonly>
                                                <label for="apellido">Apellido</label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-address-card-o"></i>
                                                    </span>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    <div class="form-material input-group floating">
                                        <input class=" form-control"  type="text" value="{{$ins->tipoDoc->nombre}}"  readonly>
                                        <label for="idTipoDoc">Tipo de documento</label>
                                        <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-drivers-license-o"></i>
                                                    </span>
                                                </div>
                                </div>
                            </div>
                            <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" value="{{$ins->numDoc}}" readonly>
                                                <label for="telefono">Número de documento</label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-slack"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text"  value="{{$ins->correoSena}}" readonly>
                                                <label for="correoSena">Correo Sena</label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-envelope-o"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                       
                                    
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text"  value="{{$ins->correoAlterno}}" readonly>
                                                <label for="correoAlterno">Correo electrónico</label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-envelope-o"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text"  value="{{$ins->municipio->nombre}}" readonly>
                                                <label for="correoAlterno">Municipio de vivienda</label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-envelope-o"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" value="{{$ins->telefono}}" readonly>
                                                <label for="telefono">Número de contacto</label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-phone"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" value="{{$ins->celular}}" readonly>
                                                <label for="celular">Número de celular</label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="si si-screen-smartphone"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="tab-pane" id="wizard-validation-material-step2" role="tabpanel">
                                     
                                        <div class="form-group">
                                        <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" value="{{$ins->area->nombre}}" readonly> 
                                                    <label for="idArea">Área</label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-black-tie"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                        <div class="form-material input-group floating">
                                                    <input class=" form-control" value="{{$ins->contrato->nombre}}" readonly>
                                                          <label for="idContrato">Vinculación</label>
                                                          <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="si si-support"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                        <div class="form-material input-group floating ">
                                                    <input class="form-control" type="date" id="" name="" value="{{$ins->fechaInicioCo}}" readonly>
                                                    <label for="fechaInicioCo">Fecha de inicio del contrato</label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-calendar"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                        <div class="form-material input-group floating">
                                                    <input class="form-control" type="date" id="" name="" value="{{$ins->fechaFinalCo}}" readonly>
                                                    <label for="fechaFinalCo">Fecha final del contrato</label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-calendar"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                        </div>

                                       <div class="form-group">
                                        <div class="form-material input-group floating">
                                                    <input class="form-control" type="text"  data-allow-clear="" value="{{$ins->profesion->nombre}}" readonly>
                                                        
                                                    <label for="idProfesion">Profesión</label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-graduation-cap"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                        {{-- <div class="form-group ">
                                        <div class="form-material input-group floating">
                                                    <input class="form-control"  type="text"id="" name=""   value="{{$ins->programaFormacion->nombre}}">
                                                    <label for="idProgramaFormacion">Programa de Formación</label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-leanpub"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div> --}}
                                        </div>

                                        <!-- END Steps Content -->
                                  
                            </div>
                           
                                        <!-- END Step Tabs -->
        
                                        <!-- Form -->
                                      
                                        <!-- END Form -->
                                    </div>
        </div>
        <div class="modal-footer">
            <!-- {{-- <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button> --}} -->
            <button type="button" class="btn btn-alt-success" data-dismiss="modal">
                <i class="fa fa-check"></i> Listo
            </button>
        </div>
    </div>
</div>
</div>

