@extends('layouts.backend')
@section('content')
<div class="content">
                    <h2 class="content-heading">Instructores</h2>
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Crear Instructor</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option">
                                    <i class="si si-user-follow"></i>
                                </button>
                            </div>
                        </div>
                        <div class="js-wizard-validation-material block">
                                <!-- Step Tabs -->
                                <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#wizard-validation-material-step1" data-toggle="tab">1. Personal</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#wizard-validation-material-step2" data-toggle="tab">2. Detalles</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#wizard-validation-material-step3" data-toggle="tab">2. Vinculación</a>
                                    </li>
                            
                                </ul>
                                <!-- END Step Tabs -->

                                <!-- Form -->
                                <form class="js-wizard-validation-material-form" action="" method="post">
                                {{ csrf_field() }}
                                    <!-- Steps Content -->
                                    <div class="block-content block-content-full tab-content" style="min-height: 267px;">
                                        <!-- Step 1 -->
                                        <div class="tab-pane active" id="wizard-validation-material-step1" role="tabpanel">
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="nombre" name="nombre" value="{{old('nombre')}}">
                                                    <label for="nombre">Nombres <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-vcard"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="apellido" name="apellido" value="{{old('apellido')}}">
                                                <label for="apellido">Apellidos <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-address-card-o"></i>
                                                    </span>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    <div class="form-material">
                                        <select class="js-select2 form-control" id="idTipoDoc" name="idTipoDoc" style="width: 100%;" data-allow-clear="true">
                                            <option value="" selected>Seleccione tipo de documento</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                            @foreach($tipoDoc as $d)
                                            <option value="{{ $d->id }}">{{$d->nombre}}</option>
                                            @endforeach
                                        </select>
                                        <label for="idTipoDoc">Tipo de documento <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="numDoc" name="numDoc" value="{{old('numDoc')}}">
                                                <label for="numDoc">Número de documento <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                    <i class="fa fa-slack"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                            
                                        </div>

                                        <div class="tab-pane" id="wizard-validation-material-step2" role="tabpanel">
                                     
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="correoSena" name="correoSena" value="{{old('correoSena')}}">
                                                    <label for="correoSena">Correo Sena <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-envelope-o"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
    
                                           
                                        
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="correoAlterno" name="correoAlterno" value="{{old('correoAlterno')}}">
                                                    <label for="correoAlterno">Correo electrónico <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-envelope-o"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                    <div class="form-material">
                                                        <select class="js-select2 form-control" id="idDepartamento" name="idDepartamento" style="width: 100%;" data-allow-clear="true">
                                                            <option value="" selected>Seleccione Departamento </option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                            @foreach($departamento as $depa)
                                                            <option value="{{ $depa->id }}">{{$depa->nombre}}</option>
                                                            @endforeach
                                                        </select>
                                                        <label for="idTipoDoc">Departamento <span class="text-danger">*</span></label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-material">
                                                    <select class="js-select2 form-control" id="idMunicipio" name="idMunicipio" style="width: 100%;" data-allow-clear="true">
                                                        <option value="" selected>Seleccione municipio </option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                                    </select>
                                                    <label for="idTipoDoc">Municipio de vivienda <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="telefono" name="telefono" value="{{old('telefono')}}">
                                                    <label for="telefono">Número de Teléfono <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-phone"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="celular" name="celular" value="{{old('celular')}}">
                                                    <label for="celular">Número de celular <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                        <i class="si si-screen-smartphone"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                    
                                        </div>
                                        <div class="tab-pane" id="wizard-validation-material-step3" role="tabpanel">
                                     
                                            <input class="form-control" type="hidden" id="idArea" name="idArea" value="{{auth()->user()->selected_category_id}}">
                                            <div class="form-group">
                                            <div class="form-material">
                                                        <select class="js-select2 form-control" id="idContrato" name="idContrato" style="width: 100%;" data-allow-clear="true">
                                                            <option value="" selected> Tipo Vinculación</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                            @foreach($contrato as  $contrato)
                                                            <option value="{{ $contrato->id }}">{{$contrato->nombre}}</option>
                                                            @endforeach
                                                        </select>
                                                        <label for="idContrato">Vinculación <span class="text-danger">*</span></label>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                    <label class="col-12" for="fechaInicio" style="margin-left: -15px !important;">Contrato</label>
                                                <div class="form-material input-group floating">
                                                    <div class="input-daterange input-group" data-date-format="yyyy-mm-dd" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                                        <input type="text" class="form-control" id="fechaInicio" name="fechaInicioCo" placeholder="Inicio" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                                        <div class="input-group-prepend input-group-append">
                                                            <span class="input-group-text font-w600">A</span>
                                                        </div>
                                                        <input type="text" class="form-control" id="fechaFinal" name="fechaFinalCo" placeholder="Fin" data-week-start="2" data-autoclose="true" data-today-highlight="true">
                                                    </div>
                                                </div>
                                            </div>
                                       
    
                                           <div class="form-group">
                                            <div class="form-material">
                                                        <select class="js-select2 form-control" id="idProfe" name="idProfesion" style="width: 100%;" data-allow-clear="true">
                                                            <option value="" selected> Seleccione profesión</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                            @foreach($profesion as  $profe)
                                                            <option value="{{$profe->id}}">{{$profe->nombre}}</option>
                                                            @endforeach
                                                        </select>
                                                        <label for="idProfesion">Profesión <span class="text-danger">*</span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <a class="link-effect text-muted mb-5 d-inline-block" href="#" data-toggle="modal" data-target="#modal-profe">
                                                    <i class="fa fa-plus-square text-muted mr-5"></i>¿Nueva Profesión?
                                                </a>
                                            </div>
                                        
                                            </div>
                                        <!-- END Steps Content -->
                                        
                                        </div>
                                    <!-- Steps Navigation -->
                                    <div class="block-content block-content-sm block-content-full bg-body-light">
                                        <div class="row">
                                            <div class="col-6">
                                                <button type="button" class="btn btn-alt-secondary" data-wizard="prev">
                                                    <i class="fa fa-angle-left mr-5"></i> Atrás
                                                </button>
                                            </div>
                                            <div class="col-6 text-right">
                                                <button type="button" class="btn btn-alt-secondary" data-wizard="next">
                                                    Siguiente <i class="fa fa-angle-right ml-5"></i>
                                                </button>
                                                <button type="submit" class="btn btn-alt-success d-none" data-wizard="finish">
                                                    <i class="fa fa-check mr-5"></i> Guardar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Steps Navigation -->
                                </form>
                                <!-- END Form -->
                            </div>
                            {{-- modal profesión --}}

                            <div class="modal fade" id="modal-profe" tabindex="-1" role="dialog" aria-labelledby="modal-terms" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-slidedown" role="document">
                                    <div class="modal-content">
                                        <div class="block block-themed block-transparent mb-0">
                                            <div class="block-header bg-primary-dark">
                                                <h3 class="block-title">Registrar Nueva Profesión</h3>
                                                    <div class="block-options">
                                                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                                            <i class="si si-close"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            <div class="block block-themed block-transparent mb-0">
                                                    <div class="block-content block-content-full text-center bg-gray">
                                                    <img class="img-avatar img-avatar96 img-avatar-thumb" src="/media/avatars/profesion.png" alt="">
                                            </div>
                                            <div class="row justify-content-center py-20">
                                                    <div class="col-xl-8">
                                                        <span id="form_resultP"></span>
                                                    <form id="form-profe" class="js-validation-material" action="" method="POST">
                                                    {{ csrf_field() }}
                                                    <div class="form-group">
                                                        <div class="form-material input-group floating">
                                                            <input class="form-control" value="{{old('nombre')}}" type="text" id="nombre" name="nombre">
                                                            <label for="nombre">Nombre <span class="text-danger">*</span></label>
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="fa fa-vcard"></i>
                                                                </span>
                                                            </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-material input-group floating">
                                                        <input class="form-control" type="text" value="{{old('descripcion')}}" id="descripcion" name="descripcion">
                                                        <label for="apellido">Descripción <span class="text-danger">*</span></label>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-address-card-o"></i>
                                                            </span>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                        <div class="modal-footer">
                                                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cerrar</button>
                                                <button type="submit" class="btn btn-alt-success">
                                                <i class="fa fa-check"></i> Guardar
                                            </button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                    </div>
@endsection
@section('js_after')
        <!-- Page JS Plugins -->
        <script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="/js/plugins/jquery-validation/additional-methods.js"></script>

        <script src="/js/pages/be_forms_validation.min.js"></script>

        <!-- Page JS Code -->
        <script src="/js/pages/be_forms_wizard.min.js"></script>
        <script src="/js/functions/select-by-department.js"></script>

        <script>
        $('#form-profe').on('submit',function(event){
            event.preventDefault();
            $.ajax({
                url:"{{ route('savePro')}}",
                method:"POST",
                data : new FormData(this),
                contentType:false,
                cache:false,
                processData:false,
                dataType:"json",
                success:function(data)
                {
                    // console.log(data);
                    var html = "";
                    if(data.errors){
                        html = '<div class="alert alert-danger">';
                            for(var count = 0; count < data.errors.length; count++)
                            {
                                html += '<p>' + data.errors[count] +
                                    '</p>';
                            }
                            html += '</div>';
                    }
                    if(data.success)
                    {
                        // console.log(data)

                        html = '<div class="alert alert-success">'
                            + data.success + '</div>';
                            $('#form-profe')[0].reset();
                            $("#idProfe").append('<option value=' + data.data["0"].id + '>' + data.data["0"].nombre + '</option>');
                    }
                    $('#form_resultP').html(html);
                }
            })
        });
        </script>
       

@endsection
