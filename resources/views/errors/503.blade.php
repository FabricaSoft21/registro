@extends('layouts.simple')
@section('content')
<div class="hero bg-white">
    <div class="hero-inner">
        <div class="content content-full">
            <div class="py-30 text-center">
                <div class="display-3 text-elegance">
                    <i class="fa fa-database"></i> 503
                </div>
                <h1 class="h2 font-w700 mt-30 mb-10">Vaya, acabas de encontrar una página de error.</h1>
                <h2 class="h3 font-w400 text-muted mb-50">Lo sentimos, pero nuestro servicio no está disponible actualmente.</h2>
                <a class="btn btn-hero btn-rounded btn-alt-secondary" href="{{url()->previous()}}">
                    <i class="fa fa-arrow-left mr-10"></i> Regresar al inicio
                </a>
            </div>
        </div>
    </div>
</div>
@endsection
