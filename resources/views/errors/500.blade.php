@extends('layouts.simple')
@section('content')
<div class="hero bg-white">
    <div class="hero-inner">
        <div class="content content-full">
            <div class="py-30 text-center">
                <div class="display-3 text-flat">
                    <i class="fa fa-times-circle-o"></i> 500
                </div>
                <h1 class="h2 font-w700 mt-30 mb-10">Vaya, acabas de encontrar una página de error.</h1>
                <h2 class="h3 font-w400 text-muted mb-50">Lo sentimos, tenemos problemas con el servidor, intentelo más tarde ...</h2>
                <a class="btn btn-hero btn-rounded btn-alt-secondary" href="{{url()->previous()}}">
                    <i class="fa fa-arrow-left mr-10"></i> Regresar al inicio
                </a>
            </div>
        </div>
    </div>
</div>
@endsection
