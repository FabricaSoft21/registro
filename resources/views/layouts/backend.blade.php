<!doctype html>
<html lang="{{ config('app.locale') }}" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>SRA</title>

        <meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Icons -->
        <link rel="shortcut icon" href="{{ asset('media/various/logo.png') }}">
        <link rel="icon" sizes="192x192" type="image/png" href="{{ asset('media/various/logo.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('media/various/logo.png') }}">


        <!-- Fonts and Styles -->
        @yield('css_before')
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
        <link rel="stylesheet" id="css-main" href="{{ mix('/css/codebase.css') }}">
        <link rel="stylesheet"  href="{{ asset('/css/toastr/toastr.css') }}">
        <link rel="stylesheet"  href="{{ asset('/js/plugins/select2/css/select2.min.css') }}">
        <link rel="stylesheet" href="/js/plugins/fullcalendar/fullcalendar.min.css">

        <!-- Page JS Plugins CSS -->
        <link rel="stylesheet" href="{{asset ('/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
        <link rel="stylesheet" href="{{asset ('/js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}">
        <link rel="stylesheet" href="{{asset ('/js/plugins/select2/css/select2.min.css') }}">
        <link rel="stylesheet" href="{{asset ('/js/plugins/jquery-tags-input/jquery.tagsinput.min.css') }}">
        <link rel="stylesheet" href="{{asset ('/js/plugins/datatables/dataTables.bootstrap4.css') }}">
        <link rel="stylesheet" href="{{asset ('/js/plugins/datatables/responsive.dataTables.min.css') }}">
        <link rel="stylesheet" href="/js/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" href="/js/plugins/sweetalert2/sweetalert2.min.css">




{{--
        <link rel="stylesheet" href="{{assets}}">
        <link rel="stylesheet" href="assets">
        <link rel="stylesheet" href="assets/js/plugins/jquery-auto-complete/jquery.auto-complete.min.css">
        <link rel="stylesheet" href="assets/js/plugins/ion-rangeslider/css/ion.rangeSlider.css">
        <link rel="stylesheet" href="assets/js/plugins/ion-rangeslider/css/ion.rangeSlider.skinHTML5.css">
        <link rel="stylesheet" href="assets/js/plugins/dropzonejs/dist/dropzone.css"> --}}


        <!-- You can include a specific file from public/css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="{{ mix('/css/themes/corporate.css') }}"> -->
        @yield('css_after')

        <!-- Scripts -->

        <script>window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};</script>
    </head>
    <body>
            <div id="page-loader" class="show"></div>
        <!-- Page Container -->
        <!--
            Available classes for #page-container:

        GENERIC

            'enable-cookies'                            Remembers active color theme between pages (when set through color theme helper Template._uiHandleTheme())

        SIDEBAR & SIDE OVERLAY

            'sidebar-r'                                 Right Sidebar and left Side Overlay (default is left Sidebar and right Side Overlay)
            'sidebar-mini'                              Mini hoverable Sidebar (screen width > 991px)
            'sidebar-o'                                 Visible Sidebar by default (screen width > 991px)
            'sidebar-o-xs'                              Visible Sidebar by default (screen width < 992px)
            'sidebar-inverse'                           Dark themed sidebar

            'side-overlay-hover'                        Hoverable Side Overlay (screen width > 991px)
            'side-overlay-o'                            Visible Side Overlay by default

            'enable-page-overlay'                       Enables a visible clickable Page Overlay (closes Side Overlay on click) when Side Overlay opens

            'side-scroll'                               Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (screen width > 991px)

        HEADER

            ''                                          Static Header if no class is added
            'page-header-fixed'                         Fixed Header

        HEADER STYLE

            ''                                          Classic Header style if no class is added
            'page-header-modern'                        Modern Header style
            'page-header-inverse'                       Dark themed Header (works only with classic Header style)
            'page-header-glass'                         Light themed Header with transparency by default
                                                        (absolute position, perfect for light images underneath - solid light background on scroll if the Header is also set as fixed)
            'page-header-glass page-header-inverse'     Dark themed Header with transparency by default
                                                        (absolute position, perfect for dark images underneath - solid dark background on scroll if the Header is also set as fixed)

        MAIN CONTENT LAYOUT

            ''                                          Full width Main Content if no class is added
            'main-content-boxed'                        Full width Main Content with a specific maximum width (screen width > 1200px)
            'main-content-narrow'                       Full width Main Content with a percentage width (screen width > 1200px)
        -->
        <div id="page-container" class="sidebar-o sidebar-inverse enable-page-overlay side-scroll page-header-fixed main-content-narrow">
            <!-- Side Overlay-->
            <aside id="side-overlay">
                <!-- Side Header -->
                <div class="content-header content-header-fullrow">
                    <div class="content-header-section align-parent">
                        <!-- Close Side Overlay -->
                        <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                        <button type="button" class="btn btn-circle btn-dual-secondary align-v-r" data-toggle="layout" data-action="side_overlay_close">
                            <i class="fa fa-times text-danger"></i>
                        </button>
                        <!-- END Close Side Overlay -->

                        <!-- User Info -->
                        <div class="content-header-item">
                            <a class="img-link mr-5" href="javascript:void(0)">
                                <img class="img-avatar img-avatar48" src="{{ asset('media/various/logo.png') }}" alt="">
                            </a>
                            <a class="align-middle link-effect text-primary-dark font-w600" href="javascript:void(0)">{{Auth::user()->name}}</a>
                        </div>
                        <!-- END User Info -->
                    </div>
                </div>
                <!-- END Side Header -->

                <!-- Side Content -->
                <div class="content-side">
                    <p>
                            <form action="{{route('editUser',auth()->user()->id)}}" method="post">
                                    @csrf
                                    <p>
                                            <div class="form-group">
                                                    <div class="form-material input-group floating">
                                                        <input class="form-control" type="text" id="name" name="name" value="{{old('name',auth()->user()->name )}}">
                                                        <label for="name">Usuario <span class="text-danger">*</span></label>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-vcard"></i>
                                                            </span>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="email" name="email" value="{{old('email',auth()->user()->email)}}">
                                                    <label for="email">Correo electrónico <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-at"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>


                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="password" id="password" name="password" value="">
                                                    <label for="password">Contraseña <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-asterisk"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="password" id="password_confirmation" name="password_confirmation" value="">
                                                    <label for="password_confirmation">Confirmar contraseña <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-asterisk"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                
                                            @if(auth()->user()->hasRole('poblaciones'))
                                                @php($role = 'poblaciones')                                               
                                            @endif
                                            @if(auth()->user()->hasRole('tecnica'))
                                                @php($role = 'tecnica')
                                            @endif
                                            @if(auth()->user()->hasRole('administrador'))
                                                @php($role = 'administrador')
                                            @endif

                                        <input type="hidden" name="rol" value="{{$role}}">

                                            <div class="form-group row">
                                                    <div class="col-lg-8 ml-auto">
                                                        <button type="submit" class="btn btn-alt-success pull-right">Actualizar</button>
                                                    </div>
                                                </div>
                                    </p>
                                    </form>
                    </p>
                </div>
                <!-- END Side Content -->
            </aside>
            <!-- END Side Overlay -->

            <!-- Sidebar -->
            <!--
                Helper classes

                Adding .sidebar-mini-hide to an element will make it invisible (opacity: 0) when the sidebar is in mini mode
                Adding .sidebar-mini-show to an element will make it visible (opacity: 1) when the sidebar is in mini mode
                    If you would like to disable the transition, just add the .sidebar-mini-notrans along with one of the previous 2 classes

                Adding .sidebar-mini-hidden to an element will hide it when the sidebar is in mini mode
                Adding .sidebar-mini-visible to an element will show it only when the sidebar is in mini mode
                    - use .sidebar-mini-visible-b if you would like to be a block when visible (display: block)
            -->
            <nav id="sidebar">
                <!-- Sidebar Content -->
                <div class="sidebar-content">
                    <!-- Side Header -->
                    <div class="content-header content-header-fullrow px-15">
                        <!-- Mini Mode -->
                        <div class="content-header-section sidebar-mini-visible-b">
                            <!-- Logo -->
                            <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
                                <span class="text-dual-primary-dark">c</span><span class="text-primary">b</span>
                            </span>
                            <!-- END Logo -->
                        </div>
                        <!-- END Mini Mode -->

                        <!-- Normal Mode -->
                        <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                            <!-- Close Sidebar, Visible only on mobile screens -->
                            <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                            <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                                <i class="fa fa-times text-danger"></i>
                            </button>
                            <!-- END Close Sidebar -->

                            <!-- Logo -->
                            <div class="content-header-item">
                                <a class="link-effect font-w700" href="/dashboard">
                                <i class="fa fa-graduation-cap text-primary"></i>
                                    <span class="font-size-xl text-white">S</span><span class="font-size-xl">R<span class="font-size-xl text-white">A</span></span>
                                </a>
                            </div>
                            <!-- END Logo -->
                        </div>
                        <!-- END Normal Mode -->
                    </div>
                    <!-- END Side Header -->

                    <!-- Side User -->
                    <div class="content-side content-side-full content-side-user px-10 align-parent">
                        <!-- Visible only in mini mode -->
                        <div class="sidebar-mini-visible-b align-v animated fadeIn">
                            <img class="img-avatar img-avatar48" src="{{ asset('media/various/logo.png') }}" alt="">
                        </div>
                        <!-- END Visible only in mini mode -->

                        <!-- Visible only in normal mode -->
                        <div class="sidebar-mini-hidden-b text-center">
                            <a class="img-link" href="javascript:void(0)">
                                <img class="img-avatar img-avatar96 animated rotateIn" src="{{ asset('media/various/logo.png') }}" alt="" >
                            </a>
                            <ul class="list-inline mt-12">
                                <li class="list-inline-item">
                                    <a class="link-effect text-dual-primary-dark font-size-xs font-w600 text-uppercase"  href="javascript:void(0)">{{Auth::user()->name}}</a>
                                </li>
                                <li class="list-inline-item">
                                    <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                                    <!-- <a class="link-effect text-dual-primary-dark" data-toggle="layout" data-action="sidebar_style_inverse_toggle" href="javascript:void(0)">
                                        <i class="si si-drop"></i>
                                    </a> -->
                                </li>

                                {{-- <li class="list-inline-item">
                                    <a class="link-effect text-dual-primary-dark js-swal-confirm" onclick="return logout(event)" href="#">
                                        <i class="si si-logout"></i>
                                    </a>
                                </li> --}}
                            </ul>
                        </div>
                        <!-- END Visible only in normal mode -->
                    </div>
                    <!-- END Side User -->
                    <!-- Side Navigation -->
                    <div class="content-side content-side-full">
                        <ul class="nav-main">
                            <li>
                                <a class="{{ request()->is('dashboard') ? ' active' : '' }}" href="/dashboard">
                                    <i class="fa fa-tasks"></i><span class="sidebar-mini-hide">Inicio</span>
                                </a>
                            </li>
                            <li>
                                <a class="{{ request()->is('programacion') ? ' active' : '' }}" href="/programacion">
                                    <i class="fa fa-calendar"></i><span class="sidebar-mini-hide">Programación</span>
                                </a>
                            </li>
                            @role('administrador')
                            <li class="nav-main-heading">
                                <span class="sidebar-mini-visible">US</span><span class="sidebar-mini-hidden">Usuarios</span>
                            </li>
                            <li class="{{ request()->is('user/*') ? ' open' : '' }}">
                                <a class="{{ request()->is('user') ? ' active' : '' }}" data-toggle="" href="/user"><i class="fa fa-user"></i><span class="sidebar-mini-hide">USUARIOS</span></a>
                            </li>
                            @endrole
                            <li class="nav-main-heading">
                                @if (auth()->user()->selected_category_id  == 1)
                                    @php($select = "Titulada")
                                    <?php $opt = 'si si-graduation mr-5' ?>
                                @endif
                                @if (auth()->user()->selected_category_id  == 2)
                                   @php($select = "Complementaria")
                                   <?php $opt = 'si si-puzzle mr-5'; ?>
                                @endif
                                 @if (auth()->user()->selected_category_id  == 3)
                                    @php($select = "Poblaciones Vulnerables")
                                    <?php $opt = 'si si-users mr-5';  ?>
                                @endif
                                    @if (auth()->user()->selected_category_id  == 4)
                                    @php($select = "Competencias básicas")
                                    <?php $opt = 'si si-layers mr-5';  ?>
                                @endif
                                @if (auth()->user()->selected_category_id  == 5)
                                    @php($select = "Media técnica")
                                    <?php $opt = 'si si-badge mr-5';  ?>
                                @endif
                            <span class="sidebar-mini-visible">MT</span><span class="sidebar-mini-hidden">{{$select}}</span>
                            </li>
                            <li class="{{ request()->is('aprendices/*') ? ' open' : '' }}">

                                <a class="{{ request()->is('aprendiz') ? ' active' : '' }}" data-toggle="" href="/aprendiz"><i class="fa fa-child"></i><span class=""> APRENDICES</span></a>

                            </li>
                            <li class="{{ request()->is('rector/*') ? ' open' : '' }}" style={{auth()->user()->selected_category_id != 5 ? "display:none" : "" }}>
                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-university"></i><span class="sidebar-mini-hide">INSTITUCIÓN EDUCATIVA</span></a>
                                <ul>
                                <li>
                                     <a class="{{ request()->is('institucion') ? ' active' : '' }}" href="/instiEdu">Instituciones Educativas</a>
                                    </li>
                                    <li>
                                        <a class="{{ request()->is('rector') ? ' active' : '' }}" href="/rector">Rectores</a>
                                    </li>
                                    <li>
                                        <a class="{{ request()->is('coordinacion') ? ' active' : '' }}" href="/coordinador">Coordinadores</a>

                                    </li>
                                    <li>
                                        <a class="{{ request()->is('docente') ? ' active' : '' }}" href="/docente">Docentes pares</a>

                                    </li>
                                </ul>
                            </li>
                            <li class="{{ request()->is('empresas/*') ? ' open' : '' }}" style="{{auth()->user()->selected_category_id != 3 ? "display:none" : "" }}">

                                    <a class="{{ request()->is('empresa') ? ' active' : '' }}" data-toggle="" href="/empresa"><i class="fa fa-industry"></i><span class=""> EMPRESAS</span></a>

                                </li>
                            <li class="{{ request()->is('Instsructores/*') ? ' open' : '' }}">
                                    <a class="{{ request()->is('Instructor') ? ' active' : '' }}" data-toggle="" href="/instructor"><i class="fa fa-users"></i><span class="">INSTRUCTORES</span></a>
                            </li>
                            <li class="{{ request()->is('formacion/*') ? ' open' : '' }}">
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-book"></i><span class="sidebar-mini-hide">FORMACIÓN</span></a>
                                    <ul>
                                        <li>
                                             <a class="{{ request()->is('programa') ? ' active' : '' }}" href="/programa">Programas</a>
                                        </li>

                                        <li>
                                            <a class="{{ request()->is('ficha') ? ' active' : '' }}" href="/ficha">Fichas</a>

                                        </li>
                                        <li>
                                            <a class="{{ request()->is('formacion') ? ' active' : '' }}" href="/formacion">Formación</a>

                                        </li>
                                    </ul>
                                </li>
                        </ul>
                    </div>
                    <!-- END Side Navigation -->
                </div>
                <!-- Sidebar Content -->
            </nav>
            <!-- END Sidebar -->

            <!-- Header -->
            <header id="page-header">
                <!-- Header Content -->
                <div class="content-header">
                    <!-- Left Section -->
                    <div class="content-header-section">
                        <!-- Toggle Sidebar -->
                        <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                        <button type="button" class="btn btn-circle btn-dual-secondary" data-toggle="layout" data-action="sidebar_toggle">
                            <i class="fa fa-navicon"></i>
                        </button>
                        <!-- END Toggle Sidebar -->

                        <!-- Open Search Section -->
                        <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                        <!-- <button type="button" class="btn btn-circle btn-dual-secondary" data-toggle="layout" data-action="header_search_on">
                            <i class="fa fa-search"></i>
                        </button> -->
                        <!-- END Open Search Section -->

                        <!-- Layout Options (used just for demonstration) -->
                        <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                         <div class="btn-group" role="group">
                                <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="{{$opt}}"></i>
                                        <span class="d-none d-sm-inline-block">{{$select}}</span>
                                    </button>
                            <div class="dropdown-menu min-width-300" aria-labelledby="page-header-options-dropdown">
                            <h5 class="h6 text-center py-10 mb-10 border-b text-uppercase" style="{{ auth()->user()->hasRole('administrador') ? '' : 'display:none' }}">Elija una opción</h5>
                                <h6 class="dropdown-header">Clasificación</h6>

                                    <?php $type = null;?>

                                    @foreach (auth()->user()->list_of_categories as $category)
                                    @if ($category->id == 1)
                                        <?php $type = 'si si-graduation mr-5' ?>
                                    @endif
                                    @if($category->id == 2)
                                        <?php $type = 'si si-puzzle mr-5'; ?>
                                    @endif
                                    @if($category->id == 3)
                                        <?php $type = 'si si-users mr-5';  ?>
                                    @endif
                                    @if($category->id == 4)
                                        <?php $type = 'si si-layers mr-5';  ?>
                                    @endif
                                    @if($category->id == 5)
                                        <?php $type = 'si si-badge mr-5';  ?>
                                    @endif
                                    <a id="list-of-categories-{{$category->id}}" class="{{ $category->id == auth()->user()->selected_category_id ? "dropdown-item active" : "dropdown-item" }}" href="" onclick="OptionSelected({{$category->id}})">
                                        <i class="{{$type}}"></i> {{$category->nombre}}
                                    </a>
                                    @endforeach
                            </div>
                        </div>
                       <!-- END Layout Options -->
                    </div>
                    <!-- END Left Section -->

                    <!-- Right Section -->
                    <div class="content-header-section">
                        <!-- User Dropdown -->
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-user d-sm-none"></i>
                                <span class="d-none d-sm-inline-block">{{Auth::user()->name}}</span>
                                <i class="fa fa-angle-down ml-5"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right min-width-200" aria-labelledby="page-header-user-dropdown">
                                <h5 class="h6 text-center py-10 mb-5 border-b text-uppercase">Opciones</h5>
                               <!--  <a class="dropdown-item" href="javascript:void(0)">
                                    <i class="si si-user mr-5"></i> Perfil
                                </a> -->

                                <!-- END Side Overlay -->

                                <!-- <div class="dropdown-divider"></div> -->
                                <a class="dropdown-item js-swal-confirm" href="javascript:void(0)" onclick="return logout(event)">
                                    <i class="si si-logout mr-5"></i> Cerrar sesión
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                </form>
                            </div>
                        </div>
                        <!-- END User Dropdown -->

                        <!-- Toggle Side Overlay -->
                        <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                        <button type="button" class="btn btn-circle btn-dual-secondary" title="Modificar" data-toggle="layout" data-action="side_overlay_toggle">
                            <i class="si si-settings"></i>
                        </button>
                        <!-- END Toggle Side Overlay -->
                    </div>
                    <!-- END Right Section -->
                </div>
                <!-- END Header Content -->

                <!-- Header Search -->
                <div id="page-header-search" class="overlay-header">
                    <div class="content-header content-header-fullrow">
                        <form action="/dashboard" method="POST">
                            @csrf
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <!-- Close Search Section -->
                                    <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                                    <button type="button" class="btn btn-secondary" data-toggle="layout" data-action="header_search_off">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <!-- END Close Search Section -->
                                </div>
                                <input type="text" class="form-control" placeholder="Search or hit ESC.." id="page-header-search-input" name="page-header-search-input">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-secondary">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                   </div>
                </div>
                <!-- END Header Search -->

                <!-- Header Loader -->
                <!-- Please check out the Activity page under Elements category to see examples of showing/hiding it -->
                <div id="page-header-loader" class="overlay-header bg-primary">
                    <div class="content-header content-header-fullrow text-center">
                        <div class="content-header-item">
                            <i class="fa fa-sun-o fa-spin text-white"></i>
                        </div>
                    </div>
                </div>
                <!-- END Header Loader -->
            </header>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                @yield('content')
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <footer id="page-footer" class="opacity-0">
                <div class="content py-20 font-size-xs clearfix">
                    <div class="float-right">
                    De <a class="font-w600" href="#" target="#">  Dahiana V. & Andrés A.</a> <i class="fa fa-heart text-pulse"></i>
                    </div>
                    <div class="float-left">
                        <a class="font-w600" href="#" target="#">Fábrica de Software</a> &copy; <span class="js-year-copy">2019</span>
                    </div>
                </div>
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- Codebase Core JS -->

        <script src="{{ mix('js/codebase.app.js') }}"></script>

        <!-- Laravel Scaffolding JS -->
        <script src="{{ mix('js/laravel.app.js') }}"></script>

        <!-- Toastr -->
        <script src="{{ asset('/js/plugins/toastr/toastr.js') }}" ></script>

        <script src="{{ asset('/js/pages/be_pages_dashboard.min.js') }}" ></script>

        <script src="{{ asset('/js/plugins/chartjs/Chart.bundle.min.js') }}" ></script>

        <!-- Page JS Plugins -->
        <script src="{{ asset('/js/functions/option-selected.js')}}"></script>
        <script src="{{ asset ('/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }} "></script>
        <script src="{{ asset ('/js/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }} "></script>
        <script src="{{ asset ('/js/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }} "></script>
        <script src="{{ asset ('/js/plugins/select2/js/select2.full.min.js') }} "></script>
        <script src="{{ asset ('/js/plugins/jquery-validation/jquery.validate.min.js') }} "></script>
        <script src="{{ asset ('/js/plugins/datatables/jquery.dataTables.min.js') }} "></script>
        <script src="{{ asset ('/js/plugins/datatables/dataTables.bootstrap4.min.js') }} "></script>
        <script src="{{ asset ('/js/pages/be_tables_datatables.min.js') }} "></script>
        <script src="{{ asset ('/js/plugins/datatables/buttons/dataTables.buttons.min.js') }} "></script>
        <script src="{{ asset ('/js/plugins/datatables/buttons/buttons.flash.min.js') }} "></script>
        <script src="{{ asset ('/js/plugins/datatables/buttons/buttons.html5.min.js') }} "></script>
        <script src="{{ asset ('/js/plugins/datatables/buttons/buttons.print.min.js') }} "></script>
        <script src="{{ asset ('/js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.js') }} "></script>
        <script src="{{ asset ('/js/plugins/ajax/jszip.min.js') }} "></script>
        <script src="{{ asset ('/js/plugins/ajax/pdfmake.min.js') }} "></script>
        <script src="{{ asset ('/js/plugins/ajax/vfs_fonts.js') }} "></script>
        <script src="{{ asset ('/js/plugins/datatables/dataTables.responsive.min.js') }} "></script>
        <script src="{{ asset ('/js/plugins/select2/js/select2.full.min.js') }}"></script>
        <script src="{{ asset ('/js/plugins/jquery-tags-input/jquery.tagsinput.min.js') }}"></script>
        <script src="{{ asset ('/js/plugins/jquery-auto-complete/jquery.auto-complete.min.js')}}"></script>
        <script src="{{ asset ('/js/plugins/masked-inputs/jquery.maskedinput.min.js') }}"></script>
        <script src="{{ asset ('/js/plugins/ion-rangeslider/js/ion.rangeSlider.min.js')}}"></script>
        <script src="{{ asset ('/js/plugins/dropzonejs/dropzone.min.js')}}"></script>
        <script src="{{ asset ('/js/plugins/pwstrength-bootstrap/pwstrength-bootstrap.min.js')}}"></script>
        <script src="/js/plugins/moment/moment.min.js"></script>
        <script src="/js/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
        <script src="/js/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js"></script>
        <script src="/js/plugins/slick/slick.min.js"></script>
        <script src="/js/pages/be_pages_dashboard.min.js"></script>
        <script src="/js/plugins/sweetalert2/sweetalert2.min.js"></script>
        <script src="/js/pages/be_ui_activity.min.js"></script>

        <!-- Page JS Helpers (BS Notify Plugin) -->
        <script>jQuery(function(){ Codebase.helpers('notify'); });</script>

        <script type="text/javascript">
            function logout(event){
                    event.preventDefault();
             }
        </script>
        <script>jQuery(function(){ Codebase.helpers(['datepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']); });</script>
        @yield('js_after')
        @include('flash-message')
    </body>
</html>
