@extends('layouts.backend')
@section('content')
                <!-- Page Content -->
                <div class="content">
                    <h2 class="content-heading">Usuarios</h2>

                    <!-- Dynamic Table Full -->
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Ver Usuarios</h3>
                            <a href="/user/create" data-toggle="tooltip" title="Crear Usuario" class="btn btn-success">
                                <i class="si si-user-follow"></i>
                            </a>
                        </div>

                        <div class="block-content block-content-full">
                            <!-- DataTables functionality is initialized with .js-dataTable-full class in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                            <table id="aprendices" class="table js-dataTable-full" width="100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Usuario</th>
                                        <th>Correo electrónico</th>
                                        <th>Estado</th>
                                        <th>Rol</th>
                                        <th>Actividad</th>


                                        <th class="noExportExc noExport">Opciones</th>

                                        {{-- <th class="d-none d-sm-table-cell" style="width: 15%;">Access</th>
                                        <th class="text-center" style="width: 15%;">Profile</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($user as $us)
                                    <tr>
                                        <td class="font-w600">{{$loop->iteration}}</td>
                                        <td class="font-w600">{{$us->name}}</td>
                                        <td class="font-w600">{{$us->email}}</td>
                                        <td>{{$us->trashed() ? 'Inactivo' : 'Activo'}}</td>
                                        <td class="font-w600">{{ucfirst($us->roles->implode('name',', '))}}</td>
                                        <td class="d-none d-sm-table-cell">
                                            
                                                @if($us->isOnline())
                                               <h6> <span class="badge badge-success">En línea</span></h6>
                                                @else
                                                <h6> <span class="badge badge-secondary">Desconectado</span></h6>
                                                @endif                            
                                            </td>
                                        <td class="text-center">
                                            @if ($us->trashed())
                                            <a href="/user/restore/{{$us->id}}" class="btn btn-sm btn-success" data-toggle="tooltip" title="Restaurar usuario">
                                                <i class="fa fa-undo"></i>
                                            </a>
                                            @else
                                            <a href="/user/edit/{{$us->id}}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Editar usuario">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <span></span>
                                            <a href="/user/delete/{{$us->id}}" class="btn btn-sm btn-danger" data-toggle="tooltip" title="Cambiar estado">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->
                </div>
@endsection

