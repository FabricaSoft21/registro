@extends('layouts.backend')

@section('content')
                <div class="content">
                    <h2 class="content-heading">Usuarios</h2>
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Actualizar Usuario</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option">
                                    <i class="si si-user-follow"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="row justify-content-center py-20">
                                <div class="col-xl-8">
                                    <form class="js-wizard-validation-material-form" action="" method="POST">
                                            {{ csrf_field() }}
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="name" name="name" value="{{old('name',$usuario->name)}}">
                                                    <label for="name">Usuario <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-vcard"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="email" name="email" value="{{old('email',$usuario->email)}}">
                                                <label for="email">Correo electrónico <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-at"></i>
                                                    </span>
                                                </div>
                                        </div>
                                    </div>


                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="password" id="password" name="password" value="">
                                                <label for="password">Contraseña <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-asterisk"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="password" id="password_confirmation" name="password_confirmation" value="">
                                                <label for="password_confirmation">Confirmar contraseña <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-asterisk"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="form-material">
                                                <select class="js-select2 form-control" id="rol" name="rol" style="width: 100%;" data-allow-clear="true">
                                                    <option value="" selected>Seleccione rol</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                    @foreach ($roles as $key => $value)
                                                    @if ($usuario->hasRole($value))
                                                        <option value="{{ $key }}" selected>{{ ucfirst($value) }}</option>
                                                    @else
                                                        <option value="{{ $key }}">{{ ucfirst($value) }}</option>
                                                    @endif
                                                @endforeach

                                                </select>
                                                <label for="rol">Rol <span class="text-danger">*</span></label>
                                        </div>
                                    </div>


                                        <div class="form-group row">
                                            <div class="col-lg-8 ml-auto">
                                                <button type="submit" class="btn btn-alt-success pull-right">Actualizar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
@endsection

@section('js_after')
        <!-- Page JS Plugins -->
        <script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="/js/plugins/jquery-validation/additional-methods.js"></script>

        <!-- Page JS Code -->
        <script src="/js/pages/be_forms_wizard.min.js"></script>

@endsection
