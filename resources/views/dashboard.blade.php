@extends('layouts.backend')
@section('content')

                <!-- Page Content -->
                <div class="content">
                        <div class="row gutters-tiny invisible" data-toggle="appear">
                            <!-- Row #1 -->
                            <div class="col-6 col-md-4 col-xl-2">
                                    <a class="block text-center" data-toggle="modal" data-target="{{count($poblacion) == 0 ? "#modal-fadein" : "#modal-fadein2" }}" title="Ver formación" id="addMeta" onclick="{{count($poblacion) != 0 ? "getPer() ": ''}}">
                                        <div class="block-content bg-gd-primary">
                                            <p class="mt-5">
                                                <i class="si si-badge fa-3x text-white-op"></i>
                                            </p>
                                            @if(count($poblacion) == 0)
                                            <p class="font-w600 text-white" id="newMeta">Nueva Meta</p>
                                            @else
                                            <p class="font-w600 text-white">Ver Meta</p>
                                            @endif
                                        </div>
                                    </a>
                                </div>
                            <div class="col-6 col-md-4 col-xl-2">
                                <a class="block text-center">
                                    <div class="block-content ribbon ribbon-bookmark ribbon-crystal ribbon-left bg-gd-dusk">
                                        @isset($poblacion)
                                        <div class="font-size-h3 font-w600 text-success" data-speed="1000" data-to="">@foreach ( $poblacion as $pob )
                                                {{$pob->metaCupos}}
                                            @endforeach</div>
                                        @endisset
                                        @if(count($poblacion) == 0)
                                        <div class="font-size-h3 font-w600 text-success" id="data-count1" data-speed="1000" data-to="">0</div>
                                        @endif
                                    <p class="font-w600 text-white">  <i class="fa fa-asterisk fa-2x text-white-op"></i> Meta de cupos para el {{date('Y')}}

                                    </p>

                                    </div>
                                </a>
                            </div>

                            <div class="col-6 col-md-4 col-xl-2">
                                <a class="block text-center" href="#">
                                    <div class="block-content ribbon ribbon-bookmark ribbon-crystal ribbon-left bg-gd-sea">
                                            @isset($poblacion)
                                            <div class="font-size-h3 font-w600 text-success" data-speed="1000" data-to="">@foreach ( $poblacion as $pob )
                                                    {{$pob->metaAprendiz}}
                                                @endforeach</div>
                                            @endisset
                                            @if(count($poblacion) == 0)
                                            <div class="font-size-h3 font-w600 text-success" id="data-count2" data-speed="1000" data-to="">0</div>
                                            @endif
                                            <p class="font-w600 text-white">  <i class="fa fa-child fa-2x text-white-op"></i> Meta de Aprendices para {{date('Y')}}

                                            </p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-md-4 col-xl-2">
                                <a class="block text-center" target="_blank" href="http://oferta.senasofiaplus.edu.co/sofia-oferta/">
                                    <div class="block-content bg-earth">
                                        <p class="mt-5">
                                            <i class="fa fa-paper-plane-o fa-3x text-white-op"></i>
                                        </p>
                                        <p class="font-w600 text-white">SofiaPlus</p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-md-4 col-xl-2">
                                <a class="block text-center" target="_blank" href="http://www.sena.edu.co/es-co/transparencia/Paginas/seriesEstadisticas.aspx">
                                    <div class="block-content bg-gd-emerald">
                                        <p class="mt-5">
                                            <i class="si si-bar-chart fa-3x text-white-op"></i>
                                        </p>
                                        <p class="font-w600 text-white">Estadisticas</p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-md-4 col-xl-2">
                                <a class="block text-center"  data-toggle="modal" data-target= "#modal-report">
                                    <div class="block-content bg-gd-corporate">
                                        <p class="mt-5">
                                            <i class="fa fa-line-chart fa-3x text-white-op"></i>
                                        </p>
                                        <p class="font-w600 text-white">Reportes</p>
                                    </div>
                                </a>
                            </div>
                            <!-- END Row #1 -->
                        </div>
                        <div class="row row-deck gutters-tiny invisible" data-toggle="appear">
                            <!-- Row #2 -->
                            <div class="col-xl-4">
                                <a class="block block-transparent bg-image d-flex align-items-stretch" href="javascript:void(0)" style="background-image: url('/media/avatars/virtualidad.jpg');">
                                    <div class="block-content block-sticky-options pt-100 bg-black-op">
                                        <div class="block-options block-options-left text-white">
                                            <div class="block-options-item">
                                                <i class="si si-book-open text-white-op"></i>
                                            </div>
                                        </div>
                                        <div class="block-options text-white">
                                            <div class="block-options-item">
                                                <i class="si si-bubbles"></i>
                                            </div>
                                            <div class="block-options-item">
                                                <i class="si si-eye"></i>
                                            </div>
                                        </div>
                                        <h2 class="h3 font-w700 text-white mb-5">Virtualidad</h2>
                                        <h3 class="h5 text-white-op"></h3>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-xl-4">
                                <a class="block block-transparent bg-image d-flex align-items-stretch" href="javascript:void(0)" style="background-image: url('/media/avatars/sena.jpg');">
                                    <div class="block-content block-sticky-options pt-100 bg-primary-dark-op">
                                        <div class="block-options block-options-left text-white">
                                            <div class="block-options-item">
                                                <i class="si si-book-open text-white-op"></i>
                                            </div>
                                        </div>
                                        <div class="block-options text-white">
                                            <div class="block-options-item">
                                                <i class="si si-bubbles"></i>
                                            </div>
                                            <div class="block-options-item">
                                                <i class="si si-eye"></i>
                                            </div>
                                        </div>
                                        <h2 class="h3 font-w700 text-white mb-5">Aprendizaje</h2>
                                        <h3 class="h5 text-white-op"></h3>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-xl-4">
                                <a class="block block-transparent bg-image d-flex align-items-stretch" href="javascript:void(0)" style="background-image: url('/media/avatars/vulnerable.png');">
                                    <div class="block-content block-sticky-options pt-100 bg-primary-op">
                                        <div class="block-options block-options-left text-white">
                                            <div class="block-options-item">
                                                <i class="si si-book-open text-white-op"></i>
                                            </div>
                                        </div>
                                        <div class="block-options text-white">
                                            <div class="block-options-item">
                                                <i class="si si-bubbles"></i>
                                            </div>
                                            <div class="block-options-item">
                                                <i class="si si-eye"></i>
                                            </div>
                                        </div>
                                        <h2 class="h3 font-w700 text-white mb-5">Vulnerabilidad</h2>
                                        <h3 class="h5 text-white-op"></h3>
                                    </div>
                                </a>
                            </div>
                            <!-- END Row #2 -->
                        </div>
                        <div class="row gutters-tiny invisible" data-toggle="appear">
                            <!-- Row #3 -->
                            <div class="col-xl-8 d-flex align-items-stretch">
                                <div class="block block-themed block-mode-loading-inverse block-transparent bg-image w-100" style="background-image: url('/media/photos/photo34@2x.jpg');">
                                    <div class="block-header bg-black-op">
                                        <h3 class="block-title">
                                            Aprendices registrados
                                        </h3>
                                        <div class="block-options">
                                            <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                                                <i class="si si-refresh"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="block-content bg-black-op">
                                        <div class="pull-r-l">
                                            <!-- Lines Chart Container functionality is initialized in js/pages/be_pages_dashboard.min.js which was auto compiled from _es6/pages/be_pages_dashboard.js -->
                                            <!-- For more info and examples you can check out http://www.chartjs.org/docs/ -->
                                            <canvas id="myChart"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 d-flex align-items-stretch">
                                <div class="block block-transparent bg-primary-dark d-flex align-items-center w-100">
                                    <div class="block-content block-content-full">
                                        <div class="py-15 px-20 clearfix border-black-op-b">
                                            <div class="float-right mt-15 d-none d-sm-block">
                                                <i class="fa fa-child fa-2x text-success"></i>
                                            </div>
                                            <div class="font-size-h3 font-w600 text-success" data-toggle="countTo" data-speed="1000" data-to="{{$aprendices}}">0</div>
                                            <div class="font-size-sm font-w600 text-uppercase text-success-light">Aprendices</div>
                                        </div>
                                        <div class="py-15 px-20 clearfix border-black-op-b">
                                            <div class="float-right mt-15 d-none d-sm-block">
                                                <i class="si si-book-open fa-2x text-danger"></i>
                                            </div>
                                            <div class="font-size-h3 font-w600 text-danger"><span data-toggle="countTo" data-speed="1000" data-to="{{$programas_formacion}}">0</span></div>
                                            <div class="font-size-sm font-w600 text-uppercase text-danger-light">Programas de formacion</div>
                                        </div>
                                        <div class="py-15 px-20 clearfix border-black-op-b">
                                            <div class="float-right mt-15 d-none d-sm-block">
                                                <i class="fa fa-industry fa-2x text-warning"></i>
                                            </div>
                                            <div class="font-size-h3 font-w600 text-warning" data-toggle="countTo" data-speed="1000" data-to="{{$empresa}}">0</div>
                                            <div class="font-size-sm font-w600 text-uppercase text-warning-light">Empresas</div>
                                        </div>
                                        <div class="py-15 px-20 clearfix border-black-op-b">
                                            <div class="float-right mt-15 d-none d-sm-block">
                                                <i class="si si-users fa-2x text-info"></i>
                                            </div>
                                            <div class="font-size-h3 font-w600 text-info" data-toggle="countTo" data-speed="1000" data-to="{{$instructores}}">0</div>
                                            <div class="font-size-sm font-w600 text-uppercase text-info-light">Instructores</div>
                                        </div>
                                        <div class="py-15 px-20 clearfix">
                                            <div class="float-right mt-15 d-none d-sm-block">
                                                <i class="fa fa-university fa-2x text-elegance"></i>
                                            </div>
                                            <div class="font-size-h3 font-w600 text-elegance" data-toggle="countTo" data-speed="1000" data-to="{{$instituciones}}">0</div>
                                            <div class="font-size-sm font-w600 text-uppercase text-elegance-light">Instituciones educativas</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END Row #3 -->
                        </div>
                    </div>
                    <!-- END Page Content -->
<div class="modal fade" id="modal-fadein" tabindex="-1" role="dialog" aria-labelledby="modal-terms" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-slidedown" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-gd-leaf">
                    <h3 class="block-title">Registrar Nueva Meta</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                <div class="block block-themed block-transparent mb-0">
                        <div class="block-content block-content-full text-center">

                </div>
                <div class="row justify-content-center py-20">
                        <div class="col-xl-8">
                            <span id="form_result"></span>
                        <form id="form-vuln" class="js-validation-material" action="" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="form-material input-group floating">
                                <input class="form-control" value="{{old('metaAprendiz')}}" type="text" id="metaAprendiz" name="metaAprendiz">
                                <label for="metaAprendiz">Meta de Aprendices <span class="text-danger">*</span></label>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="fa fa-child"></i>
                                    </span>
                                </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-material input-group floating">
                            <input class="form-control" type="text" value="{{old('metaCupos')}}" id="metaCupos" name="metaCupos">
                            <label for="metaCupos">Meta de Cupos <span class="text-danger">*</span></label>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="fa fa-asterisk"></i>
                                </span>
                            </div>
                    </div>
                </div>

                </div>
            </div>
            </div>
            <div class="modal-footer">
                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-alt-success">
                    <i class="fa fa-check"></i> Guardar
                </button>
            </div>
        </form>
        </div>
    </div>
</div>
</div>

<div class="modal fade" id="modal-fadein2" tabindex="-1" role="dialog" aria-labelledby="modal-terms" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-slidedown" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-gd-leaf">
                    <h3 class="block-title">Ver Meta</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                <div class="block block-themed block-transparent mb-0">
                        <div class="block-content block-content-full text-center">

                </div>
                <div class="row justify-content-center py-20">
                        <div class="col-xl-8">
                            <span id="form_result"></span>
                        <div class="form-group">
                            <div class="form-material input-group">
                                <input class="form-control" value="" type="text" id="metaA" name="metaAprendiz" disabled>
                                <label for="metaAprendiz">Meta aprendices<span class="text-danger">*</span></label>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="fa fa-child"></i>
                                    </span>
                                </div>
                        </div>
                    </div>

                    <div class="form-group">
                            <div class="form-material input-group">
                                <input class="form-control" value="" type="text" id="metac" name="metaAprendiz" disabled>
                                <label for="metaAprendiz">Meta cupos<span class="text-danger">*</span></label>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="fa fa-weixin"></i>
                                    </span>
                                </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-material input-group">
                            <input class="form-control" type="text" value="" id="ejecM" name="EjecA" disabled>
                            <label for="metaCupos">Ejecución meta<span class="text-danger">*</span></label>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="fa fa-certificate"></i>
                                </span>
                            </div>
                    </div>
                </div>

                    <div class="form-group">
                            <div class="form-material input-group">
                                <input class="form-control" type="text" value="" id="porcMeta" name="metaCupos" disabled>
                                <label for="metaCupos">Porcentaje de meta<span class="text-danger">*</span></label>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="fa fa-percent"></i>
                                    </span>
                                </div>
                        </div>
                    </div>

                <div class="form-group">
                    <div class="form-material input-group">
                        <input class="form-control" type="text" value="" id="ejecA" name="EjecA" disabled>
                        <label for="metaCupos">Ejecución Aprendices <span class="text-danger">*</span></label>
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="fa fa-certificate"></i>
                            </span>
                        </div>
                </div>
            </div>

                <div class="form-group">
                        <div class="form-material input-group">
                            <input class="form-control" type="text" value="" id="porcAprendiz" name="metaCupos" disabled>
                            <label for="metaCupos">Porcentaje de aprendices<span class="text-danger">*</span></label>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="fa fa-percent"></i>
                                </span>
                            </div>
                    </div>
                </div>


            </div>
            </div>
            </div>
            <div class="modal-footer">
            <form action="{{route('deleteP',1)}}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button type="submit" class="btn btn-alt-danger">
                            <i class="fa fa-trash"></i> Borrar
                        </button>
                    </form>
                    <button type="button" class="btn btn-alt-success">
                    <i class="fa fa-check"></i> Guardar
                </button>

            </div>
        </div>
    </div>
</div>
</div>

<div class="modal fade" id="modal-report"div class="modal" tabindex="-1" role="dialog" aria-labelledby="modal-large" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-gd-sea">
                    <h3 class="block-title">Generación de reportes</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                <div class="block block-themed block-transparent mb-0">
                        <div class="block-content block-content-full text-center">

                </div>
                <div class="row justify-content-center py-20">
                        <div class="col-xl-8">
                            <span id="form_result"></span>
                        <form id="form-report" class="js-validation-material" action="{{route('reportA')}}" method="GET" target="_blank">
                        <h2 class="content-heading mb-10" style="text-align:center;">Selecciona una opción</h2>
                        <div class="js-task-list">

                            <!-- Task -->
                            <div class="js-task block block-rounded mb-5 animated fadeIn" data-task-id="9" data-task-completed="false" data-task-starred="false">
                                <table class="table table-borderless table-vcenter mb-0">
                                    <tr>
                                        <td class="text-center" style="width: 50px;">
                                        <div class="custom-control custom-radio mb-5 " >
                                            <input class="custom-control-input" type="radio" name="example-radios" id="example-radio1" value="option1" checked>
                                            <label class="custom-control-label css-control-lg " for="example-radio1"></label>
                                        </div>
                                        </td>
                                        <td class="js-task-content font-w600">
                                            Aprendices
                                        </td>
                                        <td class="text-right" style="width: 100px;">
                                                <i class="fa fa-star-o"></i>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!-- END Task -->

                            <!-- Task -->
                            <div class="js-task block block-rounded mb-5 animated fadeIn" data-task-id="8" data-task-completed="false" data-task-starred="false">
                                <table class="table table-borderless table-vcenter mb-0">
                                    <tr>
                                        <td class="text-center" style="width: 50px;">
                                            <div class="custom-control custom-radio mb-5">
                                                <input class="custom-control-input" type="radio" name="example-radios" id="example-radio2" value="option2">
                                                <label class="custom-control-label css-control-lg" for="example-radio2"></label>
                                            </div>
                                        </td>
                                        <td class="js-task-content font-w600">
                                            Instructores
                                        </td>
                                        <td class="text-right" style="width: 100px;">
                                            <i class="fa fa-star-o"></i>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!-- END Task -->

                            <!-- Task -->
                            <div class="js-task block block-rounded mb-5 animated fadeIn" data-task-id="7" data-task-completed="false" data-task-starred="false">
                                <table class="table table-borderless table-vcenter mb-0">
                                    <tr>
                                        <td class="text-center" style="width: 50px;">
                                            <div class="custom-control custom-radio mb-5">
                                                <input class="custom-control-input" type="radio" name="example-radios" id="example-radio3" value="option3">
                                                <label class="custom-control-label css-control-lg" for="example-radio3"></label>
                                            </div>
                                        </td>
                                        <td class="js-task-content font-w600">
                                           Programación
                                        </td>
                                        <td class="text-right" style="width: 100px;">
                                                <i class="fa fa-star-o"></i>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!-- END Task -->
                        </div>

            </div>
            </div>
            </div>
            <div class="modal-footer">
                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-alt-success" id="submit_button">
                    <i class="fa fa-check"></i> Generar
                </button>
            </div>
        </form>
        </div>
    </div>
</div>
</div>
@endsection

@section('js_after')

<script src="/js/functions/custom-file-input.js"></script>
<!-- Page JS Plugins -->
<script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
<script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="/js/plugins/jquery-validation/additional-methods.js"></script>

<!-- Page JS Code -->
<script src="/js/pages/be_forms_wizard.min.js"></script>
<script>
        $('#form-vuln').on('submit',function(event){
            event.preventDefault();
            $.ajax({
                url:"{{ route('vulnerable')}}",
                method:"POST",
                data : new FormData(this),
                contentType:false,
                cache:false,
                processData:false,
                dataType:"json",
                success:function(data)
                {
                    // console.log(data);
                    var html = "";
                    if(data.errors){
                        html = '<div class="alert alert-danger">';
                            for(var count = 0; count < data.errors.length; count++)
                            {
                                html += '<p>' + data.errors[count] +
                                    '</p>';
                            }
                            html += '</div>';
                    }
                    if(data.success)
                    {

                    html = '<div class="alert alert-success">'
                        + data.success + '</div>';
                        $('#form-vuln')[0].reset();

                    }
                    $('#form_result').html(html);
                        $('#metaAprendiz').attr('disabled',true);
                        $('#metaCupos').attr('disabled',true);
                        $('#addMeta').attr('data-target','#modal-fadein2');
                        $('#newMeta').html(`<span>Ver meta</span>`);

                        $.ajax({
                        url:"/vulnera/show",
                        type:'get',
                        success:function(data){
                            $('#data-count1').html(`<span>${data.data["0"].metaCupos}</span>`);
                            $('#data-count2').html(`<span>${data.data["0"].metaAprendiz}</span>`);
                            $('#metaA').val(data.data["0"].metaAprendiz);
                            $('#metac').val(data.data["0"].metaCupos);

                            $('#ejecA').val(data.data["0"].ejecAprendiz);
                            $('#ejecM').val(data.data["0"].ejecCupos);

                            $('#porcAprendiz').val(Math.round((data.data["0"].ejecAprendiz * 100) / data.data["0"].metaAprendiz) + "%")
                            $('#porcMeta').val(Math.round((data.data["0"].ejecCupos * 100) / data.data["0"].metaCupos) + "%")
                        },
                        error:function(){
                            alert("error");
                        }
                    });
                }
            })
        });
</script>

<script>
        function getPer(){
        $.ajax({
        url:"/vulnera/show",
        type:'get',
        success:function(data){

            $('#metaA').val(data.data["0"].metaAprendiz);
            $('#metac').val(data.data["0"].metaCupos);

            $('#ejecA').val(data.data["0"].ejecAprendiz);
            $('#ejecM').val(data.data["0"].ejecCupos);

            $('#porcAprendiz').val(Math.round((data.data["0"].ejecAprendiz * 100) / data.data["0"].metaAprendiz) + "%")
            $('#porcMeta').val(Math.round((data.data["0"].ejecCupos * 100) / data.data["0"].metaCupos) + "%")

        },
            error:function(){
                alert("error");
            }
        });
    }
</script>

<script>

$(document).ready(function(){
  $('#submit_button').click(function() {
    opt = $("input[name='example-radios']:checked").val();

    if(opt == "option1"){
        $('#form-report').attr('action',"{{route('reportA')}}")
    }else if(opt == "option2"){
        $('#form-report').attr('action',"{{route('reportI')}}")
    }else if(opt == "option3"){
        $('#form-report').attr('action',"{{route('reportP')}}")
    }

  });
});

</script>

{{-- Canvas report --}}

<script>
var data = [];

@foreach ($dates as $date => $count )
      data.push({{ $count }})
@endforeach

var ctx = document.getElementById('myChart');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ['LUNES', 'MARTES', 'MIERCOLES', 'JUEVES', 'VIERNES','SÁBADO', 'DOMINGO'],
        datasets: [{
            label: 'Esta semana',
            fill:!0,
            data: [data[2],data[3],data[4],data[5],data[6],data[0],data[1]],
            backgroundColor:"rgba(66,165,245,.45)",
            borderColor:"rgba(66,165,245,1)",
            pointBackgroundColor:"rgba(66,165,245,1)",
            pointBorderColor:"#fff",
            pointHoverBackgroundColor:"#fff",
            pointHoverBorderColor:"rgba(66,165,245,1)",
        }]
    },
    options: {
        legend: {
             labels: {
                  fontColor: 'white'
                 }
              },
        scales: {
            yAxes: [{
                ticks: {
                    suggestedMax:50,
                    fontColor: 'white',
                    beginAtZero: true
                }
            }],
            xAxes: [{
                ticks: {
                    fontColor: 'white',
                }
            }]
        }
    }
});

</script>

@endsection
