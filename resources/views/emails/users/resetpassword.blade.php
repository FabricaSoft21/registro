@component('mail::message')
# Hola {{ $name }}
Está recibiendo este correo electrónico porque recibimos una solicitud de restablecimiento de contraseña para su cuenta.
@component('mail::button', ['url' => $url])
Restablecer la contraseña
@endcomponent
Saludos,<br>
{{ config('app.name') }}
@component('mail::subcopy', ['url' => $url])
Si tienes problemas para hacer clic en el botón "Restablecer", copia y pega la URL a continuación
en su navegador web: [{{ $url}}]({{ $url}})
@endcomponent
@endcomponent
