@extends('layouts.backend')
@section('content')
                <!-- Page Content -->
                <div class="content">
                    <h2 class="content-heading">Institución Educativa</h2>

                    <!-- Dynamic Table Full -->
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Ver Instituciones</h3>
                            <a href="/instiEdu/create" data-toggle="tooltip" title="Crear Institución" class="btn btn-success">
                                <i class="fa fa-plus"></i>
                                
                                </a>
                                <a  data-toggle="modal" type="file" data-target="#modal-fromleft" title="Importar datos" class="btn btn-secondary ml-2" style={{$cant == 0 ? "display:none" : "" }}>
                                        <i class="fa fa-folder-open-o"></i>
                                    </a>
                        </div>
                        <div class="block-content block-content-full">
                        @if($cant == 0)
                        <div class="col-md-12">
                                <div class="block">
                                    <div class="block-content block-content-full">
                                        <div class="py-20 text-center">
                                            <div class="mb-20">
                                                <i class="fa fa-close fa-5x text-danger"></i>
                                            </div>
                                            <div class="font-size-h4 font-w600">Lo sentimos</div>
                                            <div class="text-muted">Actualmente no hay ningún registro</div>
                                            <form class="js-wizard-validation-material-form" method="post" action="" enctype="multipart/form-data">
                                             @csrf
                                            <div class="pt-20 mb-3">
                                            <div class="form-group box">
                                                <input type="file" name="file-selected" id="file-selected" class="file-selected inputfile inputfile-3" />
                                                <label for="file-selected"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Elige un archivo&hellip;</span></label>
                                            </div>
                                            </div>
                                            <button type="submit" class="btn btn-rounded btn-alt-success" style="width:200px !important;">
                                                    <i class="fa fa-file-excel-o mr-5"></i> Importar
                                            </button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            @else
                            <table id="aprendices" class="table js-dataTable-full" width="100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>NIT</th>
                                        <th>Código DANE</th>
                                        <th>Institución</th>
                                        <th>Teléfono</th>
                                        <th>Dirección</th>
                                        <th>Correo</th>
                                        <th>Resolución</th>
                                        {{-- <th>Estado</th> --}}
                                        <th>Tipo</th>
                                        <th>Categoría</th>
                                        <th>Coordinación</th>
                                        <th>Rector</th>
                                        <th>Departamento</th>
                                        <th>Municipio</th>
                                        <th class="noExportExc noExport">Opciones</th>
                               
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($institucion as $col)
                                    <tr>
                                        <td class="font-w600">{{$loop->iteration}}</td>
                                        <td class="font-w600">{{$col->nit}}</td>
                                        <td class="font-w600">{{$col->codigoDane}}</td>
                                        <td class="font-w600">{{$col->nombreColegio}}</td>
                                        <td class="font-w600">{{$col->telefono}}</td>
                                        <td class="font-w600">{{$col->direccion}}</td>
                                        <td class="font-w600">{{$col->correo}}</td>
                                        <td class="font-w600">{{$col->numResolucion}}</td>
                                        {{-- <td class="font-w600">{{$col->estado->nombre}}</td> --}}
                                        <td class="font-w600">{{$col->tipoColegio->nombre}}</td>
                                        <td class="font-w600">{{$col->categoria->nombre}}</td>
                                        <td class="font-w600">{{$col->coordinador->nombre}}</td>
                                        <td class="font-w600">{{$col->rector->nombre}}</td>
                                        <td class="font-w600">{{$col->departamento->nombre}}</td>
                                        <td class="font-w600">{{$col->municipio->nombre}}</td>
                                        <td class="text-center">
                                         
                                            @if ($col->trashed())
                                            <a href="/instiEdu/restore/{{$col->id}}" class="btn btn-sm btn-success" data-toggle="tooltip" title="Restaurar Institución">
                                                <i class="fa fa-undo"></i>
                                            </a> 
                                            @else
                                            <a href="/instiEdu/edit/{{$col->id}}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Editar Institución">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <span></span>
                                             <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal-fromleft-{{$col->id}}"title="Ver Institución">
                                                <i class="fa fa-eye"></i>
                                            </button>

                                            <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-lista-{{$col->id}}"title="Ver Lista" onclick="{{"getList(".$col->id}})">
                                                    <i class="si si-list"></i>
                                            </button>

                                            <a href="/instiEdu/delete/{{$col->id}}" class="btn btn-sm btn-danger" data-toggle="tooltip" title="Cambiar estado">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                            @endif
                                          

                                        </td>
                                    </tr>
        
                                    @include('colegios.show')
                                    
                                    @endforeach
                                </tbody>
                            </table>
                            @endif
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modal-fromleft" tabindex="-1" role="dialog" aria-labelledby="modal-fromleft" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-fromleft modal-lg" role="document">
                        <div class="modal-content">
                         <div class="block block-themed block-transparent mb-0">
                                <div class="block-header bg-primary-dark">
                                <h3 class="block-title">IMPORTANCIÓN DE DATOS</h3>
                                    <div class="block-options">
                                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                            <i class="si si-close"></i>
                                     </button>
                                    </div>
                                </div>
                             <div class="block block-themed block-transparent mb-0">
                                    <div class="block block-themed">
                                            <div class="block-content block-content-full text-center bg-primary-light">
                                                <img class="img-avatar img-avatar96 img-avatar-thumb" src="media/various/logo.png" alt="">
                                             </h3>
                                            </div>
                                            <div class="col-md-12">
                                                    <!-- Simple Wizard 2 -->
                                                    <div class="js-wizard-simple block">
                                                        <!-- Step Tabs -->
                                                        <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                                            <li class="nav-item">
                                                                <a class="nav-link active" href="#wizard-simple2-step1" data-toggle="tab">IMPORTAR</a>
                                                            </li>
                                                        </ul>
                                                        <!-- END Step Tabs -->
                
                                                        <!-- Form -->
                                                        <form action="" method="post" class="js-wizard-validation-material-form" enctype="multipart/form-data">
                                                            @csrf
                                                            <div class="col-md-12">
                                                                    <div class="block">
                                                                        <div class="block-content block-content-full">
                                                                            <div class="py-20 text-center">
                                                                                <div class="mb-20">
                                                                                    <i class="fa fa-info fa-5x text-info"></i>
                                                                                </div>
                                                                                <div class="font-size-h4 font-w600">¡Espera!</div>
                                                                                <div class="text-muted">Recuerda subir un archivo con la extensión y el formato correcto.
                                                                                </div>
                                                                                <div class="pt-20 mb-3">
                                                                                <div class="form-group box">
                                                                                    <input type="file" name="file-selected" id="file-selected" class="file-selected inputfile inputfile-3" />
                                                                                    <label for="file-selected"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Elige un archivo&hellip;</span></label>
                                                                                </div>
                                                                                <div class="block block-mode-hidden">
                                                                                        <ul class="nav nav-tabs nav-tabs-block align-items-center" data-toggle="tabs" role="tablist">
                                                                                            <li class="nav-item">
                                                                                                <a class="nav-link" href="#">Vista previa</a>
                                                                                            </li>
                                                                                            <li class="nav-item ml-auto">
                                                                                                <div class="block-options mr-15">
                                                                                                    <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                                                                                                        <i class="si si-refresh"></i>
                                                                                                    </button>
                                                                                                    <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"></button>
                                                                                                </div>
                                                                                            </li>
                                                                                        </ul>
                                                                                        <div class="block-content tab-content">
                                                                                            <div class="tab-pane active" id="btabswo-static-home" role="tabpanel">
                                                                                                <div class="resp-container">

                                                                                                    <iframe width="402" height="346" frameborder="0" scrolling="no" src="https://onedrive.live.com/embed?resid=1EF43E3FE1A12FAC%21107&authkey=%21ADXsfpPUxeQkgW4&em=2&wdAllowInteractivity=False&wdHideGridlines=True&wdHideHeaders=True&wdDownloadButton=True&wdInConfigurator=True"></iframe>                                                                                                    </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                        </div>
                                         <div class="modal-footer">
                                <button type="button" class="js-swal-info3 btn btn-alt-secondary pull-left">
                                <i class="fa fa-info text-info mr-5"></i> Nota
                                </button>
                                <button type="submit" class="btn btn-alt-success">
                                <i class="fa fa-file-excel-o mr-5"></i>  Guardar
                                </button>
                            </form>
                          </div>
                        </div>
                    </div>
                </div>
@endsection

@section('js_after')
@if($cant != 0)
@foreach($institucion as $col)
@include('colegios.checkList')
@endforeach
@endif
<script src="/js/functions/custom-file-input.js"></script>
        <!-- Page JS Plugins -->
        <script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="/js/plugins/jquery-validation/additional-methods.js"></script>

        <!-- Page JS Code -->
        <script src="/js/pages/be_forms_wizard.min.js"></script>
        <script src="/js/functions/get-colegio.js" ></script>
@endsection
