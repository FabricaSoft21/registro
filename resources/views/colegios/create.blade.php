@extends('layouts.backend')
@section('content')
<div class="content">
                    <h2 class="content-heading">Instución Educativa</h2>
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Nueva institución</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option">
                                    <i class="si si-user-follow"></i>
                                </button>
                            </div>
                        </div>
                        <div class="js-wizard-validation-material block">
                                <!-- Step Tabs -->
                                <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link" href="#wizard-validation-material-step1" data-toggle="tab">1. Personal</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#wizard-validation-material-step2" data-toggle="tab">2. Contacto</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#wizard-validation-material-step3" data-toggle="tab">3. Detalles</a>
                                    </li>
                                </ul>
                                <!-- END Step Tabs -->

                                <!-- Form -->
                                <form class="js-wizard-validation-material-form" action="" method="post">
                                {{ csrf_field() }}
                                    <!-- Steps Content -->
                                    <div class="block-content block-content-full tab-content" style="min-height: 267px;">
                                        <!-- Step 1 -->
                                        <div class="tab-pane" id="wizard-validation-material-step1" role="tabpanel">
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="nit" name="nit" value="{{old('nit')}}">
                                                    <label for="nit">NIT <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                                  <i class="fa fa-barcode"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="codigoDane" name="codigoDane" value="{{old('codigoDane')}}">
                                                    <label for="codigoDane">DANE <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                                <i class="fa fa-barcode"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="nombreColegio" name="nombreColegio" value="{{old('nombreColegio')}}">
                                                    <label for="nombreColegio">Nombre del colegio <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-address-card-o"></i>
                                                        </span>
                                                    </div>
                                                 </div>
                                        </div>
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="numResolucion" name="numResolucion" value="{{old('numResolucion')}}">
                                                    <label for="numResolucion">Número de Resolución <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-hashtag"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- END Step 1 -->

                                        <!-- Step 2 -->
                                    <div class="tab-pane" id="wizard-validation-material-step2" role="tabpanel">
                                        <div class="form-group">
                                        <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="correo" name="correo" value="{{old('correo')}}">
                                                    <label for="correo">Correo electrónico <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-envelope-o"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                        <div class="form-material">
                                                    <select class="js-select2 form-control" id="idDepartamento" name="idDepartamento" style="width: 100%;" data-allow-clear="true">
                                                        <option value="" selected>Seleccione Departamento</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                        @foreach($departamento as  $dep)
                                                        <option value="{{ $dep->id }}" {{ (old("idDepartamento") == $dep->id ? "selected":"")}}>{{$dep->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="idDepartamento">Departamento <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                        <div class="form-material">
                                                    <select class="js-select2 form-control" id="idMunicipio" name="idMunicipio" style="width: 100%;" data-allow-clear="true">
                                                        <option value="" selected>Seleccione Municipio</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                    </select>
                                                    <label for="idMunicipio">Municipio <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                        <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="direccion" name="direccion" value="{{old('direccion')}}">
                                                    <label for="direccion">Dirección <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="si si-pointer"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                        <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="telefono" name="telefono" value="{{old('telefono')}}">
                                                    <label for="telefono">Teléfono <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-phone"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                        </div>
                                        </div>
                                        <!-- END Step 2 -->

                                        <!-- Step 3 -->
                                        <div class="tab-pane active" id="wizard-validation-material-step3" role="tabpanel">
                                        {{-- <div class="form-group">
                                                    <div class="form-material">
                                                        <select class="js-select2 form-control" id="idEstado" name="idEstado" style="width: 100%;" data-allow-clear="true">
                                                            <option value="" selected>Seleccione Estado</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                            @foreach($estadoColegio as $estado)
                                                            <option value="{{ $estado->id }}" {{ (old("idEstado") == $estado->id ? "selected":"")}}>{{$estado->nombre}}</option>
                                                            @endforeach
                                                        </select>
                                                        <label for="idColegio">Estado de la Institución <span class="text-danger">*</span></label>
                                                    </div>
                                                </div> --}}

                                                <div class="form-group">
                                                    <div class="form-material">
                                                        <select class="js-select2 form-control" id="idTipo" name="idTipo" style="width: 100%;" data-allow-clear="true">
                                                            <option value="" selected>Seleccione Tipo</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                            @foreach($tipo as $tipoCol)
                                                            <option value="{{ $tipoCol->id }}" {{ (old("idTipo") == $tipoCol->id ? "selected":"")}}>{{$tipoCol->nombre}}</option>
                                                            @endforeach
                                                        </select>
                                                        <label for="idTipo">Tipo de Institución <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-material">
                                                        <select class="js-select2 form-control" id="idCategoria" name="idCategoria" style="width: 100%;" data-allow-clear="true">
                                                            <option value="" selected>Seleccione Categoría</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                            @foreach($categoriaCole as $categoria)
                                                            <option value="{{ $categoria->id }}" {{ (old("idCategoria") == $categoria->id ? "selected":"")}}>{{$categoria->nombre}}</option>
                                                            @endforeach
                                                        </select>
                                                        <label for="idColegio">Categoría de la Institución <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-material">
                                                        <select class="js-select2 form-control" id="idRector" name="idRector" style="width: 100%;" data-allow-clear="true">
                                                            <option value="" selected>Seleccione Rector</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                            @foreach($rector as $rec)
                                                            <option value="{{ $rec->id }}" {{ (old("idRector") == $rec->id ? "selected":"")}}>{{$rec->nombre}}</option>
                                                            @endforeach
                                                        </select>
                                                        <label for="idColegio">Rector de la Institución <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                    <div class="col-md-4">
                                                        <a class="link-effect text-muted mb-5 d-inline-block" href="#" data-toggle="modal" data-target="#modal-rector">
                                                            <i class="fa fa-plus-square text-muted mr-5"></i>¿Nuevo rector?
                                                        </a>
                                                    </div>
                                                <div class="form-group">
                                                    <div class="form-material">
                                                        <select class="js-select2 form-control" id="idcoordinacion" name="idcoordinacion" style="width: 100%;" data-allow-clear="true">
                                                            <option value="">Seleccione Coordinador</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                            @foreach($coordinador as $coor)
                                                            <option value="{{ $coor->id }}" {{ (old("idcoordinacion") == $coor->id ? "selected":"")}}>{{$coor->nombre}}</option>
                                                            @endforeach
                                                        </select>
                                                        <label for="idColegio">Coordinador de la Institución <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <a class="link-effect text-muted mb-5 d-inline-block" href="#" data-toggle="modal" data-target="#modal-cordi">
                                                        <i class="fa fa-plus-square text-muted mr-5"></i>¿Nuevo coordinador?
                                                    </a>
                                                </div>
                                    <!-- END Steps Content -->
                                    </div>
                                    <!-- Steps Navigation -->
                                    <div class="block-content block-content-sm block-content-full bg-body-light">
                                        <div class="row">
                                            <div class="col-6">
                                                <button type="button" class="btn btn-alt-secondary" data-wizard="prev">
                                                    <i class="fa fa-angle-left mr-5"></i> Atrás
                                                </button>
                                            </div>
                                            <div class="col-6 text-right">
                                                <button type="button" class="btn btn-alt-secondary" data-wizard="next">
                                                    Siguiente <i class="fa fa-angle-right ml-5"></i>
                                                </button>
                                                <button type="submit" class="btn btn-alt-success d-none" data-wizard="finish">
                                                    <i class="fa fa-check mr-5"></i> Guardar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Steps Navigation -->
                                </form>
                                <!-- END Form -->
                            </div>

                            <!-- Modal Rector -->
                            <div class="modal fade" id="modal-rector" tabindex="-1" role="dialog" aria-labelledby="modal-terms" aria-hidden="true">
                                    <div class="modal-dialog modal-lg modal-dialog-slidedown" role="document">
                                        <div class="modal-content">
                                            <div class="block block-themed block-transparent mb-0">
                                                <div class="block-header bg-primary-dark">
                                                    <h3 class="block-title">Registrar Rector</h3>
                                                        <div class="block-options">
                                                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                                                <i class="si si-close"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                <div class="block block-themed block-transparent mb-0">
                                                        <div class="block-content block-content-full text-center bg-gd-primary">
                                                        <img class="img-avatar img-avatar96 img-avatar-thumb" src="/media/avatars/avatar15.jpg" alt="">
                                                </div>
                                                <div class="row justify-content-center py-20">
                                                        <div class="col-xl-8">
                                                            <span id="form_resultR"></span>
                                                        <form id="form-rector" class="js-validation-material2" action="" method="POST">
                                                        {{ csrf_field() }}
                                                        <div class="form-group">
                                                            <div class="form-material input-group floating">
                                                                <input class="form-control" value="{{old('nombre')}}" type="text" id="nombre" name="nombre">
                                                                <label for="nombre">Nombres <span class="text-danger">*</span></label>
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">
                                                                        <i class="fa fa-vcard"></i>
                                                                    </span>
                                                                </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="form-material input-group floating">
                                                            <input class="form-control" type="text" value="{{old('apellido')}}" id="apellido" name="apellido">
                                                            <label for="apellido">Apellidos <span class="text-danger">*</span></label>
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="fa fa-address-card-o"></i>
                                                                </span>
                                                            </div>
                                                    </div>
                                                </div>

                                                    <div class="form-group">
                                                        <div class="form-material input-group floating">
                                                            <input class="form-control" type="text" id="telefono" name="telefono" value="{{old('telefono')}}">
                                                            <label for="telefono">Número de contacto<span class="text-danger">*</span></label>
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="fa fa-phone"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                <div class="form-group">
                                                    <div class="form-material input-group floating">
                                                        <input class="form-control" type="text" id="correo" name="correo" value="{{old('correo')}}">
                                                        <label for="correo">Correo electrónico <span class="text-danger">*</span></label>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-envelope-o"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                            </div>
                                            <div class="modal-footer">
                                                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cerrar</button>
                                                    <button type="submit" class="btn btn-alt-success">
                                                    <i class="fa fa-check"></i> Guardar
                                                </button>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                        </div>
                            <!-- End Rector -->

                            <!-- Modal Coordinador -->
                            <div class="modal fade" id="modal-cordi" tabindex="-1" role="dialog" aria-labelledby="modal-terms" aria-hidden="true">
                                <div class="modal-dialog modal-lg modal-dialog-slidedown" role="document">
                                    <div class="modal-content">
                                        <div class="block block-themed block-transparent mb-0">
                                            <div class="block-header bg-primary-dark">
                                                <h3 class="block-title">Registrar Coordinador</h3>
                                                    <div class="block-options">
                                                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                                            <i class="si si-close"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            <div class="block block-themed block-transparent mb-0">
                                                    <div class="block-content block-content-full text-center bg-gd-primary">
                                                    <img class="img-avatar img-avatar96 img-avatar-thumb" src="/media/avatars/avatar15.jpg" alt="">
                                            </div>
                                            <div class="row justify-content-center py-20">
                                                    <div class="col-xl-8">
                                                        <span id="form_resultC"></span>
                                                    <form id="form-cordi" class="js-validation-material" action="" method="POST">
                                                    {{ csrf_field() }}
                                                    <div class="form-group">
                                                        <div class="form-material input-group floating">
                                                            <input class="form-control" value="{{old('nombre')}}" type="text" id="nombre" name="nombre">
                                                            <label for="nombre">Nombres <span class="text-danger">*</span></label>
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="fa fa-vcard"></i>
                                                                </span>
                                                            </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-material input-group floating">
                                                        <input class="form-control" type="text" value="{{old('apellido')}}" id="apellido" name="apellido">
                                                        <label for="apellido">Apellidos <span class="text-danger">*</span></label>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-address-card-o"></i>
                                                            </span>
                                                        </div>
                                                </div>
                                            </div>

                                                <div class="form-group">
                                                    <div class="form-material input-group floating">
                                                        <input class="form-control" type="text" id="telefono" name="telefono" value="{{old('telefono')}}">
                                                        <label for="telefono">Número de contacto<span class="text-danger">*</span></label>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-phone"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>

                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="correo" name="correo" value="{{old('correo')}}">
                                                    <label for="correo">Correo electrónico <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-envelope-o"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                        <div class="modal-footer">
                                                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cerrar</button>
                                                <button type="submit" class="btn btn-alt-success">
                                                <i class="fa fa-check"></i> Guardar
                                            </button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                    </div>
                            <!-- End Modal -->
@endsection
@section('js_after')
        <!-- Page JS Plugins -->
        <script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="/js/plugins/jquery-validation/additional-methods.js"></script>

        <!-- Page JS Code -->
        <script src="/js/pages/be_forms_wizard.min.js"></script>
        <script src="/js/pages/be_forms_validation.min.js"></script>
        <script src="/js/pages/be_forms_validation.min.1.js"></script>

        <script>
        $('#form-cordi').on('submit',function(event){
            event.preventDefault();
            $.ajax({
                url:"{{ route('saveC')}}",
                method:"POST",
                data : new FormData(this),
                contentType:false,
                cache:false,
                processData:false,
                dataType:"json",
                success:function(data)
                {
                    // console.log(data);
                    var html = "";
                    if(data.errors){
                        html = '<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">×</button>';
                            for(var count = 0; count < data.errors.length; count++)
                            {
                                html += '<p>' + data.errors[count] +
                                    '</p>';
                            }
                            html += '</div>';
                    }
                    if(data.success)
                    {
                        // console.log(data)

                        html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">×</button>'
                            + data.success + '</div>';
                            $('#form-cordi')[0].reset();
                            $("#idcoordinacion").append('<option value=' + data.data["0"].id + '>' + data.data["0"].nombre + '</option>');
                    }
                    $('#form_resultC').html(html);
                }
            })
        });

        $('#form-rector').on('submit',function(event){
            event.preventDefault();
            $.ajax({
                url:"{{ route('saveR')}}",
                method:"POST",
                data : new FormData(this),
                contentType:false,
                cache:false,
                processData:false,
                dataType:"json",
                success:function(data)
                {
                    // console.log(data);
                    var html = "";
                    if(data.errors){
                        html = '<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">×</button>';
                            for(var count = 0; count < data.errors.length; count++)
                            {
                                html += '<p>' + data.errors[count] +
                                    '</p>';
                            }
                            html += '</div>';
                    }
                    if(data.success)
                    {
                        // console.log(data)

                        html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">×</button>'
                            + data.success + '</div>';
                            $('#form-rector')[0].reset();
                            $("#idRector").append('<option value=' + data.data["0"].id + '>' + data.data["0"].nombre + '</option>');
                    }
                    $('#form_resultR').html(html);
                }
            })
        });
        </script>
        <script src="/js/functions/select-by-department.js"></script>
@endsection
