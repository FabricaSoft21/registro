<form action="" method="get">
<div class="modal fade" id="modal-fromleft-{{$col->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-fromleft" aria-hidden="true">
<div class="modal-dialog modal-dialog-fromleft modal-lg" role="document">
<div class="modal-content">
<div class="block block-themed block-transparent mb-0">
<div class="block-header bg-primary-dark">
<h3 class="block-title">{{$col->nombreColegio}}</h3>
<div class="block-options">
<button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
<i class="si si-close"></i>
</button>
</div>
</div>
<div class="block block-themed block-transparent mb-0">
<div class="block block-themed">
<div class="block-content block-content-full text-center bg-gd-primary">
    <img class="img-avatar img-avatar96 img-avatar-thumb" src="/media/avatars/avatar15.jpg" alt="">
    <h3 class="text-white mt-3">INFORMACIÓN
<i class=" si si-info"></i>
</h3>
</div>
<div class="col-md-12">
        <!-- Simple Wizard 2 -->
        <div class="js-wizard-simple block">
            <!-- Step Tabs -->
            <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                <li class="nav-item">
                <a class="nav-link active" href="#wizard-simple2-step1-{{$col->id}}" data-toggle="tab">1. Personal</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#wizard-simple2-step2-{{$col->id}}" data-toggle="tab">2. Contacto</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#wizard-simple2-step3-{{$col->id}}" data-toggle="tab">3. Extra</a>
                </li>
            </ul>
            <!-- END Step Tabs -->

            <!-- Form -->
            <form action="" method="post">
                <!-- Steps Content -->
                <div class="block-content block-content-full tab-content" style="min-height: 267px;">
                    <!-- Step 1 -->
                    <div class="tab-pane active" id="wizard-simple2-step1-{{$col->id}}" role="tabpanel">
                        <div class="form-group">
                            <div class="form-material floating">
                            <input class="form-control" type="text" id="wizard-simple2-firstname" name="wizard-simple2-firstname" value="{{$col->nit}}" readonly>
                                <label for="wizard-simple2-firstname">Nit</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material floating">
                                <input class="form-control" type="text" id="wizard-simple2-lastname" name="wizard-simple2-lastname" value="{{$col->codigoDane}}" readonly>
                                <label for="wizard-simple2-lastname">Código DANE</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material floating">
                    <input class="form-control" type="email" id="wizard-simple2-email" name="wizard-simple2-email" value="{{$col->correo}}" readonly>
                                <label for="wizard-simple2-email">Número de Resolución</label>
                            </div>
                        </div>
                        
                        
                       
                    </div>
                    <!-- END Step 1 -->

                    <!-- Step 2 -->
                 

                    <div class="tab-pane" id="wizard-simple2-step2-{{$col->id}}" role="tabpanel">
                            <div class="form-group">
                                <div class="form-material floating">
                                    <input class="form-control" type="email" id="wizard-simple2-email" name="wizard-simple2-email" value="{{$col->departamento->nombre}}" readonly>
                                    <label for="wizard-simple2-email">Departamento</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-material floating">
                                    <input class="form-control" type="email" id="wizard-simple2-email" name="wizard-simple2-email" value="{{$col->municipio->nombre}}" readonly>
                                    <label for="wizard-simple2-email">Municipio</label>
                                </div>
                            </div>
                            <div class="form-group">
                            <div class="form-material floating">
                                <input class="form-control" type="email" id="wizard-simple2-email" name="wizard-simple2-email" value="{{$col->direccion}}" readonly>
                                <label for="wizard-simple2-email">Dirección</label>
                            </div>
                        </div>
                            <div class="form-group">
                            <div class="form-material floating">
                                <input class="form-control" type="email" id="wizard-simple2-email" name="wizard-simple2-email" value="{{$col->telefono}}" readonly>
                                <label for="wizard-simple2-email">Número de contacto</label>
                            </div>
                        </div>

                  
                            <div class="form-group">
                                <div class="form-material floating">
                                    <input class="form-control" type="email" id="wizard-simple2-email" name="wizard-simple2-email" value="{{$col->correo}}" readonly>
                                    <label for="wizard-simple2-email">Correo Electrónico</label>
                                </div>
                            </div>
                    </div>
                    <!-- END Step 2 -->

                    <!-- Step 3 -->
                    <div class="tab-pane" id="wizard-simple2-step3-{{$col->id}}" role="tabpanel">
                            <div class="form-group">
                            {{-- <div class="form-material floating">
                                <input class="form-control" type="email" id="wizard-simple2-email" name="wizard-simple2-email" value="{{$col->estado->nombre}}" readonly>
                                <label for="wizard-simple2-email">Estado del Colegio</label>
                                
                            </div> --}}
                        </div>
                        <div class="form-group">
                            <div class="form-material floating">
                                <input class="form-control" type="email" id="wizard-simple2-email" name="wizard-simple2-email" value="{{$col->categoria->nombre}}" readonly>
                                <label for="wizard-simple2-email">Tipo de Colegio</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material floating">
                                <input class="form-control" type="email" id="wizard-simple2-email" name="wizard-simple2-email" value="{{$col->rector->nombre}}" readonly>
                                <label for="wizard-simple2-email">Rector del Colegio</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-material floating">
                                <input class="form-control" type="text" id="wizard-simple2-email" name="wizard-simple2-email" value="{{$col->coordinador->nombre}}" readonly>
                                <label for="wizard-simple2-email">Coordinador del Colegio</label>
                            </div>
                        </div>
                      

                    <!-- END Step 3 -->
                </div>
                <!-- END Steps Content -->
            </form>
            <!-- END Form -->
        </div>
</div>
<div class="modal-footer">
<!-- <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button> -->
<button type="button" class="btn btn-alt-success" data-dismiss="modal">
<i class="fa fa-check"></i> Perfecto
</button>
</div>
</div>
</div>
</div>
</form>

