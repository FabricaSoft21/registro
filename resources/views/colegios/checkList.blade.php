
<div class="modal fade" id="modal-lista-{{$col->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-terms" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-slidedown" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-gd-earth">
                    <h3 class="block-title">Lista de Chequeo {{$col->id}}</h3>
                        <input type="hidden" id="Colegio" value="{{$col->id}}">
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                    <div class="block block-themed block-transparent mb-0">
                            {{-- <div class="block-content block-content-full text-center bg-gd-primary">
                            <img class="img-avatar img-avatar96 img-avatar-thumb" src="/media/avatars/avatar15.jpg" alt="">
                    </div> --}}
                    <div class="row justify-content-center py-20">
                            <div class="col-xl-8">
                                <span id="form_resultR"></span>
        <div class="block">
            {{-- @if() --}}
                <div class="block-header block-header-default">
                    <h3 class="block-title">Institución Educativa - {{$col->nombreColegio}}</h3>
                    <div class="block-options">
                        {{-- <div class="block-options-item">
                            <code>.table</code>
                        </div> --}}
                    </div>
                </div>
                <div class="block-content">
                <div id="message-{{$col->id}}"></div>
                        <form id="form-checkList" class="js-validation-material" action="" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                    <div class="form-material input-group floating">
                                    <input class="form-control" type="text" id="pregunta-{{$col->id}}" name="pregunta" value="">
                                        <label for="correo">Nuevo item (Criterio de evaluación) <span class="text-danger">*</span></label>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="fa fa-plus-circle"></i>
                                            </span>
                                        </div>
                                    </div>
                                <div id="form-{{$col->id}}"><button type="button" class="btn btn-alt-success save-btn pull-right" onclick="saveContent('{{$col->id}}')">Guardar</button> </div>
                                </div>
                        </form>
                        <div style="margin-bottom: 50px; !important"></div>
                <form action="{{route('list', $col->id)}}" method="POST">
                <input type="hidden" name="idColegio" id="idColegio" value="{{$col->id}}">
                    @csrf
                    <table class="table table-vcenter data-table">
                        <thead>
                            <tr>
                                {{-- <th class="text-center" style="width: 50px;">#</th> --}}
                                <th>Criterios de evaluación </th>
                                <th class="d-none d-sm-table-cell" style="width: 15%;">¿Cumple?</th>
                                <th>Opciones</th>
                                {{-- <th class="text-center" style="width: 100px;">Actions</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($lista as $list)
                            <tr>
                                    {{-- <td class="font-w600">{{$loop->iteration}}</td> --}}
                            <td class="font-w600 column_name" data-column_name="pregunta" data-id ="{{$list->id}}" data-name="{{$list->pregunta}}"><div id="edit-{{$list->id}}-{{$col->id}}">{{$list->pregunta}}</div></td>
                                    <td>
                                    <label class="css-control css-control-sm css-control-primary css-switch">
                                    <input type="hidden" name="pregunta[]" value="{{$list->id}}">
                                    <input id="check-{{$list->id."-".$col->id}}" name="check[]" type="checkbox" class="css-control-input" value="{{$list->id}}">
                                    <span class="css-control-indicator" ></span>
                                        <span id="text-{{$list->id."-".$col->id}}"></span>
                                    </label>
                                    </td>
                                <td class="font-w600"><div id="btn-{{$list->id}}-{{$col->id}}"><button type="button" class="btn btn-alt-primary btn-edit" onclick="editContent('{{$list->id}}','{{$list->pregunta}}','{{$col->id}}')">Editar</button></div></td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>

                </div>
            </div>

        </div>

    </div>
    <div class="modal-footer">
            <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-alt-success">
            <i class="fa fa-check"></i> Guardar cambios
            </button>
        </div>
</div>
</div>

</div>
</div>
</form>
</div>

<script>

function getList(id) {
        $.ajax({
            url:"/lista/show/"+id,
            type:'get',
            success:function(data){
                // console.log(data);
                if(data != null){
                    for(i of data){

                    if(i.state != 0){
                        $('#check-'+i.idPregunta+"-"+id).prop('checked',true);
                        $('#text-'+i.idPregunta+"-"+id).text("Si")

                    }else{
                        $('#text-'+i.idPregunta+"-"+id).text("No")
                    }

                    }
                }
        },
        error:function(){
        console.log("error");
        }
    });
}

</script>

<script>

    function editContent(id,name,idColegio) {

        $('#edit-'+id+"-"+idColegio).html(`<input name="edit_name" id="edit-name-${id}-${idColegio}" value="${name}" style="width: 400px;">`);
        $('#btn-'+id+"-"+idColegio).html(`<button type="button" class="btn btn-alt-info btn-update" onclick="update('${id}','${idColegio}')">Actualizar</button><br/><button type="button" class="btn btn btn-alt-danger btn-cancel" onclick="cancelEdit('${id}','${name}','${idColegio}')">Cancelar</button>`);

    }

    function update(id,idColegio){

        var _token = $('input[name="_token"]').val();
        var name = $('#edit-name-'+id+"-"+idColegio).val();

        if ($('#edit-name-'+id+"-"+idColegio).val() != "") {
            $.ajax({
            url:"{{ route('updateCE') }}",
            method:"POST",
            data:{ column_value:$('#edit-name-'+id+"-"+idColegio).val(), id:id, _token:_token},
            success:function(data)
                {
                    $('#message-'+idColegio).html(data);
                    $('#edit-'+id+"-"+idColegio).html("").text(name);
                    $('#btn-'+id+"-"+idColegio).html(`<button type="button" class="btn btn-alt-primary btn-edit" onclick="editContent('${id}','${name}','${idColegio}')">Editar</button>`);
                }
            });
        }else{
            $('#message-'+idColegio).html("<div class='alert alert-danger'>Ingresa algún caracter </div>");
        }
    }

    function cancelEdit(id,name,idColegio){
        $('#edit-'+id+"-"+idColegio).html("").text(name);
        $('#btn-'+id+"-"+idColegio).html(`<button type="button" class="btn btn-alt-primary btn-edit" onclick="editContent('${id}','${name}','${idColegio}')">Editar</button>`);
    }


function saveContent(id) {

    var _token = $('input[name="_token"]').val();

    if ($('#pregunta-'+id).val() != "") {
        $.ajax({
            url:"{{ route('saveCE')}}",
            method:"POST",
            data : {column_value:$('#pregunta-'+id).val(), _token:_token,column_name:'pregunta'},
            dataType:"json",
            success:function(data)
            {

                var html = "";
                if(data.errors){
                    html = '<div class="alert alert-danger">';
                        for(var count = 0; count < data.errors.length; count++)
                        {
                            html += '<p>' + data.errors[count] +
                                '</p>';
                        }
                        html += '</div>';
                }
                if(data.success)
                {
                    html = '<div class="alert alert-success">'
                        + data.success + '</div>';
                        $('#form-checkList')[0].reset();
                        $(".data-table tbody").append(`<tr>
                        <td class="font-w600 column_name"><div id="edit-${data.data["0"].id}-${id}">${data.data["0"].pregunta}</div></td>
                        <td>
                            <label class="css-control css-control-sm css-control-primary css-switch">
                            <input type="hidden" name="pregunta[]" value="${data.data["0"].id}">
                            <input id="check-${data.data["0"].id+"-"+id}" name="check[]" type="checkbox" class="css-control-input" value="${data.data["0"].id}">
                            <span class="css-control-indicator" ></span>
                            <span id="text-${data.data["0"].id+"-"+id}"></span>
                            </label>
                        </td>
                        <td class="font-w600">
                            <div id="btn-${data.data["0"].id}-${id}"><button type="button" class="btn btn-alt-primary btn-edit" onclick="editContent('${data.data["0"].id}','${data.data["0"].pregunta}','${id}')">Editar</button></div>`);
                }
                $('#message-'+id).html(html);
            }
        });

    }else{
        $('#message-'+id).html("<div class='alert alert-danger'>El campo pregunta es requerido </div>");
    }

    }

</script>