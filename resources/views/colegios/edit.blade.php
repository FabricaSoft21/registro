@extends('layouts.backend')
@section('content')
<div class="content">
                    <h2 class="content-heading">Instución Educativa</h2>
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Modificar institución</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option">
                                    <i class="fa fa-gears"></i>
                                </button>
                            </div>
                        </div>
                        <div class="js-wizard-validation-material block">
                                <!-- Step Tabs -->
                                <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#wizard-validation-material-step1" data-toggle="tab">1. Personal</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#wizard-validation-material-step2" data-toggle="tab">2. Contácto</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#wizard-validation-material-step3" data-toggle="tab">3. Detalles</a>
                                    </li>
                                </ul>
                                <!-- END Step Tabs -->

                                <!-- Form -->
                                <form class="js-wizard-validation-material-form" action="" method="post">
                                {{ csrf_field() }}
                                    <!-- Steps Content -->
                                    <div class="block-content block-content-full tab-content" style="min-height: 267px;">
                                        <!-- Step 1 -->
                                        <div class="tab-pane active" id="wizard-validation-material-step1" role="tabpanel">
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="nit" name="nit" value="{{$colegio->nit}}">
                                                    <label for="nit">Nit <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-vcard"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="codigoDane" name="codigoDane" value="{{$colegio->codigoDane}}">
                                                    <label for="codigoDane">Codigo Dane <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-address-card-o"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="nombreColegio" name="nombreColegio" value="{{$colegio->nombreColegio}}">
                                                    <label for="nombreColegio">Nombre del colegio <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-address-card-o"></i>
                                                        </span>
                                                    </div>
                                                 </div>
                                        </div>
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="numResolucion" name="numResolucion" value="{{$colegio->numResolucion}}">
                                                    <label for="numResolucion">Número de Resolución <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-phone"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- END Step 1 -->

                                        <!-- Step 2 -->
                                    <div class="tab-pane" id="wizard-validation-material-step2" role="tabpanel">
                                        <div class="form-group">
                                        <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="correo" name="correo" value="{{$colegio->correo}}">
                                                    <label for="correo">Correo electrónico <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-envelope-o"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                                <div class="form-material">
                                                            <select class="js-select2 form-control" id="idDepartamento" name="idDepartamento" style="width: 100%;" data-allow-clear="true">
                                                                <option value="" selected>Seleccione Departamento</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                                @foreach($departamento as  $dep)
                                                                <option value="{{ $dep->id }}" {{$dep->id == $colegio->idDepartamento ? 'selected' : '' }} >{{$dep->nombre}}</option>
                                                                @endforeach
                                                            </select>
                                                            <label for="idDepartamento">Departamento <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                <div class="form-material">
                                                            <select class="js-select2 form-control" id="idMunicipio" name="idMunicipio" style="width: 100%;" data-allow-clear="true">
                                                                <option value="" selected>Seleccione Municipio</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                                @foreach($municipio as  $muni)
                                                                <option value="{{ $muni->id }}"{{$muni->id == $colegio->idMunicipio ? 'selected' : '' }} >{{$muni->nombre}}</option>
                                                                @endforeach
                                                            </select>
                                                            <label for="idMunicipio">Municipio <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                        <div class="form-group">
                                        <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="direccion" name="direccion" value="{{$colegio->direccion}}">
                                                    <label for="direccion">Dirección <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="si si-pointer"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                        <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="telefono" name="telefono" value="{{$colegio->telefono}}">
                                                    <label for="telefono">Teléfono <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-phone"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                        </div>
                                        </div>

                                        <!-- END Step 2 -->

                                        <!-- Step 3 -->
                                        <div class="tab-pane" id="wizard-validation-material-step3" role="tabpanel">

                                        {{-- <div class="form-group">
                                                    <div class="form-material">
                                                        <select class="js-select2 form-control" id="idEstado" name="idEstado" style="width: 100%;" data-allow-clear="true">
                                                            <option value="" selected>Seleccione Estado</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                            @foreach($estadoColegio as $estado)
                                                            <option value="{{ $estado->id }}" {{$estado->id == $colegio->idEstado ? 'selected' : ''}}>{{$estado->nombre}}</option>
                                                            @endforeach
                                                        </select>
                                                        <label for="idColegio">Estado de la Institución <span class="text-danger">*</span></label>
                                                    </div>
                                                </div> --}}
                                                <div class="form-group">
                                                    <div class="form-material">
                                                        <select class="js-select2 form-control" id="idTipo" name="idTipo" style="width: 100%;" data-allow-clear="true">
                                                            <option value="" selected>Seleccione Tipo</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                            @foreach($tipo as $tipoCol)
                                                            <option value="{{ $tipoCol->id }}"{{$tipoCol->id == $colegio->idTipo ? 'selected' : ''}} >{{$tipoCol->nombre}}</option>
                                                            @endforeach
                                                        </select>
                                                        <label for="idTipo">Tipo de Institución <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-material">
                                                        <select class="js-select2 form-control" id="idCategoria" name="idCategoria" style="width: 100%;" data-allow-clear="true">
                                                            <option value="" selected>Seleccione Categoría</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                            @foreach($categoriaCole as $categoria)
                                                            <option value="{{ $categoria->id }}" {{$categoria->id == $colegio->idCategoria ? 'selected' : ''}}>{{$categoria->nombre}}</option>
                                                            @endforeach
                                                        </select>
                                                        <label for="idColegio">Categoría de la Institución <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-material">
                                                        <select class="js-select2 form-control" id="idRector" name="idRector" style="width: 100%;" data-allow-clear="true">
                                                            <option value="" selected>Seleccione Rector</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                            @foreach($rector as $rec)
                                                            <option value="{{ $rec->id }}" {{$rec->id == $colegio->idRector ? 'selected' : ''}}>{{$rec->nombre}}</option>
                                                            @endforeach
                                                        </select>
                                                        <label for="idColegio">Rector de la Institución <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="form-material">
                                                        <select class="js-select2 form-control" id="idcoordinacion" name="idcoordinacion" style="width: 100%;" data-allow-clear="true">
                                                            <option value="" selected>Seleccione Coordinador</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                            @foreach($coordinador as $coor)
                                                            <option value="{{ $coor->id }}" {{$coor->id == $colegio->idcoordinacion ? 'selected' : ''}}>{{$coor->nombre}}</option>
                                                            @endforeach
                                                        </select>
                                                        <label for="idColegio">Coordinador de la Institución <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                    <!-- END Steps Content -->
                                    </div>
                                    <!-- Steps Navigation -->
                                    <div class="block-content block-content-sm block-content-full bg-body-light">
                                        <div class="row">
                                            <div class="col-6">
                                                <button type="button" class="btn btn-alt-secondary" data-wizard="prev">
                                                    <i class="fa fa-angle-left mr-5"></i> Atrás
                                                </button>
                                            </div>
                                            <div class="col-6 text-right">
                                                <button type="button" class="btn btn-alt-secondary" data-wizard="next">
                                                    Siguiente <i class="fa fa-angle-right ml-5"></i>
                                                </button>
                                                <button type="submit" class="btn btn-alt-success d-none" data-wizard="finish">
                                                    <i class="fa fa-check mr-5"></i> Guardar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Steps Navigation -->
                                </form>
                                <!-- END Form -->
                            </div>
@endsection
@section('js_after')
        <!-- Page JS Plugins -->
        <script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="/js/plugins/jquery-validation/additional-methods.js"></script>

        <!-- Page JS Code -->
        <script src="/js/pages/be_forms_wizard.min.js"></script>
        <script src="/js/functions/select-by-department.js"></script>

@endsection
