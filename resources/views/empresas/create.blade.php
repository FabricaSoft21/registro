@extends('layouts.backend')

@section('content')
                <div class="content">
                    <h2 class="content-heading"></h2>
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Registar Empresa</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option">
                                    <i class="si si-user-follow"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="row justify-content-center py-20">
                                <div class="col-xl-8">
                                    <form class="js-wizard-validation-material-form" action="" method="POST">
                                            {{ csrf_field() }}
                                            {{-- <input class="form-control" value="{{$coordi['id'] + 1}}" type="hidden" id="id" name="id"> --}}
                                            {{-- <input class="form-control" value="{{date('Y-m-d h:i:s')}}" type="hidden" id="created_at" name="created_at"> --}}
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" value="{{old('nit')}}" type="text" id="nit" name="nit">
                                                    <label for="nit">NIT <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-vcard"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" value="{{old('nombre')}}" id="nombre" name="nombre">
                                                <label for="nombre">Nombre <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-address-card-o"></i>
                                                    </span>
                                                </div>
                                        </div>
                                    </div>

                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="telefono" name="telefono" value="{{old('telefono')}}">
                                                <label for="telefono">Número de contacto<span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-phone"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                   
                                    <div class="form-group">
                                        <div class="form-material input-group floating">
                                            <input class="form-control" type="text" id="correo" name="correo" value="{{old('correo')}}">
                                            <label for="correo">Correo electrónico <span class="text-danger">*</span></label>
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="fa fa-envelope-o"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" value="{{old('nombreEncargado')}}" id="nombreEncargado" name="nombreEncargado">
                                                <label for="nombreEncargado">nombre del Encargado <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-address-card-o"></i>
                                                    </span>
                                                </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="telefonoEncargado" name="telefonoEncargado" value="{{old('telefonoEncargado')}}">
                                                <label for="telefonoEncargado">Número de contacto del encargado<span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-phone"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <input class="form-control" value="{{$last==null? 1 : $last->id+1}}" type="hidden" id="idEmpresa" name="idEmpresa">
                                        <div class="form-group">
                                                <div class="form-material">
                                                    <select class="js-select2 form-control" id="idPoblacion" name="idPoblacion[]"  style="width: 100%;" data-allow-clear="true" data-placeholder="Seleccione Poblacion(es)" multiple>
                                                        {{-- <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin --> --}}
                                                        @foreach($tipoPobla as $tipo)
                                                        <option value="{{ $tipo->id }}" {{ (collect(old('idPoblacion'))->contains($tipo->id)) ? 'selected':'' }}>{{$tipo->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="idPoblacion">Poblacion(es)</label>
                                            </div>
                                                    <label class="css-control css-control-sm css-control-primary css-checkbox">
                                                    <input type="checkbox" class="css-control-input" id="checkbox">
                                                    <span class="css-control-indicator"></span> ¿Seleccionar todos?
                                                    </label>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-lg-8 ml-auto">
                                                <button type="submit" class="btn btn-alt-success pull-right">Registrar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
@endsection

@section('js_after')
        <!-- Page JS Plugins -->
        <script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="/js/plugins/jquery-validation/additional-methods.js"></script>

        <!-- Page JS Code -->
        <script src="/js/pages/be_forms_wizard.min.js"></script>
        <script>
                $("#checkbox").click(function(){
                    if($("#checkbox").is(':checked') ){
                        $("#idPoblacion > option").prop("selected","selected");
                        $("#idPoblacion").trigger("change");
                    }else{
                        $("#idPoblacion > option").prop("selected",false);
                        $("#idPoblacion").trigger("change");
                    }
                });
                </script>

@endsection
