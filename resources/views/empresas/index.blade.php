@extends('layouts.backend')
@section('content')
                <!-- Page Content -->
                <div class="content">
                    <h2 class="content-heading">Empresas</h2>

                    
                    @if($cant != 0)
                    <form action="{{route('detailEmp')}}" method="post">
                    @endif

                    @csrf

                    <!-- Dynamic Table Full -->
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Ver Empresas</h3>
                            <a href="/empresa/create" data-toggle="tooltip" title="Registrar empresa" class="btn btn-success">
                                <i class="fa fa-plus"></i>
                               
                            </a>
                            <a  data-toggle="modal" type="file" data-target="#modal-fromleft" title="Importar datos" class="btn btn-secondary ml-2" style={{$cant == 0 ? "display:none" : "" }}>
                                <i class="fa fa-folder-open-o"></i>
                            </a>
                            <button type="button" class="btn btn-alt-info ml-2" data-toggle="modal" data-target="#modal-fadein" id="btnAsociar" disabled="disabled">Asociar ficha</button>
                        </div>
                        <div class="modal fade" id="modal-fadein" tabindex="-1" role="dialog" aria-labelledby="modal-fadein" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="block block-themed block-transparent mb-0">
                                            <div class="block-header bg-primary-dark">
                                                <h3 class="block-title">Asociar ficha</h3>
                                                <div class="block-options">
                                                    <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                                        <i class="si si-close"></i>
                                                    </button>
                                                </div>
                                            </div>

                                            <div class="block-content">
                                                    <div class="form-group">
                                                            <div class="form-material">
                                                                <select class="js-select2 form-control" id="idFormacionf" name="idFormacion" style="width: 100%;" data-allow-clear="true" >
                                                                    <option value="" selected>Seleccione ficha</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                                    @foreach($formacion as $for)
                                                                    <option value="{{$for->id}}" {{ (old("idFormacion") == $for->id ? "selected":"") }}>{{$for->codigo . ' - ' .$for->nombreFicha }}</option>
                                                                    @endforeach
                                                                </select>
                                                                <label for="idTipoDoc">Ficha <span class="text-danger">*</span></label>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                                <button type="button" class="js-swal-info2 btn btn-alt-secondary pull-left">
                                                        <i class="fa fa-info text-info mr-5"></i> Nota
                                                </button>
                                            <button type="submit" class="btn btn-alt-success">
                                                <i class="fa fa-check"></i> Guardar
                                            </button>
                                        </div>
                                    </div>
                                </div>

                        </div>
                        <div class="block-content block-content-full">
                            <!-- DataTables functionality is initialized with .js-dataTable-full class in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                            @if($cant == 0)
                            <div class="col-md-12">
                                <div class="block">
                                    <div class="block-content block-content-full">
                                        <div class="py-20 text-center">
                                            <div class="mb-20">
                                                <i class="fa fa-close fa-5x text-danger"></i>
                                            </div>
                                            <div class="font-size-h4 font-w600">Lo sentimos</div>
                                            <div class="text-muted">Actualmente no hay ningún registro</div>
                                            <form class="js-wizard-validation-material-form" method="post" action="" enctype="multipart/form-data">
                                             @csrf
                                            <div class="pt-20 mb-3">
                                            <div class="form-group box">
                                                <input type="file" name="file-selected" id="file-selected" class="file-selected inputfile inputfile-3" />
                                                <label for="file-selected"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Elige un archivo&hellip;</span></label>
                                            </div>
                                            </div>
                                            <button type="submit" class="btn btn-rounded btn-alt-success" style="width:200px !important;">
                                                    <i class="fa fa-file-excel-o mr-5"></i> Importar
                                            </button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            @else
                            <table id="empresa" class="table  js-table-checkable js-dataTable-full" width="100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th></th>
                                        <th>NIT</th>
                                        <th>Nombre</th>
                                        <th>Correo</th>
                                        <th>Teléfono</th>
                                        <th> Nombre del Encargado</th>
                                        <th>Teléfono del Encargado</th>

                                        <th class="noExportExc noExport">Opciones     </th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($empresa as $emp)
                                    <tr>
                                        <td class="font-w600">{{$loop->iteration}}</td>
                                        <td> <label class="css-control css-control-sm css-control-warning css-checkbox">
                                            <input type="radio" class="css-control-input" id="idAsociar" name="id[]" value="{{$emp->id}}">
                                                <span class="css-control-indicator"></span>
                                            </label>
                                         </td>
                                        <td class="font-w600">{{$emp->nit}}</td>
                                        <td class="font-w600">{{mb_strtoupper($emp->nombre)}}</td>
                                        <td class="font-w600">{{mb_strtoupper($emp->correo)}}</td>
                                        <td class="font-w600">{{$emp->telefono}}</td>
                                        <td class="font-w600">{{mb_strtoupper($emp->nombreEncargado)}}</td>
                                        <td class="font-w600">{{$emp->telefonoEncargado}}</td>
                                        <td class="text-center">
                                                @if ($emp->trashed())
                                                <a href="/empresa/restore/{{$emp->id}}" class="btn btn-sm btn-success" data-toggle="tooltip" title="Restaurar empresa">
                                                    <i class="fa fa-undo"></i>
                                                </a>
                                                @else
                                                <a href="/empresa/edit/{{$emp->id}}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Editar Empresa">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <span></span>
                                            <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal-fromleft-{{$emp->id}}"title="Ver Empresa">
                                                    <i class="fa fa-eye"></i>
                                                </button>
                                                <a href="/empresa/delete/{{$emp->id}}" class="btn btn-sm btn-danger" data-toggle="tooltip" title="Eliminar Empresa">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                         @endif
                                         </td>
                                        </tr>
                                        @include('empresas/show')
                                        @endforeach
                                    </tbody>
                                </table>
                            </form>
                            @endif
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->
                </div>

    <script src="/js/functions/custom-file-input.js"></script>
    <div class="modal fade" id="modal-fromleft" tabindex="-1" role="dialog" aria-labelledby="modal-fromleft" aria-hidden="true">
    <div class="modal-dialog modal-dialog-fromleft modal-lg" role="document">
        <div class="modal-content">
         <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                <h3 class="block-title">IMPORTANCIÓN DE DATOS</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                     </button>
                    </div>
                </div>
             <div class="block block-themed block-transparent mb-0">
                    <div class="block block-themed">
                            <div class="block-content block-content-full text-center bg-primary-light">
                                <img class="img-avatar img-avatar96 img-avatar-thumb" src="media/various/logo.png" alt="">
                             </h3>
                            </div>
                            <div class="col-md-12">
                                    <!-- Simple Wizard 2 -->
                                    <div class="js-wizard-simple block">
                                        <!-- Step Tabs -->
                                        <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#wizard-simple2-step1" data-toggle="tab">IMPORTAR</a>
                                            </li>
                                        </ul>
                                        <!-- END Step Tabs -->

                                        <!-- Form -->
                                        <form action="" method="post" class="js-wizard-validation-material-form" enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-md-12">
                                                    <div class="block">
                                                        <div class="block-content block-content-full">
                                                            <div class="py-20 text-center">
                                                                <div class="mb-20">
                                                                    <i class="fa fa-info fa-5x text-info"></i>
                                                                </div>
                                                                <div class="font-size-h4 font-w600">¡Espera!</div>
                                                                <div class="text-muted">Recuerda subir un archivo con la extensión y el formato correcto.
                                                                </div>
                                                                <div class="pt-20 mb-3">
                                                                <div class="form-group box">
                                                                    <input type="file" name="file-selected" id="file-selected" class="file-selected inputfile inputfile-3" />
                                                                    <label for="file-selected"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Elige un archivo&hellip;</span></label>
                                                                </div>
                                                                <div class="block block-mode-hidden">
                                                                        <ul class="nav nav-tabs nav-tabs-block align-items-center" data-toggle="tabs" role="tablist">
                                                                            <li class="nav-item">
                                                                                <a class="nav-link" href="#">Vista previa</a>
                                                                            </li>
                                                                            <li class="nav-item ml-auto">
                                                                                <div class="block-options mr-15">
                                                                                    <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                                                                                        <i class="si si-refresh"></i>
                                                                                    </button>
                                                                                    <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"></button>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                        <div class="block-content tab-content">
                                                                            <div class="tab-pane active" id="btabswo-static-home" role="tabpanel">
                                                                                <div class="resp-container">
                                                                                    <iframe width="402" height="346" frameborder="0" scrolling="no" src="https://onedrive.live.com/embed?resid=1EF43E3FE1A12FAC%21110&authkey=%21AEtOXYGo-XCygSE&em=2&wdAllowInteractivity=False&wdHideGridlines=True&wdHideHeaders=True&wdDownloadButton=True&wdInConfigurator=True"></iframe>                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                         <div class="modal-footer">
                 <!-- {{-- <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button> --}} -->
                <button type="submit" class="btn btn-alt-success">
                <i class="fa fa-file-excel-o mr-5"></i>  Guardar
                </button>
            </form>
          </div>
        </div>
    </div>
</div>


@endsection

@section('js_after')

<script src="/js/functions/custom-file-input.js"></script>
        <!-- Page JS Plugins -->
        <script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="/js/plugins/jquery-validation/additional-methods.js"></script>

        <!-- Page JS Code -->
        <script src="/js/pages/be_forms_wizard.min.js"></script>
        <script>
                function getPoblaciones(id){
                    $.ajax({
                        url:"/empresa/show/"+id,
                        type:'get',
                        success:function(data){
                            // console.log(data);
                            if(data.length != 0){
                             var template = ``;
                             for(i of data){
                                 template+=`<option selected>${i.nombre}</option>`;
                             }
                             $("#idPoblacion-"+id).html(template);
                            }else{
                                var template = ``;
                                template+=`<option selected>No tiene Población</option>`;
                                $("#idPoblacion-"+id).html(template);
                            }
                        },
                        error:function(){
                            alert("error");
                        }
                    });
                }
        </script>

<script>
    jQuery(function () {
        Codebase.helpers('table-tools');
    });
</script>

<script>
    $('.css-control-input').click(function() {
        if ($(this).is(':checked')) {
            $('#btnAsociar').removeAttr('disabled');

        } else {
            $('#btnAsociar').attr('disabled', 'disabled');
        }
    });
</script>



@endsection
