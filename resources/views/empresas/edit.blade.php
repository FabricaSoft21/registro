@extends('layouts.backend')

@section('content')
                <div class="content">
                    <h2 class="content-heading"></h2>
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Modificar empresaresa</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option">
                                    <i class="si si-user-follow"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="row justify-content-center py-20">
                                <div class="col-xl-8">
                                    <form class="js-wizard-validation-material-form" action="" method="POST">
                                            {{ csrf_field() }}
                                            {{-- <input class="form-control" value="{{$coordi['id'] + 1}}" type="hidden" id="id" name="id"> --}}
                                            {{-- <input class="form-control" value="{{date('Y-m-d h:i:s')}}" type="hidden" id="created_at" name="created_at"> --}}
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" value="{{$empresa->nit}}" type="text" id="nit" name="nit">
                                                    <label for="nit">NIT <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-vcard"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" value="{{$empresa->nombre}}" id="nombre" name="nombre">
                                                <label for="nombre">Nombre <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-address-card-o"></i>
                                                    </span>
                                                </div>
                                        </div>
                                    </div>

                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="telefono" name="telefono" value="{{$empresa->telefono}}">
                                                <label for="telefono">Número de contacto<span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-phone"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                   
                                    <div class="form-group">
                                        <div class="form-material input-group floating">
                                            <input class="form-control" type="text" id="correo" name="correo" value="{{$empresa->correo}}">
                                            <label for="correo">Correo electrónico <span class="text-danger">*</span></label>
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="fa fa-envelope-o"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" value="{{$empresa->nombreEncargado}}" id="nombreEncargado" name="nombreEncargado">
                                                <label for="nombreEncargado">nombre del Encargado <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-address-card-o"></i>
                                                    </span>
                                                </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="telefonoEncargado" name="telefonoEncargado" value="{{$empresa->telefonoEncargado}}">
                                                <label for="telefonoEncargado">Número de contacto del encargado<span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-phone"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                                <div class="form-material">
                                                    <select {{count($data) == 1 ? 'disabled' : ''}} class="js-select2 form-control" id="idPoblacion" name="idPoblacion[]"  style="width: 100%;" data-allow-clear="true" multiple>
                                                        @if(count($data) == 1)
                                                        <option selected>No tiene Población</option>
                                                        @else
                                                        @foreach($tipoPobla->whereIn('nombre', $data) as $tip)
                                                        <option value="{{$tip["id"]}}" selected>{{$tip["nombre"]}}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                    <label for="idPoblacion">Poblacion(es)</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                                <div class="form-material">
                                                    <select {{count($data) -1 == $countPobla ? 'disabled' : ''}} class="js-select2 form-control" id="Poblaciones" name="Poblaciones[]"  style="width: 100%;" data-allow-clear="true" data-placeholder="¿Más poblaciones?" multiple>
                                                        @if(count($data) -1 == $countPobla)
                                                        <option selected>Resultados no encontrados</option>
                                                        @else
                                                        <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                        @foreach($tipoPobla->whereNotIn('nombre', $data) as $pobla)
                                                        <option value="{{ $pobla->id }}">{{$pobla->nombre}}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                    <label for="Poblaciones">Agregar Población (Opcional)</label>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-lg-8 ml-auto">
                                                <button type="submit" class="btn btn-alt-success pull-right">Actulizar empresa</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
@endsection

@section('js_after')
        <!-- Page JS Plugins -->
        <script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="/js/plugins/jquery-validation/additional-methods.js"></script>

        <!-- Page JS Code -->
        <script src="/js/pages/be_forms_wizard.min.js"></script>

@endsection
