<div class="modal fade" id="modal-fromleft-{{$emp->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-fromleft" aria-hidden="true">
    <div class="modal-dialog modal-dialog-fromleft modal-lg" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                <h3 class="block-title">{{$emp->nombre}}</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block block-themed block-transparent mb-0">
                        <div class="block block-themed">
                                <div class="block-content block-content-full text-center bg-gd-primary">
                                    <img class="img-avatar img-avatar96 img-avatar-thumb" src="/media/avatars/avatar15.jpg" alt="">
    
                                 <h3 class="text-white mt-3">INFORMACIÓN
    
                                     <i class=" si si-info"></i>
                                 </h3>
                                 </div>
                                  
                                <div class="col-md-12">
                                        <!-- Simple Wizard 2 -->
                                        <div class="js-wizard-simple block">
                                            <!-- Step Tabs -->
                                            <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#wizard-validation-material-step1" data-toggle="tab">1. Principal</a>
                                        </li>
                                
                                    </ul>
    
                                    <form action="{{{url ("/empresa/show/$emp->id")}}}" method="get">
                                        <!-- Steps Content -->
                                        <div class="block-content block-content-full tab-content" style="min-height: 267px;">
                                            <!-- Step 1 -->
                                            <div class="tab-pane active" id="wizard-validation-material-step1" role="tabpanel">
                                                    <div class="form-group">
                                                            <div class="form-material input-group floating">
                                                                <input class="form-control" type="text"  value="{{$emp->nit}}" id="wizard-simple2-firstname" name="wizard-simple2-firstname" readonly>
                                                                <label for="nombre">NIT</label>
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">
                                                                        <i class="fa fa-vcard"></i>
                                                                    </span>
                                                                </div>
                                                        </div>
                                                    </div>
                                                <div class="form-group">
                                                    <div class="form-material input-group floating">
                                                        <input class="form-control" type="text"  value="{{$emp->nombre}}" id="wizard-simple2-firstname" name="wizard-simple2-firstname" readonly>
                                                        <label for="nombre">Nombre</label>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-vcard"></i>
                                                            </span>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text"  value="{{$emp->correo}}" readonly>
                                                    <label for="apellido">Correo Electrónico</label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-address-card-o"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                            
                
                                            
                                        
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" value="{{$emp->telefono}}" readonly>
                                                    <label for="telefono">Número de contacto</label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-phone"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                         
                                            <div class="form-group">
                                            <div class="form-material input-group floating">
                                                        <input class="form-control" type="text" value="{{$emp->nombreEncargado}}" readonly> 
                                                        <label for="idArea">Nombre de persona Encargada</label>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                    <i class="fa fa-vcard"></i>
                                                            </span>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                    <div class="form-material input-group floating">
                                                        <input class="form-control" type="text" value="{{$emp->telefonoEncargado}}" readonly>
                                                        <label for="telefono">Teléfono de persona Encargada</label>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-phone"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            {{-- <div class="form-group ">
                                            <div class="form-material input-group floating">
                                                        <input class=" form-control" value="{{$emp->poblacion->nombre}}" readonly>
                                                              <label for="idContrato">Tipo de población</label>
                                                              <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="si si-support"></i>
                                                            </span>
                                                        </div>
                                                </div>
                                            </div> --}}
                                            <div class="form-group">
                                                    <div class="form-material">
                                                    <select disabled class="js-select2 form-control" id="idPoblacion-{{$emp->id}}" name="idPoblacion[]"  style="width: 100%;" data-allow-clear="true" data-placeholder="Seleccione poblacion(es)" multiple>
                                                            @foreach($tipoPobla as $pobla)
                                                            <option selected value="{{$pobla["id"]}}">{{$pobla["nombre"]}}</option>;
                                                            @endforeach
                                                        </select>
                                                        <label for="idPoblacion">Poblacion(es)</label>
                                                </div>
                                            </div>
                                            
                                            
                                            <!-- END Steps Content -->
                                      
                                </div>
                               
                                            <!-- END Step Tabs -->
            
                                            <!-- Form -->
                                          
                                            <!-- END Form -->
                                        </div>
            </div>
            <div class="modal-footer">
                <!-- {{-- <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button> --}} -->
                <button type="button" class="btn btn-alt-success" data-dismiss="modal">
                    <i class="fa fa-check"></i> Listo
                </button>
            </div>
        </div>
    </div>
    </div>
    
    