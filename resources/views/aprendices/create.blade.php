@extends('layouts.backend')
@section('content')
<div class="content">
                    <h2 class="content-heading">Aprendices</h2>
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Crear aprendiz</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option">
                                    <i class="si si-user-follow"></i>
                                </button>
                            </div>
                        </div>
                        <div class="js-wizard-validation-material block">
                                <!-- Step Tabs -->
                                <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#wizard-validation-material-step1" data-toggle="tab">1. Personal</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#wizard-validation-material-step2" data-toggle="tab">2. Detalles</a>
                                    </li>
                                    <li class="nav-item" style="{{auth()->user()->selected_category_id != 5 ? 'display:none' : ''}}">
                                        <a class="nav-link" href="#wizard-validation-material-step3" data-toggle="tab">3. Extra</a>
                                    </li>
                                </ul>
                                <!-- END Step Tabs -->

                                <!-- Form -->
                                <form class="js-wizard-validation-material-form" action="" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="tipoAprendiz" value="{{auth()->user()->selected_category_id}}">
                                    <!-- Steps Content -->
                                    <div class="block-content block-content-full tab-content" style="min-height: 267px;">
                                        <!-- Step 1 -->
                                        <div class="tab-pane active" id="wizard-validation-material-step1" role="tabpanel">
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="nombre" name="nombre" value="{{old('nombre')}}">
                                                    <label for="nombre">Nombres <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-vcard"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="apellido" name="apellido" value="{{old('apellido')}}">
                                                    <label for="apellido">Apellidos <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-address-card-o"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                            <div class="form-group">
                                                    <div class="form-material input-group floating">
                                                        <input type="text" class="datepicker form-control" id="fechaNacimiento" name="fechaNacimiento" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="yyyy-mm-dd" value="{{old('fechaNacimiento')}}">
                                                        <label for="fechaNacimiento">Fecha de nacimiento <span class="text-danger">*</span></label>
                                                        <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-calendar"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="correo" name="correo" value="{{old('correo')}}">
                                                    <label for="correo">Correo electrónico <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-envelope-o"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="direccion" name="direccion" value="{{old('direccion')}}">
                                                    <label for="direccion">Dirección <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="si si-pointer"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END Step 1 -->

                                        <!-- Step 2 -->
                                        <div class="tab-pane" id="wizard-validation-material-step2" role="tabpanel">
                                            <div class="form-group">
                                                    <div class="form-material input-group floating">
                                                        <input class="form-control" type="text" id="telefono" name="telefono" value="{{old('telefono')}}">
                                                        <label for="telefono">Número de contacto <span class="text-danger">*</span></label>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-phone"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                        <div class="form-group">
                                                <div class="form-material">
                                                    <select class="js-select2 form-control" id="idTipoDoc" name="idTipoDoc" style="width: 100%;" data-allow-clear="true">
                                                        <option value="" selected>Seleccione tipo de documento</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                        @foreach($docs as $doc)

                                                        <option value="{{ $doc->id}}" {{ (old("idTipoDoc") == $doc->id ? "selected":"") }}>{{$doc->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="idTipoDoc">Tipo de documento <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="numDoc" name="numDoc" value="{{old('numDoc')}}">
                                                <label for="numDoc">Número de identificación <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-id-card-o"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                                <input class="form-control" value="{{$last==null? 1 : $last->id+1}}" type="hidden" id="idAprendiz" name="idAprendiz">
                                        <div class="form-group">
                                                <div class="form-material">
                                                    <select class="js-select2 form-control" id="idDiscapacidad" name="idDiscapacidad[]"  style="width: 100%;" data-allow-clear="true" data-placeholder="Seleccione discapacidad(es)" multiple>
                                                        {{-- <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin --> --}}
                                                        @foreach($disc as $dis)
                                                        <option value="{{ $dis->id }}" {{ (collect(old('idDiscapacidad'))->contains($dis->id)) ? 'selected':'' }}>{{$dis->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="idDiscapacidad">Discapacidad(es)</label>
                                            </div>
                                                    <label class="css-control css-control-sm css-control-primary css-checkbox">
                                                    <input type="checkbox" class="css-control-input" id="checkbox">
                                                    <span class="css-control-indicator"></span> ¿Seleccionar todos?
                                                    </label>
                                                        <a class="link-effect text-muted mb-5 d-inline-block pull-right mt-10" href="#" data-toggle="modal" data-target="#modal-disc">
                                                            <i class="fa fa-plus-square text-muted mr-5"></i>¿Nueva Discapacidad?
                                                        </a>
                                        </div>

                                        </div>
                                        <!-- END Step 2 -->

                                        <!-- Step 3 -->
                                        <div class="tab-pane" id="wizard-validation-material-step3" role="tabpanel">

                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="nombreAcudiente" name="nombreAcudiente" value="{{old('nombreAcudiente')}}">
                                                <label for="nombreAcudiente">Nombre acudiente <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-vcard"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="telAcudiente" name="telAcudiente" value="{{old('telAcudiente')}}">
                                                <label for="telAcudiente">Teléfono acudiente <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-phone"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                            <div class="form-group">
                                                <div class="form-material">
                                                    <select class="js-select2 form-control" id="idColegio" name="idColegio" style="width: 100%;" data-allow-clear="true">
                                                        <option value="" selected>Seleccione colegio</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                        @foreach($colegio as $c)
                                                        <option value="{{ $c->id }}" {{ (old("idColegio") == $c->id ? "selected":"") }}>
                                                        {{$c->nombreColegio}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="idColegio">Colegio <span class="text-danger">*</span></label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-material">
                                                    <select class="js-select2 form-control" id="idGrado" name="idGrado" style="width: 100%;" data-allow-clear="true">
                                                        <option value="" selected>Seleccione grado</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                        @foreach($grado as $g)
                                                        <option value="{{ $g->id }}" {{ (old("idGrado") == $g->id ? "selected":"") }}>{{$g->grado}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="idGrado">Grado <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                        <!-- END Step 3 -->
                                    </div>
                                    <!-- END Steps Content -->

                                    <!-- Steps Navigation -->
                                    <div class="block-content block-content-sm block-content-full bg-body-light">
                                        <div class="row">
                                            <div class="col-6">
                                                <button type="button" class="btn btn-alt-secondary" data-wizard="prev">
                                                    <i class="fa fa-angle-left mr-5"></i> Atrás
                                                </button>
                                            </div>
                                            <div class="col-6 text-right">
                                                <button type="button" class="btn btn-alt-secondary" data-wizard="next">
                                                    Siguiente <i class="fa fa-angle-right ml-5"></i>
                                                </button>
                                                <button type="submit" class="btn btn-alt-success d-none" data-wizard="finish">
                                                    <i class="fa fa-check mr-5"></i> Guardar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Steps Navigation -->
                                </form>
                                <!-- END Form -->
                            </div>

                                        <!-- Modal discapacidad -->
                                        <div class="modal fade" id="modal-disc" tabindex="-1" role="dialog" aria-labelledby="modal-terms" aria-hidden="true">
                                            <div class="modal-dialog modal-lg modal-dialog-slidedown" role="document">
                                                <div class="modal-content">
                                                    <div class="block block-themed block-transparent mb-0">
                                                        <div class="block-header bg-primary-dark">
                                                            <h3 class="block-title">Registrar Nueva Discapacidad</h3>
                                                                <div class="block-options">
                                                                    <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                                                        <i class="si si-close"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        <div class="block block-themed block-transparent mb-0">
                                                                <div class="block-content block-content-full text-center bg-gd-primary">
                                                                <img class="img-avatar img-avatar96 img-avatar-thumb" src="/media/avatars/avatar15.jpg" alt="">
                                                        </div>
                                                        <div class="row justify-content-center py-20">
                                                                <div class="col-xl-8">
                                                                    <span id="form_resultDis"></span>
                                                                <form id="form-disc" class="js-validation-material2" action="" method="POST">
                                                                {{ csrf_field() }}
                                                                <div class="form-group">
                                                                    <div class="form-material input-group floating">
                                                                        <input class="form-control" value="{{old('nombre')}}" type="text" id="nombre" name="nombre">
                                                                        <label for="nombre">Nombre<span class="text-danger">*</span></label>
                                                                        <div class="input-group-append">
                                                                            <span class="input-group-text">
                                                                                <i class="fa fa-vcard"></i>
                                                                            </span>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="form-material input-group floating">
                                                                    <input class="form-control" type="text" value="{{old('descripcion')}}" id="descripcion" name="descripcion">
                                                                    <label for="descripcion">Descripción <span class="text-danger">*</span></label>
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text">
                                                                            <i class="fa fa-address-card-o"></i>
                                                                        </span>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                             
                                                    </div>
                                                    </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                            <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cerrar</button>
                                                            <button type="submit" class="btn btn-alt-success">
                                                            <i class="fa fa-check"></i> Guardar
                                                        </button>
                                                    </div>
                                                </form>
                                                </div>
                                            </div>
                                        </div>
                                </div>
@endsection
@section('js_after')
        <!-- Page JS Plugins -->
        <script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="/js/plugins/jquery-validation/additional-methods.js"></script>

        <!-- Page JS Code -->
        <script src="/js/pages/be_forms_wizard.min.js"></script>
        <script>
        $("#checkbox").click(function(){
            if($("#checkbox").is(':checked') ){
                $("#idDiscapacidad > option").prop("selected","selected");
                $("#idDiscapacidad").trigger("change");
            }else{
                $("#idDiscapacidad > option").prop("selected",false);
                $("#idDiscapacidad").trigger("change");
            }
        });
        </script>
        <script type="text/javascript">

            $('.datepicker').datepicker({
                endDate: new Date(),
                language: 'es',
                todayHighlight: !0,
                autoclose: !0,
                weekStart: 0,
                orientation: 'bottom' // Position issue when using BS4, set it to bottom until officially supported
            });

        </script>

        <script>
                $('#form-disc').on('submit',function(event){
            event.preventDefault();
            $.ajax({
                url:"{{ route('saveDis')}}",
                method:"POST",
                data : new FormData(this),
                contentType:false,
                cache:false,
                processData:false,
                dataType:"json",
                success:function(data)
                {
                    // console.log(data);
                    var html = "";
                    if(data.errors){
                        html = '<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">×</button>';
                            for(var count = 0; count < data.errors.length; count++)
                            {
                                html += '<p>' + data.errors[count] +
                                    '</p>';
                            }
                            html += '</div>';
                    }
                    if(data.success)
                    {
                        // console.log(data)

                        html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">×</button>'
                            + data.success + '</div>';
                            $('#form-disc')[0].reset();
                            $("#idDiscapacidad").append('<option value=' + data.data["0"].id + '>' + data.data["0"].nombre + '</option>');
                    }
                    $('#form_resultDis').html(html);
                }
            })
        });

        </script>

@endsection
