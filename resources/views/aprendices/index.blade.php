@extends('layouts.backend')
@section('content')
                <!-- Page Content -->
                <div class="content">
                    <h2 class="content-heading">Aprendices</h2>

                    <!-- Dynamic Table Full -->
                    @if($cant != 0)
                    <form action="{{route('detailA')}}" method="post" class="js-wizard-validation-material-form" >
                    @endif
                    @csrf
                            <div class="block">
                            <div class="block-header block-header-default">
                            <h3 class="block-title">Ver aprendices</h3>
                            <a href="/aprendiz/create" data-toggle="tooltip" title="Crear Aprendiz" class="btn btn-success">
                                <i class="si si-user-follow"></i>
                            </a>
                            <a  data-toggle="modal" type="file" data-target="#modal-fromleft" title="Importar datos" class="btn btn-secondary ml-2" style={{$cant == 0 ? "display:none" : "" }}>
                                <i class="fa fa-folder-open-o"></i>
                        </a>
                        
                            <button type="button" class="btn btn-alt-info ml-2" data-toggle="modal" data-target="#modal-fadein" id="btnAsociar" disabled="disabled">Asociar ficha</button>
                            
                            <button style="{{auth()->user()->selected_category_id != 1 ? 'display:none' : ''}}" type="button" class="btn btn-alt-info ml-2" data-toggle="modal" data-target="#modal-fadein2" id="btnAsociar2" disabled="disabled">Asociar curso</button>

                            </div>
                            <div class="modal fade" id="modal-fadein" tabindex="-1" role="dialog" aria-labelledby="modal-fadein" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="block block-themed block-transparent mb-0">
                                            <div class="block-header bg-primary-dark">
                                                <h3 class="block-title">Asociar ficha</h3>
                                                <div class="block-options">
                                                    <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                                        <i class="si si-close"></i>
                                                    </button>
                                                </div>
                                            </div>

                                            <div class="block-content">
                                                    <div class="form-group">
                                                            <div class="form-material">
                                                                <select class="js-select2 form-control" id="idFormacion" name="idFormacion" style="width: 100%;" data-allow-clear="true">
                                                                    <option value="" selected>Seleccione ficha</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                                    @if (auth()->user()->selected_category_id == 1)
                                                                    @php($cla = $FTitulada)
                                                                    @endif
                                                                    @if (auth()->user()->selected_category_id == 2)
                                                                        @php($cla = $FComplementaria)
                                                                    @endif
                                                                    @if (auth()->user()->selected_category_id == 3)
                                                                        @php($cla = $FPoblacion)
                                                                    @endif
                                                                    @if (auth()->user()->selected_category_id == 4)
                                                                        @php($cla = $FCompetencia)
                                                                    @endif
                                                                    @if (auth()->user()->selected_category_id == 5)
                                                                        @php($cla = $FMedia)
                                                                    @endif
                                                                    @foreach($cla as $for)

                                                                    <option value="{{$for->id}}" {{ (old("idFormacion") == $for->id ? "selected":"") }}>{{$for->nombreFicha. ' - ' .$for->codigo}}</option>
                                                                    @endforeach
                                                                </select>

                                                                <label for="idTipoDoc">Ficha <span class="text-danger">*</span></label>
                                                        </div>
                                                    </div>


                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                                <button type="button" class="js-swal-info btn btn-alt-secondary pull-left">
                                                        <i class="fa fa-info text-info mr-5"></i> Nota
                                                </button>
                                            <button type="submit" class="btn btn-alt-success">
                                                <i class="fa fa-check"></i> Guardar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade" id="modal-fadein2" tabindex="-1" role="dialog" aria-labelledby="modal-fadein" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="block block-themed block-transparent mb-0">
                                                <div class="block-header bg-primary-dark">
                                                    <h3 class="block-title">Asociar Curso</h3>
                                                    <div class="block-options">
                                                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                                            <i class="si si-close"></i>
                                                        </button>
                                                    </div>
                                                </div>
    
                                                <div class="block-content">
                                               
    
                                                        <div class="form-group">
                                                            <div class="form-material">
                                                                <select class="js-select2 form-control" id="idCurso" name="idCurso" style="width: 100%;" data-allow-clear="true">
                                                                    <option value="" selected>Asociar curso</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                                    @foreach($cursos as $cur)
                                                                    <option value="{{$cur->id}}" {{ (old("idCurso") == $cur->id ? "selected":"") }}>{{$cur->nombreFicha. ' - ' .$cur->codigo}}</option>
                                                                    @endforeach
                                                                </select>
    
                                                                <label for="idTipoDoc">Curso<span class="text-danger"></span></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                    <button type="button" class="js-swal-info btn btn-alt-secondary pull-left">
                                                            <i class="fa fa-info text-info mr-5"></i> Nota
                                                    </button>
                                                <button type="submit" class="btn btn-alt-success">
                                                    <i class="fa fa-check"></i> Guardar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <div class="block-content block-content-full">
                            <!-- DataTables functionality is initialized with .js-dataTable-full class in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                            @if($cant == 0)
                            <div class="col-md-12">
                                <div class="block">
                                    <div class="block-content block-content-full">
                                        <div class="py-20 text-center">
                                            <div class="mb-20">
                                                <i class="fa fa-close fa-5x text-danger"></i>
                                            </div>
                                            <div class="font-size-h4 font-w600">Lo sentimos</div>
                                            <div class="text-muted">Actualmente no hay ningún registro</div>
                                            <form class="js-wizard-validation-material-form" method="post" action="{{route('import')}}" enctype="multipart/form-data">
                                             @csrf
                                            <div class="pt-20 mb-3">
                                            <div class="form-group box">
                                                <input type="file" name="file-selected" id="file-selected" class="file-selected inputfile inputfile-3" />
                                                <label for="file-selected"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Elige un archivo&hellip;</span></label>
                                            </div>
                                            </div>
                                            <button type="submit" class="btn btn-rounded btn-alt-success" style="width:200px !important;">
                                                    <i class="fa fa-file-excel-o mr-5"></i> Importar
                                                </button>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @else

                            <table id="aprendices" class="table js-table-checkable js-dataTable-full" width="100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>
                                            <label class="css-control css-control-sm css-control-warning css-checkbox py-0">
                                                <input type="checkbox" class="css-control-input" id="check-all" name="check-all">
                                                <span class="css-control-indicator"></span>
                                            </label>
                                        </th>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Nacimiento</th>
                                        <th>Tipo Id.</th>
                                        <th>Identificación</th>
                                        <th>Estado</th>
                                        <th>Dirección</th>
                                        <th>Correo</th>
                                        <th>Telefono</th>
                                        <th>Dirección</th>
                                        @if(auth()->user()->selected_category_id == 5)
                                        <th>Colegio</th>
                                        <th>Grado</th>
                                        <th>Acudiente</th>
                                        <th>Tel. Acudiente</th>
                                        @endif
                                        <th class="noExportExc noExport">Opciones</th>

                                        {{-- <th class="d-none d-sm-table-cell" style="width: 15%;">Access</th>
                                        <th class="text-center" style="width: 15%;">Profile</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (auth()->user()->selected_category_id == 1)
                                        @php($type = $titulada)
                                    @endif
                                    @if (auth()->user()->selected_category_id == 2)
                                        @php($type = $complementaria)
                                    @endif
                                    @if (auth()->user()->selected_category_id == 3)
                                        @php($type = $poblacion)
                                    @endif
                                    @if (auth()->user()->selected_category_id == 4)
                                        @php($type = $competencia)
                                    @endif
                                    @if (auth()->user()->selected_category_id == 5)
                                        @php($type = $media)
                                    @endif
                                @foreach($type as $aprendiz)
                                    <tr>
                                        <td class="font-w600">{{$loop->iteration}}</td>
                                        @if($aprendiz->idestado == 3 || $aprendiz->idestado == 4 || $aprendiz->idestado == 5)
                                        <td></td>
                                        @else
                                        <td>
                                            <label class="css-control css-control-sm css-control-warning css-checkbox">
                                            <input type="checkbox" class="css-control-input" id="idAsociar" name="id[]" value="{{$aprendiz->id}}">
                                                <span class="css-control-indicator"></span>
                                            </label>
                                        </td>
                                        @endif
                                        <td class="font-w600">{{$aprendiz->nombre}}</td>
                                        <td class="font-w600">{{$aprendiz->apellido}}</td>
                                        <td class="font-w600">{{$aprendiz->fechaNacimiento}}</td>
                                        <td class="font-w600">{{$aprendiz->type->nombre}}</td>
                                        <td class="font-w600">{{$aprendiz->numDoc}}</td>
                                        @if($aprendiz->idestado == 1)
                                            @php($state = "badge badge-warning")
                                        @endif
                                        @if($aprendiz->idestado == 2)
                                            @php($state = "badge badge-info")
                                        @endif
                                        @if($aprendiz->idestado == 3 || $aprendiz->idestado == 4)
                                            @php($state = "badge badge-danger")
                                        @endif
                                        @if($aprendiz->idestado == 5)
                                            @php($state = "badge badge-success")
                                        @endif

                                        <td class="font-w600"><span class="{{$state}}">{{$aprendiz->state->nombre}}</span></td>
                                        <td class="font-w600">{{$aprendiz->direccion}}</td>
                                        <td class="font-w600">{{$aprendiz->correo}}</td>
                                        <td class="font-w600">{{$aprendiz->telefono}}</td>
                                        <td class="font-w600">{{$aprendiz->direccion}}</td>
                                        @if(auth()->user()->selected_category_id == 5)
                                        <td class="font-w600">{{$aprendiz->idColegio == null ? 'No aplica' : $aprendiz->school->nombreColegio}}</td>
                                        <td class="font-w600">{{$aprendiz->idGrado == null ?  'No aplica' :  $aprendiz->grade->grado}}</td>
                                        <td class="font-w600">{{$aprendiz->nombreAcudiente == null ? 'No aplica' : $aprendiz->nombreAcudiente}}</td>
                                        <td class="font-w600">{{$aprendiz->telAcudiente == null ? 'No aplica' : $aprendiz->telAcudiente}}</td>
                                        @endif
                                        <td class="text-center">
                                            <a href="/aprendiz/edit/{{$aprendiz->id}}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Editar Aprendiz">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <span></span>
                                        <a  class="btn btn-sm btn-info" href="" data-toggle="modal" data-target="#modal-fromleft-{{$aprendiz->id}}"title="Ver Aprendiz" onclick="{{"getDiscapacidades(".$aprendiz->id}})">
                                                <i class="fa fa-eye"></i>
                                        </a>
                                        </td>
                                    </tr>
                                    @include('aprendices.show')
                                    @endforeach
                                </tbody>
                            </table>
                            </form>
                            @endif
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->
                </div>

                <script src="/js/functions/custom-file-input.js"></script>

<div class="modal fade" id="modal-fromleft" tabindex="-1" role="dialog" aria-labelledby="modal-fromleft" aria-hidden="true">
    <div class="modal-dialog modal-dialog-fromleft modal-lg" role="document">
        <div class="modal-content">
         <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                <h3 class="block-title">IMPORTANCIÓN DE DATOS</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                     </button>
                    </div>
                </div>
             <div class="block block-themed block-transparent mb-0">
                    <div class="block block-themed">
                            <div class="block-content block-content-full text-center bg-primary-light">
                                <img class="img-avatar img-avatar96 img-avatar-thumb" src="media/various/logo.png" alt="">
                             </h3>
                            </div>
                            <div class="col-md-12">
                                    <!-- Simple Wizard 2 -->
                                    <div class="js-wizard-simple block">
                                        <!-- Step Tabs -->
                                        <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#wizard-simple2-step1" data-toggle="tab">IMPORTAR</a>
                                            </li>
                                        </ul>
                                        <!-- END Step Tabs -->

                                        <!-- Form -->
                                    <form action="" method="post" class="js-wizard-validation-material-form" enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-md-12">
                                                    <div class="block">
                                                        <div class="block-content block-content-full">
                                                            <div class="py-20 text-center">
                                                                <div class="mb-20">
                                                                    <i class="fa fa-info fa-5x text-info"></i>
                                                                </div>
                                                                <div class="font-size-h4 font-w600">¡Espera!</div>
                                                                <div class="text-muted">Recuerda subir un archivo con la extensión y el formato correcto.
                                                                </div>
                                                                <div class="pt-20 mb-3">
                                                                <div class="form-group box">
                                                                    <input type="file" name="file-selected" id="file-selected" class="file-selected inputfile inputfile-3" />
                                                                    <label for="file-selected"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Elige un archivo&hellip;</span></label>
                                                                </div>
                                                                <div class="block block-mode-hidden">
                                                                        <ul class="nav nav-tabs nav-tabs-block align-items-center" data-toggle="tabs" role="tablist">
                                                                            <li class="nav-item">
                                                                                <a class="nav-link" href="#">Vista previa</a>
                                                                            </li>
                                                                            <li class="nav-item ml-auto">
                                                                                <div class="block-options mr-15">
                                                                                    <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                                                                                        <i class="si si-refresh"></i>
                                                                                    </button>
                                                                                    <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"></button>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                        <div class="block-content tab-content">
                                                                            <div class="tab-pane active" id="btabswo-static-home" role="tabpanel">
                                                                                <div class="resp-container">
                                                                                    <iframe width="402" height="346" frameborder="0" scrolling="no" src="https://onedrive.live.com/embed?resid=1EF43E3FE1A12FAC%21105&authkey=%21AAYdoRwKKACBy4w&em=2&wdAllowInteractivity=False&wdHideGridlines=True&wdHideHeaders=True&wdDownloadButton=True&wdInConfigurator=True"></iframe>                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                         <div class="modal-footer">
                <button type="button" class="js-swal-info3 btn btn-alt-secondary pull-left">
                        <i class="fa fa-info text-info mr-5"></i> Nota
                </button>
                <button type="submit" class="btn btn-alt-success">
                <i class="fa fa-file-excel-o mr-5"></i>  Guardar
                </button>
            </form>
          </div>
        </div>
    </div>
</div>

@endsection

@section('js_after')

<!-- Page JS Plugins -->
<script src="/js/functions/custom-file-input.js"></script>
<script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
<script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="/js/plugins/jquery-validation/additional-methods.js"></script>

<!-- Page JS Code -->
<script src="/js/pages/be_forms_wizard.min.js"></script>

<script>
        function getDiscapacidades(id){
            $.ajax({
                url:"/aprendiz/show/"+id,
                type:'get',
                success:function(data){
                    if (data.fic != null) {
                        $("#Ficha-"+id).val(data.fic.codigo + " - " + data.fic.nombreFicha);
                    } else {
                        $("#Ficha-"+id).val("Sin asignar");
                    }
                    if(data.disc.length != 0){
                     var template = ``;

                     for(i of data.disc){
                         template+=`<option selected>${i.nombre}</option>`;

                     }
                     $("#idDiscapacidad-"+id).html(template);

                    }else{
                        $("#idDiscapacidad-"+id).html('<option selected>No tiene discapacidades</option>');
                    }
                },
                error:function(){
                    console.log("error");
                }
            });
        }
</script>

<script>
    jQuery(function () {
        Codebase.helpers('table-tools');
    });
</script>

<script>
    $('.css-control-input').click(function() {
        if ($(this).is(':checked')) {
            $('#btnAsociar').removeAttr('disabled');

        } else {
            $('#btnAsociar').attr('disabled', 'disabled');
        }
    });
</script>

<script>
        $('.css-control-input').click(function() {
            if ($(this).is(':checked')) {
                $('#btnAsociar2').removeAttr('disabled');
    
            } else {
                $('#btnAsociar2').attr('disabled', 'disabled');
            }
        });
    </script>

@endsection
