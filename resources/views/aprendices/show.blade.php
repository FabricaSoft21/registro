    <form action="{{{url ("/aprendiz/show/$aprendiz->id")}}}" method="get">
    <div class="modal fade" id="modal-fromleft-{{$aprendiz->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-fromleft" aria-hidden="true">
        <div class="modal-dialog modal-dialog-fromleft modal-lg" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                    <h3 class="block-title">{{$aprendiz->nombre}} {{$aprendiz->apellido}}</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block block-themed block-transparent mb-0">
                            <div class="block block-themed">
                                    <div class="block-content block-content-full text-center bg-gd-primary">
                                        <img class="img-avatar img-avatar96 img-avatar-thumb" src="/media/avatars/avatar15.jpg" alt="">
                                        <h3 class="text-white mt-3">INFORMACIÓN
                                 <i class=" si si-info"></i>
                             </h3>
                                    </div>
                                    <div class="col-md-12">
                                            <!-- Simple Wizard 2 -->
                                            <div class="js-wizard-simple block">
                                                <!-- Step Tabs -->
                                                <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                                    <li class="nav-item">
                                                    <a class="nav-link active" href="#wizard-simple2-step1-{{$aprendiz->id}}" data-toggle="tab">1. Personal</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#wizard-simple2-step2-{{$aprendiz->id}}" data-toggle="tab">2. Detalles</a>
                                                    </li>
                                                <li class="nav-item" style="{{auth()->user()->selected_category_id != 5 ? 'display:none' : ''}}">
                                                        <a class="nav-link" href="#wizard-simple2-step3-{{$aprendiz->id}}" data-toggle="tab">3. Extra</a>
                                                    </li>
                                                </ul>
                                                <!-- END Step Tabs -->

                                                <!-- Form -->
                                                <form action="" method="post">
                                                    <!-- Steps Content -->
                                                    <div class="block-content block-content-full tab-content" style="min-height: 267px;">
                                                        <!-- Step 1 -->
                                                        <input class="form-control" type="hidden" id="wizard-simple2-firstname" name="idUser" value="{{$aprendiz->id}}" readonly>

                                                        <div class="tab-pane active" id="wizard-simple2-step1-{{$aprendiz->id}}" role="tabpanel">
                                                            <div class="form-group">
                                                                <div class="form-material floating">
                                                                <input class="form-control" type="text" id="wizard-simple2-firstname" name="wizard-simple2-firstname" value="{{$aprendiz->nombre}}" readonly>
                                                                    <label for="wizard-simple2-firstname">Nombre</label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="form-material floating">
                                                                    <input class="form-control" type="text" id="wizard-simple2-lastname" name="wizard-simple2-lastname" value="{{$aprendiz->apellido}}" readonly>
                                                                    <label for="wizard-simple2-lastname">Apellido</label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="form-material floating">
                                                                    <input class="form-control" type="email" id="wizard-simple2-email" name="wizard-simple2-email" value="{{$aprendiz->correo}}" readonly>
                                                                    <label for="wizard-simple2-email">Correo electrónico</label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="form-material floating">
                                                                    <input class="js-datepicker form-control" type="text" id="wizard-simple2-email" name="wizard-simple2-email" value="{{$aprendiz->fechaNacimiento}}" disabled>
                                                                    <label for="wizard-simple2-email">Fecha de nacimiento</label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="form-material floating">
                                                                    <input class="form-control" type="email" id="wizard-simple2-email" name="wizard-simple2-email" value="{{$aprendiz->telefono}}" readonly>
                                                                    <label for="wizard-simple2-email">Número de contacto</label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="form-material floating">
                                                                    <input class="form-control" type="email" id="wizard-simple2-email" name="wizard-simple2-email" value="{{$aprendiz->direccion}}" readonly>
                                                                    <label for="wizard-simple2-email">Dirección</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- END Step 1 -->

                                                        <!-- Step 2 -->
                                                        <div class="tab-pane" id="wizard-simple2-step2-{{$aprendiz->id}}" role="tabpanel">
                                                                <div class="form-group">
                                                                    <div class="form-material floating">
                                                                        <input class="form-control" type="email" id="wizard-simple2-email" name="wizard-simple2-email" value="{{$aprendiz->type->nombre}}" readonly>
                                                                        <label for="wizard-simple2-email">Tipo de documento</label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="form-material floating">
                                                                        <input class="form-control" type="email" id="wizard-simple2-email" name="wizard-simple2-email" value="{{$aprendiz->numDoc}}" readonly>
                                                                        <label for="wizard-simple2-email">Número de identificación</label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="form-material">
                                                                    <select disabled class="js-select2 form-control" id="idDiscapacidad-{{$aprendiz->id}}" name="idDiscapacidad[]"  style="width: 100%;" data-allow-clear="true" data-placeholder="Seleccione discapacidad(es)" multiple>
                                                                            @foreach($disc as $dis)
                                                                            <option selected value="{{$dis["id"]}}">{{$dis["nombre"]}}</option>;
                                                                            @endforeach
                                                                        </select>
                                                                        <label for="idDiscapacidad">Discapacidad(es)</label>
                                                                </div>
                                                            </div>
                                                                <div class="form-group">
                                                                    <div class="form-material">
                                                                        <input class="form-control" type="text" id="Ficha-{{$aprendiz->id}}" name="Ficha" value="" readonly>
                                                                        <label for="Ficha">Ficha</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- END Step 2 -->

                                                        <!-- Step 3 -->
                                                        @if(auth()->user()->selected_category_id == 5)
                                                        <div class="tab-pane" id="wizard-simple2-step3-{{$aprendiz->id}}" role="tabpanel">
                                                              <div class="form-group">
                                                                <div class="form-material floating">
                                                                    <input class="form-control" type="email" id="wizard-simple2-email" name="wizard-simple2-email" value="{{$aprendiz->nombreAcudiente}}" readonly>
                                                                    <label for="wizard-simple2-email">Nombre acudiente</label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="form-material floating">
                                                                    <input class="form-control" type="email" id="wizard-simple2-email" name="wizard-simple2-email" value="{{$aprendiz->telAcudiente}}" readonly>
                                                                    <label for="wizard-simple2-email">Teléfono acudiente</label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="form-material">
                                                                <select disabled class="js-select2 form-control" id="idTipoDoc" name="idTipoDoc" style="width: 100%;" data-allow-clear="true">
                                                                <option value="{{$aprendiz->school->id}}" selected>{{$aprendiz->school->nombreColegio}}</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                                </select>
                                                                <label for="idTipoDoc">Colegio</label>
                                                            </div>
                                                        </div>
                                                            <div class="form-group">
                                                                <div class="form-material">
                                                                <select disabled class="js-select2 form-control" id="idTipoDoc" name="idTipoDoc" style="width: 100%;" data-allow-clear="true">
                                                                <option value="{{$aprendiz->grade->id}}" selected>{{$aprendiz->grade->grado}}</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                                </select>
                                                                <label for="idTipoDoc">Grado</label>
                                                            </div>
                                                        </div>
                                                        <!-- END Step 3 -->
                                                    </div>
                                                    @endif
                                                    <!-- END Steps Content -->
                                                </form>
                                                <!-- END Form -->
                                            </div>
                </div>
                <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button> --}}
                    <button type="button" class="btn btn-alt-success" data-dismiss="modal">
                        <i class="fa fa-check"></i> Perfecto
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>

