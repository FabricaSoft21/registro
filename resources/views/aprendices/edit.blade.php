@extends('layouts.backend')
@section('content')
<div class="content">
                    <h2 class="content-heading">Aprendices</h2>
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Crear aprendiz</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option">
                                    <i class="si si-user-follow"></i>
                                </button>
                            </div>
                        </div>
                        <div class="js-wizard-validation-material block">
                                <!-- Step Tabs -->
                                <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#wizard-validation-material-step1" data-toggle="tab">1. Personal</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#wizard-validation-material-step2" data-toggle="tab">2. Detalles</a>
                                    </li>
                                    <li class="nav-item" style="{{auth()->user()->selected_category_id != 5 ? 'display:none' : ''}}">
                                        <a class="nav-link" href="#wizard-validation-material-step3" data-toggle="tab">3. Extra</a>
                                    </li>
                                </ul>
                                <!-- END Step Tabs -->

                                <!-- Form -->
                                <form class="js-wizard-validation-material-form" action="" method="post">
                                <input class="form-control" value="{{$aprendiz->id}}" type="hidden" id="idAprendiz" name="idAprendiz">

                                {{ csrf_field() }}
                                    <!-- Steps Content -->
                                    <div class="block-content block-content-full tab-content" style="min-height: 267px;">
                                        <!-- Step 1 -->
                                        <div class="tab-pane active" id="wizard-validation-material-step1" role="tabpanel">
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                <input class="form-control" value="{{$aprendiz->nombre}}" type="text" id="nombre" name="nombre">
                                                    <label for="nombre">Nombres <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-vcard"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" value="{{$aprendiz->apellido}}" id="apellido" name="apellido">
                                                    <label for="apellido">Apellidos <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-address-card-o"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                            <div class="form-group">
                                                    <div class="form-material input-group floating">
                                                        <input type="text" class="datepicker form-control" value="{{$aprendiz->fechaNacimiento}}" id="fechaNacimiento" name="fechaNacimiento" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="yyyy-mm-dd">
                                                        <label for="fechaNacimiento">Fecha de nacimiento <span class="text-danger">*</span></label>
                                                        <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-calendar"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="correo" name="correo" value="{{$aprendiz->correo}}">
                                                    <label for="correo">Correo electrónico <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-envelope-o"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="telefono" name="telefono" value="{{$aprendiz->telefono}}">
                                                    <label for="telefono">Número de contacto <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-phone"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="direccion" name="direccion" value="{{$aprendiz->direccion}}">
                                                    <label for="direccion">Dirección <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="si si-pointer"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END Step 1 -->

                                        <!-- Step 2 -->
                                        <div class="tab-pane" id="wizard-validation-material-step2" role="tabpanel">
                                        <div class="form-group">
                                                <div class="form-material">
                                                    <select class="js-select2 form-control" id="idTipoDoc" name="idTipoDoc" style="width: 100%;" data-allow-clear="true">
                                                        <option value="" selected>Seleccione tipo de documento</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                        @foreach($docs as $doc)
                                                        <option value="{{ $doc->id }}" {{$doc->id == $aprendiz->idTipoDoc ? 'selected' : ''}}>{{$doc->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="idTipoDoc">Tipo de documento <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="numDoc" name="numDoc" value="{{$aprendiz->numDoc}}">
                                                <label for="numDoc">Número de identificación <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-id-card-o"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                                <div class="form-group">
                                                    <div class="form-material">
                                                        <select {{count($data) == 1 ? 'disabled' : ''}} class="js-select2 form-control" id="idDiscapacidad" name="idDiscapacidad[]"  style="width: 100%;" data-allow-clear="true" multiple>
                                                            @if(count($data) == 1)
                                                            <option selected>No tiene discapacidades</option>
                                                            @else
                                                            @foreach($disc->whereIn('nombre', $data) as $dis)
                                                            <option value="{{$dis["id"]}}" selected>{{$dis["nombre"]}}</option>
                                                            @endforeach
                                                            @endif
                                                        </select>
                                                        <label for="idDiscapacidad">Discapacidad(es)</label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                    <div class="form-material">
                                                        <select {{count($data) -1 == $countDisc ? 'disabled' : ''}} class="js-select2 form-control" id="Discapacidad" name="Discapacidades[]"  style="width: 100%;" data-allow-clear="true" data-placeholder="¿Más discapacidades?" multiple>
                                                            @if(count($data) -1 == $countDisc)
                                                            <option selected>Resultados no encontrados</option>
                                                            @else
                                                            <option></option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                            @foreach($disc->whereNotIn('nombre', $data) as $dis)
                                                            <option value="{{ $dis->id }}">{{$dis->nombre}}</option>
                                                            @endforeach
                                                            @endif
                                                        </select>
                                                        <label for="Discapacidad">Agregar discapacidad (Opcional)</label>
                                                </div>
                                            </div>

                                                    <div class="form-group">
                                                            <div class="form-material">
                                                                <select class="js-select2 form-control" id="idestado" name="idestado" style="width: 100%;" data-allow-clear="true">
                                                                    <option value="" selected>Seleccione estado del aprendiz</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                                    @foreach($estado as $es)
                                                                    <option value="{{ $es->id }}" {{$es->id == $aprendiz->idestado ? 'selected' : ''}}>{{$es->nombre}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <label for="idestado">Estado <span class="text-danger">*</span></label>
                                                        </div>
                                                    </div>

                                        </div>
                                        <!-- END Step 2 -->

                                        <!-- Step 3 -->
                                        <div class="tab-pane" id="wizard-validation-material-step3" role="tabpanel">
                                            
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="nombreAcudiente" name="nombreAcudiente" value="{{$aprendiz->nombreAcudiente}}">
                                                <label for="nombreAcudiente">Nombre acudiente <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-vcard"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="telAcudiente" name="telAcudiente" value="{{$aprendiz->telAcudiente}}">
                                                <label for="telAcudiente">Teléfono acudiente <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-phone"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                            <div class="form-group">
                                                <div class="form-material">
                                                    <select class="js-select2 form-control" id="idColegio" name="idColegio" style="width: 100%;" data-allow-clear="true">
                                                        <option value="" selected>Seleccione colegio</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                        @foreach($colegio as $c)
                                                        <option value="{{ $c->id }}" >{{$c->nombreColegio}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="idColegio">Colegio <span class="text-danger">*</span></label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-material">
                                                    <select class="js-select2 form-control" id="idGrado" name="idGrado" style="width: 100%;" data-allow-clear="true">
                                                        <option value="" selected>Seleccione grado</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                        @foreach($grado as $g)
                                                        <option value="{{ $g->id }}" {{$g->id == $aprendiz->idGrado ? 'selected' : ''}}>{{$g->grado}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="idGrado">Grado <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                        <!-- END Step 3 -->
                                    </div>
                                    <!-- END Steps Content -->

                                    <!-- Steps Navigation -->
                                    <div class="block-content block-content-sm block-content-full bg-body-light">
                                        <div class="row">
                                            <div class="col-6">
                                                <button type="button" class="btn btn-alt-secondary" data-wizard="prev">
                                                    <i class="fa fa-angle-left mr-5"></i> Anterior
                                                </button>
                                            </div>
                                            <div class="col-6 text-right">
                                                <button type="button" class="btn btn-alt-secondary" data-wizard="next">
                                                    Siguiente <i class="fa fa-angle-right ml-5"></i>
                                                </button>
                                                <button type="submit" class="btn btn-alt-success d-none" data-wizard="finish">
                                                    <i class="fa fa-check mr-5"></i> Actualizar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Steps Navigation -->
                                </form>
                                <!-- END Form -->
                            </div>
@endsection
@section('js_after')
        <!-- Page JS Plugins -->
        <script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="/js/plugins/jquery-validation/additional-methods.js"></script>

        <!-- Page JS Code -->
        <script src="/js/pages/be_forms_wizard.min.js"></script>

        <script>
        
        $('.datepicker').datepicker({
                endDate: new Date(),
                language: 'es',
                todayHighlight: !0,
                autoclose: !0,
                weekStart: 0,
                orientation: 'bottom' // Position issue when using BS4, set it to bottom until officially supported
            });

        </script>

@endsection
