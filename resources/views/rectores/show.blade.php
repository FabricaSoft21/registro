<div class="modal fade" id="modal-fromleft-{{$rector->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-fromleft" aria-hidden="true">
<div class="modal-dialog modal-dialog-fromleft modal-lg" role="document">
    <div class="modal-content">
        <div class="block block-themed block-transparent mb-0">
            <div class="block-header bg-primary-dark">
            <h3 class="block-title">{{$rector->nombre}} {{$rector->apellido}}</h3>
                <div class="block-options">
                    <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                        <i class="si si-close"></i>
                    </button>
                </div>
            </div>
            <div class="block block-themed block-transparent mb-0">
                    <div class="block block-themed">
                            <div class="block-content block-content-full text-center bg-gd-primary">
                                <img class="img-avatar img-avatar96 img-avatar-thumb" src="/media/avatars/avatar15.jpg" alt="">
                             <h3 class="text-white mt-3">INFORMACIÓN
                                 <i class=" si si-info"></i>
                             </h3>
                            </div>
                            
                            <div class="col-md-12">
                                    <!-- Simple Wizard 2 -->
                                    <div class="js-wizard-simple block">
                                        <!-- Step Tabs -->
                                        <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#wizard-simple2-step1-{{$rector->id}}" data-toggle="tab">PERSONAL</a>
                                            </li>
                                            <li class="nav-item">
                                                    <a class="nav-link" href="#wizard-simple2-step2-{{$rector->id}}" data-toggle="tab">COLEGIO</a>
                                            </li>
                                        </ul>
                                        <!-- END Step Tabs -->
        
                                        <!-- Form -->
                                        <form action="{{{url ("/rector/show/$rector->id")}}}" method="get">
                                            <!-- Steps Content -->
                                            <div class="block-content block-content-full tab-content" style="min-height: 267px;">
                                                <!-- Step 1 -->
                                                <div class="tab-pane active" id="wizard-simple2-step1-{{$rector->id}}" role="tabpanel">
                                                    <div class="form-group">
                                                        <div class="form-material floating">
                                                            <input class="form-control" value="{{$rector->nombre}}" type="text" id="wizard-simple2-firstname" name="wizard-simple2-firstname" readonly>
                                                            <label for="wizard-simple2-firstname">Nombre</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="form-material floating">
                                                            <input class="form-control" type="text" value="{{$rector->apellido}}"  id="wizard-simple2-lastname" name="wizard-simple2-lastname" readonly>
                                                            <label for="wizard-simple2-lastname">Apellido</label>
                                                        </div>
                                                    </div>
  
                                                            <div class="form-group">
                                                                    <div class="form-material floating">
                                                                        <input class="form-control"  retype="text" value="{{$rector->telefono}}"  id="wizard-simple2-lastname" name="wizard-simple2-lastname" readonly>
                                                                        <label for="wizard-simple2-lastname">Teléfono</label>
                                                                    </div>
                                                                </div>
                                                               
                                                    <div class="form-group">
                                                        <div class="form-material floating">
                                                            <input class="form-control" type="email"  value="{{$rector->correo}}" id="wizard-simple2-email" name="wizard-simple2-email" readonly>
                                                            <label for="wizard-simple2-email">Correo electrónico</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END Step 1 -->
                                                <div class="tab-pane" id="wizard-simple2-step2-{{$rector->id}}" role="tabpanel">
                                                    <div class="form-group">
                                                        <div class="form-material">
                                                            <input class="form-control" type="email" id="nombre-{{$rector->id}}" name="nombre-{{$rector->id}}" value="" readonly>
                                                            <label for="nombre-{{$rector->id}}">Nombre del colegio</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                            <div class="form-material">
                                                                <input class="form-control" type="email" id="direccion-{{$rector->id}}" name="direccion-{{$rector->id}}" value="" readonly>
                                                                <label for="direccion-{{$rector->id}}">Dirección del colegio</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                                <div class="form-material">
                                                                    <input class="form-control" type="email" id="telefono-{{$rector->id}}" name="telefono-{{$rector->id}}" value="" readonly>
                                                                    <label for="telefono-{{$rector->id}}">Teléfono del colegio</label>
                                                            </div>
                                                        </div>
                                            </div>
                                        </form>
                                        <!-- END Form -->
                                    </div>
        </div>
        <div class="modal-footer">
            <!-- {{-- <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button> --}} -->
            <button type="button" class="btn btn-alt-success" data-dismiss="modal">
                <i class="fa fa-check"></i> Listo
            </button>
        </div>
    </div>
</div>
</div>

