@extends('layouts.backend')
@section('content')
                <!-- Page Content -->
                <div class="content">
                    <h2 class="content-heading">Formación</h2>

                    <!-- Dynamic Table Full -->
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Ver Fichas</h3>
                            <a href="/ficha/create" data-toggle="tooltip" title="Crear ficha" class="btn btn-success">
                                <i class="fa fa-plus"></i>                              
                            </a>
                        
                        </div>
                        <div class="block-content block-content-full">
                            <!-- DataTables functionality is initialized with .js-dataTable-full class in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                            @if($cant == 0)
                            <div class="col-md-12">
                                <div class="block">
                                    <div class="block-content block-content-full">
                                        <div class="py-20 text-center">
                                            <div class="mb-20">
                                                <i class="fa fa-close fa-5x text-danger"></i>
                                            </div>
                                            <div class="font-size-h4 font-w600">Lo sentimos</div>
                                            <div class="text-muted">Actualmente no hay ningún registro</div>
                                          
                                    </div>
                                </div>
                            </div>
                            @else
                            <table id="ficha" class="table js-dataTable-full" width="100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nombre</th>
                                        <th>Código</th>    
                                        <th>Programa formación</th>   
                                        <th>Estado</th>
                                        <th class="noExportExc noExport">Opciones</th>

                                        {{-- <th class="d-none d-sm-table-cell" style="width: 15%;">Access</th>
                                        <th class="text-center" style="width: 15%;">Profile</th> --}}
                                    </tr>
                                </thead>
                                <tbody>

                                    @if (auth()->user()->selected_category_id==1)
                                        @php($clasi=$titulada)
                                    @endif

                                    @if (auth()->user()->selected_category_id==2)
                                         @php($clasi=$complementaria)
                                    @endif

                                    @if (auth()->user()->selected_category_id==3)
                                         @php($clasi=$poblacion)
                                    @endif  

                                    @if (auth()->user()->selected_category_id==4)
                                        @php($clasi=$competencia)
                                    @endif  

                                    @if (auth()->user()->selected_category_id==5)
                                        @php($clasi=$media)
                                     @endif  

                                @foreach($clasi as $fic)
                                    <tr>
                                        <td class="font-w600">{{$loop->iteration}}</td>
                                        <td class="font-w600">{{$fic->nombreFicha}}</td>
                                        <td class="font-w600">{{$fic->codigo}}</td>
                                        <td class="font-w600">{{$fic->programa->nombre}}</td>
                                        <td class="font-w600">{{$fic->estado->nombre}}</td>
                                        <td class="text-center">
                                            <a href="/ficha/edit/{{$fic->id}}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Editar ficha">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <span></span>
                                        <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal-fromleft-{{$fic->id}}"title="Ver ficha">
                                                <i class="fa fa-eye"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    @include('fichas/show')
                                    @endforeach
                                </tbody>
                            </table>
                            @endif
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->
                </div>

    <script src="/js/functions/custom-file-input.js"></script>
    <div class="modal fade" id="modal-fromleft" tabindex="-1" role="dialog" aria-labelledby="modal-fromleft" aria-hidden="true">
    <div class="modal-dialog modal-dialog-fromleft modal-lg" role="document">
        <div class="modal-content">
         <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                <h3 class="block-title">IMPORTANCIÓN DE DATOS</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                     </button>
                    </div>
                </div>
             <div class="block block-themed block-transparent mb-0">
                    <div class="block block-themed">
                            <div class="block-content block-content-full text-center bg-primary-light">
                                <img class="img-avatar img-avatar96 img-avatar-thumb" src="media/various/logo.png" alt="">
                             </h3>
                            </div>
                            <div class="col-md-12">
                                    <!-- Simple Wizard 2 -->
                                    <div class="js-wizard-simple block">
                                        <!-- Step Tabs -->
                                        <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#wizard-simple2-step1" data-toggle="tab">IMPORTAR</a>
                                            </li>
                                        </ul>
                                        <!-- END Step Tabs -->

                                        <!-- Form -->
                                        <form action="" method="post" class="js-wizard-validation-material-form" enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-md-12">
                                                    <div class="block">
                                                        <div class="block-content block-content-full">
                                                            <div class="py-20 text-center">
                                                                <div class="mb-20">
                                                                    <i class="fa fa-info fa-5x text-info"></i>
                                                                </div>
                                                                <div class="font-size-h4 font-w600">¡Espera!</div>
                                                                <div class="text-muted">Recuerda subir un archivo con la extensión y el formato correcto.</div>
                                                                <a class="link-effect mt-5 d-inline-block" href="https://misenaeduco-my.sharepoint.com/:x:/g/personal/arango1_misena_edu_co/EdSmjtnGVYFLtcf9BgTiFkgBqSnIiGlMT0lzTNr5z_Bm4g?e=MgFs6D" target="_blank">
                                                                    Preview <i class="fa fa-external-link mr-5"></i> 
                                                                </a> 
                                                                <div class="pt-20 mb-3">
                                                                <div class="form-group box">
                                                                    <input type="file" name="file-selected" id="file-selected" class="file-selected inputfile inputfile-3" />
                                                                    <label for="file-selected"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Elige un archivo&hellip;</span></label>
                                                                </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                         <div class="modal-footer">
                 <!-- {{-- <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button> --}} -->
                <button type="submit" class="btn btn-alt-success">
                <i class="fa fa-file-excel-o mr-5"></i>  Guardar
                </button>
            </form>
          </div>
        </div>
    </div>
</div>


@endsection

@section('js_after')
        <script src="/js/functions/custom-file-input.js"></script>
        <!-- Page JS Plugins -->
        <script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="/js/plugins/jquery-validation/additional-methods.js"></script>

        <!-- Page JS Code -->
        <script src="/js/pages/be_forms_wizard.min.js"></script>
@endsection
