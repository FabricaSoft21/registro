@extends('layouts.backend')

@section('content')
                <div class="content">
                    <h2 class="content-heading">Formación</h2>
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Modificar ficha</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option">
                                    <i class="fa fa-cogs"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="row justify-content-center py-20">
                                <div class="col-xl-8">
                                    <form class="js-wizard-validation-material-form" action="" method="POST">
                                            {{ csrf_field() }}
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="nombreFicha" name="nombreFicha" value="{{$ficha->nombreFicha}}">
                                                    <label for="nombreFicha">Nombre ficha <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-vcard"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="codigo" name="codigo" value="{{$ficha->codigo}}">
                                                <label for="codigo">Código <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-barcode"></i>
                                                    </span>
                                                </div>
                                        </div>
                                    </div>

                                                                                <div class="form-group">
                                                <div class="form-material">
                                                    <select class="js-select2 form-control" id="idProgramaFormacion" name="idProgramaFormacion" style="width: 100%;" data-allow-clear="true">
                                                        <option value="" selected>Seleccione programa de formación</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                        @foreach($programa as $pro)
                                                        
                                                        <option value="{{ $pro->id}}" {{$pro->id == $ficha->idProgramaFormacion ? "selected":"" }}>{{$pro->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="idProgramaFormacion">Programa formación <span class="text-danger">*</span></label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                                <div class="form-material">
                                                    <select class="js-select2 form-control" id="idTipoFicha" name="idTipoFicha" style="width: 100%;" data-allow-clear="true">
                                                        <option value="" selected>Seleccione estado de la ficha</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                        @foreach($tipo as $t)
                                                        
                                                        <option value="{{ $t->id}}" {{$t->id == $ficha->idTipoFicha ? "selected":"" }}>{{$t->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="idTipoFicha">Estado ficha <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                                                         
                                       
                                        <div class="form-group row">
                                            <div class="col-lg-8 ml-auto">
                                                <button type="submit" class="btn btn-alt-success pull-right">Actualizar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
@endsection

@section('js_after')
        <!-- Page JS Plugins -->
        <script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="/js/plugins/jquery-validation/additional-methods.js"></script>

        <!-- Page JS Code -->
        <script src="/js/pages/be_forms_wizard.min.js"></script>

@endsection