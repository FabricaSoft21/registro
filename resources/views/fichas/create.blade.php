@extends('layouts.backend')

@section('content')
                <div class="content">
                    <h2 class="content-heading">Formación</h2>
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Registar Ficha</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option">
                                    <i class="si si-user-follow"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="row justify-content-center py-20">
                                <div class="col-xl-8">
                                    <form class="js-wizard-validation-material-form" action="" method="POST">
                                            {{ csrf_field() }}
                                            <input class="form-control" value="{{$ficha['id'] + 1}}" type="hidden" id="id" name="id">
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" value="{{old('nombreFicha')}}" type="text" id="nombreFicha" name="nombreFicha">
                                                    <label for="nombreFicha">Nombre ficha <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-vcard"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" value="{{old('codigo')}}" id="codigo" name="codigo">
                                                <label for="codigo">Código <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-barcode"></i>
                                                    </span>
                                                </div>
                                        </div>
                                    </div>

                                            <div class="form-group">
                                                <div class="form-material">
                                                    <select class="js-select2 form-control" id="idProgramaFormacion" name="idProgramaFormacion" style="width: 100%;" data-allow-clear="true">
                                                        <option value="" selected>Seleccione programa de formación</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                        @if (auth()->user()->selected_category_id==1)
                                                        @php($type=$PTitulada)
                                                        @endif
                    
                                                        @if (auth()->user()->selected_category_id==2)
                                                            @php($type=$PComplementaria)
                                                        @endif
                    
                                                        @if (auth()->user()->selected_category_id==3)
                                                            @php($type=$Poblacion)
                                                        @endif  
                    
                                                        @if (auth()->user()->selected_category_id==4)
                                                            @php($type=$PCompetencia)
                                                        @endif  
                    
                                                        @if (auth()->user()->selected_category_id==5)
                                                            @php($type=$PMedia)
                                                        @endif 
                                                        @foreach($type as $pro)
                                                        
                                                        <option value="{{ $pro->id}}" {{ (old("idProgramaFormacion") == $pro->id ? "selected":"") }}>{{$pro->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="idProgramaFormacion">Programa formación <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" value="Etapa lectiva" id="" name="" readonly>
                                                <label for="apellido">Estado de la ficha </label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-address-card-o"></i>
                                                    </span>
                                                </div>
                                        </div>
                                    </div>

                                        <div class="form-group row">
                                            <div class="col-lg-8 ml-auto">
                                                <button type="submit" class="btn btn-alt-success pull-right">Registrar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
@endsection

@section('js_after')
        <!-- Page JS Plugins -->
        <script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="/js/plugins/jquery-validation/additional-methods.js"></script>

        <!-- Page JS Code -->
        <script src="/js/pages/be_forms_wizard.min.js"></script>

@endsection
