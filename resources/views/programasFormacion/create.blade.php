@extends('layouts.backend')

@section('content')
                <div class="content">
                    <h2 class="content-heading">Programas de formación</h2>
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Registar Programa</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option">
                                    <i class="si ssi-book-open"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="row justify-content-center py-20">
                                <div class="col-xl-8">
                                    <form class="js-wizard-validation-material-form" action="" method="POST">
                                            {{ csrf_field() }}
                                            {{-- <input class="form-control" value="{{$coordi['id'] + 1}}" type="hidden" id="id" name="id">
                                            <input class="form-control" value="{{date('Y-m-d h:i:s')}}" type="hidden" id="created_at" name="created_at"> --}}
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" value="{{old('nombre')}}" type="text" id="nombre" name="nombre">
                                                    <label for="nombre">Nombre <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-vcard"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                    <div class="form-group" style="display: {{auth()->user()->selected_category_id == 1 || auth()->user()->selected_category_id == 5  ? 'none' : ''}}">
                                                <div class="form-material">
                                                            <select class="js-select2 form-control" id="idFamilia" name="idFamilia" style="width: 100%;" data-allow-clear="true">
                                                                <option value="" selected>Seleccione familia</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                                @foreach($familia as  $fami)
                                                                <option value="{{ $fami->id }}">{{$fami->nombre}}</option>
                                                                @endforeach
                                                            </select>
                                                            <label for="idMunicipio">Familia <span class="text-danger">*</span></label>
                                                    </div>
                                                    <a class="link-effect text-muted mb-5 d-inline-block " href="#" data-toggle="modal" data-target="#modal-familia">
                                                            <i class="fa fa-plus-square text-muted mr-5"></i>¿Nueva Familia?
                                                        </a>
                                                </div>


                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" value="{{old('codigo')}}" id="codigo" name="codigo">
                                                <label for="codigo">Código <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-barcode"></i>
                                                    </span>
                                                </div>
                                        </div>
                                    </div>

                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="version" name="version" value="{{old('version')}}">
                                                <label for="version"> Versión del programa<span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="si si-bar-chart"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                   
                                        <div class="form-group">
                                                <div class="form-material">
                                                            <select class="js-select2 form-control" id="idNivel" name="idNivel" style="width: 100%;" data-allow-clear="true">
                                                                <option value="" selected>Seleccione Nivel</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                                @foreach($nivel as  $niv)
                                                                <option value="{{ $niv->id }}">{{$niv->nombre}}</option>
                                                                @endforeach
                                                            </select>
                                                            <label for="idMunicipio">Nivel del programa <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>

                                            <input type="hidden" name="idTipo_formacion" value="{{auth()->user()->selected_category_id}}">

                                                {{-- <div class="form-group">
                                                        <div class="form-material">
                                                                    <select class="js-select2 form-control" id="idTipo_formacion" name="idTipo_formacion" style="width: 100%;" data-allow-clear="true">
                                                                        <option value="" selected>Seleccione tipo</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                                        @foreach($tipoFormacion as  $tipo)
                                                                        <option value="{{ $tipo->id }}">{{$tipo->nombre}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    <label for="idMunicipio">Tipo de formación <span class="text-danger">*</span></label>
                                                            </div>
                                                        </div> --}}

                                        <div class="form-group row">
                                            <div class="col-lg-8 ml-auto">
                                                <button type="submit" class="btn btn-alt-success pull-right">Registrar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                        <!-- Modal familia -->
                        <div class="modal fade" id="modal-familia" tabindex="-1" role="dialog" aria-labelledby="modal-terms" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-slidedown" role="document">
                                <div class="modal-content">
                                    <div class="block block-themed block-transparent mb-0">
                                        <div class="block-header bg-primary-dark">
                                            <h3 class="block-title">Registrar Nueva Familia</h3>
                                                <div class="block-options">
                                                    <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                                        <i class="si si-close"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        <div class="block block-themed block-transparent mb-0">
                                                <div class="block-content block-content-full text-center bg-gd-primary">
                                                <img class="img-avatar img-avatar96 img-avatar-thumb" src="/media/avatars/avatar15.jpg" alt="">
                                        </div>
                                        <div class="row justify-content-center py-20">
                                                <div class="col-xl-8">
                                                    <span id="form_resultF"></span>
                                                <form id="form-fami" class="js-validation-material2" action="" method="POST">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <div class="form-material input-group floating">
                                                        <input class="form-control" value="{{old('nombre')}}" type="text" id="nombre" name="nombre">
                                                        <label for="nombre">Nombre <span class="text-danger">*</span></label>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-ge"></i>
                                                            </span>
                                                        </div>
                                                </div>
                                            </div>     
                                    </div>
                                    </div>
                                    </div>
                                    <div class="modal-footer">
                                            <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cerrar</button>
                                            <button type="submit" class="btn btn-alt-success">
                                            <i class="fa fa-check"></i> Guardar
                                        </button>
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                </div>
@endsection

@section('js_after')
        <!-- Page JS Plugins -->
        <script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="/js/plugins/jquery-validation/additional-methods.js"></script>

        <script src="/js/pages/be_forms_validation.min.js"></script>

        <!-- Page JS Code -->
        <script src="/js/pages/be_forms_wizard.min.js"></script>

        <script>
                $('#form-fami').on('submit',function(event){
            event.preventDefault();
            $.ajax({
                url:"{{ route('saveFami')}}",
                method:"POST",
                data : new FormData(this),
                contentType:false,
                cache:false,
                processData:false,
                dataType:"json",
                success:function(data)
                {
                    // console.log(data);
                    var html = "";
                    if(data.errors){
                        html = '<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">×</button>';
                            for(var count = 0; count < data.errors.length; count++)
                            {
                                html += '<p>' + data.errors[count] +
                                    '</p>';
                            }
                            html += '</div>';
                    }
                    if(data.success)
                    {
                        // console.log(data)

                        html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">×</button>'
                            + data.success + '</div>';
                            $('#form-fami')[0].reset();
                            $("#idFamilia").append('<option value=' + data.data["0"].id + '>' + data.data["0"].nombre + '</option>');
                    }
                    $('#form_resultF').html(html);
                }
            })
        });

        </script>

@endsection
