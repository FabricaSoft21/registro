@extends('layouts.backend')
@section('content')
                <!-- Page Content -->
                <div class="content">
                    <h2 class="content-heading">Programas de formación</h2>

                    <!-- Dynamic Table Full -->
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Ver Programas de formación</h3>
                            <a href="/programa/create" data-toggle="tooltip" title="Crear programa" class="btn btn-success">
                                <i class="fa fa-plus"></i>
                            </a>
                            <a  data-toggle="modal" type="file" data-target="#modal-fromleft" title="Importar datos" class="btn btn-secondary ml-2" style={{$cant == 0 ? "display:none" : "" }}>
                                <i class="fa fa-folder-open-o"></i>
                            </a>
                            <button type="button" class="btn btn-alt-info ml-2" id="btnAsociar" disabled onclick="getTopics(1)">Competencias</button>

                        </div>
                        <div class="block-content block-content-full">
                            <!-- DataTables functionality is initialized with .js-dataTable-full class in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                            @if($cant == 0)
                            <div class="col-md-12">
                                <div class="block">
                                    <div class="block-content block-content-full">
                                        <div class="py-20 text-center">
                                            <div class="mb-20">
                                                <i class="fa fa-close fa-5x text-danger"></i>
                                            </div>
                                            <div class="font-size-h4 font-w600">Lo sentimos</div>
                                            <div class="text-muted">Actualmente no hay ningún registro</div>
                                            <form class="js-wizard-validation-material-form" method="post" action="{{route('importI')}}" enctype="multipart/form-data">
                                             @csrf
                                            <div class="pt-20 mb-3">
                                            <div class="form-group box">
                                                <input type="file" name="file-selected" id="file-selected" class="file-selected inputfile inputfile-3" />
                                                <label for="file-selected"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Elige un archivo&hellip;</span></label>
                                            </div>
                                            </div>
                                            <button type="submit" class="btn btn-rounded btn-alt-success" style="width:200px !important;">
                                                    <i class="fa fa-file-excel-o mr-5"></i> Importar
                                            </button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            @else
                            <table id="programa" class="table  js-table-checkable js-dataTable-full" width="100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th></th>
                                        <th>Nombre</th>
                                        <th>Familia</th>
                                        <th>Código</th>
                                        <th>Versión</th>
                                        <th>Nivel</th>
                                        <th>Tipo formación</th>
                                        <th class="noExportExc noExport">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (auth()->user()->selected_category_id==1)
                                    @php($clasi=$titulada)
                                    @endif
                                    @if (auth()->user()->selected_category_id==2)
                                    @php($clasi=$complementaria)
                                    @endif
                                    @if (auth()->user()->selected_category_id==3)
                                    @php($clasi=$poblacion)
                                    @endif
                                    @if (auth()->user()->selected_category_id == 4)
                                    @php($clasi = $competencias)
                                    @endif
                                    @if (auth()->user()->selected_category_id == 5)
                                    @php($clasi = $media)
                                    @endif

                                @foreach($clasi as $prog)
                                    <tr>
                                        <td class="font-w600">{{$loop->iteration}}</td>
                                        <td>
                                            <label class="css-control css-control-sm css-control-warning css-checkbox">
                                            <input type="radio" class="css-control-input" id="idAsociar" name="idAsociar" value="{{$prog->id}}" data-programa="{{$prog->id}}">
                                                <span class="css-control-indicator"></span>
                                            </label>
                                        </td>
                                        <td class="font-w600">{{$prog->nombre}}</td>
                                        <td class="font-w600">{{$prog->idFamilia == null ? 'No aplica' : $prog->familia->nombre}}</td>
                                        <td class="font-w600">{{$prog->codigo}}</td>
                                        <td class="font-w600">{{$prog->version}}</td>
                                        <td class="font-w600">{{$prog->nivel->nombre}}</td>
                                        <td class="font-w600">{{$prog->typeF->nombre}}</td>



                                        {{-- <td class="text-center">1</td>
                                        <td class="font-w600">Susan Day</td>
                                        <td class="d-none d-sm-table-cell">customer1@example.com</td>
                                        <td class="d-none d-sm-table-cell">
                                            <span class="badge badge-primary">Personal</span>
                                        </td> --}}
                                        <td class="text-center">
                                            <a href="/programa/edit/{{$prog->id}}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Editar programa">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <span></span>
                                        <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal-fromleft-{{$prog->id}}"title="Ver programa">
                                                <i class="fa fa-eye"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    @include('programasFormacion/show')
                                    @endforeach
                                </tbody>
                            </table>
                            @endif
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->
                </div>

    <script src="/js/functions/custom-file-input.js"></script>
    <div class="modal fade" id="modal-fromleft" tabindex="-1" role="dialog" aria-labelledby="modal-fromleft" aria-hidden="true">
    <div class="modal-dialog modal-dialog-fromleft modal-lg" role="document">
        <div class="modal-content">
         <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                <h3 class="block-title">IMPORTANCIÓN DE DATOS</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                     </button>
                    </div>
                </div>
             <div class="block block-themed block-transparent mb-0">
                    <div class="block block-themed">
                            <div class="block-content block-content-full text-center bg-primary-light">
                                <img class="img-avatar img-avatar96 img-avatar-thumb" src="media/various/logo.png" alt="">
                             </h3>
                            </div>
                            <div class="col-md-12">
                                    <!-- Simple Wizard 2 -->
                                    <div class="js-wizard-simple block">
                                        <!-- Step Tabs -->
                                        <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#wizard-simple2-step1" data-toggle="tab">IMPORTAR</a>
                                            </li>
                                        </ul>
                                        <!-- END Step Tabs -->

                                        <!-- Form -->
                                        <form action="" method="post" class="js-wizard-validation-material-form" enctype="multipart/form-data">
                                            @csrf
                                            <div class="col-md-12">
                                                    <div class="block">
                                                        <div class="block-content block-content-full">
                                                            <div class="py-20 text-center">
                                                                <div class="mb-20">
                                                                    <i class="fa fa-info fa-5x text-info"></i>
                                                                </div>
                                                                <div class="font-size-h4 font-w600">¡Espera!</div>
                                                                <div class="text-muted">Recuerda subir un archivo con la extensión y el formato correcto.
                                                                </div>
                                                                <div class="pt-20 mb-3">
                                                                <div class="form-group box">
                                                                    <input type="file" name="file-selected" id="file-selected" class="file-selected inputfile inputfile-3" />
                                                                    <label for="file-selected"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Elige un archivo&hellip;</span></label>
                                                                </div>
                                                                <div class="block block-mode-hidden">
                                                                        <ul class="nav nav-tabs nav-tabs-block align-items-center" data-toggle="tabs" role="tablist">
                                                                            <li class="nav-item">
                                                                                <a class="nav-link" href="#">Vista previa</a>
                                                                            </li>
                                                                            <li class="nav-item ml-auto">
                                                                                <div class="block-options mr-15">
                                                                                    <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                                                                                        <i class="si si-refresh"></i>
                                                                                    </button>
                                                                                    <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"></button>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                        <div class="block-content tab-content">
                                                                            <div class="tab-pane active" id="btabswo-static-home" role="tabpanel">
                                                                                <div class="resp-container">
                                                                                    <iframe width="402" height="346" frameborder="0" scrolling="no" src="https://onedrive.live.com/embed?resid=1EF43E3FE1A12FAC%21111&authkey=%21AGtXiMdqWIfYYlo&em=2&wdAllowInteractivity=False&wdHideGridlines=True&wdHideHeaders=True&wdDownloadButton=True&wdInConfigurator=True"></iframe>                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                         <div class="modal-footer">
                 <!-- {{-- <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button> --}} -->
                <button type="submit" class="btn btn-alt-success">
                <i class="fa fa-file-excel-o mr-5"></i>  Guardar
                </button>
            </form>
          </div>
        </div>
    </div>
</div>

@endsection

@section('js_after')

<div class="modal" id="modal-normal" tabindex="-1" role="dialog" aria-labelledby="modal-normal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Competencias de Formación</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block block-themed">
                    <div class="block-content block-content-full text-center bg-gd-primary">
                            <img class="img-avatar img-avatar96 img-avatar-thumb" src="/media/avatars/Books-1.jpg" alt="">
                        </div>
                    <div class="block-content">

                            <div class="js-wizard-simple block">
                                    <!-- Step Tabs -->
                                    <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                        <li class="nav-item">
                                        <a class="nav-link active" href="#wizard-simple2-step1" data-toggle="tab">Competencias asociadas </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#wizard-simple2-step2" data-toggle="tab">Listado de competencias</a>
                                        </li>
                                        <li class="nav-item">
                                                <a class="nav-link" href="#wizard-simple2-step3" data-toggle="tab">Crear competencia</a>
                                            </li>
                                    </ul>
                                    <!-- END Step Tabs -->

                                    <!-- Form -->
                                        <!-- Steps Content -->
                                        <div class="block-content block-content-full tab-content" style="min-height: 267px;">
                                            <!-- Step 1 -->
                                            <input class="form-control" type="hidden" id="wizard-simple2-firstname" name="idUser" value="" readonly>

                                            <div class="tab-pane active" id="wizard-simple2-step1" role="tabpanel">
                                                    <form action="{{route('removeC',1)}}" method="post">
                                                    @csrf
                                                    <input type="hidden" class="idPrograma" name="idPrograma" value="">
                                                    <button id="btnCompeC" type="submit" class="btn btn-alt-danger pull-right" disabled>
                                                            Remover  <i class="fa fa-minus-square"></i>
                                                       </button>
                                                       <br/>

                                                    <div id="show-competencia">
                                                    </form>

                                                </div>

                                            </div>

                                            <!-- Step 2 -->
                                            <div class="tab-pane" id="wizard-simple2-step2" role="tabpanel">
                                                <div>
                                                    <form action="{{route('detailP')}}" method="post">
                                                    @csrf
                                                    <input type="hidden" class="idPrograma" name="idPrograma" value="">
                                                    <button id="btnCompe" type="submit" class="btn btn-alt-success pull-right" disabled>
                                                            Asociar  <i class="fa fa-plus-square"></i>
                                                       </button>
                                                       <br/>
                                                       <div id="list-competencia"></div>
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="wizard-simple2-step3" role="tabpanel">
                                                    <div class="block">
                                                            <div class="block-header block-header-default">
                                                                <h3 class="block-title">Registar Competencia</h3>
                                                                <div class="block-options">
                                                                    <button type="button" class="btn-block-option">
                                                                        <i class="si si-user-follow"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div class="block-content">
                                                                <div class="row justify-content-center py-20">
                                                                    <div class="col-xl-8">
                                                                            <span id="form_resultC"></span>
                                                                        <form class="js-wizard-validation-material-form" action="" method="POST" id="compe">
                                                                                {{ csrf_field() }}
                                                                                  <div class="form-group">
                                                                                    <div class="form-material input-group floating">
                                                                                        <input class="form-control" value="{{auth()->user()->selected_category_id}}" type="hidden" id="idTipoCompetencia" name="idTipoCompetencia">
                                                                                        <input class="form-control" value="{{old('nombre')}}" type="text" id="nombre" name="nombre">
                                                                                        <label for="nombre">Nombre de la competencia <span class="text-danger">*</span></label>
                                                                                        <div class="input-group-append">
                                                                                            <span class="input-group-text">
                                                                                                <i class="fa fa-vcard"></i>
                                                                                            </span>
                                                                                        </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <div class="form-material">
                                                                                <select class="js-select2 form-control" id="idTipoCompetencia" name="idTipoCompetencia" style="width: 100%;" data-allow-clear="true">
                                                                                        <option value="" selected>Seleccione tipo</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                                                        @foreach($tipoC as $for)
                                                                                        <option value="{{$for->id}}">{{$for->nombre}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                    <label for="idTipoDoc">Tipo competencia <span class="text-danger">*</span></label>
                                                                                </div>
                                                                            </div>

                                                                                <button type="submit" class="btn btn-alt-success pull-right">Agregar</button>
                                                                                </div>
                                                                        </form>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                </div>

                                            <!-- END Step 2 -->
                                        <!-- END Steps Content -->
                                    <!-- END Form -->
                                </div>
                    </div>
                </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cerrar</button>
                        {{-- <button type="button" class="btn btn-alt-success" data-dismiss="modal">
                            <i class="fa fa-check"></i> Perfecto
                        </button> --}}
                </div>
            </div>
            </div>

        </div>

<script src="/js/functions/custom-file-input.js"></script>
        <!-- Page JS Plugins -->
        <script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="/js/plugins/jquery-validation/additional-methods.js"></script>

        <!-- Page JS Code -->
        <script src="/js/pages/be_forms_wizard.min.js"></script>
        <script>
            $('.css-control-input').click(function() {
                if ($(this).is(':checked')) {
                    $('#btnAsociar').removeAttr('disabled');
                        $('#info-ficha').text($(this).data('ficha'));

                } else {
                    $('#btnAsociar').attr('disabled', 'disabled');
                }
            });

            $("#btnAsociar").on("click", function(){
                // $('#modal-fadein').modal('toggle');
                $('#modal-normal').modal('show');
            });

            function getTopics(id){

            id = $("input[name='idAsociar']:checked").val()
            $('.idPrograma').val(id)

            $('#show-competencia').html(`<table id="competencia-info" class="table"><thead>
            <tr>
                <th></th>
                <th>Nombre</th>

            </tr>
            </thead>

            <tbody id="competencias-info">

            </tbody>

            </table>`);

            $('#list-competencia').html(`<table class="table js-table-checkable" width="100%" id="tbl-compe"><thead>
            <tr>
                <th>
                    <label class="css-control css-control-sm css-control-warning css-checkbox py-0">
                        <input type="checkbox" class="css-control-input chk-compe" id="check-all" name="check-all">
                        <span class="css-control-indicator"></span>
                    </label>
                </th>
                <th>Nombre</th>

            </tr>
            </thead>

            <tbody id="compe-list">

            </tbody>

            </table>`);

            $('#competencia-info').DataTable(
                {
                columnDefs:[{orderable:!1}],
                pageLength:8,
                lengthMenu:[[5,8,15,20],
                [5,8,15,20]],
                autoWidth:!1,responsive:true,
                language:{"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"},
                dom: 'Bfrtip',
                buttons: [{extend:'excelHtml5',
                exportOptions: {columns: "thead th:not(.noExportExc)"}},
                {extend:'pdf',exportOptions: {columns: "thead th:not(.noExport)"}}],
                "ajax": "/competencia/show/"+id,
                "columns": [
                    {"data" : "id",
                    render : function(data, type, row) {
                        return `<label class="css-control css-control-sm css-control-warning css-checkbox"> <input type="checkbox" class="css-control-input chk-compe" id="idAsociar" name="idCompetencias[]" value="${data}"><span class="css-control-indicator"></span></label>`
                        }
                    },
                    { "data" : "nombre"},
                    ]
            });

            $('#tbl-compe').DataTable(
                {
                columnDefs:[{orderable:!1}],
                pageLength:6,
                lengthMenu:[[5,8,15,20],
                [5,8,15,20]],
                autoWidth:!1,
                responsive:true,
                language:{"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"},
                dom: 'Bfrtip',
                buttons: [{extend:'excelHtml5',
                exportOptions: {columns: "thead th:not(.noExportExc)"}},
                {extend:'pdf',exportOptions: {columns: "thead th:not(.noExport)"}}],
                "ajax": "/getCompetencias",
                "columns": [
                    {"data" : "id",
                    render : function(data, type, row) {
                        return `<label class="css-control css-control-sm css-control-warning css-checkbox"> <input type="checkbox" class="css-control-input chk-compe" id="idAsociar" name="id[]" value="${data}"><span class="css-control-indicator"></span></label>`
                        }
                    },
                    { "data" : "nombre"},
                    ]
            });

        }

        $('#compe').on('submit',function(event){
            event.preventDefault();
            $.ajax({
                url:"{{ route('saveCo')}}",
                method:"POST",
                data : new FormData(this),
                contentType:false,
                cache:false,
                processData:false,
                dataType:"json",
                success:function(data)
                {
                    // console.log(data);
                    var html = "";
                    if(data.errors){
                        html = '<div class="alert alert-danger">  <button type="button" class="close" data-dismiss="alert">×</button>';
                            for(var count = 0; count < data.errors.length; count++)
                            {
                                html += '<p>' + data.errors[count] +
                                    '</p>';
                            }
                            html += '</div>';
                    }
                    if(data.success)
                    {
                        // console.log(data)

                        html = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">×</button>'
                            + data.success + '</div>';
                            $('#compe')[0].reset();
                            $('form-material select').val("0")
                    }
                    $('#form_resultC').html(html);
                    $('#tbl-compe').DataTable().ajax.reload();
                }
            })
        });

        </script>

    <script>
        $(document).on('click', '#btnAsociar', function(e){
            Codebase.helpers('table-tools');
            });

            $(document).on('click', '.chk-compe', function(e){
            if ($(this).is(':checked')) {
                $('#btnCompe').removeAttr('disabled');
                $('#btnCompeC').removeAttr('disabled');

            } else {
                $('#btnCompe').attr('disabled', 'disabled');
                $('#btnCompeC').removeAttr('disabled');

            }
        });

    </script>

@endsection
