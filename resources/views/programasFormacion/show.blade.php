<div class="modal fade" id="modal-fromleft-{{$prog->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-fromleft" aria-hidden="true">
    <div class="modal-dialog modal-dialog-fromleft modal-lg" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                <h3 class="block-title">{{$prog->nombre}}</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block block-themed block-transparent mb-0">
                        <div class="block block-themed">
                                <div class="block-content block-content-full text-center bg-gd-primary">
                                    <img class="img-avatar img-avatar96 img-avatar-thumb" src="/media/avatars/avatar15.jpg" alt="">
    
                                 <h3 class="text-white mt-3">INFORMACIÓN
    
                                     <i class=" si si-info"></i>
                                 </h3>
                                </div>
                                
                                <div class="col-md-12">
                                        <!-- Simple Wizard 2 -->
                                        <div class="js-wizard-simple block">
                                            <!-- Step Tabs -->
                                            <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#wizard-simple2-step1-{{$prog->id}}" data-toggle="tab">GENERAL</a>
                                                </li>
            
                                            </ul>
                                            <!-- END Step Tabs -->
            
                                            <!-- Form -->
                                            <form action="" method="get">
                                                <!-- Steps Content -->
                                                <div class="block-content block-content-full tab-content" style="min-height: 267px;">
                                                    <!-- Step 1 -->
                                                    <div class="tab-pane active" id="wizard-simple2-step1-{{$prog->id}}" role="tabpanel">
                                                        <div class="form-group">
                                                            <div class="form-material floating">
                                                            <input class="form-control" value="{{$prog->nombre}}" type="text" id="wizard-simple2-firstname" name="wizard-simple2-firstname" readonly>
                                                                <label for="wizard-simple2-firstname">Nombre</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="form-material floating">
                                                                <input class="form-control" type="text" value="{{$prog->idFamilia == null ? 'no aplica' : $prog->familia->nombre}}"  id="wizard-simple2-lastname" name="wizard-simple2-lastname" readonly>
                                                                <label for="wizard-simple2-lastname">Familia</label>
                                                            </div>
                                                        </div>
                                                      
                                                          
                                                                <div class="form-group">
                                                                        <div class="form-material floating">
                                                                            <input class="form-control"  type="text" value="{{$prog->codigo}}"  id="wizard-simple2-lastname" name="wizard-simple2-lastname" readonly>
                                                                            <label for="wizard-simple2-lastname">Código</label>
                                                                        </div>
                                                                    </div>
                                                                 
                                                        <div class="form-group">
                                                            <div class="form-material floating">
                                                                <input class="form-control" type="email"  value="{{$prog->version}}" id="wizard-simple2-email" name="wizard-simple2-email" readonly>
                                                                <label for="wizard-simple2-email">Versión</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="form-material floating">
                                                                <input class="form-control" type="email"  value="{{$prog->nivel->nombre}}" id="wizard-simple2-email" name="wizard-simple2-email" readonly>
                                                                <label for="wizard-simple2-email">Versión</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="form-material floating">
                                                                <input class="form-control" type="email"  value="{{$prog->typeF->nombre}}" id="wizard-simple2-email" name="wizard-simple2-email" readonly>
                                                                <label for="wizard-simple2-email">Versión</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- END Step 1 -->
                                                </div>
                                                   
                                            </form>
                                            <!-- END Form -->
                                        </div>
            </div>
            <div class="modal-footer">
                <!-- {{-- <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</buts
                    
                    ton> --}} -->
                <button type="button" class="btn btn-alt-success" data-dismiss="modal">
                    <i class="fa fa-check"></i> Listo
                </button>
            </div>
        </div>
    </div>
    </div>
    
    