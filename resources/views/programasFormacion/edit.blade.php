@extends('layouts.backend')

@section('content')
                <div class="content">
                    <h2 class="content-heading">Programas de formación</h2>
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Modificar Programa</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option">
                                    <i class="fa fa-cogs"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="row justify-content-center py-20">
                                <div class="col-xl-8">
                                    <form class="js-wizard-validation-material-form" action="" method="POST">
                                            {{ csrf_field() }}
                                            {{-- <input class="form-control" value="{{$coordi['id'] + 1}}" type="hidden" id="id" name="id">
                                            <input class="form-control" value="{{date('Y-m-d h:i:s')}}" type="hidden" id="created_at" name="created_at"> --}}
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" value="{{$programa->nombre}}" type="text" id="nombre" name="nombre">
                                                    <label for="nombre">Nombre <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-vcard"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="display: {{auth()->user()->selected_category_id == 1 ? 'none' : ''}}">
                                                <div class="form-material">
                                                            <select class="js-select2 form-control" id="idFamilia" name="idFamilia" style="width: 100%;" data-allow-clear="true">
                                                                <option value="" selected>Seleccione familia</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                                @foreach($familia as  $fami)
                                                        <option value="{{ $fami->id }}"{{$fami->id == $programa->idFamilia ? 'selected' : '' }} >{{$fami->nombre}}</option>
                                                                @endforeach
                                                            </select>
                                                            <label for="idFamilia">Familia <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" value="{{$programa->codigo}}" id="codigo" name="codigo">
                                                <label for="codigo">Código <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-barcode"></i>
                                                    </span>
                                                </div>
                                        </div>
                                    </div>

                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="version" name="version" value="{{$programa->version}}">
                                                <label for="version"> Versión del programa<span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                            <i class="si si-bar-chart"></i>
                                                        </span>
                                                </div>
                                            </div>
                                        </div>

                                   
                                        <div class="form-group">
                                                <div class="form-material">
                                                            <select class="js-select2 form-control" id="idNivel" name="idNivel" style="width: 100%;" data-allow-clear="true">
                                                                <option value="" selected>Seleccione Nivel</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                                @foreach($nivel as  $niv)
                                                                <option value="{{ $niv->id }}"{{$niv->id == $programa->idNivel ? 'selected' : '' }}> {{$niv->nombre}}</option>
                                                                @endforeach
                                                            </select>
                                                            <label for="idMunicipio">Nivel del programa <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                        <div class="form-material">
                                                                    <select class="js-select2 form-control" id="idTipo_formacion" name="idTipo_formacion" style="width: 100%;" data-allow-clear="true">
                                                                        <option value="" selected>Seleccione tipo</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                                        @foreach($tipoFormacion as  $tipo)
                                                                        <option value="{{ $tipo->id }}" {{$tipo->id==$programa->idTipo_formacion ? 'selected':''}}>{{$tipo->nombre}}</option>
                                                                

                                                                        @endforeach
                                                                    </select>
                                                                    <label for="idMunicipio">Tipo de formación <span class="text-danger">*</span></label>
                                                            </div>
                                                </div>

                                        <div class="form-group row">
                                            <div class="col-lg-8 ml-auto">
                                                <button type="submit" class="btn btn-alt-success pull-right">Actualizar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
@endsection

@section('js_after')
        <!-- Page JS Plugins -->
        <script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="/js/plugins/jquery-validation/additional-methods.js"></script>

        <!-- Page JS Code -->
        <script src="/js/pages/be_forms_wizard.min.js"></script>

@endsection
