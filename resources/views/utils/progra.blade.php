
{{-- <div class="modal fade" id="modal-progra" tabindex="-1" role="dialog" aria-labelledby="modal-terms" aria-hidden="true"> --}}
        <div class="modal fade" id="modal-progra" tabindex="-1" role="dialog" aria-labelledby="modal-terms" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-slidedown" role="document">
                    <div class="modal-content">
                        <div class="block block-themed block-transparent mb-0">
                            <div class="block-header bg-gd-lake">
                                <h3 class="block-title">Registrar Nueva Programación</h3>
                                    <div class="block-options">
                                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                            <i class="si si-close"></i>
                                        </button>
                                    </div>
                                </div>

                            <div class="row justify-content-center py-20">
                                    <div class="col-xl-8">
                                        <span id="form_resultP"></span>
                                    <form id="form-programa" class="js-validation-material" action="#" method="POST">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                            <div class="form-material">
                                                <select class="js-select2 form-control" id="idinstructor" name="idinstructor" style="width: 100%;" data-allow-clear="true">
                                                    <option value="" selected>Seleccione Instructor</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                    @foreach($instructor as $intruc)
                                                    <option value="{{ $intruc->id }}" {{ (old("idinstructor") == $intruc->id ? "selected":"")}}>{{$intruc->nombre ." ". $intruc->apellido}}</option>
                                                    @endforeach
                                                </select>
                                                <label for="idinstructor">Instructor <span class="text-danger">*</span></label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                                <div class="form-material">
                                                    <select class="js-select2 form-control" id="idFormacion" name="idFormacion" style="width: 100%;" data-allow-clear="true">
                                                        <option value="" selected>Seleccione programa de Formación</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                        @foreach($programaFormacion as $forma)
                                                        <option value="{{ $forma->id }}" {{ (old("idFormacion") == $forma->id ? "selected":"")}}>{{$forma->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="idFormacion">Programa de Formación <span class="text-danger">*</span></label>
                                                </div>
                                            </div>

                              <div class="form-group">
                                    <div class="form-material input-group">
                                            <label for="lugar">Fecha <span class="text-danger">*</span></label>
                                            <div class="input-daterange input-group" data-date-format="yyyy-mm-dd" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                                <input type="text" class="form-control " id="fechaInicio" name="fechaInicio" placeholder="Inicio" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                                <div class="input-group-prepend input-group-append">
                                                    <span class="input-group-text font-w600">A</span>
                                                </div>
                                                <input type="text" class="form-control" id="fechaFinal" name="fechaFinal" placeholder="Fin" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                    </div>
                                </div>
                      
                                <div class="form-group">
                                        <div class="input-group">
                                                <label for="lugar">Hora <span class="text-danger">*</span></label>
                                            <div class="input-group date" data-target-input="nearest">
                                            <input type="text" class="form-control datetimepicker-input" id="horaInicio" name="horaInicio" placeholder="Inicio" data-target="#horaInicio" >
                                                <div class="input-group-prepend input-group-append" data-target="#horaInicio" data-toggle="datetimepicker">
                                                    <span class="input-group-text font-w600">A</span>
                                                </div>
                                                <input type="text" class="form-control" id="horaFinal" name="horaFinal" placeholder="Fin" data-target="#horaFinal" data-toggle="datetimepicker">
                                            </div>
                                        </div>
                                        </div>
                              
                            <div class="form-group">
                                <div class="form-material">
                                    <select class="js-select2 form-control" id="lugar" name="lugar" style="width: 100%;" data-allow-clear="true">
                                        <option value="" selected>Seleccione Lugar</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                         <option value="1">Institución Educativa</option>
                                         <option value="2">Empresa</option>
                                         <option value="3">Sena</option>
                                    </select>
                                    <label for="lugar">Lugar <span class="text-danger">*</span></label>
                                </div>
                            </div>

                            <div class="form-group" id="form-selection">
                                    <div class="form-material">
                                        <select class="js-select2 form-control" id="selectLugar" name="selectLugar" style="width: 100%;" data-allow-clear="true">
                                            <option value="" selected>Seleccione Lugar</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                        </select>
                                        <label for="lugar"><span id="labelLugar">Seleccione una opción</span></label>
                                    </div>
                                </div>

                                <div class="form-group row">
                                        <label class="col-12">Días </label>
                                        <div class="col-12">
                                            @foreach ($dia as $dias)
                                            <div class="custom-control custom-checkbox custom-control-inline mb-5">
                                        <input class="custom-control-input" type="checkbox" name="dias[]" id="{{$dias->id}}" value="{{$dias->id}}">
                                                <label class="custom-control-label" for="{{$dias->id}}">{{$dias->nombre}}</label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>

                        </div>
                        </div>
                        </div>
                        <div class="modal-footer">
                                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-alt-success">
                                <i class="fa fa-check"></i> Guardar
                            </button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
    </div>
