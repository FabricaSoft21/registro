@extends('layouts.backend')
@section('content')
 <!-- Page Content -->
                <div class="content">
                        <!-- Calendar and Events functionality is initialized in js/pages/be_comp_calendar.min.js which was auto compiled from _es6/pages/be_comp_calendar.js -->
                        <!-- For more info and examples you can check out https://fullcalendar.io/ -->
                        <div class="block">
                            <div class="block-content">
                                <div class="row items-push">
                                    <div class="col-xl-12">
                                            <div class="form-group row">
                                                        <h2 style="margin-left:20px !important">Filtros</h2>
                                                        <div class='col-lg-2 ml-auto' style="display: inline-block">
                                                            <h2>Opciones</h2>
                                                        </div>
                                                 </div>
                                     <div class="form-group row">
                                        <div class="form-group col-lg-2">
                                           <select class="js-select2 form-control mb-2" id='selector' name="" style="width: 100%;" data-allow-clear="true">
                                                <option value="" selected>Seleccione Ficha</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                @foreach($formacion as  $forma)
                                                <option value="{{$forma->ficha->id}}">{{$forma->ficha->codigo . ' - ' . $forma->ficha->nombreFicha}}</option>
                                                @endforeach
                                           </select>
                                        </div>
                                        <div class="form-group col-lg-2">
                                            <select class="js-select2 form-control"  id='selectorI' name="" style="width: 100%;" data-allow-clear="true">
                                                <option value="" selected>Seleccione Instructor</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                @foreach($instructor as  $ins)
                                                <option value="{{$ins->id}}">{{$ins->nombre . " " . $ins->apellido}}</option>
                                                @endforeach
                                           </select>
                                        </div>

                                            <div class='col-lg-2 ml-auto' style="display: inline-block">
                                                <button type="button" class="btn btn-success mb-5" data-toggle="modal" data-target="#modal-progra">
                                                    <i class="fa fa-plus mr-5"></i>Nuevo
                                                </button>

                                                <a type="button" class="btn btn-info mb-5" href="/events">
                                                        <i class="fa fa-eye mr-5"></i>Ver
                                                </a>

                                            </div>
                                     </div>

                                        <!-- Calendar Container -->
                                    <div class="calendar"></div>
                                    </div>
                                </div>
                                    <div class="col-xl-3 d-none d-xl-block">
                                        <!-- Add Event Form -->
                                        {{-- <form class="js-form-add-event mb-30" action="be_comp_calendar.html" method="post">
                                            <div class="input-group">
                                                <input type="text" class="js-add-event form-control" placeholder="Add Event..">
                                                <div class="input-group-append">
                                                    <button type="submit" class="btn btn-secondary">
                                                        <i class="fa fa-plus-circle"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form> --}}
                                        <!-- END Add Event Form -->

                                        <!-- Event List -->
                                        {{-- <ul class="js-events list list-events">
                                            <li class="bg-info-light">Project Mars</li>
                                            <li class="bg-success-light">Cinema</li>
                                            <li class="bg-danger-light">Project X</li>
                                            <li class="bg-warning-light">Skype Meeting</li>
                                            <li class="bg-info-light">Codename PX</li>
                                            <li class="bg-success-light">Weekend Adventure</li>
                                            <li class="bg-warning-light">Meeting</li>
                                            <li class="bg-success-light">Walk the dog</li>
                                            <li class="bg-info-light">AI schedule</li>
                                        </ul> --}}
                                        {{-- <div class="text-center">
                                            <em class="font-size-xs text-muted"><i class="fa fa-arrows"></i> Drag and drop events on the calendar</em>
                                        </div> --}}
                                        <!-- END Event List -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END Calendar -->
                    </div>
                    <!-- END Page Content -->

@endsection

@section('js_after')
<script src="/js/pages/be_forms_validation.min.js"></script>
@include('utils.progra')

<script src="/js/plugins/moment/moment.min.js"></script>
<script src="/js/plugins/fullcalendar/fullcalendar.min.js"></script>
<script src="/js/plugins/fullcalendar/locale/es.js"> </script>

<!-- Page JS Code -->
<script src="/js/pages/be_comp_calendar.min.js"></script>
<script>

    $('#horaInicio').datetimepicker({
            format: 'H:mm'
    });

    $('#horaFinal').datetimepicker({
        format: 'H:mm',
        useCurrent: false
    });

    $("#horaInicio").on("dp.change", function (e) {
        $('#horaFinal').data("DateTimePicker").minDate(e.date);
    });
    $("#horaFinal").on("dp.change", function (e) {
        $('#horaInicio').data("DateTimePicker").maxDate(e.date);
    });

</script>

<script>
        $(document).ready(function(){
            // var selectedFeed = $('#selector').find(':selected').data('feed');
            // $('.calendar').fullCalendar('removeEvents');
            $('.calendar').fullCalendar({
                firstDay:1,
                editable:false,
                eventLimit: false,
                locale: 'es',
                droppable:!0,
                allDaySlot: false,
                header: {left:"title",


                right:"prev,next today month,agendaWeek,agendaDay,listWeek"},
                events:[
                @foreach($formacion as $forma)
                {
                    //2019-07-15 07:00:00.000000
                    title : '{{'Formación - '.$forma->ficha->codigo}}',
                    start : '{{$forma->fechaInicio.' '.$forma->horaInicio}}',
                    end : '{{$forma->fechaFinal.' '.$forma->horaFinal}}',
                    color: '#ebf5df',
                    description: '{{$forma->ficha->codigo}}',
                    trip: '{{$forma->jornada}}',
                    dateWork: '{{$forma->fechaInicioProd}}',
                    state: '{{$forma->estado->nombre}}'

                },
                @endforeach

                ],
                eventClick: function(calEvent, jsEvent, view) {
                $('#idProgramacion').val(calEvent.id);
                $('#modalTitle').html(calEvent.title);
                $('#modalDescription').val(calEvent.description);
                $('#modalTrip').val(calEvent.trip);
                $('#modalStart').val(moment(calEvent.start).format('H:m A'));
                $('#modalEnd').val(moment(calEvent.end).format('H:m A'));
                $('#modalStartDate').val(moment(calEvent.start).format('DD-MM-YYYY'));
                $('#modalEndDate').val(moment(calEvent.end).format('DD-MM-YYYY'));
                $('#modalDateWork').val(moment(calEvent.dateWork).format('DD-MM-YYYY'));
                $('#modalState').val(calEvent.state);
                $('#editModal').modal();

                var id = $('#idProgramacion').val();
                $('#btnEditar').attr('href','/events/edit/'+id)
                $.ajax({
                    url:"eventsDay/"+id,
                    type:'get',
                    success:function(data){
                        if(data.length != null){
                        var template = ``;

                        for(i of data){
                            template+=`<option selected>${i}</option>`;

                        }
                        $("#idDias").html(template);

                        }else{
                            $("#idDias").html('<option selected>No hay días seleccionados</option>');
                        }
                    },
                    error:function(){
                        console.log("error");
                    }
                });
                
            },
            

            })

            document.getElementById("selectDias").style.display = "none";
            document.getElementById("btnEditar").style.display = "none";

        });

        // Al detectar seleccion o cambio en la ficha
        $("#selector").change(function(event){

        $('#selectorI').val('');
        $('#labelDescription').text('Ficha');
        $('#labelTrip').text('Jornada');
        $('#labelState').text('Estado');
        document.getElementById("selectDias").style.display = "none";
        document.getElementById("btnEditar").style.display = "none";

        // Obtenemos todos datos de ficha
        event.preventDefault();
        $.get("events/"+event.target.value+"",function(response,state){
            $.get("events/"+event.target.value+"",function(response,state){

            });
        if(response.length < 0)
        {
            // Si seleccionas la primera opcion por defecto "seleccionar" no hace nada
        }
        else
        {
            var newEventsList = new Array();
            // Nuevo evento
            $('.calendar').fullCalendar('removeEvents');
            for(i=0; i < response.length; i++){
            newEventsList [i] = new Object();
            newEventsList [i]["id"] = response[i].idProgramacion;
            newEventsList [i]["title"] = response[i].codigo + " - " + response[i].nombreFicha;
            newEventsList [i]["start"] = response[i].fechaInicio + " " + response[i].horaInicio;
            newEventsList [i]["end"] = response[i].fechaFinal + " " + response[i].horaFinal;
            newEventsList [i]["color"] = '#ebf5df';
            newEventsList [i]["description"] = response[i].codigo;
            newEventsList [i]["trip"] = response[i].jornada;
            newEventsList [i]["dateWork"] = response[i].fechaInicioProd;
            newEventsList [i]["state"] = response[i].estadoFormacion;

            }

            $('.calendar').fullCalendar('addEventSource', newEventsList,true);
            $('.calendar').fullCalendar('rerenderEvents');       

            }

            });
        });


        // Al detectar seleccion o cambio en la ficha
        $("#selectorI").change(function(event){
        $('#selector').val('');
        $('#labelDescription').text('Programa de formación');
        $('#labelTrip').text('Profesión');
        $('#labelState').text('Lugar');
        document.getElementById("selectDias").style.display = "";
        document.getElementById("btnEditar").style.display = "";


        // Obtenemos todos datos de ficha
        event.preventDefault();
        $.get("eventsI/"+event.target.value+"",function(response,state){
        if(response.length < 0)
        {
            // Si seleccionas la primera opcion por defecto "seleccionar" no hace nada
        }
        else
        {
            var newEventsList = new Array();
            // Nuevo evento
            $('.calendar').fullCalendar('removeEvents');

            for(i=0; i < response.length; i++){

                for (j = 0; j < response[i]["fechas"].length; j++) {

                        newEventsList[j] = new Object();
                        newEventsList[j]["id"] = response[i].idProgramacion;
                        newEventsList[j]["title"] = response[i].nombre + " " + response[i].apellido;
                        newEventsList[j]["start"] = response[i]["fechas"][j] + " " + response[i].horaInicio;
                        newEventsList[j]["end"] =  response[i]["fechas"][j] + " " + response[i].horaFinal;
                        newEventsList[j]["color"] = '#9ACBF3';
                        newEventsList[j]["description"] = response[i].nombrePrograma;
                        newEventsList[j]["trip"] = response[i].profesion;
                        newEventsList[j]["dateWork"] = response[i].fechaInicioProd;
                        if (response[i].lugar == 1) {
                            newEventsList[j]["state"] = "Colegio - " + response[i].colegio.nombreColegio;
                        } else if (response[i].lugar == 2) {
                            newEventsList[j]["state"] = "Empresa - " + response[i].empresa.nombre;
                        } else {
                            newEventsList[j]["state"] = "SENA";
                        }

                    }
                }
            $('.calendar').fullCalendar('addEventSource', newEventsList,true);
            $('.calendar').fullCalendar('rerenderEvents');
            }

            });
        });

    </script>
    <script>

    $(function(){
            $('#lugar').on('change',onSelectedLugar);
        });

        function onSelectedLugar(){
        var lug = $(this).val();

            //AJAX

            if(! lug ){
                $('#selectLugar').html(`<option value="" selected>Seleccione Lugar</option>`);
                return;
            }

            $.get(`/utils/programa/${lug}`,function(data){

                if(lug == 1){
                        $('#selectLugar').attr('name','idColegio')
                        // $('#selectLugar').attr('id','idColegio')
                        $('#labelLugar').text('Institución Educativa');

                    }else if(lug == 2){
                        $('#selectLugar').attr('name','idEmpresa')
                        // $('#selectLugar').attr('id','idEmpresa')
                        $('#labelLugar').text('Empresa');

                    }else{
                        $('#labelLugar').text('SENA');

                    }

                if(lug == 1 || lug == 2){
                    var html_select = `<option value="" selected>Seleccione Lugar</option>`;

                }else if( lug == 3) {
                    var html_select = `<option value="" selected>No aplica </option>`;
                }

                for(i of data){
                    if(lug == 1){
                        html_select+= `<option value="${i.id}">${i.nombreColegio}</option>`;
                    }else if(lug == 2){
                        html_select+= `<option value="${i.id}">${i.nombre}</option>`;
                    }

                }
                $('#selectLugar').html(html_select);
            });

        }

            $('#form-programa').on('submit',function(event){
                      event.preventDefault();
                      $.ajax({
                          url:"{{ route('saveP')}}",
                          method:"POST",
                          data : new FormData(this),
                          contentType:false,
                          cache:false,
                          processData:false,
                          dataType:"json",
                          success:function(data)
                          {
                              var html = "";
                              if(data.errors){
                                  html = '<div class="alert alert-danger">';
                                      for(var count = 0; count < data.errors.length; count++)
                                      {
                                          html += '<p>' + data.errors[count] +
                                              '</p>';
                                      }
                                      html += '</div>';
                              }
                              if(data.success)
                              {

                                  html = '<div class="alert alert-success">'
                                      + data.success + '</div>';
                                      $('#form-programa')[0].reset();
                              }
                              $('#form_resultP').html(html);
                          },
                       error:function(e){
                          console.log(e);
                       }
                      })
                  });
          </script>

    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="modal-fromleft" aria-hidden="true">
        <div class="modal-dialog modal-dialog-fromleft modal-lg" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                    <h3 class="block-title" id="modalTitle"></h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block block-themed block-transparent mb-0">
                            <div class="block block-themed">
                                    <div class="block-content block-content-full text-center bg-gd-primary">
                                        {{-- <img class="img-avatar img-avatar96 img-avatar-thumb" src="/media/avatars/avatar15.jpg" alt=""> --}}
                                        <h3 class="text-white mt-3">INFORMACIÓN
                                 <i class=" si si-info"></i>
                             </h3>
                                    </div>
                                    <div class="col-md-12">
                                            <!-- Simple Wizard 2 -->
                                            <div class="js-wizard-simple block">
                                                <!-- Step Tabs -->
                                                <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                                    <li class="nav-item">
                                                    <a class="nav-link active" href="#wizard-simple2-step1" data-toggle="tab">1. General </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#wizard-simple2-step2" data-toggle="tab">2. Detalles</a>
                                                    </li>
                                                </ul>
                                                <!-- END Step Tabs -->

                                                <!-- Form -->
                                            <form action=""  method="get">
                                                    <!-- Steps Content -->
                                                    <div class="block-content block-content-full tab-content" style="min-height: 267px;">
                                                        <!-- Step 1 -->
                                                        <input class="form-control" type="hidden" id="idProgramacion" name="idProgramacion" value="" readonly>

                                                        <div class="tab-pane active" id="wizard-simple2-step1" role="tabpanel">
                                                            <div class="form-group">
                                                                <div class="form-material">
                                                                <input class="form-control" type="text" id="modalDescription" name="modalDescription" value="" readonly>
                                                                    <label for="modalDescription" id="labelDescription">Ficha</label>
                                                                </div>
                                                            </div>

                                                               <div class="form-group">
                                                                    <div class="form-material">
                                                                    <input class="form-control" type="text" id="modalTrip" name="modalTrip" value="" readonly>
                                                                        <label for="modalTrip" id="labelTrip">Jornada</label>
                                                                    </div>
                                                                </div>

                                                            <div class="form-group">
                                                                <div class="form-material">
                                                                    <input class="form-control" type="text" id="modalStart" name="modalStart" value="" readonly>
                                                                    <label for="modalStart">Hora de inicio</label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="form-material">
                                                                    <input class="form-control" type="email" id="modalEnd" name="modalEnd" value="" readonly>
                                                                    <label for="modalEnd">Hora  de finalización</label>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <!-- END Step 1 -->

                                                        <!-- Step 2 -->
                                                        <div class="tab-pane" id="wizard-simple2-step2" role="tabpanel">
                                                                <div class="form-group">
                                                                    <div class="form-material">
                                                                        <input class="form-control" type="email" id="modalStartDate" name="modalStartDate" value="" readonly>
                                                                        <label for="modalStartDate">Fecha de inicio</label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="form-material">
                                                                        <input class="form-control" type="email" id="modalEndDate" name="modalEndDate" value="" readonly>
                                                                        <label for="modalEndDate">Fecha de finalización</label>
                                                                    </div>
                                                                </div>
                                                            <div class="form-group" style="{{auth()->user()->selected_category_id != 1 ? 'display:none' : 'none'}}">
                                                                        <div class="form-material">
                                                                            <input class="form-control" type="email" id="modalDateWork" name="modalDateWork" value="" readonly>
                                                                            <label for="modalDateWork">Inicio etapa práctica</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                            <div class="form-material">
                                                                                <input class="form-control" type="email" id="modalState" name="modalState" value="" readonly>
                                                                                <label for="modalState" id="labelState">Estado</label>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group" id="selectDias">
                                                                                <div class="form-material">
                                                                                <select disabled class="js-select2 form-control" id="idDias" name="idDiscapacidad[]"  style="width: 100%;" data-allow-clear="true" data-placeholder="Seleccione discapacidad(es)" multiple>
                                                                                    </select>
                                                                                    <label for="idDiscapacidad">Dia(s)</label>
                                                                            </div>
                                                                        </div>
                                                                 </div>
                                                        <!-- END Step 2 -->
                                                    <!-- END Steps Content -->
                                                </form>
                                                <!-- END Form -->
                                            </div>
                </div>
                <div class="modal-footer">
                    
                        <a href="#" class="btn btn-alt-info" id="btnEditar"><i class="fa fa-edit"></i>Editar</a>
                    <button type="button" class="btn btn-alt-success" data-dismiss="modal">
                        <i class="fa fa-check"></i> Perfecto
                    </button>
                </div>
            </div>
        </div>
    </div>


@endsection
