@extends('layouts.backend')

@section('content')
                <div class="content">
                    <h2 class="content-heading">Programación</h2>
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Modificar programación</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option">
                                    <i class="fa fa-gears"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="row justify-content-center py-20">
                                <div class="col-xl-8">
                                    <form class="js-wizard-validation-material-form" action="" method="POST">
                                            {{ csrf_field() }}
                                            <div class="form-group">
                                                    <div class="form-material">
                                                        <select class="js-select2 form-control" id="idinstructor" name="idinstructor" style="width: 100%;" data-allow-clear="true">
                                                            <option value="" selected>Seleccione Instructor</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                            @foreach($instructor as $intruc)
                                                            <option value="{{ $intruc->id }}" {{$programacion->idinstructor == $intruc->id ? 'selected' : ""}}>{{$intruc->nombre }}</option>
                                                            @endforeach
                                                        </select>
                                                        <label for="idinstructor">Instructor <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                        <div class="form-material">
                                                            <select class="js-select2 form-control" id="idFormacion" name="idFormacion" style="width: 100%;" data-allow-clear="true">
                                                                <option value="" selected>Seleccione programa de Formación</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                                @foreach($programaFormacion as $forma)
                                                                <option value="{{ $forma->id }}" {{ $programacion->idFormacion == $forma->id ? "selected":""}}>{{$forma->nombre}}</option>
                                                                @endforeach
                                                            </select>
                                                            <label for="idFormacion">Programa de Formación <span class="text-danger">*</span></label>
                                                        </div>
                                                    </div>

                                      <div class="form-group">
                                            <div class="form-material input-group">
                                                    <label for="lugar">Fecha <span class="text-danger">*</span></label>
                                                    <div class="input-daterange input-group" data-date-format="yyyy-mm-dd" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                                    <input type="text" class="form-control " id="fechaInicio" name="fechaInicio" placeholder="Inicio" data-week-start="1" data-autoclose="true" data-today-highlight="true" value="{{$programacion->fechaInicio}}">
                                                        <div class="input-group-prepend input-group-append">
                                                            <span class="input-group-text font-w600">A</span>
                                                        </div>
                                                        <input type="text" class="form-control" id="fechaFinal" name="fechaFinal" placeholder="Fin" data-week-start="1" data-autoclose="true" data-today-highlight="true" value="{{$programacion->fechaFinal}}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                                <div class="input-group">
                                                        <label for="lugar">Hora <span class="text-danger">*</span></label>
                                                    <div class="input-group date" data-target-input="nearest">
                                                        <input type="text" class="form-control datetimepicker-input" id="horaInicio" name="horaInicio" placeholder="Inicio" data-target="#horaInicio" value="{{$programacion->horaInicio}}">
                                                        <div class="input-group-prepend input-group-append" data-target="#horaInicio" data-toggle="datetimepicker">
                                                            <span class="input-group-text font-w600">A</span>
                                                        </div>
                                                        <input type="text" class="form-control" id="horaFinal" name="horaFinal" placeholder="Fin" data-target="#horaFinal" data-toggle="datetimepicker" value="{{$programacion->horaFinal}}">
                                                    </div>
                                                </div>
                                                </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <select class="js-select2 form-control" id="lugar" name="lugar" style="width: 100%;" data-allow-clear="true">
                                                <option value="" selected>Seleccione Lugar</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                 <option value="1" {{$programacion->lugar == 1 ? 'selected' : ''}}>Institución Educativa</option>
                                                 <option value="2" {{$programacion->lugar == 2 ? 'selected' : ''}}>Empresa</option>
                                                 <option value="3" {{$programacion->lugar == 3 ? 'selected' : ''}}>Sena</option>
                                            </select>
                                            <label for="lugar">Lugar <span class="text-danger">*</span></label>
                                        </div>
                                    </div>

                                    <div class="form-group" id="form-selection">
                                            <div class="form-material">
                                                <select class="js-select2 form-control" id="selectLugar" name="" style="width: 100%;" data-allow-clear="true">
                                                    <option value="" selected>Seleccione Lugar</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                    @if($programacion->lugar == 1)
                                                        @php($lug = $colegio)
                                                        <?php $type = $programacion->idColegio ?>
                                                    @endif
                                                    @if($programacion->lugar == 2)
                                                    @php($lug = $empresa)
                                                    <?php $type = $programacion->idEmpresa ?>
                                                    @endif
                                                    @isset($lug)
                                                    @foreach($lug as $programa)
                                                    <option value="{{ $programa->id }}" {{ $type == $programa->id ? "selected":""}}>{{$programacion->lugar == 1 ? $programa->nombreColegio : $programa->nombre }}</option>
                                                    @endforeach
                                                    @endisset
                                                </select>
                                                <label for="lugar"><span id="labelLugar">Seleccione una opción</span></label>
                                            </div>
                                    </div>

                                        <div class="form-group row">
                                                <label class="col-12">Días </label>
                                                <div class="col-12">
                                                    @php($cant = 0)
                                                    @foreach ($dia as $dias)
                                                    <div class="custom-control custom-checkbox custom-control-inline mb-5">
                                                        <input class="custom-control-input" type="checkbox" name="dias[]" id="{{$dias->id}}" value="{{$dias->id}}"
                                                        @foreach ($data as $d)
                                                            {{$d == $dias->id ? 'checked' : ''}}
                                                        @endforeach
                                                        >
                                                        <label class="custom-control-label" for="{{$dias->id}}">{{$dias->nombre}}</label>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>

                                        <div class="form-group row">
                                            <div class="col-lg-6">
                                                    <a href="/events" class="btn btn-alt-primary">Volver a lista</a>
                                                </div>
                                            <div class="col-lg-6 ml-auto">
                                                <button type="submit" class="btn btn-alt-success pull-right">Actualizar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
@endsection

@section('js_after')
        <!-- Page JS Plugins -->
        <script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="/js/plugins/jquery-validation/additional-methods.js"></script>

        <!-- Page JS Code -->
        <script src="/js/pages/be_forms_wizard.min.js"></script>
        <script>

        $('#horaInicio').datetimepicker({
                format: 'H:mm'
        });

        $('#horaFinal').datetimepicker({
            format: 'H:mm',
            useCurrent: false
        });

        $("#horaInicio").on("dp.change", function (e) {
            $('#horaFinal').data("DateTimePicker").minDate(e.date);
        });
        $("#horaFinal").on("dp.change", function (e) {
            $('#horaInicio').data("DateTimePicker").maxDate(e.date);
        });

        </script>
        <script>
            $(function(){
            $('#lugar').on('change',onSelectedLugar);
        });

        function onSelectedLugar(){
        var lug = $(this).val();

            //AJAX

            if(! lug ){
                $('#selectLugar').html(`<option value="" selected>Seleccione Lugar</option>`);
                return;
            }

            $.get(`/utils/programa/${lug}`,function(data){

        if(lug == 1){
                $('#selectLugar').attr('name','idColegio')
                $('#labelLugar').text('Institución Educativa');

            }else if(lug == 2){
                $('#selectLugar').attr('name','idEmpresa')
                $('#labelLugar').text('Empresa');

            }else{

                $('#labelLugar').text('SENA');

            }

            if(lug == 1 || lug == 2){
                var html_select = `<option value="" selected>Seleccione Lugar</option>`;

            }else if( lug == 3) {
                var html_select = `<option value="" selected>No aplica </option>`;
            }

            for(i of data){
                if(lug == 1){
                    html_select+= `<option value="${i.id}">${i.nombreColegio}</option>`;
                }else if(lug == 2){
                    html_select+= `<option value="${i.id}">${i.nombre}</option>`;
                }

            }
            $('#selectLugar').html(html_select);
        });

    }
    </script>

@endsection
