@extends('layouts.backend')
@section('content')
                <!-- Page Content -->
                <div class="content">
                    <h2 class="content-heading">Programación</h2>

                    <!-- Dynamic Table Full -->
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Ver programaciones</h3>
                            <a href="/programacion" data-toggle="tooltip" title="Crear Programación" class="btn btn-success">
                                <i class="fa fa-calendar-plus-o"></i>
                            </a>
                        </div>
                        <div class="block-content block-content-full">
                        <!-- DataTables functionality is initialized with .js-dataTable-full class in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                            @if($cant == 0)
                            <div class="col-md-12">
                                <div class="block">
                                    <div class="block-content block-content-full">
                                        <div class="py-20 text-center">
                                            <div class="mb-20">
                                                <i class="fa fa-close fa-5x text-danger"></i>
                                            </div>
                                            <div class="font-size-h4 font-w600">Lo sentimos</div>
                                            <div class="text-muted">Actualmente no hay ningún registro</div>
                                    </div>
                                </div>
                            </div>
                            @else
                            <table id="programacion" class="table js-dataTable-full" width="100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Programa de formación</th>
                                        <th>Instructor</th>    
                                        <th>Fecha Inicio</th>   
                                        <th>Fecha Final</th>
                                        <th>Hora Inicio</th>   
                                        <th>Hora Final</th>
                                        <th>Lugar</th>
                                        <th>Nombre del lugar</th>
                                        <th class="noExportExc noExport">Opciones</th>

                                        {{-- <th class="d-none d-sm-table-cell" style="width: 15%;">Access</th>
                                        <th class="text-center" style="width: 15%;">Profile</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($programaFormacion as $programa)
                                    <tr>
                                        <td class="font-w600">{{$loop->iteration}}</td>
                                        <td class="font-w600">{{$programa->formacion->nombre}}</td>
                                        <td class="font-w600">{{$programa->instructor->nombre}}</td>
                                        <td class="font-w600">{{$programa->fechaInicio}}</td>
                                        <td class="font-w600">{{$programa->fechaFinal}}</td>
                                        <td class="font-w600">{{$programa->horaInicio}}</td>
                                        <td class="font-w600">{{$programa->horaFinal}}</td>
                                        @if ($programa->idColegio != null)
                                            @php($lug = $programa->colegio->nombreColegio)
                                            <td class="font-w600">Institución Educativa</td>
                                            <td class="font-w600">{{$lug}}</td> 
                                        @endif
                                        @if ($programa->idEmpresa != null)
                                            @php($lug = $programa->empresa->nombre)
                                            <td class="font-w600">Empresa</td>
                                            <td class="font-w600">{{$lug}}</td>
                                        @endif
                                        @if ($programa->idEmpresa == null && $programa->idColegio == null)
                                            <td class="font-w600">SENA</td>     
                                            <td class="font-w600">SENA</td>   
                                        @endif
                                                                            
                                        <td class="text-center">
                                            <a href="/events/edit/{{$programa->id}}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Editar programación">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </td>
                                    </tr>

                                    @endforeach
                                </tbody>
                            </table>
                            @endif
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->
                </div>
@endsection

@section('js_after')
        <script src="/js/functions/custom-file-input.js"></script>
        <!-- Page JS Plugins -->
        <script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="/js/plugins/jquery-validation/additional-methods.js"></script>

        <!-- Page JS Code -->
        <script src="/js/pages/be_forms_wizard.min.js"></script>
        <script src="/js/functions/get-colegio.js" ></script>
@endsection
