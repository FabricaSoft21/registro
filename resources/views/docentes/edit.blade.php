@extends('layouts.backend')
@section('content')
<div class="content">
                    <h2 class="content-heading">Institución educativa</h2>
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Modificar Docente</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option">
                                    <i class="fa fa-gears"></i>
                                </button>
                            </div>
                        </div>
                        <div class="js-wizard-validation-material block">
                                <!-- Step Tabs -->
                                <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                    <li class="nav-item">
                                        {{-- <a class="nav-link active" href="#wizard-validation-material-step1" data-toggle="tab">1. Personal</a> --}}
                                    </li>
                                </ul>
                                <!-- END Step Tabs -->

                                <!-- Form -->
                                <form class="js-wizard-validation-material-form" action="" method="post">
                                {{ csrf_field() }}
                                    <!-- Steps Content -->
                                    <div class="block-content block-content-full tab-content" style="min-height: 267px;">
                                        <!-- Step 1 -->
                                        <div class="tab-pane active" id="wizard-validation-material-step1" role="tabpanel">
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" type="text" id="nombre" name="nombre" value="{{$docente->nombre}}">
                                                    <label for="nombre">Nombres </label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-vcard"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="apellido" name="apellido" value="{{$docente->apellido}}">
                                                <label for="apellido">Apellidos </label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-address-card-o"></i>
                                                    </span>
                                                </div>
                                        </div>
                                    </div>
                              
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="correo" name="correo" value="{{$docente->correo}}">
                                                <label for="correoSena">Correo </label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-envelope-o"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="telefono" name="telefono" value="{{$docente->telefono}}">
                                                <label for="telefono">Número de contacto </label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-phone"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                
                                     
                                        <div class="form-group">
                                        <div class="form-material">
                                                    <select class="js-select2 form-control" id="idcolegio" name="idcolegio" style="width: 100%;" data-allow-clear="true">
                                                        <option value="" >Seleccione la Institución</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                        @foreach($colegio as  $cole)
                                                        <option value="{{ $cole->id }}" {{$cole->id == $docente->idcolegio ? 'selected' : ''}}>{{$cole->nombreColegio}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="idArea">Institución Educativa </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                        <div class="form-material">
                                                    <select class="js-select2 form-control" id="idPrograma" name="idPrograma" style="width: 100%;" data-allow-clear="true">
                                                        <option value=""> Programa formación</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                        @foreach($programa as  $pro)
                                                        <option value="{{ $pro->id }}" {{$pro->id == $docente->idPrograma ? 'selected' : ''}}>{{$pro->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="idPrograma">Programa de formación </label>
                                            </div>
                                        </div>
                                     
                                        <div class="form-group row">
                                            <div class="col-lg-8 ml-auto">
                                                <button type="submit" class="btn btn-alt-info pull-right">Actualizar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
@endsection

@section('js_after')
        <!-- Page JS Plugins -->
        <script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="/js/plugins/jquery-validation/additional-methods.js"></script>

        <!-- Page JS Code -->
        <script src="/js/pages/be_forms_wizard.min.js"></script>

@endsection