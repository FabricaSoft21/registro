@extends('layouts.backend')

@section('content')
                <div class="content">
                    <h2 class="content-heading">Institución Educativa</h2>
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Registrar Docente par</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option">
                                    <i class="si si-user-follow"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="row justify-content-center py-20">
                                <div class="col-xl-8">
                                    <form class="js-wizard-validation-material-form" action="" method="POST">
                                            {{ csrf_field() }}
                                            {{-- <input class="form-control" value="{{$coordi['id'] + 1}}" type="hidden" id="id" name="id">
                                            <input class="form-control" value="{{date('Y-m-d h:i:s')}}" type="hidden" id="created_at" name="created_at"> --}}
                                            <div class="form-group">
                                                <div class="form-material input-group floating">
                                                    <input class="form-control" value="{{old('nombre')}}" type="text" id="nombre" name="nombre">
                                                    <label for="nombre">Nombres <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="fa fa-vcard"></i>
                                                        </span>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" value="{{old('apellido')}}" id="apellido" name="apellido">
                                                <label for="apellido">Apellidos <span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-address-card-o"></i>
                                                    </span>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-material input-group floating">
                                            <input class="form-control" type="text" id="correo" name="correo" value="{{old('correo')}}">
                                            <label for="correo">Correo electrónico <span class="text-danger">*</span></label>
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="fa fa-envelope-o"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>


                                        <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <input class="form-control" type="text" id="telefono" name="telefono" value="{{old('telefono')}}">
                                                <label for="telefono">Número de contacto<span class="text-danger">*</span></label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-phone"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="form-material">
                                                <select class="js-select2 form-control" id="idcolegio" name="idcolegio" style="width: 100%;" data-allow-clear="true">
                                                    <option value="" selected> Seleccione Institución </option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                    @foreach($colegio as $cole)
                                                    <option value="{{ $cole->id }}">{{$cole->nombreColegio}}</option>
                                                    @endforeach
                                                </select>
                                                <label for="idTipoDoc">Institución Educativa al que pertenece <span class="text-danger">*</span></label>
                                        </div>
                                    </div>
                              
                                    
                                    <div class="form-group">
                                        <div class="form-material">
                                            <select class="js-select2 form-control" id="idprograma" name="idprograma" style="width: 100%;" data-allow-clear="true">
                                                <option value="" selected> Seleccione programa </option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                @foreach($programa as $pro)
                                                <option value="{{ $pro->id }}">{{$pro->nombre}}</option>
                                                @endforeach
                                            </select>
                                            <label for="idTipoDoc">Programa Formación<span class="text-danger">*</span></label>
                                    </div>
                                </div>
                          


                                        <div class="form-group row">
                                            <div class="col-lg-8 ml-auto">
                                                <button type="submit" class="btn btn-alt-success pull-right">Registrar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
@endsection

@section('js_after')
        <!-- Page JS Plugins -->
        <script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="/js/plugins/jquery-validation/additional-methods.js"></script>

        <!-- Page JS Code -->
        <script src="/js/pages/be_forms_wizard.min.js"></script>

@endsection
