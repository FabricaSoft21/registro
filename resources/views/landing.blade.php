@extends('layouts.simple')

@section('content')
    <!-- Hero -->
    <div class="bg-image" style="background-image: url('{{ asset('/media/photos/AdobeStock_101631848.jpeg') }}');">
        <div class="hero bg-black-op overflow-hidden">
            <div class="hero-inner">
                <div class="content content-full text-center">
                    <div class="pt-100 pb-150">
                        <h1 class="font-w700 display-4 mt-20 invisible" data-toggle="appear" data-timeout="50">
                            S<span class="font-w700 text-primary">R</span><span class="font-w700 display-4 mt-20">A</span>
                        </h1>
                        <h2 class="h3 font-w400 text-white mb-50 invisible" data-toggle="appear" data-class="animated fadeInDown" data-timeout="300">
                            Bienvenid@ a SRA (Software de registro académico).
                            <br/>
                            Espero sea de gran utilidad !
                        </h2>
                        <div class="invisible" data-toggle="appear" data-class="animated fadeInDown" data-timeout="300">
                            <a class="btn btn-hero btn-noborder btn-success min-width-175 mb-10 mx-5" href="/dashboard">
                                <i class="fa fa-fw fa-arrow-right mr-1"></i> Ingresar al sistema !
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Hero -->
@endsection
