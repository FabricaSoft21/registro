@extends('layouts.backend')
@section('content')
                <!-- Page Content -->
                <div class="content">
                    <h2 class="content-heading">Formación</h2>

                    <!-- Dynamic Table Full -->
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Ver Formaciones</h3>
                            <a href="/formacion/create" data-toggle="tooltip" title="Registrar formación" class="btn btn-success">
                                <i class="fa fa-plus"></i>
                            </a>
                            <button type="button" class="btn btn-alt-info ml-2" id="btnAsociar" disabled onclick="getStudents(1)">Ver información</button>
                        </div>
                        <div class="block-content block-content-full">
                            <!-- DataTables functionality is initialized with .js-dataTable-full class in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                            @if($cant == 0)
                            <div class="col-md-12">
                                <div class="block">
                                    <div class="block-content block-content-full">
                                        <div class="py-20 text-center">
                                            <div class="mb-20">
                                                <i class="fa fa-close fa-5x text-danger"></i>
                                            </div>
                                            <div class="font-size-h4 font-w600">Lo sentimos</div>
                                            <div class="text-muted">Actualmente no hay ningún registro</div>
                                            <form class="js-wizard-validation-material-form" method="post" action="" enctype="multipart/form-data">
                                             @csrf

                                        </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @else

                            <table id="aprendices" class="table js-table-checkable js-dataTable-full" width="100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th></th>
                                        <th>Ficha</th>
                                        <th>Fecha inicio</th>
                                        <th>Fecha final</th>
                                        <th>Jornada</th>
                                        <th>Hora inicio</th>
                                        <th>Hora final</th>
                                        <th>Inicio etapa productiva</th>
                                        <th>Estado</th>

                                        <th class="noExportExc noExport">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (auth()->user()->selected_category_id == 1)
                                        @php($clasi=$titulada)
                                    @endif

                                    @if (auth()->user()->selected_category_id == 2)
                                        @php($clasi=$complementaria)
                                    @endif

                                    @if (auth()->user()->selected_category_id == 3)
                                        @php($clasi=$poblacion)
                                    @endif

                                    @if (auth()->user()->selected_category_id == 4)
                                        @php($clasi=$competencia)
                                    @endif

                                    @if (auth()->user()->selected_category_id == 5)
                                        @php($clasi=$media)
                                    @endif

                                @foreach($clasi as $form)
                                    <tr>
                                        <td class="font-w600">{{$loop->iteration}}</td>
                                        <td>
                                            <label class="css-control css-control-sm css-control-warning css-checkbox">
                                            <input type="radio" class="css-control-input" id="idAsociar" name="idAsociar" value="{{$form->id}}" data-ficha="{{$form->ficha->nombreFicha. ' - ' .$form->ficha->codigo}}">
                                                <span class="css-control-indicator"></span>
                                            </label>
                                        </td>
                                        <td class="font-w600">{{$form->ficha->codigo}}</td>
                                        <td class="font-w600">{{$form->fechaInicio}}</td>
                                        <td class="font-w600">{{$form->fechaFinal}}</td>
                                        <td class="font-w600">{{$form->jornada == "AM" ? 'Diurna' : 'Nocturna'}}</td>
                                        <td class="font-w600">{{$form->horaInicio}}</td>
                                        <td class="font-w600">{{$form->horaFinal}}</td>
                                        <td class="font-w600">{{$form->fechaInicioProd == null ? 'No aplica' : $form->fechaInicioProd  }}</td>
                                        <td class="font-w600">{{$form->estado->nombre}}</td>

                                        <td class="text-center">
                                            <a href="/formacion/edit/{{$form->id}}" class="btn btn-sm btn-primary" title="Editar formación">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <span></span>
                                        <button  class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal-fromleft-{{$form->id}}"title="Ver formación">
                                                <i class="fa fa-eye"></i>
                                        </button>
                                        </td>
                                    </tr>
                                    @include('formacion.show')

                                    @endforeach
                                </tbody>
                            </table>
                            @endif
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->
                </div>

            <div class="modal fade" id="modal-fadein" tabindex="-1" role="dialog" aria-labelledby="modal-fadein" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="block block-themed block-transparent mb-0">
                                <div class="block-header bg-primary-dark">
                                    <h3 class="block-title">Información de la ficha</h3>
                                    <div class="block-options">
                                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                            <i class="si si-close"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="block block-themed block-transparent mb-0">
                                        <div class="block block-themed">
                                                <div class="block-content block-content-full text-center bg-gd-primary">
                                                    <h3 class="text-white mt-3"> <span id ="info-ficha"></span>
                                                        <i class="si si-graduation ml-5"></i>
                                                    </h3>
                                                </div>

                                <div class="block-content">

                                        <div class="js-wizard-simple block">
                                                <!-- Step Tabs -->
                                                <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                                    <li class="nav-item">
                                                    <a class="nav-link active" href="#wizard-simple2-step1" data-toggle="tab">Aprendices </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#wizard-simple2-step2" data-toggle="tab">Instructores</a>
                                                    </li>
                                                    <li class="nav-item" style="{{auth()->user()->selected_category_id != 5 ? 'display:none' : ''}}">
                                                            <a class="nav-link" href="#wizard-simple2-step3" data-toggle="tab">Docentes</a>
                                                        </li>

                                                    <li class="nav-item" style="{{auth()->user()->selected_category_id != 3 ? 'display:none' : ''}}">
                                                        <a class="nav-link" href="#wizard-simple2-step4" data-toggle="tab">Empresas</a>
                                                    </li>
                                                </ul>
                                                <!-- END Step Tabs -->

                                                <!-- Form -->
                                                    <!-- Steps Content -->
                                                    <div class="block-content block-content-full tab-content" style="min-height: 267px;">
                                                        <!-- Step 1 -->
                                                        <input class="form-control" type="hidden" id="wizard-simple2-firstname" name="idUser" value="" readonly>

                                                        <div class="tab-pane active" id="wizard-simple2-step1" role="tabpanel">
                                                        <form action="{{route('removeA',4334)}}" method="post" id="form_aprendiz">
                                                                <input type="hidden" name="idFormacion" value="" class="idFormacion">
                                                                @csrf
                                                                <button id="btnCompe" type="submit" class="btn btn-alt-danger pull-right" disabled>
                                                                        Remover  <i class="fa fa-minus-square"></i>
                                                                   </button>
                                                                <br/>
                                                                <div id="show-aprendiz"></div>
                                                            </form>
                                                        </div>
                                                        <!-- END Step 1 -->

                                                        <!-- Step 2 -->
                                                        <div class="tab-pane" id="wizard-simple2-step2" role="tabpanel">


                                                        <form action="{{route('removeI',2)}}" method="post" id="form-instructor">
                                                            <input type="hidden" name="idFormacion" value="" class="idFormacion">
                                                            @csrf
                                                            <button id="btnIns" type="submit" class="btn btn-alt-danger pull-right" disabled>
                                                                    Remover  <i class="fa fa-minus-square"></i>
                                                            </button>
                                                            <br/>
                                                            <div id="show-instructor"></div>
                                                        </form>
                                                        </div>
                                                        <!-- END Step 2 -->

                                                        <!-- Step 3 -->
                                                        <div class="tab-pane" id="wizard-simple2-step3" role="tabpanel">
                                                            <form action="{{route('removeD',8)}}" method="post" id="form-docente">
                                                                <input type="hidden" name="idFormacion" value="" class="idFormacion">
                                                                    @csrf
                                                                    <button id="btnDoc" type="submit" class="btn btn-alt-danger pull-right" disabled>
                                                                            Remover  <i class="fa fa-minus-square"></i>
                                                                       </button>
                                                                    <br/>
                                                            <div id="show-docente"></div>

                                                        </div>
                                                        <!-- END Step 3 -->
                                                         <!-- Step 4 -->
                                                         <div class="tab-pane" id="wizard-simple2-step4" role="tabpanel">
                                                                <form action="{{route('removeE',9)}}" method="post" id="form-empresa">
                                                                    <input type="hidden" name="idFormacion" value="" class="idFormacion">
                                                                        @csrf
                                                                        <button id="btnEmp" type="submit" class="btn btn-alt-danger pull-right" disabled>
                                                                                Remover  <i class="fa fa-minus-square"></i>
                                                                           </button>
                                                                        <br/>
                                                            <div id="show-empresa">

                                                             </div>

                                                        </div>
                                                    </div>
                                                        <!-- END Step 4 -->

                                                    <!-- END Steps Content -->
                                                <!-- END Form -->
                                            </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cerrar</button>
                                {{-- <button type="submit" class="btn btn-alt-success">
                                    <i class="fa fa-check"></i> Guardar
                                </button> --}}
                            </div>
                        </div>
                    </div>
                </div>

@endsection

@section('js_after')

<script src="/js/functions/custom-file-input.js"></script>
<!-- Page JS Plugins -->
<script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
<script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="/js/plugins/jquery-validation/additional-methods.js"></script>

<!-- Page JS Code -->
<script src="/js/pages/be_forms_wizard.min.js"></script>

<script>
        $('.css-control-input').click(function() {
            if ($(this).is(':checked')) {
                $('#btnAsociar').removeAttr('disabled');
                    $('#info-ficha').text($(this).data('ficha'));

            } else {
                $('#btnAsociar').attr('disabled', 'disabled');
            }
        });

        $("#btnAsociar").on("click", function(){
            $('#modal-fadein').modal('show');
        });

        function getStudents(id){

        id = $("input[name='idAsociar']:checked").val()

        $('.idFormacion').val(id);

        $('#show-aprendiz').html(`<table id="aprendiz-info" class="table"><thead>
        <tr>
            <th></th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Teléfono</th>
            <th>Correo</th>
            <th>Estado</th>

        </tr>
        </thead>

        <tbody id="aprendices-info">

        </tbody>

        </table>`);


        $('#show-instructor').html(`<table id="instructor-info" class="table"><thead>
        <tr>
            <th></th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Teléfono</th>
            <th>Correo</th>
            <th>Tipo</th>

        </tr>
        </thead>

        <tbody id="instructores-info">

        </tbody>

        </table>`);

        $('#show-docente').html(`<table id="docente-info" class="table"><thead>
        <tr>
            <th></th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Teléfono</th>
            <th>Correo</th>

        </tr>
        </thead>

        <tbody id="docentes-info">

        </tbody>

        </table>`);

        $('#show-empresa').html(`<table id="empresa-info" class="table"><thead>
        <tr>
            <th></th>
            <th>NIT</th>
            <th>Nombre</th>
            <th>Teléfono</th>
            <th>Correo</th>

        </tr>
        </thead>

        <tbody id="empresas-info">

        </tbody>

        </table>`);


        $('#aprendiz-info').DataTable(
            {
            columnDefs:[{orderable:!1}],
            pageLength:8,
            lengthMenu:[[5,8,15,20],
            [5,8,15,20]],
            autoWidth:!1,responsive:true,
            language:{"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"},
            dom: 'Bfrtip',
            buttons: [{extend:'excelHtml5',
            exportOptions: {columns: "thead th:not(.noExportExc)"}},
            {extend:'pdf',exportOptions: {columns: "thead th:not(.noExport)"}}],
            "ajax": "/formacion/show/"+id,
            "columns": [
                {"data" : "aprendiz_id",
                render : function(data, type, row) {
                    return `<label class="css-control css-control-sm css-control-warning css-checkbox"> <input type="checkbox" class="css-control-input chk-compe" id="idAsociar" name="idAprendiz[]" value="${data}"><span class="css-control-indicator"></span></label>`
                    }
                },
                { "data" : "nombre"},
                { "data" : "apellido"},
                { "data" : "telefono"},
                { "data" : "correo"},
                { "data" : "estadoAprendiz"},
                 ]
         });

         $('#instructor-info').DataTable(
            {
            columnDefs:[{orderable:!1}],
            pageLength:8,
            lengthMenu:[[5,8,15,20],
            [5,8,15,20]],
            autoWidth:!1,responsive:true,
            language:{"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"},
            dom: 'Bfrtip',
            buttons: [{extend:'excelHtml5',
            exportOptions: {columns: "thead th:not(.noExportExc)"}},
            {extend:'pdf',exportOptions: {columns: "thead th:not(.noExport)"}}],
            "ajax": "/formacion/showI/"+id,
            "columns": [
                {"data" : "id",
                render : function(data, type, row) {
                    return `<label class="css-control css-control-sm css-control-warning css-checkbox"> <input type="checkbox" class="css-control-input chk-instructor" id="idAsociar" name="idInstructor[]" value="${data}"><span class="css-control-indicator"></span></label>`
                    }
                },
                { "data" : "nombre"},
                { "data" : "apellido"},
                { "data" : "telefono"},
                { "data" : "correoAlterno"},
                { "data" : "tipo"}
        ]
         });
         $('#docente-info').DataTable(
            {
            columnDefs:[{orderable:!1}],
            pageLength:8,
            lengthMenu:[[5,8,15,20],
            [5,8,15,20]],
            autoWidth:!1,responsive:true,
            language:{"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"},
            dom: 'Bfrtip',
            buttons: [{extend:'excelHtml5',
            exportOptions: {columns: "thead th:not(.noExportExc)"}},
            {extend:'pdf',exportOptions: {columns: "thead th:not(.noExport)"}}],
            "ajax": "/formacion/showD/"+id,
            "columns": [
                {"data" : "id",
                render : function(data, type, row) {
                    return `<label class="css-control css-control-sm css-control-warning css-checkbox"> <input type="checkbox" class="css-control-input chk-Docente" id="idAsociar" name="idDocente[]" value="${data}"><span class="css-control-indicator"></span></label>`
                    }
                },
                { "data" : "nombre"},
                { "data" : "apellido"},
                { "data" : "telefono"},
                { "data" : "correo"},
        ]
         });

         $('#empresa-info').DataTable(
            {
            columnDefs:[{orderable:!1}],
            pageLength:8,
            lengthMenu:[[5,8,15,20],
            [5,8,15,20]],
            autoWidth:!1,responsive:true,
            language:{"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"},
            dom: 'Bfrtip',
            buttons: [{extend:'excelHtml5',
            exportOptions: {columns: "thead th:not(.noExportExc)"}},
            {extend:'pdf',exportOptions: {columns: "thead th:not(.noExport)"}}],
            "ajax": "/formacion/showE/"+id,
            "columns": [
                {"data" : "id",
                render : function(data, type, row) {
                    return `<label class="css-control css-control-sm css-control-warning css-checkbox"> <input type="checkbox" class="css-control-input chk-empresa" id="idAsociar" name="idEmpresa[]" value="${data}"><span class="css-control-indicator"></span></label>`
                    }
                },
                { "data" : "nit"},
                { "data" : "nombre"},
                { "data" : "telefono"},
                { "data" : "correo"},
        ]
         });

    }
</script>

<script>
    $(document).on('click', '#btnAsociar', function(e){
        Codebase.helpers('table-tools');
        });

        $(document).on('click', '.chk-compe', function(e){
        if ($(this).is(':checked')) {
            $('#btnCompe').removeAttr('disabled');

        } else {
            $('#btnCompe').attr('disabled', 'disabled');
        }
    });

    $(document).on('click', '.chk-instructor', function(e){
        if ($(this).is(':checked')) {
            $('#btnIns').removeAttr('disabled');

        } else {
            $('#btnIns').attr('disabled', 'disabled');
        }
    });
    
    $(document).on('click', '.chk-Docente', function(e){
        if ($(this).is(':checked')) {
            $('#btnDoc').removeAttr('disabled');

        } else {
            $('#btnDoc').attr('disabled', 'disabled');
        }
    });

    $(document).on('click', '.chk-empresa', function(e){
        if ($(this).is(':checked')) {
            $('#btnEmp').removeAttr('disabled');

        } else {
            $('#btnEmp').attr('disabled', 'disabled');
        }
    });

</script>

@endsection
