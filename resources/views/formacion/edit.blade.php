@extends('layouts.backend')
@section('content')
<div class="content">
                    <h2 class="content-heading">Formación</h2>
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Modificar formación</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option">
                                    <i class="si si-user-follow"></i>
                                </button>
                            </div>
                        </div>
                        <div class="js-wizard-validation-material block">
                                <!-- Step Tabs -->
                                <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                </ul>
                                <!-- END Step Tabs -->

                                <!-- Form -->
                                <form class="js-wizard-validation-material-form" action="" method="post">
                                {{ csrf_field() }}
                                    <!-- Steps Content -->
                                    <div class="block-content block-content-full tab-content" style="min-height: 267px;">
                                        <!-- Step 1 -->
                                        <div class="tab-pane active" id="wizard-validation-material-step1" role="tabpanel">
                                        <div class="form-group">
                                                <div class="form-material">
                                                    <select class="js-select2 form-control" id="idFicha" name="idFicha" style="width: 100%;" data-allow-clear="true">
                                                        <option value="">Seleccione ficha</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                        @if (auth()->user()->selected_category_id == 1)
                                                        @php($type = $fichaT)
                                                        @endif
                                                        @if (auth()->user()->selected_category_id == 2)
                                                            @php($type = $fichaC)
                                                        @endif
                                                        @if (auth()->user()->selected_category_id == 3)
                                                            @php($type = $fichaP)
                                                        @endif
                                                        @if (auth()->user()->selected_category_id == 4)
                                                            @php($type = $fichaCB)
                                                        @endif
                                                        @if (auth()->user()->selected_category_id == 5)
                                                            @php($type = $fichaM)
                                                        @endif
                                                        @foreach($type as $fi)

                                                        <option value="{{$fi->id}}" {{$fi->id == $formacion->idFicha ? 'selected' : ''}}>{{$fi->nombreFicha. ' - ' . $fi->codigo}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="idFicha">Ficha <span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                            <div class="form-group">
                                            <div class="form-material input-group floating">
                                                <div class="input-daterange input-group" data-date-format="yyyy-mm-dd" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                                <input type="text" class="form-control" id="fechaInicio" value="{{$formacion->fechaInicio}}" name="fechaInicio" placeholder="Inicio" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                                    <div class="input-group-prepend input-group-append">
                                                        <span class="input-group-text font-w600">A</span>
                                                    </div>
                                                    <input type="text" class="form-control" id="fechaFinal" value="{{$formacion->fechaFinal}}" name="fechaFinal" placeholder="Fin" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                                                </div>
                                            </div>
                                            </div>

                                        <div class="form-group">
                                                <div class="form-material">
                                                    <select class="js-select2 form-control" id="jornada" name="jornada" style="width: 100%;" data-allow-clear="true">
                                                        <option value="" selected>Seleccione jornada</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                        <option value="AM" {{ $formacion->jornada == "AM" ? 'selected': '' }}>Diurna</option>
                                                        <option value="PM" {{ $formacion->jornada == "PM" ? 'selected': '' }}>Nocturna</option>
                                                    </select>
                                                    <label for="jornada">Jornada <span class="text-danger">*</span></label>
                                            </div>
                                        </div>

                                            <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group date" data-target-input="nearest">
                                                <input type="text" class="form-control datetimepicker-input" value="{{$formacion->horaInicio}}" id="horaInicio" name="horaInicio" placeholder="Inicio" data-target="#horaInicio">
                                                    <div class="input-group-prepend input-group-append" data-target="#horaInicio" data-toggle="datetimepicker">
                                                        <span class="input-group-text font-w600">A</span>
                                                    </div>
                                                <input type="text" class="form-control" id="horaFinal" name="horaFinal" value="{{$formacion->horaFinal}}" placeholder="Fin" data-target="#horaFinal" data-toggle="datetimepicker">
                                                </div>
                                            </div>
                                            </div>

                                            <div class="form-group" style="display: {{auth()->user()->selected_category_id == 1 ? '' : 'none'}}">
                                                <div class="form-material input-group floating">
                                                <input type="text" class="js-datepicker form-control"  value="{{$formacion->fechaInicioProd}}" id="fechaInicioProd" name="fechaInicioProd" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="yyyy-mm-dd" value="{{old('fechaInicioProd')}}">
                                                    <label for="fechaInicioProd">Fecha etapa productiva <span class="text-danger">*</span></label>
                                                    <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-material">
                                            <select class="js-select2 form-control" id="idEstado" name="idEstado" style="width: 100%;" data-allow-clear="true">
                                                <option value="" selected>Seleccione estado</option><!-- Required for data-placeholder attribute to work with Select2 plugin -->
                                                @foreach($estado as $est)

                                                <option value="{{ $est->id}}" {{$formacion->idEstado == $est->id ? "selected":"" }}>{{$est->nombre}}</option>
                                                @endforeach
                                            </select>
                                            <label for="idEstado">Estado formación <span class="text-danger">*</span></label>
                                    </div>
                                </div>

                                        </div>
                                        <!-- END Step 1 -->
                                    <!-- END Steps Content -->

                                    <!-- Steps Navigation -->
                                    <div class="form-group row">
                                        <div class="col-lg-8 ml-auto">
                                            <button type="submit" class="btn btn-alt-success pull-right">Actualizar</button>
                                        </div>
                                    <!-- END Steps Navigation -->
                                </form>
                                <!-- END Form -->
                            </div>
@endsection
@section('js_after')
        <!-- Page JS Plugins -->
        <script src="/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="/js/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="/js/plugins/jquery-validation/additional-methods.js"></script>

        <!-- Page JS Code -->
        <script src="/js/pages/be_forms_wizard.min.js"></script>
        <script type="text/javascript">
            $(function () {
            $('#horaInicio').datetimepicker({
            format: 'H:mm'
            });

            $('#horaFinal').datetimepicker({
                format: 'H:mm',
                useCurrent: false
            });

            $("#horaInicio").on("dp.change", function (e) {
                $('#horaFinal').data("DateTimePicker").minDate(e.date);
            });
            $("#horaFinal").on("dp.change", function (e) {
                $('#horaInicio').data("DateTimePicker").maxDate(e.date);
            });

            });
        </script>

@endsection
