    <form  method="get">
    <div class="modal fade" id="modal-fromleft-{{$form->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-fromleft" aria-hidden="true">
        <div class="modal-dialog modal-dialog-fromleft modal-lg" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Formación - {{$form->ficha->codigo}}</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block block-themed block-transparent mb-0">
                            <div class="block block-themed">
                                    <div class="block-content block-content-full text-center bg-gd-primary">
                                        <img class="img-avatar img-avatar96 img-avatar-thumb" src="/media/avatars/avatar15.jpg" alt="">
                                        <h3 class="text-white mt-3">INFORMACIÓN
                                 <i class=" si si-info"></i>
                             </h3>
                                    </div>
                                    <div class="col-md-12">
                                            <!-- Simple Wizard 2 -->
                                            <div class="js-wizard-simple block">
                                                <!-- Step Tabs -->
                                                <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                                    <li class="nav-item">
                                                    <a class="nav-link active" href="#wizard-simple2-step1-{{$form->id}}" data-toggle="tab">1. General </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#wizard-simple2-step2-{{$form->id}}" data-toggle="tab">2. Detalles</a>
                                                    </li>
                                                </ul>
                                                <!-- END Step Tabs -->

                                                <!-- Form -->
                                                <form action="" method="post">
                                                    <!-- Steps Content -->
                                                    <div class="block-content block-content-full tab-content" style="min-height: 267px;">
                                                        <!-- Step 1 -->
                                                        <input class="form-control" type="hidden" id="wizard-simple2-firstname" name="idUser" value="{{$form->id}}" readonly>

                                                        <div class="tab-pane active" id="wizard-simple2-step1-{{$form->id}}" role="tabpanel">
                                                            <div class="form-group">
                                                                <div class="form-material floating">
                                                                <input class="form-control" type="text" id="wizard-simple2-firstname" name="wizard-simple2-firstname" value="{{$form->ficha->codigo}}" readonly>
                                                                    <label for="wizard-simple2-firstname">Ficha</label>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                    <div class="form-material floating">
                                                                    <input class="form-control" type="text" id="wizard-simple2-firstname" name="wizard-simple2-firstname" value="{{$form->jornada == "AM" ? 'Diurna' : 'Nocturna'}}" readonly>
                                                                        <label for="wizard-simple2-firstname">Jornada</label>
                                                                    </div>
                                                                </div>

                                                            <div class="form-group">
                                                                <div class="form-material floating">
                                                                    <input class="form-control" type="text" id="wizard-simple2-lastname" name="wizard-simple2-lastname" value="{{$form->horaInicio}}" readonly>
                                                                    <label for="wizard-simple2-lastname">Hora de inicio</label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="form-material floating">
                                                                    <input class="form-control" type="email" id="wizard-simple2-email" name="wizard-simple2-email" value="{{$form->horaFinal}}" readonly>
                                                                    <label for="wizard-simple2-email">Hora  de finalización</label>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <!-- END Step 1 -->

                                                        <!-- Step 2 -->
                                                        <div class="tab-pane" id="wizard-simple2-step2-{{$form->id}}" role="tabpanel">
                                                                <div class="form-group">
                                                                    <div class="form-material floating">
                                                                        <input class="form-control" type="email" id="wizard-simple2-email" name="wizard-simple2-email" value="{{$form->fechaInicio}}" readonly>
                                                                        <label for="wizard-simple2-email">Fecha de inicio</label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="form-material floating">
                                                                        <input class="form-control" type="email" id="wizard-simple2-email" name="wizard-simple2-email" value="{{$form->fechaFinal}}" readonly>
                                                                        <label for="wizard-simple2-email">Fecha de finalización</label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                        <div class="form-material floating">
                                                                            <input class="form-control" type="email" id="wizard-simple2-email" name="wizard-simple2-email" value="{{$form->fechaInicioProd == null ? 'No aplica' : $form->fechaInicioProd}}" readonly>
                                                                            <label for="wizard-simple2-email">Inicio etapa práctica</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                            <div class="form-material floating">
                                                                                <input class="form-control" type="email" id="wizard-simple2-email" name="wizard-simple2-email" value="{{$form->estado->nombre}}" readonly>
                                                                                <label for="wizard-simple2-email">Estado</label>
                                                                            </div>
                                                                        </div>
                                                                 </div>
                                                        <!-- END Step 2 -->
                                                    <!-- END Steps Content -->
                                                </form>
                                                <!-- END Form -->
                                            </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-alt-success" data-dismiss="modal">
                        <i class="fa fa-check"></i> Perfect
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>

