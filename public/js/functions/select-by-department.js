$(function(){
    $('#idDepartamento').on('change',onSelectedDepartment);
});

function onSelectedDepartment(){
var department_id = $(this).val();

    //AJAX

    if(! department_id ){
        $('#idMunicipio').html(`<option value="" selected>Seleccione Municipio</option>`);
        return;
    }

    $.get(`/municipio/${department_id }/departamento`,function(data){
        var html_select = `<option value="" selected>Seleccione Municipio</option>`;
        for(i of data){
            html_select+= `<option value="${i.id}">${i.nombre}</option>`;
        }
        $('#idMunicipio').html(html_select);
    });

}