function getColegio(id){
    $.ajax({
        url:"/coordinador/show/"+id,
        type:'get',
        success:function(data){
            if(data.length != 0){
                $("#nombre-"+id).val(data["0"].nombreColegio);
                $("#direccion-"+id).val(data["0"].direccion);
                $("#telefono-"+id).val(data["0"].telefono);
            }else{
                $("#nombre-"+id).val("Sin colegio");
                $("#direccion-"+id).val("Sin dirección");
                $("#telefono-"+id).val("Sin télefono");
            }
        },
        error:function(){
            alert("error");
        }
    });
}

function getColegioR(id){
    $.ajax({
        url:"/rector/show/"+id,
        type:'get',
        success:function(data){
            console.log(data);
            if(data.length != 0){
                $("#nombre-"+id).val(data["0"].nombreColegio);
                $("#direccion-"+id).val(data["0"].direccion);
                $("#telefono-"+id).val(data["0"].telefono);
            }else{
                $("#nombre-"+id).val("Sin colegio");
                $("#direccion-"+id).val("Sin dirección");
                $("#telefono-"+id).val("Sin télefono");
            }
        },
        error:function(){
            alert("error");
        }
    });
}